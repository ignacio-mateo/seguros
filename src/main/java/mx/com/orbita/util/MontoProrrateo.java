/*
 * 	Calcula los montos proporcionales a pagar por la prima de Gastos Medicos.
 * 	NOTA: Se obtienen los dias de los meses del anio a partir de la clase DiasDelMes
 *  
 */
package mx.com.orbita.util;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.SimpleTimeZone;
import java.util.GregorianCalendar;

public class MontoProrrateo {

    private SimpleTimeZone pdt;
    private Calendar calendar;
    private java.util.Date trialTime;
    private String[] ids;

    /** Prorrateo de Montos de la prima de Gastos Medicos Mayores */
    public MontoProrrateo() {
        ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
        pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
        calendar = new GregorianCalendar(pdt);
        trialTime = new java.util.Date();
        calendar.setTime(trialTime);
    }
    
    /** 
     * 	Calcula el monto proprocional a pagar por la prima del seguro 
     *  Toma como base un anio natural para realizar sus calculos
     *  
     */
    public double calculaMontoProporcional(double monto ) {

      int mesactual = 0; int diaactual = 0; int totaldias = 0; int diasrestantes = 0;
      int diasmeses = 0; double costoxdia = 0; double parteproporcional = 0; int totaldiasxanio = 0;
      
      // Declaramos la clase que nos proporciona los dias de los meses del anio en curso
      DiasDelMes daysmonth = new DiasDelMes(calendar.get(Calendar.YEAR));
      
      // Declaramos el total de dias por mes y les asignamos el valor de c/u
      int m1  = daysmonth.getDaysOfJanuary();
      int m2  = daysmonth.getDaysOfFebruary();
      int m3  = daysmonth.getDaysOfMarch();
      int m4  = daysmonth.getDaysOfApril();
      int m5  = daysmonth.getDaysOfMay();
      int m6  = daysmonth.getDaysOfJune();
      int m7  = daysmonth.getDaysOfJuly();
      int m8  = daysmonth.getDaysOfAugust();
      int m9  = daysmonth.getDaysOfSeptember();
      int m10 = daysmonth.getDaysOfOctober();
      int m11 = daysmonth.getDaysOfNovember();
      int m12 = daysmonth.getDaysOfDecember();
      
      //obtenemos el total de dias x anio
      totaldiasxanio = m1 + m2 + m3 + m4 + m5 + m6 + m7 + m8 + m9 + m10 + m11 + m12;
      
      //obtenemos el mes y dia actual
      mesactual = calendar.get(Calendar.MONTH) + 1;
      diaactual = calendar.get(Calendar.DATE);
   
            /* calculamos los dias restantes y la suma de los dias de los meses a partir del mes actual */
      		switch(mesactual){
      			case 1:
      				diasrestantes =  m1 - diaactual ;
                    diasmeses =  m2 + m3 + m4 + m5;
                    break;
      			case 2:
      				diasrestantes =  m2 - diaactual ;
      				diasmeses =  m3 + m4 + m5;
                    break;
      			case 3:
      				diasrestantes =  m3 - diaactual ;
      				diasmeses = m4 + m5;
                    break;
      			case 4:
      				diasrestantes =  m4 - diaactual ;
      				diasmeses =  m5;
                    break;
      			case 5:
      				diasrestantes =  m5 - diaactual ;
                    break;
      			case 6:
      				diasrestantes =  m6 - diaactual ;
      				diasmeses =  m7 + m8 + m9 + m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                    break;
      			case 7:
      				diasrestantes =  m7 - diaactual ;
      				diasmeses =  m8 + m9 + m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                    break;
      			case 8:
      				diasrestantes =  m8 - diaactual ;
      				diasmeses = m9 + m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                    break;
      			case 9:
      				diasrestantes =  m9 - diaactual ;
      				diasmeses = m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                    break;
      			case 10:
      				diasrestantes =  m10 - diaactual ;
      				diasmeses = m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                    break;
      			case 11:
      				diasrestantes =  m11 - diaactual ;
      				diasmeses = m12 +  m1 + m2 + m3 + m4 + m5;
                    break;
      			case 12:
      				diasrestantes =  m12 - diaactual ;
      				diasmeses =  m1 + m2 + m3 + m4 + m5;
                    break;
      			default:
                    diasrestantes = 0;
                    diasmeses = 0;
             }

             // calculamos el total de dias
             totaldias = diasrestantes + diasmeses + 1;
             
             // calculamos el costo por dia
             costoxdia = monto/totaldiasxanio;
             
             // calculamos la parte proporcional
             parteproporcional = totaldias * costoxdia;

             // retornamos el valor
             return parteproporcional;

     } 
    
    public double calculaMontoProporcional(double monto, int mes, int dia ) {

        int mesactual = 0; int diaactual = 0; int totaldias = 0; int diasrestantes = 0;
        int diasmeses = 0; double costoxdia = 0; double parteproporcional = 0; int totaldiasxanio = 0;
        
        // Declaramos la clase que nos proporciona los dias de los meses del anio en curso
        DiasDelMes daysmonth = new DiasDelMes(calendar.get(Calendar.YEAR));
        
        // Declaramos el total de dias por mes y les asignamos el valor de c/u
        int m1  = daysmonth.getDaysOfJanuary();
        int m2  = daysmonth.getDaysOfFebruary();
        int m3  = daysmonth.getDaysOfMarch();
        int m4  = daysmonth.getDaysOfApril();
        int m5  = daysmonth.getDaysOfMay();
        int m6  = daysmonth.getDaysOfJune();
        int m7  = daysmonth.getDaysOfJuly();
        int m8  = daysmonth.getDaysOfAugust();
        int m9  = daysmonth.getDaysOfSeptember();
        int m10 = daysmonth.getDaysOfOctober();
        int m11 = daysmonth.getDaysOfNovember();
        int m12 = daysmonth.getDaysOfDecember();
        
        //obtenemos el total de dias x anio
        totaldiasxanio = m1 + m2 + m3 + m4 + m5 + m6 + m7 + m8 + m9 + m10 + m11 + m12;
        
        //obtenemos el mes y dia actual
        mesactual = mes;
        diaactual = dia;

              /* calculamos los dias restantes y la suma de los dias de los meses a partir del mes actual */
        		switch(mesactual){
        			case 1:
        				diasrestantes =  m1 - diaactual ;
                      diasmeses =  m2 + m3 + m4 + m5;
                      break;
        			case 2:
        				diasrestantes =  m2 - diaactual ;
        				diasmeses =  m3 + m4 + m5;
                      break;
        			case 3:
        				diasrestantes =  m3 - diaactual ;
        				diasmeses = m4 + m5;
                      break;
        			case 4:
        				diasrestantes =  m4 - diaactual ;
        				diasmeses =  m5;
                      break;
        			case 5:
        				diasrestantes =  m5 - diaactual ;
                      break;
        			case 6:
        				diasrestantes =  m6 - diaactual ;
        				diasmeses =  m7 + m8 + m9 + m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                      break;
        			case 7:
        				diasrestantes =  m7 - diaactual ;
        				diasmeses =  m8 + m9 + m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                      break;
        			case 8:
        				diasrestantes =  m8 - diaactual ;
        				diasmeses = m9 + m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                      break;
        			case 9:
        				diasrestantes =  m9 - diaactual ;
        				diasmeses = m10 + m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                      break;
        			case 10:
        				diasrestantes =  m10 - diaactual ;
        				diasmeses = m11 + m12 +  m1 + m2 + m3 + m4 + m5;
                      break;
        			case 11:
        				diasrestantes =  m11 - diaactual ;
        				diasmeses = m12 +  m1 + m2 + m3 + m4 + m5;
                      break;
        			case 12:
        				diasrestantes =  m12 - diaactual ;
        				diasmeses =  m1 + m2 + m3 + m4 + m5;
                      break;
        			default:
                      diasrestantes = 0;
                      diasmeses = 0;
               }

               // calculamos el total de dias
               totaldias = diasrestantes + diasmeses + 1;
               
               // calculamos el costo por dia
               costoxdia = monto/totaldiasxanio;
               
               // calculamos la parte proporcional
               parteproporcional = totaldias * costoxdia;

               // retornamos el valor
               return parteproporcional;

    } 
    
}
