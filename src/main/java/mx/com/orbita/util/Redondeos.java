package mx.com.orbita.util;

import java.text.DecimalFormat;

public class Redondeos {
	
	public static double roundNum(double num){
	
		double valor = 0;
		valor = num;

		valor = valor*100;
		valor = java.lang.Math.round(valor);
		valor = valor/100;
		
		return valor;
	}
	
	public static String formateadorMiles(Double cantidad){
		DecimalFormat formateador = new DecimalFormat("###,##0.00");
		return formateador.format(cantidad);
	}
	

}
