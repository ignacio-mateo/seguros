package mx.com.orbita.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class Fechas {
		
	/**
	 * Formato de fechas
	 * @param date
	 * @param formato
	 * @return
	 */
	public String formatoFechas(Date date, String formato){
		SimpleDateFormat formateador = new SimpleDateFormat(formato,new Locale("es","ES"));
		formateador.applyPattern(formato);
		return formateador.format(date);
	}
	
	public long restarFechas(Date fechaMayor, Date fechaMenor) {
		long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		return dias;
	}
	
	/**
	 * Obtiene la clave (rango de anios) a partir de la fecha de nacimiento formato: yyyy-MM-dd
	 * @param birthday
	 * @return
	 */
	public String getCodigoAnio(String birthday) {
		String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);;
		SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
		Calendar calendar = new GregorianCalendar(pdt);
		Date trialTime = new Date();
		calendar.setTime(trialTime);
        String clave;
        int edad=0; int restantes=0;
        
        //definimos las variables por default
        int dia=1; int mes=1; int anio= calendar.get(Calendar.YEAR) - 64;
        
        //validamos la entrada
        if ( birthday == null  || birthday.equals("")  ) {
            return ("");
        } else {
            //Descomponemos la fecha, formato yyyy-mm-dd
            anio = Integer.parseInt(birthday.substring(0,birthday.indexOf("-") ) );
            mes  = Integer.parseInt(birthday.substring(birthday.indexOf("-") + 1 , birthday.lastIndexOf("-") ) );
            dia  = Integer.parseInt(birthday.substring(birthday.lastIndexOf("-") + 1));
        }   
        
        //obtenemos el anio, mes y dia actual
        int anioactual = calendar.get(Calendar.YEAR);
        int mesactual  = calendar.get(Calendar.MONTH);
        int diaactual  = calendar.get(Calendar.DATE);
        
        //realizamos el calculo de la edad
        if (anioactual - anio == 0) restantes = 0;
        else if (mes > (mesactual+1) && anioactual - anio != 0) restantes = 1;
        else if (mes == (mesactual + 1) && anioactual - anio != 0) {
             if (dia > diaactual) restantes = 1;
             else restantes = 0;
        }
        edad = anioactual - anio - restantes;
        
        //validamos la edad para prporcionar una clave
        if (edad >= 0 && edad <= 19) clave = "0-19";
        else if (edad >= 20 && edad <= 24) clave = "20-24";
        else if (edad >= 25 && edad <= 29) clave = "25-29";
        else if (edad >= 30 && edad <= 34) clave = "30-34";
        else if (edad >= 35 && edad <= 39) clave = "35-39";
        else if (edad >= 40 && edad <= 44) clave = "40-44";
        else if (edad >= 45 && edad <= 49) clave = "45-49";
        else if (edad >= 50 && edad <= 54) clave = "50-54";
        else if (edad >= 55 && edad <= 59) clave = "55-59";
        else if (edad >= 60 && edad <= 64) clave = "60-64";
        else if (edad >= 65 && edad <= 69) clave = "65-69";
        else if (edad >= 70 && edad <= 74) clave = "70-74";
        else if (edad >= 75 && edad <= 80) clave = "75-80";
        else clave="";
        return (clave);
    }
	
}
