package mx.com.orbita.util;

public class Codigo {
		
	/*		Id de solicitudes		*/
	public static final Integer ID_SOLICITUD = 1;
	public static final Integer ID_DEPENDIENTE = 2;
	public static final Integer ID_CONSULMED = 3;
	
	/*		Tipos de Operaciones	*/
	public static final Integer ID_OPERACION_ALTA = 1;
	public static final Integer ID_OPERACION_BAJA = 2;
	
	/*		Tipos de Solicitudes	*/
	public static final Integer TIPO_SOL_ALTA = 1;
	public static final Integer TIPO_SOL_BAJA = 2;
	
	/*		Estatus de una Solicitud	*/
	public static final Integer EST_SOL_ALTA_EN_PROCESO = 1;
	public static final Integer EST_SOL_ALTA_AUTORIZADA = 2;
	public static final Integer EST_SOL_ALTA_RECHAZADA = 3;
		
	public static final Integer EST_SOL_BAJA_EN_PROCESO = 4;
	public static final Integer EST_SOL_BAJA = 5;				// Solo aplica para los dependientes (Normales y Consulmed)
	public static final Integer EST_SOL_BAJA_RECHAZADA = 6;
	public static final Integer EST_SOL_BAJA_AUTORIZADA = 7;	// Solo aplica a la solicitud, dura hasta el final de la renovacion
	public static final Integer EST_SOL_BAJA_TOTAL = 8;			// Aplica a todos (sol y dep) cuando se ejecuta la renovacion
	public static final Integer EST_SOL_BAJA_X_FINIQUITO = 9;	// Aplica a todos (sol y dep) cuando hay un finiquito
		
	public static final Integer LISTA_CONSULMED_REGISTRADOS = 1;
	public static final Integer LISTA_CONSULMED_DISPONIBLES = 2;
	
	/*		Id Conyuge		*/
	public static final Integer ID_PARENTESCO_ESPOSO = 3;
	public static final Integer ID_PARENTESCO_ESPOSA = 4;
	
	/*		Tipo Costos para cambios de categorias		*/
	public static final Integer ID_COSTO_SOL = 1;
	public static final Integer ID_COSTO_DEP = 2;
	public static final Integer ID_COSTO_AMPL = 3;
	
	/*		Tipo de Cobertura	*/
	public static final Integer ID_TIPO_COB_N = 0;
	public static final Integer ID_TIPO_COB_A1 = 1;
	public static final Integer ID_TIPO_COB_A2 = 2;
	public static final Integer ID_TIPO_COB_A3 = 3;
			
	/*		Reportes		*/
	public static final Integer ID_EMPLEADO_CONFIANZA = 1;
	public static final Integer ID_EMPLEADO_SINDICALIZADO = 2;

	/*		Titulos		*/
	public static final String TITULO_SOL_COTIZACION = "COTIZACION DE SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_SOL_ALTA = "SOLICITUD DE ALTA AL SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_SOL_ALTA_DEPENDIENTE = "SOLICITUD DE ALTA DE DEPENDIENTES AL SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_SOL_ALTA_CONSULMED = "SOLICITUD DE ALTA DE DEPENDIENTES AL SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_SOL_BAJA = "SOLICITUD DE BAJA AL SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_SOL_BAJA_DEPENDIENTE = "SOLICITUD DE BAJA DE DEPENDIENTES AL SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_SOL_BAJA_CONSULMED = "SOLICITUD DE BAJA DE DEPENDIENTES AL SEGURO DE GASTOS M&Eacute;DICOS MAYORES";
	public static final String TITULO_ALTA_AUTORIZAR_SOL = "SOLICITUDES DE ALTA EN PROCESO DE AUTORIZACI&OacuteN";
	public static final String TITULO_ALTA_AUTORIZAR_DEP = "SOLICITUDES DE DEPENDIENTES EN PROCESO DE AUTORIZACI&OacuteN";
	public static final String TITULO_ALTA_AUTORIZAR_CON = "SOLICITUDES CONSULMED EN PROCESO DE AUTORIZACI&OacuteN";
	public static final String TITULO_BAJA_AUTORIZAR_SOL = "SOLICITUDES DE BAJA EN PROCESO DE AUTORIZACI&OacuteN";
	public static final String TITULO_BAJA_AUTORIZAR_DEP = "SOLICITUDES DE DEPENDIENTES EN PROCESO DE AUTORIZACI&OacuteN";
	public static final String TITULO_BAJAA_AUTORIZAR_CON = "SOLICITUDES CONSULMED EN PROCESO DE AUTORIZACI&OacuteN";
			
	/*		Mensajes	*/
	public static final String MSJ_PROCESANDO_SOL = "La solicitud que realizaste esta siendo procesada";
	
	public static final String MSJ_IMPRIME_SOL = "Imprime la solicitud y enviala al Departamento de Recursos Humanos de tu "
			+ "region o al Departamento de Seguros y Fianzas si eres de R9 o Corporativo.";
	
	public static final String MSJ_ALTA_SOL = "Te recordamos que si no entregas la solicitud firmada al departamento correspondiente " 
			+"en un lapso de 15 d&iacuteas h&aacutebiles a partir de este momento, no contaras con Gastos Medicos Mayores.";
	
	public static final String MSJ_ALTA_DEP = "Te recordamos que si no entregas la solicitud firmada al departamento correspondiente " 
			+"en un lapso de 15 d&iacuteas h&aacutebiles a partir de este momento, tu dependiente no contar&aacute con Gastos Medicos Mayores.";
	
	public static final String MSJ_BAJA_SOL = "Te recordamos que si no entregas la solicitud firmada al departamento correspondiente " 
			+"en un lapso de 15 d&iacuteas h&aacutebiles a partir de este momento, no se tramitar&aacute tu baja.";
	
	public static final String MSJ_BAJA_DEP = "Te recordamos que si no entregas la solicitud firmada al departamento correspondiente " 
			+"en un lapso de 15 d&iacuteas h&aacutebiles a partir de este momento, no se tramitar&aacute tu baja de dependiente.";
	
	public static final String EXTENSIONES = "Si tiene dudas sobre como realizar otra operacion comunicate con Seguros y Fianzas Corporativo. Ext. 1234, 5678";
	
	public static final String LINK_ALTA = "<a href=\"#\" onclick=\"seleccionarOpcion('consultaAltaSolicitud.htm');\">Click para ir a visualizar la solicitud</a>";
	public static final String LINK_BAJA = "<a href=\"#\" onclick=\"seleccionarOpcion('consultaBajaSolicitud.htm');\">Click para ir a visualizar la solicitud</a>";
	public static final String LINK_ALTA_DEP = "<a href=\"#\" onclick=\"seleccionarOpcion('consultaAltaIdDependiente.htm');\">Click para ir a visualizar la solicitud</a>";
	public static final String LINK_BAJA_DEP = "<a href=\"#\" onclick=\"seleccionarOpcion('consultaBajaIdDependiente.htm');\">Click para ir a visualizar la solicitud</a>";
		
	public static final String SOL_MENSAJE_ACT = "Actualizaciones Exitosas";
	public static final String SOL_AUTORIZADA = "El Numero de Empleado proporcionado ya tiene una solicitud de Gastos Medicos Mayores.";
	public static final String SOL_BAJA = "El Numero de Empleado proporcionado tiene una solicitud de Baja Gastos Medicos Mayores.";
	public static final String SOL_NO_EXISTE = "El Numero de Empleado proporcionado no tiene Solicitud de Alta de Gastos Medicos Mayores.";
	public static final String SOL_BAJA_NO_EXISTE = "El Numero de Empleado proporcionado no tiene Solicitud de Baja de Gastos Medicos Mayores.";
	public static final String SOL_MENSAJE_NO_DEP = "No existen dependientes";
	public static final String SOL_MENSAJE_BAJA_TOT = "El Numero de Empleado proporcionado no tiene solicitud de Gastos Medicos Mayores y/o no esta Autorizada";
	public static final String MSJ_REEMBOLSO = "Se ha generado la solicitud para su reembolso";
	public static final String SOL_MENSAJE_FOL_REEMB = "No existen folios";
	public static final String SOL_MENSAJE_DEP_VER_ALTA = "<b>Favor de ir al Menu CONSULTA SOL, Alta Dependientes e Imprimirla</b>";
	public static final String SOL_MENSAJE_DEP_VER_BAJA = "<b>Favor de ir al Menu CONSULTA SOL, Baja Dependientes e Imprimirla</b>";
	public static final String DEPENDIENTES_PROCESO = "Existen dependientes en proceso de autorizacion, favor de primero autorizar";
			
	public static final String CONSENTIMIENTO_LLENADO = "EL LLENADO DE ESTE DOCUMENTO FORMA PARTE DEL TRAMITE " +
			"PARA SOLICITAR LA INSCRIPCI&OacuteN AL SEGURO DE GASTOS MEDICOS MAYORES.";
	public static final String CONSENTIMIENTO_FIRMA = "LA INSCRIPCI&OacuteN PROCEDER&Aacute A PARTIR DE LA FECHA QUE INDIQUE EL SELLO Y " +
			"FIRMA EN ESTA SOLICITUD.";
	public static final String CONSENTIMIENTO_EXT = "DUDAS O COMENTARIOS FAVOR DE COMUNICARSE A LAS EXT. 4526, 6835.";
	
	public static final String AVISO_RENOVACION = "El sistema de Gastos Medicos Mayores esta temporalmente cerrado debido al periodo de renovacion, una vez"
			+ " concluido el periodo se podra volver a ingresar nuevamente";
	
	public static final String AVISO_CIRCULAR = "El Periodo para contestar la encuesta de renovacion ha termindo";
	public static final String AVISO_RENOVACION_EXITOSA = "Ha completado exitosamente la encuesta, gracias.";
	
	public static final String EMPLEADO_NO_ENCONTRADO = "El empleado no existe, favor de verificarlo";
	public static final String EMPLEADO_NO_FINIQUITADO = "El empleado no tiene finiquito, favor de verificarlo";
	public static final String EMPLEADO_FINIQUITADO = "El empleado ya fue finiquitado";
	public static final String EMPLEADO_A3 = "El empleado ya cuenta con cobertura A3";
	public static final String AVISO_DATOS_FISCALES = "El Modulo de Datos Fiscales esta temporalmente cerrado";
	public static final String MSJ_MEMORIAL_EXITOSO = "la contratacion de Gastos Funerarios ha sido exitosa";
		
	
	/* 		Reembolsos		*/
	public static final String RAZON_SOCIAL = "EMPRESA S.A. DE C.V.";
	public static final String POLIZA = "COLECTIVA";
	public static final String CONTRATANTE = "EMPRESA S.A. DE C.V.";
	public static final String EMISOR = "00000";
	public static final String PLAN_SIND = "SINDICALIZADO";
	public static final String PLAN_CONF = "CONFIANZA";
	public static final String NO_POLIZA_SIND_REPORTE_SINIESTROS = "1111";
	public static final String NO_POLIZA_CONF_REPORTE_SINIESTROS = "222222";
	public static final String NO_POLIZA_CONF_AVISO_ACCIDENTE = "11111 11111 11111";
	public static final String NO_POLIZA_SIND_AVISO_ACCIDENTE = "22222 22222 22222";
	
	
	
	/* 		Constancias		*/
	public static final String CONS_ERROR = "El empleado no concuerda con el tipo de constancia";
	public static final String CONS_ASEGURADORA = "Seguros S.A.";
	public static final String CONS_DOMICILIO = "Av. Sin nombre 0, Col. Sin Colonia C.P. 00000, Mexico D.F";
	public static final String CONS_RFC = "S901201RFC";
	public static final String CONS_POLIZA_SIND = "11111-5678";
	public static final String CONS_POLIZA_CONF = "22222-5678";
	public static final String CONS_VIGENCIA = "01/JUN/2017 al 01/JUN/2018";
		public static final String CONS_SUMA_ASEG_BASICA = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A1 = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A2_N = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A2_E = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A3_N = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A3_E = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A4_N = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
		public static final String CONS_SUMA_ASEG_A4_E = "$1,000,000.00 M.N. por padecimiento, excepto Cesarea y Parto Natural";
	public static final String CONS_DEDUCIBLE_SIND = "$5,136.99";
	public static final String CONS_DEDUCIBLE_CONF = "$5,136.99";
	public static final String CONS_COASEGURO = "10%, Excepto en Enfermedades de Nariz, aplica 30%";
	public static final String CONS_CESAREA_SIND = "$77,054.88";
	public static final String CONS_CESAREA_CONF = "$77,054.88";
	public static final String CONS_PARTO_NAT_SIND = "$64,212.40";
	public static final String CONS_PARTO_NAT_CONF = "$64,212.40";
	
	
	/* 		DEPENDIENTE INDIVIDUAL PARA CALCULAR AMPLIACION		*/
	public static final Integer DEP_INDIVIDUAL_AMPLIACION = 1;
	
	/*		Renovacion y Circular		*/
	public static final Boolean FLAG_PERIODO_RENOVACION = false;		// true = bloquear acceso al sistema , false = acceso al sistema	 	
	public static final Boolean FLAG_ACCESO_CIRCULAR = false;			// true = bloquear acceso circular, false = acceso circular
	public static final int DIA_RENOVACION = 1;
	public static final int MES_RENOVACION = 6;
	public static final int PERIODO_INICIAL = 2019;
	public static final int PERIODO_FINAL = 2020;
	public static final int ESTATUS_RENOV_EXITOSA = 1;
	public static final int ESTATUS_RENOV_PENDIENTE = 2;
	public static final int RESPUESTA_RENOV_SI = 1;
	public static final int RESPUESTA_RENOV_NO = 2;
	public static final int DIA_RENOVACION_SEPT = 10;
	public static final int MES_RENOVACION_SEPT = 9;
	
	/*		Datos Fiscales	*/
	public static final Boolean FLAG_DATOS_FISCALES = false;		// true = bloquear modulo, false = acceso al modulo	  
			
	/*		PARAMETROS LDAP		*/
	public static final Boolean LDAP_ACTIVAR = false;				// true = autenticacion activada, false = autenticacion desactivada
	public static final String LDAP_URL = "";
	public static final String LDAP_HOST = "";
	public static final String LDAP_URL_BASE = "";
	public static final Integer LDAP_PUERTO = 8000;
	public static final String LDAP_ID_APP = "";
	public static final String LDAP_PASWD_APP = "";

	public static final String LIGA_REEMBOLSOS = "";
	public static final String LIGA_REEMBOLSOS_UNO = "";
	public static final String LIGA_REEMBOLSOS_DOS = "";

	public static final String KEYP = "15646^&amp;%$3(),>2134bgGz*-+e7hd";
	public static final String SHA1_ALGORITHM = "PBEWithMD5AndTripleDES";
	public static final String  ENCODING = "UTF-8";
	public static final int ITERACIONES = 2;
	public static final String MODULO_REEMBOLSOS = "FIANZAS";
	public static final String ID_EMPRESA = "1111";
	public static final String FORMATO_FECHA = "ddMMyyyy-HHmmss";
	
}
