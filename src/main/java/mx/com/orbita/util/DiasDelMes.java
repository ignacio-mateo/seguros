/*
 * 	Obtiene el Total de Dias po Anio, incluyendo anios bisiestos 
 */
package mx.com.orbita.util;

public class DiasDelMes extends java.lang.Object {
    
    private static final int daysOfJanuary = 31;    
    private int daysOfFebruary = 28;
    private static final int daysOfMarch = 31;
    private static final int daysOfApril = 30;
    private static final int daysOfMay = 31;
    private static final int daysOfJune = 30;
    private static final int daysOfJuly = 31;
    private static final int daysOfAugust = 31;
    private static final int daysOfSeptember = 30;
    private static final int daysOfOctober = 31;
    private static final int daysOfNovember = 30;
    private static final int daysOfDecember = 31;

    public DiasDelMes() {
    }
    
    /** 
     *  Obtiene los dias de los meses a partir de un anio en especifico, recibe como parametro el anio
     */
    public DiasDelMes(int year){
        if ((year % 4) == 0) {
            if ((year%100) == 0 && (year%400) != 0) {
                setDaysOfFebruary(28);
            } else {
                setDaysOfFebruary(29);
            }
        } else {
            setDaysOfFebruary(28);
        }
    }
    
    public int getDaysOfApril() {
        return daysOfApril;
    }
    
    public int getDaysOfAugust() {
        return daysOfAugust;
    }
    
    public int getDaysOfDecember() {
        return daysOfDecember;
    }
    
    public int getDaysOfFebruary() {
        return daysOfFebruary;
    }
    
    private void setDaysOfFebruary(int daysOfFebruary) {
        this.daysOfFebruary = daysOfFebruary;
    }
    
    public int getDaysOfJanuary() {
        return daysOfJanuary;
    }
    
    public static int getDaysofjanuary() {
		return daysOfJanuary;
	}

    public int getDaysOfJuly() {
        return daysOfJuly;
    }
    
    public int getDaysOfJune() {
        return daysOfJune;
    }

    public int getDaysOfMarch() {
        return daysOfMarch;
    }

    public int getDaysOfMay() {
        return daysOfMay;
    }
    

    public int getDaysOfNovember() {
        return daysOfNovember;
    }
    

    public int getDaysOfOctober() {
        return daysOfOctober;
    }
    
    public int getDaysOfSeptember() {
        return daysOfSeptember;
    }
    
}
