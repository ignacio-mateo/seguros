package mx.com.orbita.dao;

import java.util.Date;
import java.util.List;
import mx.com.orbita.to.CircularTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;

public interface CircularDao {

	void setRenovacion(CircularTO circularTO);
	void setMemorial(CircularTO circularTO);
	Integer setDependienteMemorial(ConsulmedTO consulmedTO,Date fechaNac);
	Integer eliminarDependienteMemorial(Integer idSolicitud);
	List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSol);

}
