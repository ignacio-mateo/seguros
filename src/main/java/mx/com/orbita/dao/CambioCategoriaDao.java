package mx.com.orbita.dao;

import java.util.Date;
import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface CambioCategoriaDao {

	EmpleadoTO getEmpleado(Integer numEmp);
	Integer getSolicitudExiste(Integer numEmp, Integer idTipoSol, Integer idEstSol);
	SolicitudTO getSolicitud(Integer numEmp);
	List<DependienteTO> getDependiente(Integer numEmp);
	Integer getTarifaConf(String clave, String sexo);
	Integer getTarifaSind(Integer idTarifa);
	Integer getDependienteProceso(Integer numEmp, Integer estatusSol);
	Integer setCategoria(Integer numEmp, Integer idSol, Double costoAnt, Double costoNvo, Integer categoriaAnt, Integer categoriaNvo, Date fechaCambio, Integer idAdmin);
	Integer setEmpleado(Integer idTipoEmpl, Integer numEmp);
	Integer setSolicitud(Double costoEmp, Double costoAmp, Double costoDep, Double costoTot, Integer idTipoCobertura, Integer numEmp, Integer idSol);
	Integer setDependiente(Double costoDep, Integer idSol);

}
