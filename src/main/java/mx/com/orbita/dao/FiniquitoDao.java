package mx.com.orbita.dao;

import java.util.Date;
import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.FiniquitoTO;
import mx.com.orbita.to.SolicitudTO;

public interface FiniquitoDao {
	
	List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol);
	List<DependienteTO> getDependienteSol(Integer numEmp, Integer tipoSol, Integer estatusSol);
	List<DependienteTO> getConsulmedSol(Integer numEmp, Integer tipoSol, Integer estatusSol);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	FiniquitoTO getFiniquito(Integer numEmp);
	Integer getTipoCobertura(Integer numEmp);
	Double getCostoTotal(Integer numEmp);
	Integer getIdSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	Integer setSolicitud(FiniquitoTO finiquitoTO, Integer tipoSol, Integer estatusSol);
	Integer setDependiente(Integer idSol, Integer tipoSol, Integer estatusSol,Date fechaFiniquito);
	Integer setConsulmed(Integer idSol, Integer tipoSol, Integer estatusSol,Date fechaFiniquito);
	Integer setFiniquito(FiniquitoTO finiquitoTO);
	
}
