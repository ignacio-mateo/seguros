package mx.com.orbita.dao;

import mx.com.orbita.to.DatFiscalesTO;

public interface DatosFiscalesDao {
	
	DatFiscalesTO getDatosFiscales(Integer numEmp);
	void setDatosFiscales(DatFiscalesTO datos);
		

}
