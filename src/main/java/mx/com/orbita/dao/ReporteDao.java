package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.ReporteParamsTO;
import mx.com.orbita.to.ReporteTO;

public interface ReporteDao {
	
	List<ReporteTO> getCambioCategoria(ReporteParamsTO reporteTO);
	List<ReporteTO> getNominaAlta(ReporteParamsTO reporteTO);
	List<ReporteTO> getNominaBaja(ReporteParamsTO reporteTO);
	List<ReporteTO> getNominaDepAdicion(ReporteParamsTO reporteTO);
	List<ReporteTO> getNominaDepBaja(ReporteParamsTO reporteTO);
	List<ReporteTO> getReporteActual(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmedAlta(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmedBajaTotal(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmedAdicion(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmedBaja(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmedTotal(ReporteParamsTO reporteTO);
	List<ReporteTO> getDatosFiscalesAlta(ReporteParamsTO reporteTO);
	List<ReporteTO> getDatosFiscalesBaja(ReporteParamsTO reporteTO);
	List<ReporteTO> getDatosFiscalesFiniquito(ReporteParamsTO reporteTO);
	List<ReporteTO> getAmpliacionesDep(ReporteParamsTO reporteTO);
	List<ReporteTO> getAmpliaciones(ReporteParamsTO reporteTO);
	List<ReporteTO> getDescargaSolAlta(ReporteParamsTO reporteTO);
	List<ReporteTO> getDescargaSolBaja(ReporteParamsTO reporteTO);
	List<ReporteTO> getDescargaAdicion(ReporteParamsTO reporteTO);
	List<ReporteTO> getDescargaBajaDep(ReporteParamsTO reporteTO);
	List<ReporteTO> getRenovEncuesta();
	List<ReporteTO> getRenovSi(ReporteParamsTO reporteTO);
	List<ReporteTO> getRenovNo(ReporteParamsTO reporteTO);
	List<ReporteTO> getRenovNoContestaron(ReporteParamsTO reporteTO);
	List<ReporteTO> getMemorialEncuesta();
	List<ReporteTO> getMemorialDependiente();
	
}
