package mx.com.orbita.dao;

public interface SistemaDao{
	
	Integer existeSolicitudBaja(Integer numEmp, Integer tipoSolA, Integer estSolA, Integer tipoSolB, Integer estSolB, Integer tipoSolC, Integer estSolC);
	Integer existeSolicitudAlta(Integer numEmp, Integer tipoSolA, Integer estSolA, Integer estSolB);
	Integer existeSolicitudAut(Integer numEmp, Integer tipoSol, Integer estSol);
	Integer existeSolicitud(Integer numEmp, Integer tipoSolA, Integer estatusSol);
	Integer existeSolicitudBajaDep(Integer numEmp, Integer tipoSolA, Integer estSolA, Integer estSolB, 
			Integer tipoSolB, Integer estSolC, Integer estSolD);
	Integer getPerfilAdmin(Integer numEmp);
	
}
