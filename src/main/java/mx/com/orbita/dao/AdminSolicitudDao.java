package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.FechaTO;
import mx.com.orbita.to.SolicitudAdminTO;
import mx.com.orbita.to.SolicitudTO;

public interface AdminSolicitudDao {
	
	Integer getSolicitudExiste(Integer numEmp);
	SolicitudAdminTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol, Integer idParentescoA, Integer idParentescoB, Integer estatusRechazado);
	List<SolicitudAdminTO> getSolicitudes(Integer numEmp);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	Double getDependienteCosto(Integer idSol, Integer tipoSol, Integer estSol, Integer idDep);
	Integer getDependienteTotal(Integer idSol, Integer tipoSol, Integer estSol, Integer idDep);
	List<FechaTO> getFechaSol(Integer idSol);
	List<FechaTO> getFechaSol(Integer idSol, Integer tipoSol, Integer idEst, Integer idDep);
	List<SolicitudAdminTO> getDependiente(Integer numEmp);
	DependienteTO getDepCostos(Integer idDep);
	List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol);
	List<DependienteTO> getDependienteEditado(Integer numEmp, Integer tipoSol, Integer estatusSol, Integer idDep);
	List<SolicitudAdminTO> getConsulmed(Integer numEmp);
	List<ConsulmedTO> getConsulmed(Integer numEmp, Integer tipoSol, Integer estatusSol);
	List<SolicitudAdminTO> getMemorial(Integer numEmp);
	void setSolicitud(SolicitudAdminTO solicitudAdminTO);
	void setSolicitudCosto(SolicitudAdminTO solicitudAdminTO);
	void setSolicitudCostoxDep(SolicitudAdminTO solicitudAdminTO);
	void setDependiente(SolicitudAdminTO solicitudAdminTO);
	void setDependienteCosto(SolicitudAdminTO solicitudAdminTO);
	void setCondulmed(SolicitudAdminTO solicitudAdminTO);
	void setCondulmedFechaAlta(SolicitudAdminTO solicitudAdminTO);
	
}
