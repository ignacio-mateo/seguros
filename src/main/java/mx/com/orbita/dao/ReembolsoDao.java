package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.ReembolsoConceptoTO;
import mx.com.orbita.to.ReembolsoRelacionTO;

public interface ReembolsoDao {
	
	ReembolsoRelacionTO getReembolso(Integer numEmp, Integer idReembolso);
	Integer getReembolsoID(Integer numEmp);
	List<Integer> getReembolsoListID(Integer numEmp);
	List<ReembolsoConceptoTO> getReembolsoConceptos(Integer idReembolso);
	Integer setReembolso(ReembolsoRelacionTO reembolsoRelacionTO, Integer numEmp);
	Integer setReembolsoConcepto(ReembolsoConceptoTO reembolsoConceptoTO);
	List<DependienteTO> getDependientes(Integer numEmp, Integer tipoSol, Integer estatusSol);

}
