package mx.com.orbita.dao;

import java.util.Date;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface SolAltaDao {

	double getMontoSindicalizado(String claveEdad, String sexo);
	double getMontoConfianza(int tipo);
	double getMontoAmpliacion(int tipoAmpl);
	double getMontoMemorial(int tipoMemorial);
	Integer setEmpleado(EmpleadoTO empleadoTO);
	Integer setSolicitud(SolicitudTO solicitudTO);
	Integer setDependiente(DependienteTO dependienteTO, Integer idSolicitud, Date fecha);
	Integer setConsulmed(ConsulmedTO consulmedTO, Integer idSolicitud, Date fecha);
	Integer setMemorial(ConsulmedTO consulmedTO, Integer idSolicitud, Date fecha);
		
}
