package mx.com.orbita.dao;

import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoAdrisaTO;

public interface EmpleadoDao {
	
	EmpleadoTO datosEmpleado(Integer numEmp);
	ReembolsoAdrisaTO datosEmpleadoReembolso(Integer numEmp);
	String getCorreo(Integer numEmp);

}
