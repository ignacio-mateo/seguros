package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.CoberturaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface CambioCoberturaDao {

	EmpleadoTO getEmpleado(Integer numEmp);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol);
	Integer getDependienteProceso(Integer numEmp, Integer estatusSol);
	Integer setCoberturaHistorico(CoberturaTO coberturaTO);
	Integer setCambioCobertura(CoberturaTO coberturaTO);
	Integer setCambioCoberturaDep(CoberturaTO coberturaTO);

}
