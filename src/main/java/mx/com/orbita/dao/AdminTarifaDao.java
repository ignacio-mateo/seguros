package mx.com.orbita.dao;

import java.util.List;

import mx.com.orbita.to.TarifaTO;

public interface AdminTarifaDao {

	List<TarifaTO> getTarifaConfianza();
	List<TarifaTO> getTarifaSindicalizado();
	List<TarifaTO> getTarifaAmplia();
	void setTarifaConfianza(Integer tarifaAnt, Integer tarifa, Integer idTarifa);
	void setTarifaSindicalizado(Integer tarifaAnt,Integer tarifa, Integer idTarifa);
	void setTarifaAmplia(Integer tarifaAnt, Integer tarifa, Integer idTarifa);

}
