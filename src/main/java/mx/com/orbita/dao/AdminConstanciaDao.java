package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.ConstanciaTO;
import mx.com.orbita.to.DependienteTO;

public interface AdminConstanciaDao {
	
	ConstanciaTO getEmpleado(Integer numEmp, Integer tipoSol, Integer estSol);
	List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol);

}
