package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.AmpliacionTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface SolConsultaDao {
	
	EmpleadoTO getEmpleadoBD(Integer numEmp);
	
	Integer getSolicitudId(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	SolicitudTO getSolicitudBaja(Integer numEmp, Integer tipoSolA, Integer estatusSolA, Integer tipoSolB, Integer estatusSolB, Integer estatusSolC, Integer estatusSolD);
	SolicitudTO getSolicitudCob(Integer numEmp, Integer tipoSol, Integer estatusSol);
	List<AmpliacionTO> getTipoCob();
	List<AmpliacionTO> getTipoCobMem();
	String getTipoCob(Integer idTipoCob);
	
	List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<DependienteTO> getDependienteRenov(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<Integer> getIdDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB, Integer estatusSolC);
	DependienteTO getDependienteAlta(Integer idDependiente);
	DependienteTO getDependienteBaja(Integer idDependiente);
	List<DependienteTO> getDependienteBaja(Integer idSolicitud, Integer tipoSolA, Integer tipoSolB, Integer estatusSolA, Integer estatusSolB, Integer estatusSolC);
	List<Integer> getDependienteBajaxId(Integer idSolicitud, Integer tipoSolA, Integer estatusSolA, Integer tipoSolB, Integer estatusSolB, Integer estatusSolC, Integer estatusSolD, Integer estatusSolE);
	
	List<ConsulmedTO> getConsulmed(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<Integer> getIdConsulmed(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB, Integer estatusSolC);
	ConsulmedTO getConsulmedAlta(Integer idDependiente);
	ConsulmedTO getConsulmedBaja(Integer idDependiente);
	List<ConsulmedTO> getConsulmedBaja(Integer idSolicitud, Integer tipoSolA, Integer tipoSolB, Integer estatusSolA, Integer estatusSolB, Integer estatusSolC);
	List<Integer> getConsulmedBajaxId(Integer idSolicitud, Integer tipoSolA, Integer estatusSolA, Integer tipoSolB, Integer estatusSolB, Integer estatusSolC, Integer estatusSolD, Integer estatusSolE);
	List<Integer> getConsulmedIdParentesco(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB,
			Integer estatusConA, Integer estatusConB, Integer estatusConC, Integer estatusConD, Integer estatusConE, Integer estatusConF);
	Integer getConsulmedTotal(Integer tipoSol, Integer estatusSol, Integer numEmp, Integer estatusDepA, Integer estatusDepB);
		
	List<ConsulmedTO> getMemorial(Integer idSolicitud);
}
