package mx.com.orbita.dao;

import java.util.List;

public interface SolBajaDao {
	
	List<Integer> getIdDependiente(Integer tipoSol, Integer estatusSol, Integer numEmp);
	List<Integer> getIdConsulmed(Integer tipoSol, Integer estatusSol, Integer numEmp);
	void setSolicitud(Integer numEmp, Integer estatusSol, Integer sol);
	void setDependiente(Integer idDependiente, Integer estatusSol);
	void setConsulmed(Integer idDependiente, Integer estatusSol);
}
