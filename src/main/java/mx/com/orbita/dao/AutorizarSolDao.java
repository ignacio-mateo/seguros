package mx.com.orbita.dao;

import java.util.Date;
import java.util.List;
import mx.com.orbita.to.AutorizacionTO;
import mx.com.orbita.to.DependienteTO;

public interface AutorizarSolDao {
	
	List<AutorizacionTO> getProcesarSolicitudes(Integer idOperacion, Integer tipoSol, Integer estatusSol);
	List<AutorizacionTO> getProcesarSolicitudesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol);
	List<AutorizacionTO> getProcesarDependientes(Integer idOperacion, Integer tipoSol, Integer estatusSol, Integer estatusCicloSol, Integer estatusDep);
	List<AutorizacionTO> getProcesarDependientesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol,Integer estatusCicloSol, Integer estatusDep);
	List<AutorizacionTO> getProcesarConsulmed(Integer idOperacion, Integer tipoSol, Integer estatusSol, Integer estatusCicloSol, Integer estatusDep);
	List<AutorizacionTO> getProcesarConsulmedBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol,Integer estatusCicloSol, Integer estatusDep);
	Integer getTotalDep(Integer idSolDep, Integer tipoSol, Integer estatusSol);
	Integer getTotalDep(Integer idSolDep,Integer tipoSol,Integer estatusSolAut,Integer estatusSol);
	Double getCostoTotDep(Integer idSol);
	Double getCostoTotAmpliacion(Integer idSol);
	Double getCostoAmpliacionEmp(Integer idSol);
	Double getCostoAmpliacionDep(Integer idSol);
	Double getCostoAmpliacionDepTot(Integer idSol, Integer tipoSol, Integer idEst, Integer idDep);
	String getFechaAlta(Integer idSol);
	List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer idEst);
	List<DependienteTO> getDependienteFechas(Integer idSolicitud, Integer tipoSol, Integer idEst);
	DependienteTO getDependienteFecha(Integer idSolicitud);
	void setProcesarSolicitudesAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud);
	void setProcesarSolicitudesBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Date fecha, String comentario, Integer idSolicitud);
	void setProcesarDependientesAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud);
	void setProcesarDependientesBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud);
	void setProcesarSolDependientes(Double costoTotal, Double costoDependientes, Double costoAmpliacion, Integer idSolicitud);
	void setProcesarCostoCancelacion(Double costo, Double costoAmp, Integer idSolicitud);
	void setProcesarDependientesXsolAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Integer idSolicitud);
	void setProcesarDependientesXsolBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Date fecha, Integer idSolicitud, Integer estRechazado);
	void setProcesarConsulmedAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud);
	void setProcesarConsulmedBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud);
	void setProcesarConsulmedXsolAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Integer idSolicitud);
	void setProcesarConsulmedXsolBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Date fecha, Integer idSolicitud, Integer estRechazado);
	void setProcesarMemorialXsolAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Integer idSolicitud);
	void setUpdateSol(Double costoEmp, Double costoAmpInd, Double costoDep, Double costoTotal, Double costoAmpl, Double costoMem, Integer idSolicitud);
	void setUpdateSolDep(Double costoDep,Double costoAmpl,Integer idSolicitud);
	
}
