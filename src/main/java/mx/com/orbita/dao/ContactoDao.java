package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.ContactoTO;

public interface ContactoDao {
	
	List<ContactoTO> getContacto();

}
