package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AdminTarifaDao;
import mx.com.orbita.to.TarifaTO;

@Component
public class AdminTarifaDaoImpl implements AdminTarifaDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_TARIF_CONF = "SELECT ID_TARIFA, DESCRIPCION, TARIFA FROM CAT_TARIFA_CONF ORDER BY ID_TARIFA";
	public static final String QUERY_TARIF_SIND = "SELECT ID_TARIFA, CLAVE_EDAD, SEXO, TARIFA FROM CAT_TARIFA_SIND ORDER BY ID_TARIFA";
	public static final String QUERY_TARIF_AMPLIA = "SELECT ID_TARIFA, DESCRIPCION, TARIFA FROM CAT_TARIFA_AMP ORDER BY ID_TARIFA";
	public static final String UPDATE_TARIF_CONF = "UPDATE CAT_TARIFA_CONF SET TARIFA_ANT = ?, TARIFA = ? WHERE ID_TARIFA = ?";
	public static final String UPDATE_TARIF_SIND = "UPDATE CAT_TARIFA_SIND SET TARIFA_ANT = ?, TARIFA = ? WHERE ID_TARIFA = ?";
	public static final String UPDATE_TARIF_AMPLIA = "UPDATE CAT_TARIFA_AMP SET TARIFA_ANT = ?, TARIFA = ? WHERE ID_TARIFA = ?";	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TarifaTO> getTarifaConfianza() {
		List<TarifaTO> contactos = new ArrayList<TarifaTO>();
		contactos = jdbcTemplate.query(QUERY_TARIF_CONF, new RowMapper(){
			public TarifaTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					TarifaTO p = new TarifaTO();
					p.setId(rs.getInt(1));
					p.setDescripcion(rs.getString(2));
					p.setTarifa(rs.getInt(3));
		    	 return p;
		      }
		      });
		return contactos;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TarifaTO> getTarifaSindicalizado() {
		List<TarifaTO> contactos = new ArrayList<TarifaTO>();
		contactos = jdbcTemplate.query(QUERY_TARIF_SIND, new RowMapper(){
			public TarifaTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					TarifaTO p = new TarifaTO();
					p.setId(rs.getInt(1));
					p.setDescripcion(rs.getString(2));
					p.setSexo(rs.getString(3));
					p.setTarifa(rs.getInt(4));
		    	 return p;
		      }
		      });
		return contactos;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TarifaTO> getTarifaAmplia() {
		List<TarifaTO> contactos = new ArrayList<TarifaTO>();
		contactos = jdbcTemplate.query(QUERY_TARIF_AMPLIA, new RowMapper(){
			public TarifaTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					TarifaTO p = new TarifaTO();
					p.setId(rs.getInt(1));
					p.setDescripcion(rs.getString(2));
					p.setTarifa(rs.getInt(3));
		    	 return p;
		      }
		      });
		return contactos;
	}
		
	public void setTarifaConfianza(Integer tarifaAnt, Integer tarifa, Integer idTarifa){
		jdbcTemplate.update(UPDATE_TARIF_CONF, tarifaAnt, tarifa, idTarifa);
	}
	
	public void setTarifaSindicalizado(Integer tarifaAnt, Integer tarifa, Integer idTarifa){
		jdbcTemplate.update(UPDATE_TARIF_SIND, tarifaAnt, tarifa, idTarifa);
	}

	public void setTarifaAmplia(Integer tarifaAnt, Integer tarifa, Integer idTarifa){
		jdbcTemplate.update(UPDATE_TARIF_AMPLIA, tarifaAnt, tarifa, idTarifa);
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}
