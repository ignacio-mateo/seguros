package mx.com.orbita.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CircularDao;
import mx.com.orbita.to.CircularTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import java.util.List;
import java.util.Date;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.dao.EmptyResultDataAccessException;

@Component
public class CircularDaoImpl implements CircularDao{

	private static final Logger logger = Logger.getLogger(CircularDaoImpl.class);
	private JdbcTemplate jdbcTemplate;

	public static final String MERGE_RENOVACION = "MERGE INTO RENOVACION R " +
			"USING (SELECT ? ID_EMPLEADO, ? ID_RENOVACION,? ID_RESP, ? FECHA_RESPUESTA, ? " +
			"FECHA_ACTUALIZACION, ? ID_TIPO_COB FROM DUAL) F " +
			"ON (R.ID_EMPLEADO = F.ID_EMPLEADO) "+
			"WHEN MATCHED THEN "+
			"		UPDATE SET R.ID_RENOVACION = F.ID_RENOVACION,"+
			"					R.ID_RESP = F.ID_RESP, "+
			"					R.FECHA_RESPUESTA = F.FECHA_RESPUESTA, "+
			"					R.FECHA_ACTUALIZACION = F.FECHA_ACTUALIZACION, "+
			"					R.ID_TIPO_COB = F.ID_TIPO_COB "+
			" WHEN NOT MATCHED THEN " +
			"		INSERT(R.ID_EMPLEADO,R.ID_RENOVACION,R.ID_RESP,R.FECHA_RESPUESTA,R.FECHA_ACTUALIZACION,R.ID_TIPO_COB) "+
			"     VALUES(F.ID_EMPLEADO,F.ID_RENOVACION,F.ID_RESP,F.FECHA_RESPUESTA,F.FECHA_ACTUALIZACION,F.ID_TIPO_COB) ";

	public static final String MERGE_MEMORIAL = "MERGE INTO CONTRATAR_MEMORIAL M " +
			"USING (SELECT ? ID_EMPLEADO, ? FECHA_RESPUESTA, ? ID_TIPO_COB_MEM FROM DUAL) F " +
			"ON (M.ID_EMPLEADO = F.ID_EMPLEADO) "+
			"WHEN MATCHED THEN "+
			"		UPDATE SET M.FECHA_RESPUESTA = F.FECHA_RESPUESTA, "+
			"					M.ID_TIPO_COB_MEM = F.ID_TIPO_COB_MEM "+
			" WHEN NOT MATCHED THEN " +
			"		INSERT(M.ID_EMPLEADO,M.FECHA_RESPUESTA,M.ID_TIPO_COB_MEM) "+
			"     VALUES(F.ID_EMPLEADO,F.FECHA_RESPUESTA,F.ID_TIPO_COB_MEM) ";


	public static final String DEPENDIENTES = "SELECT C.ID_CONSULMED,C.NOMBRE,C.A_PATERNO," +
			"C.A_MATERNO,TO_CHAR(C.FECHA_NACIMIENTO,'DD-MM-yyyy') FECHA_NACIMIENTO " +
			"FROM SOLICITUD S " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"WHERE S.ID_EMPLEADO = ? " +
			"AND C.ID_TIPO_SOL = ? " +
			"AND C.ID_ESTATUS = ?";

	public static final String INSERT_MEMORIAL = "INSERT INTO MEMORIAL(Nombre, A_Paterno, A_Materno, " +
			"Id_Sexo, Fecha_Nacimiento, Id_Parentesco,Id_Solicitud, Id_Tipo_Sol) " +
			"VALUES(?,?,?,?,?,?,?,?)";

	public static final String DELETE_MEMORIAL = "DELETE FROM MEMORIAL WHERE ID_SOLICITUD = ?";

	public void setRenovacion(CircularTO circularTO){
		jdbcTemplate.update(MERGE_RENOVACION,
				circularTO.getNumEmp(),
				circularTO.getEstRenov(),
				circularTO.getRespRenov(),
				circularTO.getFechaResp(),
				circularTO.getFechaAct(),
				circularTO.getTipoCob());
	}

	public void setMemorial(CircularTO circularTO){
		jdbcTemplate.update(MERGE_MEMORIAL,
				circularTO.getNumEmp(),
				circularTO.getFechaResp(),
				circularTO.getRespRenov());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSol) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(DEPENDIENTES, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setNombre(rs.getString(2));
					dependiente.setApellidoP(rs.getString(3));
					dependiente.setApellidoM(rs.getString(4));
					dependiente.setFechaNac(rs.getString(5));
					return dependiente;
				}}, idSolicitud, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}

	public Integer setDependienteMemorial(ConsulmedTO consulmedTO,Date fechaNac){
		Integer resultado = jdbcTemplate.update(INSERT_MEMORIAL,
				consulmedTO.getNombre().toUpperCase(),
				consulmedTO.getApellidoP().toUpperCase(),
				consulmedTO.getApellidoM().toUpperCase(),
				consulmedTO.getIdSexo(),
				fechaNac,
				consulmedTO.getIdParentesco(),
				consulmedTO.getIdSolicitud(),
				consulmedTO.getIdTipoSol());
		return resultado;
	}

	public Integer eliminarDependienteMemorial(Integer idSolicitud){
		Integer resultado = jdbcTemplate.update(DELETE_MEMORIAL,idSolicitud);
		return resultado;
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
