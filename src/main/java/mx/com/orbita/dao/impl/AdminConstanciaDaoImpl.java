package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AdminConstanciaDao;
import mx.com.orbita.to.ConstanciaTO;
import mx.com.orbita.to.DependienteTO;

@Component
public class AdminConstanciaDaoImpl implements AdminConstanciaDao{
	
	private static final Logger logger = Logger.getLogger(AdminConstanciaDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
		
	public static final String QUERY_EMPLEADO = "SELECT E.ID_EMPLEADO,E.ID_TIPO_EMPLEADO," +
			"S.ID_TIPO_SOL,S.ID_ESTATUS,E.NOMBRE,E.A_PATERNO,E.A_MATERNO,S.FECHA_AUTORIZACION," +
			"S.FECHA_CANCELACION,CTE.DESCRIPCION,E.ID_REGION,CTC.DESCRIPCION " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN CAT_TIPO_EMPLEADO CTE ON E.ID_TIPO_EMPLEADO = CTE.ID_TIPO_EMPLEADO "+ 
			"INNER JOIN CAT_TIPO_COBERTURA CTC ON S.ID_TIPO_COB = CTC.ID_TIPO_COB " +
			"WHERE E.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?";
	
	public static final String QUERY_DEPENDIENTE = "SELECT D.ID_DEPENDIENTE,D.NOMBRE,D.A_PATERNO," +
			"D.A_MATERNO,D.FECHA_ALTA_AUT,CP.PARENTESCO,CES.DESCRIPCION " +
			"FROM SOLICITUD S INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_PARENTESCO CP ON D.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_ESTATUS_SOL CES ON CES.ID_ESTATUS = D.ID_ESTATUS " +
			"WHERE S.ID_EMPLEADO = ? AND D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?";
	
	public ConstanciaTO getEmpleado(Integer numEmp, Integer tipoSol, Integer estSol){
		ConstanciaTO datos = new ConstanciaTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_EMPLEADO, new RowMapper<ConstanciaTO>(){
				public ConstanciaTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					ConstanciaTO solicitudTO = new ConstanciaTO();
					solicitudTO.setNumEmp(rs.getInt(1));
					solicitudTO.setIdTipoEmp(rs.getInt(2));
					solicitudTO.setIdTipoSol(rs.getInt(3));
					solicitudTO.setIdEstatus(rs.getInt(4));
					solicitudTO.setNombre(rs.getString(5));
					solicitudTO.setApellidoP(rs.getString(6));
					solicitudTO.setApellidoM(rs.getString(7));
					solicitudTO.setFechaAut(rs.getDate(8));
					solicitudTO.setFechaCan(rs.getDate(9));
					solicitudTO.setTipoEmp(rs.getString(10));
					solicitudTO.setIdRegion(rs.getInt(11));
					solicitudTO.setTipoCobertura(rs.getString(12));
					return solicitudTO;
				}
			}, numEmp, tipoSol, estSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return datos;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol){
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setNombre(rs.getString(2));
					dependiente.setApellidoP(rs.getString(3));
					dependiente.setApellidoM(rs.getString(4));
					dependiente.setFecha(rs.getDate(5));
					dependiente.setParentesco(rs.getString(6));
					dependiente.setEstatusDep(rs.getString(7));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}
