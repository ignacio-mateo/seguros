package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.ContactoDao;
import mx.com.orbita.to.ContactoTO;

@Component
public class ContactoDaoImpl implements ContactoDao{

	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_CONTACTOS = "SELECT ID_CONTACTO, NOMBRE, PUESTO, TELEFONO, EXTENSION " +
			"FROM CAT_CONTACTO WHERE ID_CONTACTO BETWEEN 1 AND 5 ORDER BY ID_CONTACTO";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ContactoTO> getContacto() {
		List<ContactoTO> contactos = new ArrayList<ContactoTO>();
		contactos = jdbcTemplate.query(QUERY_CONTACTOS, new RowMapper(){
			public ContactoTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ContactoTO p = new ContactoTO();
					p.setId(rs.getInt(1));
					p.setNombre(rs.getString(2));
					p.setPuesto(rs.getString(3));
					p.setTelefono(rs.getInt(4));
					p.setExtension(rs.getInt(5));
		    	 return p;
		      }
		      });
		return contactos;
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
