package mx.com.orbita.dao.impl;

import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SolAltaDao;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

@Component
public class SolAltaDaoImpl implements SolAltaDao{
	
	private static final Logger logger = Logger.getLogger(SolAltaDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_TARIFA_SIND = "SELECT TARIFA FROM CAT_TARIFA_SIND WHERE CLAVE_EDAD = ? AND SEXO = ? ";
	public static final String QUERY_TARIFA_CONF = "SELECT TARIFA FROM CAT_TARIFA_CONF WHERE ID_TARIFA = ? ";
	public static final String QUERY_TARIFA_PRIMA = "SELECT TARIFA FROM CAT_TARIFA_AMP WHERE ID_TARIFA = ? ";	
	public static final String QUERY_TARIFA_MEMORIAL = "SELECT TARIFA FROM CAT_TARIFA_MEMORIAL WHERE ID_TARIFA = ? ";
	
	public static final String INSERT_EMPLEADO = "INSERT INTO EMPLEADO(Id_Empleado, Nombre, A_Paterno, A_Materno, " +
			"Departamento, Fecha_Nacimiento, Id_Sexo, Id_Tipo_Empleado, Puesto, Fecha_Ingreso, Id_Region, Centro) " +
			"VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

	public static final String INSERT_SOLICITUD = "INSERT INTO SOLICITUD(Id_Empleado, Costo_Empleado, " +
			"Costo_Ampliacion,Costo_Dependientes,Costo_Cob_Memorial,Costo_Total,Id_Tipo_Sol,Id_Tipo_Cob,Id_Tipo_Cob_Mem) " +
			"VALUES(?,?,?,?,?,?,?,?,?)";

	public static final String INSERT_DEPENDIENTE = "INSERT INTO DEPENDIENTE(Nombre, A_Paterno, A_Materno, " +
			"Id_Sexo, Fecha_Nacimiento, Costo_Dependiente, Id_Parentesco, Id_Solicitud, Id_Tipo_Sol) " +
			"VALUES(?,?,?,?,?,?,?,?,?)";

	public static final String INSERT_CONSULMED = "INSERT INTO CONSULMED(Nombre, A_Paterno, A_Materno, " +
			"Id_Sexo, Fecha_Nacimiento, Id_Parentesco,Id_Solicitud, Id_Tipo_Sol) " +
			"VALUES(?,?,?,?,?,?,?,?)";
	
	public static final String INSERT_MEMORIAL = "INSERT INTO MEMORIAL(Nombre, A_Paterno, A_Materno, " +
			"Id_Sexo, Fecha_Nacimiento, Id_Parentesco,Id_Solicitud, Id_Tipo_Sol) " +
			"VALUES(?,?,?,?,?,?,?,?)";
		
	/**
	 * 
	 * Obtiene la tarifa del empleado sindicalizado
	 * @param claveEdad: rango de edad del empleado
	 * @param sexo: clave (M-F)
	 * 
	 */
	public double getMontoSindicalizado(String claveEdad, String sexo) {
		double monto = 0;
		monto = jdbcTemplate.queryForObject(QUERY_TARIFA_SIND, Double.class,claveEdad,sexo);
		logger.debug("  TarifaSindicalizado: ["+monto+"]");
		return monto;
	}
	
	/**
	 * 
	 * Obtiene la tarifa para el empleado de confianza
	 * @param tipo: categoria de confianza
	 * 
	 */
	public double getMontoConfianza(int tipo) {
		double monto = 0;
		monto = jdbcTemplate.queryForObject(QUERY_TARIFA_CONF, Double.class, tipo);
		logger.debug("  TarifaConfianza: ["+monto+"]");
		return monto;
	}
	
	/**
	 * 
	 * Obtiene la tarifa de ampliacion
	 * @param tipoAmpl: tipo de ampliacion.
	 * 
	 */
	public double getMontoAmpliacion(int tipoAmpl){
		double monto = 0;
		monto = jdbcTemplate.queryForObject(QUERY_TARIFA_PRIMA, Double.class, tipoAmpl);
		logger.debug("  TarifaAmpliacion: ["+monto+"]");
		return monto;
	}
	
	/**
	*
	* Obtiene la tarifa de memorial
	* @param tipoMemorial: tipo de memorial.
	*
	*/
	public double getMontoMemorial(int tipoMemorial){
		double monto = 0;
		monto = jdbcTemplate.queryForObject(QUERY_TARIFA_MEMORIAL, Double.class, tipoMemorial);
		logger.debug("  TarifaMemorial: ["+monto+"]");
		return monto;
	}
	
	/**
	 * 
	 * Inserta el empleado en la Tabla EMPLEADO
	 * @param empleadoTO: Bean con los datos del empleado
	 * 
	 */
	public Integer setEmpleado(EmpleadoTO empleadoTO){
		Integer resultado = jdbcTemplate.update(INSERT_EMPLEADO, 
				empleadoTO.getNumEmp(),
				empleadoTO.getNombre(),
				empleadoTO.getApellidoP(),
				empleadoTO.getApellidoM(),
				empleadoTO.getDepartamento(),
				empleadoTO.getFechaNac(),
				empleadoTO.getIdSexo(),
				empleadoTO.getTipoEmp(),
				empleadoTO.getPuesto(),
				empleadoTO.getFechaIng(),
				empleadoTO.getIdRegion(),
				empleadoTO.getCentro());
		return resultado;
	}
	
	/**
	 * 
	 * Inserta la solicitud en la Tabla SOLICITUD
	 * @param solicitudTO: Bean con los datos de la solicitud.
	 * 
	 */
	public Integer setSolicitud(SolicitudTO solicitudTO){
		Integer resultado = jdbcTemplate.update(INSERT_SOLICITUD, 
					solicitudTO.getNumEmp(),
					solicitudTO.getCostoEmpleado(),
					solicitudTO.getCostoAmpliacion(),
					solicitudTO.getCostoDependiente(),
					solicitudTO.getCostoMemorial(),
					solicitudTO.getCostoTotal(),
					solicitudTO.getIdTipoSolicitud(),
					solicitudTO.getIdTipoCobertura(),
					solicitudTO.getIdTipoMemorial());
		return resultado;
	}
	
	/**
	 * 
	 * Inserta el dependiete en la Tabla DEPENDIENTE
	 * @param dependienteTO: Bean con los datos del dependiente
	 * @param idSolicitud
	 * @param fecha 
	 * 
	 */
	public Integer setDependiente(DependienteTO dependienteTO, Integer idSolicitud, Date fecha){
		Integer resultado = jdbcTemplate.update(INSERT_DEPENDIENTE, 
				dependienteTO.getNombre().toUpperCase(),
				dependienteTO.getApellidoP().toUpperCase(),
				dependienteTO.getApellidoM().toUpperCase(),
				dependienteTO.getIdSexo(),
				fecha,
				dependienteTO.getCostoDep(),
				dependienteTO.getIdParentesco(),
				idSolicitud,
				dependienteTO.getIdTipoSol());
		return resultado;
	}
	
	/**
	 * 
	 * Inserta el dependiente consulmed en la Tabla CONSULMED
	 * @param consulmedTO: Bean con los datos del dependiente consulmed
	 * @param idSolicitud
	 * @param fecha
	 * 
	 */
	public Integer setConsulmed(ConsulmedTO consulmedTO, Integer idSolicitud, Date fecha){
		Integer resultado = jdbcTemplate.update(INSERT_CONSULMED,
					consulmedTO.getNombre().toUpperCase(),
					consulmedTO.getApellidoP().toUpperCase(),
					consulmedTO.getApellidoM().toUpperCase(),
					consulmedTO.getIdSexo(),
					fecha,
					consulmedTO.getIdParentesco(),
					idSolicitud,
					consulmedTO.getIdTipoSol());
		return resultado;
	}
	
	public Integer setMemorial(ConsulmedTO consulmedTO, Integer idSolicitud, Date fecha){
		Integer resultado = jdbcTemplate.update(INSERT_MEMORIAL,
					consulmedTO.getNombre().toUpperCase(),
					consulmedTO.getApellidoP().toUpperCase(),
					consulmedTO.getApellidoM().toUpperCase(),
					consulmedTO.getIdSexo(),
					fecha,
					consulmedTO.getIdParentesco(),
					idSolicitud,
					consulmedTO.getIdTipoSol());
		return resultado;
	}
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}