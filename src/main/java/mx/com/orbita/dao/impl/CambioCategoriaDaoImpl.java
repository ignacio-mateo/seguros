package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CambioCategoriaDao;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

@Component
public class CambioCategoriaDaoImpl implements CambioCategoriaDao {

	private static final Logger logger = Logger.getLogger(CambioCategoriaDaoImpl.class);
	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_SOLICITUD_EXISTE = "SELECT ID_SOLICITUD FROM SOLICITUD " +
			"WHERE ID_EMPLEADO = ? AND ID_TIPO_SOL = ? AND ID_ESTATUS = ?";

	public static final String QUERY_EMPLEADO = "SELECT E.ID_EMPLEADO, 'EMPLEADO', E.A_PATERNO, E.A_MATERNO, E.NOMBRE, "
			+ "TO_CHAR(E.FECHA_NACIMIENTO,'dd-MM-yyyy'), E.ID_SEXO, CS.SEXO, E.ID_TIPO_EMPLEADO, CE.DESCRIPCION "
			+ "FROM EMPLEADO E INNER JOIN CAT_TIPO_EMPLEADO CE on E.ID_TIPO_EMPLEADO = CE.ID_TIPO_EMPLEADO "
			+ "INNER JOIN CAT_SEXO CS on E.ID_SEXO = CS.ID_SEXO "
			+ "WHERE E.ID_EMPLEADO = ?";

	public static final String QUERY_SOLICITUD = "SELECT S.ID_SOLICITUD,S.ID_TIPO_SOL,CS.DESCRIPCION,CE.DESCRIPCION," +
			"TO_CHAR(S.FECHA_AUTORIZACION,'dd/MM/yyyy'), S.COSTO_EMPLEADO, S.COSTO_AMPLIACION, S.COSTO_DEPENDIENTES, " +
			"S.COSTO_TOTAL, S.ID_TIPO_COB, CT.DESCRIPCION " +
			"FROM EMPLEADO E inner join SOLICITUD S on E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN CAT_TIPO_COBERTURA CT on S.ID_TIPO_COB = CT.ID_TIPO_COB " +
			"INNER JOIN CAT_ESTATUS_SOL CE on CE.ID_ESTATUS = S.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_SOL CS on CS.ID_TIPO_SOL = S.ID_TIPO_SOL " +
			"WHERE E.ID_EMPLEADO = ?";

	public static final String QUERY_DEPENDIENTE = "SELECT D.ID_DEPENDIENTE, D.A_PATERNO, D.A_MATERNO, " +
			"D.NOMBRE,D.ID_PARENTESCO, CP.PARENTESCO, D.COSTO_DEPENDIENTE, D.ID_SEXO, CS.SEXO, " +
			"TO_CHAR(D.FECHA_NACIMIENTO,'yyyy-MM-dd') FECHA_NAC, TO_CHAR(D.FECHA_ALTA_AUT,'dd/MM/yyyy') FECHA_ALTA, " +
			"CT.DESCRIPCION TIPO_SOL, CES.DESCRIPCION EST_SOL " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S on E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D on S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_PARENTESCO CP on D.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_SEXO CS on D.ID_SEXO = CS.ID_SEXO " +
			"INNER JOIN CAT_TIPO_SOL CT on D.ID_TIPO_SOL = CT.ID_TIPO_SOL " +
			"INNER JOIN CAT_ESTATUS_SOL CES on D.ID_ESTATUS = CES.ID_ESTATUS " +
			"WHERE E.ID_EMPLEADO = ?";

	public static final String QUERY_DEPENDIENTE_EN_PROCESO = "SELECT COUNT(*) FROM EMPLEADO E "
			+ "INNER JOIN SOLICITUD S on E.ID_EMPLEADO = S.ID_EMPLEADO "
			+ "INNER JOIN DEPENDIENTE D on S.ID_SOLICITUD = D.ID_SOLICITUD "
			+ "WHERE E.ID_EMPLEADO = ? AND D.ID_ESTATUS = ?";

	public static final String QUERY_TARIFA_SIND = "SELECT T.TARIFA FROM CAT_TARIFA_SIND T WHERE T.CLAVE_EDAD = ? AND T.SEXO = ?";

	public static final String QUERY_TARIFA_CONF = "SELECT T.TARIFA FROM CAT_TARIFA_CONF T WHERE T.ID_TARIFA = ?";

	public static final String INSERT_CATEGORIA = "INSERT INTO CAMBIO_CATEGORIA(ID_EMPLEADO, ID_SOLICITUD, COSTO_ANT, COSTO_NUEVO, " +
			"CATEGORIA_ANT, CATEGORIA_NVO, FECHA_CAMBIO_CAT, ID_ADMIN_AUT) VALUES(?,?,?,?,?,?,?,?)";

	public static final String UPDATE_EMPLEADO = "UPDATE EMPLEADO SET ID_TIPO_EMPLEADO = ? WHERE ID_EMPLEADO = ?";

	public static final String UPDATE_SOLICITUD = "UPDATE SOLICITUD SET COSTO_EMPLEADO = ?, COSTO_AMPLIACION = ?, COSTO_DEPENDIENTES = ?, "
			+ "COSTO_TOTAL = ?, ID_TIPO_COB = ? WHERE ID_EMPLEADO = ? AND ID_SOLICITUD = ?";

	public static final String UPDATE_DEPENDIENTE = "UPDATE DEPENDIENTE SET COSTO_DEPENDIENTE = ? WHERE ID_DEPENDIENTE = ?";

	public EmpleadoTO getEmpleado(Integer numEmp){
		EmpleadoTO datos = new EmpleadoTO();
		datos = jdbcTemplate.queryForObject(QUERY_EMPLEADO, new RowMapper<EmpleadoTO>(){
			public EmpleadoTO mapRow(ResultSet rs, int rowNum) throws SQLException{
				EmpleadoTO empleadoTO = new EmpleadoTO();
				empleadoTO.setNumEmp(rs.getInt(1));
				empleadoTO.setDescEmpleado(rs.getString(2));
				empleadoTO.setApellidoP(rs.getString(3));
				empleadoTO.setApellidoM(rs.getString(4));
				empleadoTO.setNombre(rs.getString(5));
				empleadoTO.setFechaNacF(rs.getString(6));
				empleadoTO.setIdSexo(rs.getInt(7));
				empleadoTO.setSexo(rs.getString(8));
				empleadoTO.setTipoEmp(rs.getInt(9));
				empleadoTO.setDescTipoEmp(rs.getString(10));
				return empleadoTO;
			}
		}, numEmp);
		return datos;
	}

	public Integer getSolicitudExiste(Integer numEmp, Integer idTipoSol, Integer idEstSol){
		Integer existeSol = 0;
		try {
			existeSol = jdbcTemplate.queryForObject(QUERY_SOLICITUD_EXISTE, Integer.class, numEmp, idTipoSol, idEstSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			existeSol = 0;
		}
		return existeSol;
	}

	public SolicitudTO getSolicitud(Integer numEmp) {
		SolicitudTO solicitudTO = new SolicitudTO();
		try {
			solicitudTO = jdbcTemplate.queryForObject(QUERY_SOLICITUD, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitud = new SolicitudTO();
					solicitud.setIdSol(rs.getInt(1));
					solicitud.setIdTipoSolicitud(rs.getInt(2));
					solicitud.setTipoSolicitud(rs.getString(3));
					solicitud.setEstatusSol(rs.getString(4));
					solicitud.setFechaAutorizacion(rs.getString(5));
					solicitud.setCostoEmpleado(rs.getDouble(6));
					solicitud.setCostoAmpliacion(rs.getDouble(7));
					solicitud.setCostoDependiente(rs.getDouble(8));
					solicitud.setCostoTotal(rs.getDouble(9));
					solicitud.setIdTipoCobertura(rs.getInt(10));
					solicitud.setTipoCobertura(rs.getString(11));
					return solicitud;
				}
			},numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			solicitudTO.setIdSol(0);
		}
		return solicitudTO;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer numEmp) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		lista = jdbcTemplate.query(QUERY_DEPENDIENTE, new RowMapper(){
			public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				DependienteTO dependiente = new DependienteTO();
				dependiente.setIdDependiente(rs.getInt(1));
				dependiente.setApellidoP(rs.getString(2));
				dependiente.setApellidoM(rs.getString(3));
				dependiente.setNombre(rs.getString(4));
				dependiente.setIdParentesco(rs.getInt(5));
				dependiente.setParentesco(rs.getString(6));
				dependiente.setCostoDep(rs.getDouble(7));
				dependiente.setIdSexo(rs.getInt(8));
				dependiente.setSexo(rs.getString(9));
				dependiente.setFechaNac(rs.getString(10));
				dependiente.setFechaAltaAut(rs.getString(11));
				dependiente.setTipoSol(rs.getString(12));
				dependiente.setEstatusDep(rs.getString(13));
				return dependiente;
		}}, numEmp);
		return lista;
	}

	public Integer getTarifaConf(String clave, String sexo){
		Integer costo = 0;
		try {
			costo = jdbcTemplate.queryForObject(QUERY_TARIFA_SIND, Integer.class, clave, sexo);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existe costo");
		}
		logger.info("  costo: "+costo);
		return costo;
	}

	public Integer getTarifaSind(Integer idTarifa){
		Integer costo = 0;
		try {
			costo = jdbcTemplate.queryForObject(QUERY_TARIFA_CONF, Integer.class, idTarifa);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existe costo");
		}
		logger.info("  costo: "+costo);
		return costo;
	}

	public Integer getDependienteProceso(Integer numEmp, Integer estatusSol){
		Integer total = 0;
		try {
			total = jdbcTemplate.queryForObject(QUERY_DEPENDIENTE_EN_PROCESO, Integer.class, numEmp, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existe dependientes en proceso");
		}
		return total;
	}

	public Integer setCategoria(Integer numEmp, Integer idSol, Double costoAnt, Double costoNvo, Integer categoriaAnt,
			Integer categoriaNvo, Date fechaCambio, Integer idAdmin){
		Integer resultado = jdbcTemplate.update(INSERT_CATEGORIA,numEmp,idSol,costoAnt,costoNvo,categoriaAnt,categoriaNvo,fechaCambio,idAdmin);
		return resultado;
	}

	public Integer setEmpleado(Integer idTipoEmpl, Integer numEmp){
		Integer resultado = jdbcTemplate.update(UPDATE_EMPLEADO, idTipoEmpl,numEmp);
		return resultado;
	}

	public Integer setSolicitud(Double costoEmp, Double costoAmp, Double costoDep, Double costoTot,
			Integer idTipoCobertura, Integer numEmp, Integer idSol){
		Integer resultado = jdbcTemplate.update(UPDATE_SOLICITUD, costoEmp, costoAmp, costoDep, costoTot, idTipoCobertura, numEmp, idSol);
		return resultado;
	}

	public Integer setDependiente(Double costoDep, Integer idSol){
		Integer resultado = jdbcTemplate.update(UPDATE_DEPENDIENTE, costoDep, idSol);
		return resultado;
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
