package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CambioCoberturaDao;
import mx.com.orbita.to.CoberturaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

@Component
public class CambioCoberturaDaoImpl implements CambioCoberturaDao {

	private static final Logger logger = Logger.getLogger(CambioCoberturaDaoImpl.class);
	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_EMPLEADO = "SELECT E.ID_EMPLEADO, 'EMPLEADO', E.A_PATERNO, E.A_MATERNO, E.NOMBRE, "
			+ "TO_CHAR(E.FECHA_NACIMIENTO,'dd-MM-yyyy'), E.ID_SEXO, E.ID_TIPO_EMPLEADO, CE.DESCRIPCION "
			+ "FROM EMPLEADO E INNER JOIN CAT_TIPO_EMPLEADO CE on E.ID_TIPO_EMPLEADO = CE.ID_TIPO_EMPLEADO "
			+ "WHERE E.ID_EMPLEADO = ?";

	public static final String QUERY_SOLICITUD = "SELECT S.ID_SOLICITUD,S.ID_TIPO_SOL,TS.DESCRIPCION,S.ID_ESTATUS,ES.DESCRIPCION,"+
			"S.ID_TIPO_COB, C.DESCRIPCION, S.COSTO_EMPLEADO, S.COSTO_DEPENDIENTES, S.COSTO_AMPLIACION, S.COSTO_TOTAL,"+
			"TO_CHAR(S.FECHA_AUTORIZACION,'DD') AS DIA, "+
			"TO_CHAR(S.FECHA_AUTORIZACION,'MM') AS MES, "+
			"TO_CHAR(S.FECHA_AUTORIZACION,'YYYY') AS ANIO, "+
			"TO_CHAR(S.FECHA_AUTORIZACION,'DD/MM/YYYY')  AS FECHA_AUTORIZACION, "+
			"TO_CHAR(S.FECHA_RENOVACION,'DD') AS DIA_REN, "+
			"TO_CHAR(S.FECHA_RENOVACION,'MM') AS MES_REN, "+
			"TO_CHAR(S.FECHA_RENOVACION,'YYYY') AS ANIO_REN, "+
			"TO_CHAR(S.FECHA_RENOVACION,'DD/MM/YYYY') AS FECHA_RENOVACION "+
			"FROM SOLICITUD S "+
			"INNER JOIN CAT_TIPO_SOL TS ON S.ID_TIPO_SOL = TS.ID_TIPO_SOL "+
			"INNER JOIN CAT_ESTATUS_SOL ES ON S.ID_ESTATUS = ES.ID_ESTATUS "+
			"INNER JOIN CAT_TIPO_COBERTURA C ON S.ID_TIPO_COB = C.ID_TIPO_COB "+
			"WHERE S.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?";

	public static final String QUERY_DEPENDIENTE_EN_PROCESO = "SELECT COUNT(*) FROM EMPLEADO E "
			+ "INNER JOIN SOLICITUD S on E.ID_EMPLEADO = S.ID_EMPLEADO "
			+ "INNER JOIN DEPENDIENTE D on S.ID_SOLICITUD = D.ID_SOLICITUD "
			+ "WHERE E.ID_EMPLEADO = ? AND D.ID_ESTATUS = ?";

	public static final String QUERY_DEPENDIENTE = "SELECT D.ID_DEPENDIENTE, D.A_PATERNO, D.A_MATERNO, " +
			"D.NOMBRE,D.ID_PARENTESCO, CP.PARENTESCO, D.COSTO_DEPENDIENTE, D.ID_SEXO, CS.SEXO, " +
			"TO_CHAR(D.FECHA_NACIMIENTO,'yyyy-MM-dd') FECHA_NAC, TO_CHAR(D.FECHA_ALTA_AUT,'dd/MM/yyyy') FECHA_ALTA, " +
			"CT.DESCRIPCION TIPO_SOL, CES.DESCRIPCION EST_SOL " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S on E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D on S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_PARENTESCO CP on D.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_SEXO CS on D.ID_SEXO = CS.ID_SEXO " +
			"INNER JOIN CAT_TIPO_SOL CT on D.ID_TIPO_SOL = CT.ID_TIPO_SOL " +
			"INNER JOIN CAT_ESTATUS_SOL CES on D.ID_ESTATUS = CES.ID_ESTATUS " +
			"WHERE S.ID_EMPLEADO = ? AND D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?";

	public final static String INSERT_COBERTURA_HIS = "INSERT INTO CAMBIO_COBERTURA(ID_EMPLEADO, " +
			"ID_SOLICITUD,COSTO_AMPLIACION, COSTO_TOTAL, ID_TIPO_COB, FECHA_CAMBIO_COB, ID_ADMIN_AUT) " +
			"VALUES(?,?,?,?,?,?,?)";

	public final static String UPDATE_COB_SOL = "UPDATE SOLICITUD SET " +
			"COSTO_AMPL_IND = ?, " +
			"COSTO_AMPLIACION = ?, " +
			"COSTO_TOTAL = ?, " +
			"ID_TIPO_COB = ?, " +
			"ID_ADMIN_AUT_ALTA = ? " +
			"WHERE ID_SOLICITUD = ?";

	public final static String UPDATE_COB_DEP = "UPDATE DEPENDIENTE SET " +
			"COSTO_AMPLIACION = ? " +
			"WHERE ID_SOLICITUD = ? " +
			"AND ID_TIPO_SOL = ? " +
			"AND ID_ESTATUS = ?";

	public EmpleadoTO getEmpleado(Integer numEmp){
		EmpleadoTO datos = new EmpleadoTO();
		datos = jdbcTemplate.queryForObject(QUERY_EMPLEADO, new RowMapper<EmpleadoTO>(){
			public EmpleadoTO mapRow(ResultSet rs, int rowNum) throws SQLException{
				EmpleadoTO empleadoTO = new EmpleadoTO();
				empleadoTO.setNumEmp(rs.getInt(1));
				empleadoTO.setDescEmpleado(rs.getString(2));
				empleadoTO.setApellidoP(rs.getString(3));
				empleadoTO.setApellidoM(rs.getString(4));
				empleadoTO.setNombre(rs.getString(5));
				empleadoTO.setFechaNacF(rs.getString(6));
				empleadoTO.setIdSexo(rs.getInt(7));
				empleadoTO.setTipoEmp(rs.getInt(8));
				empleadoTO.setDescTipoEmp(rs.getString(9));
				return empleadoTO;
			}
		}, numEmp);
		return datos;
	}

	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		SolicitudTO datos = new SolicitudTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_SOLICITUD, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitudTO = new SolicitudTO();
					solicitudTO.setIdSol(rs.getInt(1));
					solicitudTO.setIdTipoSolicitud(rs.getInt(2));
					solicitudTO.setTipoSolicitud(rs.getString(3));
					solicitudTO.setIdEstatusSol(rs.getInt(4));
					solicitudTO.setEstatusSol(rs.getString(5));
					solicitudTO.setIdTipoCobertura(rs.getInt(6));
					solicitudTO.setTipoCobertura(rs.getString(7));
					solicitudTO.setCostoEmpleado(rs.getDouble(8));
					solicitudTO.setCostoDependiente(rs.getDouble(9));
					solicitudTO.setCostoAmpliacion(rs.getDouble(10));
					solicitudTO.setCostoTotal(rs.getDouble(11));
					solicitudTO.setDiaAut(rs.getInt(12));
					solicitudTO.setMesAut(rs.getInt(13));
					solicitudTO.setAnioAut(rs.getInt(14));
					solicitudTO.setFechaAutorizacion(rs.getString(15));
					solicitudTO.setDiaRenov(rs.getInt(16));
					solicitudTO.setMesRenov(rs.getInt(17));
					solicitudTO.setAnioRenov(rs.getInt(18));
					solicitudTO.setFechaRenovacion(rs.getString(19));
					return solicitudTO;
				}
			}, numEmp, tipoSol,estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			datos.setIdSol(0);
		}
		return datos;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		lista = jdbcTemplate.query(QUERY_DEPENDIENTE, new RowMapper(){
			public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				DependienteTO dependiente = new DependienteTO();
				dependiente.setIdDependiente(rs.getInt(1));
				dependiente.setApellidoP(rs.getString(2));
				dependiente.setApellidoM(rs.getString(3));
				dependiente.setNombre(rs.getString(4));
				dependiente.setIdParentesco(rs.getInt(5));
				dependiente.setParentesco(rs.getString(6));
				dependiente.setCostoDep(rs.getDouble(7));
				dependiente.setIdSexo(rs.getInt(8));
				dependiente.setSexo(rs.getString(9));
				dependiente.setFechaNac(rs.getString(10));
				dependiente.setFechaAltaAut(rs.getString(11));
				dependiente.setTipoSol(rs.getString(12));
				dependiente.setEstatusDep(rs.getString(13));
				return dependiente;
		}}, numEmp, tipoSol, estatusSol);
		return lista;
	}

	public Integer getDependienteProceso(Integer numEmp, Integer estatusSol){
		Integer num_dep = 0;
		try {
			num_dep = jdbcTemplate.queryForObject(QUERY_DEPENDIENTE_EN_PROCESO, Integer.class, numEmp, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existe dependientes en proceso");
		}
		logger.info("  numDepProceso: ["+num_dep+"]");
		return num_dep;
	}

	public Integer setCoberturaHistorico(CoberturaTO coberturaTO){
		Integer resultado = jdbcTemplate.update(INSERT_COBERTURA_HIS,
								coberturaTO.getNumEmpSolicitado(),
								coberturaTO.getIdSol(),
								coberturaTO.getCostoAmpliacionAnt(),
								coberturaTO.getCostoTotalAnt(),
								coberturaTO.getIdTipoAmplAnt(),
								coberturaTO.getFechaAct(),
								coberturaTO.getNumEmp());
		return resultado;
	}

	public Integer setCambioCobertura(CoberturaTO coberturaTO){
		Integer resultado = jdbcTemplate.update(UPDATE_COB_SOL,
						coberturaTO.getCostoAmpliacionInd(),
						coberturaTO.getCostoAmpliacion(),
						coberturaTO.getCostoTotal(),
						coberturaTO.getIdTipoAmpl(),
						coberturaTO.getNumEmp(),
						coberturaTO.getIdSol());
		return resultado;
	}

	public Integer setCambioCoberturaDep(CoberturaTO coberturaTO){
		Integer resultado = jdbcTemplate.update(UPDATE_COB_DEP,
				coberturaTO.getCostoAmpliacionInd(),
				coberturaTO.getIdSol(),
				coberturaTO.getIdTipoSolicitud(),
				coberturaTO.getIdEstatusSol());
		return resultado;
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
