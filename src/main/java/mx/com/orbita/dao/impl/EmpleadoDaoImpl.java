package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.EmpleadoDao;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoAdrisaTO;
import mx.com.orbita.util.Fechas;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class EmpleadoDaoImpl implements EmpleadoDao {
	
	private static final Logger logger = Logger.getLogger(EmpleadoDaoImpl.class);
	private JdbcTemplate jdbcTemplateEmp;

	public static final String QUERY_EMPLEADO = "SELECT C_NUMEMP,C_NOMBRE,C_AP_PATERNO,C_AP_MATERNO, " +
			"C_PUESTO,C_REGION,C_SEXO,C_TIPOEMP,C_RFC,C_DEPARTAMENTO,C_UBICACION,D_FECHA_NAC,D_FECHA_ING " +
			"FROM EMPLEADOS WHERE C_NUMEMP = ?";
	
	public static final String QUERY_EMPLEADO_REEMBOLSO = "SELECT E.C_NUMEMP, E.C_NOMBRE, E.C_AP_PATERNO, " +
			"E.C_AP_MATERNO, E.C_REGION, E.C_DEPARTAMENTO, E.C_GERENCIA, E.C_CENTRO_COSTO, E.C_NOM_GERENTE " +
			"FROM EMPLEADOS E WHERE E.C_NUMEMP = ?";
		
	public static final String QUERY_CORREO = "SELECT E_MAIL FROM EMPLEADOS WHERE C_NUMEMP = ?";
	
	public EmpleadoTO datosEmpleado(Integer numEmp){
		logger.info("EmpleadoDaoImpl.datosEmpleado");
		logger.info("  Parametros del DAO [numEmp: "+numEmp+"]");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		EmpleadoTO empleado = new EmpleadoTO();
		Fechas fecha = new Fechas();
		Date date = new Date();
		String fechaNac = "";

		try{
			if(numEmp==1200){
				fechaNac = "1984-07-01";
				date = format.parse(fechaNac);
				empleado.setNombre("IGNACIO DE JESUS");
				empleado.setApellidoP("MATEO");
				empleado.setApellidoM("ALVAREZ");
				empleado.setPuesto("ADMINISTRADOR");
				empleado.setNumEmp(1200);
				empleado.setIdRegion(9);
				empleado.setRegion("R09");
				empleado.setIdSexo(1);
				empleado.setSexo("M");
				empleado.setTipoEmp(1);
				empleado.setRfc("415244125");
				empleado.setCentro("CORPORATIVO");
				empleado.setDescEmpleado("C");
			}else{
				empleado.setNumEmp(0);
			}

			// Fecha Ingreso
			empleado.setFechaIng(new Date());
			empleado.setFechaIngF(fecha.formatoFechas(new Date(),"EEE, MMM dd yyyy").toUpperCase());

			// Fecha Nacimiento
			empleado.setFechaNac(date);
			empleado.setFechaNacF(fecha.formatoFechas(date,"EEE, MMM dd yyyy").toUpperCase());

			// Fecha Actual
			empleado.setFechaActualF(fecha.formatoFechas(new Date(),"EEE, MMM dd yyyy").toUpperCase());
			empleado.setFechaActual(new Date());

		}catch(Exception e){

		}

		logger.info("  Datos del Empleado");
		logger.info("  fecha de inngreso: "+empleado.getFechaIng());
		logger.info("  fecha de ingresoF: "+empleado.getFechaIngF());

		logger.info("  fecha de nacimiento: "+empleado.getFechaNac());
		logger.info("  fecha de nacimientoF: "+empleado.getFechaNacF());

		logger.info("  fecha actual: "+empleado.getFechaActual());
		logger.info("  fecha actualF: "+empleado.getFechaActualF());

		return empleado;
	}
		
	public ReembolsoAdrisaTO datosEmpleadoReembolso(Integer numEmp){
		ReembolsoAdrisaTO datos = new ReembolsoAdrisaTO();
		try {
			datos = jdbcTemplateEmp.queryForObject(QUERY_EMPLEADO_REEMBOLSO, new RowMapper<ReembolsoAdrisaTO>(){
				public ReembolsoAdrisaTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					ReembolsoAdrisaTO empleadoTO = new ReembolsoAdrisaTO();
					empleadoTO.setNumEmp(rs.getInt(1));
					empleadoTO.setNombre(rs.getString(2));
					empleadoTO.setApellidoP(rs.getString(3));
					empleadoTO.setApellidoM(rs.getString(4));
					empleadoTO.setRegion(rs.getString(5));
					empleadoTO.setDepartamento(rs.getString(6));
					empleadoTO.setGerencia(rs.getString(7));
					empleadoTO.setCentroCostos(rs.getString(8));
					empleadoTO.setNombreGerente(rs.getString(9));
					return empleadoTO;
				}
			}, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			datos.setNumEmp(0);
		}
		return datos;
	}
	
	public String getCorreo(Integer numEmp){
		String correo = "";
		try {
			correo = jdbcTemplateEmp.queryForObject(QUERY_CORREO, String.class, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  EmptyResultDataAccessException: "+e.getMessage());
		}
		return correo;
	}	

	@Autowired
	public void setJdbcTemplateEmp(JdbcTemplate jdbcTemplateEmp) {
		this.jdbcTemplateEmp = jdbcTemplateEmp;
	}
	
}