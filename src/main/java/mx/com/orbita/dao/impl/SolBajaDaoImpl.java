package mx.com.orbita.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SolBajaDao;

@Component
public class SolBajaDaoImpl implements SolBajaDao{
	
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_DEPENDIENTE = "SELECT D.ID_DEPENDIENTE FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"WHERE D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS = ? " +
			"AND E.ID_EMPLEADO = ?";
	
	public static final String QUERY_CONSULMED = "SELECT C.ID_CONSULMED FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"WHERE C.ID_TIPO_SOL = ? " +
			"AND C.ID_ESTATUS = ? " +
			"AND E.ID_EMPLEADO = ?";
	
	public static final String UPDATE_BAJA_SOLICITUD = "UPDATE SOLICITUD SET ID_ESTATUS = ?, " +
			"FECHA_CANCELACION = ? WHERE ID_EMPLEADO = ? AND ID_SOLICITUD = ?";
	
	public static final String UPDATE_BAJA_DEPENDIENTE = "UPDATE DEPENDIENTE SET ID_ESTATUS = ?, " +
			"FECHA_BAJA = ? WHERE ID_DEPENDIENTE = ?";
	
	public static final String UPDATE_BAJA_CONSULMED = "UPDATE CONSULMED SET ID_ESTATUS = ?, " +
			"FECHA_BAJA = ? WHERE ID_CONSULMED = ?";
	
	public List<Integer> getIdDependiente(Integer tipoSol, Integer estatusSol, Integer numEmp){
		List<Integer> idDependiente = new ArrayList<Integer>();	
		idDependiente = jdbcTemplate.queryForList(QUERY_DEPENDIENTE, Integer.class, tipoSol, estatusSol, numEmp);
		return idDependiente;
	}
	
	public List<Integer> getIdConsulmed(Integer tipoSol, Integer estatusSol, Integer numEmp){
		List<Integer> idDependiente = new ArrayList<Integer>();	
		idDependiente = jdbcTemplate.queryForList(QUERY_CONSULMED, Integer.class, tipoSol, estatusSol, numEmp);
		return idDependiente;
	}
	
	
	public void setSolicitud(Integer numEmp, Integer estatusSol, Integer sol) {
		jdbcTemplate.update(UPDATE_BAJA_SOLICITUD, estatusSol, new Date(), numEmp, sol);
	}

	public void setDependiente(Integer idDependiente, Integer estatusSol) {
		jdbcTemplate.update(UPDATE_BAJA_DEPENDIENTE, estatusSol, new Date(), idDependiente);
	}

	public void setConsulmed(Integer idDependiente, Integer estatusSol) {
		jdbcTemplate.update(UPDATE_BAJA_CONSULMED, estatusSol, new Date(), idDependiente);
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}