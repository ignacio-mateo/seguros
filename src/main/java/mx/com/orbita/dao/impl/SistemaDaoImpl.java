package mx.com.orbita.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SistemaDao;

@Component
public class SistemaDaoImpl implements SistemaDao{
	
	private static final Logger logger = Logger.getLogger(SistemaDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_PERFIL = "select id_perfil from ADMINISTRADOR where id_empleado = ?";
	
	public static final String QUERY_EMP = "select id_empleado, nombre, a_paterno, a_paterno " +
			"from empleado where id_empleado = ?";
	
	public static final String QUERY_SOLICITUD_AUT = "select s.id_solicitud " +
			"from solicitud s where s.id_empleado = ? " +
			"and s.id_tipo_sol = ? and s.id_estatus = ?";
	
	public static final String QUERY_SOLICITUD = "select s.id_solicitud " +
			"from solicitud s where s.id_empleado = ? " +
			"and s.id_tipo_sol = ? and s.id_estatus <> ?";
	
	public static final String QUERY_SOLICITUD_ALTA = "SELECT s.id_solicitud FROM solicitud s "+ 
			"WHERE s.id_empleado = ? AND S.ID_TIPO_SOL = ? AND (S.ID_ESTATUS = ? OR S.ID_ESTATUS = ?)";
	
	public static final String QUERY_SOLICITUD_BAJA = "SELECT s.id_solicitud FROM solicitud s "+ 
			"WHERE s.id_empleado = ? AND ((S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?) "+ 
			"or (S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?) or (S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?))";
	
	public static final String QUERY_SOLICITUD_BAJA_DEP = "SELECT S.ID_SOLICITUD FROM SOLICITUD S " +
			"WHERE S.ID_EMPLEADO = ? " +
			"AND (  	(S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?) OR " +
			"			(S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?) OR " +
			"			(S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?) OR " +
			"			(S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?))  ";
	
	
	public Integer getPerfilAdmin(Integer numEmp) {
		Integer idPerfil = 0;
		try {
			idPerfil = jdbcTemplate.queryForObject(QUERY_PERFIL, Integer.class, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- No tiene perfil administrador --");
			idPerfil = 0;
		}
		return idPerfil;
	}
	
	public Integer existeSolicitud(Integer numEmp, Integer tipoSol,Integer estatusSol){
		Integer existe = 0;
		try {
			existe = jdbcTemplate.queryForObject(QUERY_SOLICITUD, Integer.class, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- NO EXISTE SOL --");
			existe = 0;
		}
		return existe;
	}
	
	public Integer existeSolicitudAlta(Integer numEmp, Integer tipoSolA, Integer estSolA, Integer estSolB){
		Integer existe = 0;
		try {
			existe = jdbcTemplate.queryForObject(QUERY_SOLICITUD_ALTA, Integer.class, numEmp, tipoSolA, estSolA, estSolB);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- NO EXISTE SOL --");
			existe = 0;
		}
		return existe;
	}
	
	public Integer existeSolicitudAut(Integer numEmp, Integer tipoSol, Integer estSol){
		Integer existe = 0;
		try {
			existe = jdbcTemplate.queryForObject(QUERY_SOLICITUD_AUT, Integer.class, numEmp, tipoSol, estSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- NO EXISTE SOL --");
			existe = 0;
		}
		return existe;
	}
	
	public Integer existeSolicitudBaja(Integer numEmp, Integer tipoSolA, Integer estSolA, Integer tipoSolB, Integer estSolB, Integer tipoSolC, Integer estSolC){
		Integer existe = 0;
		try {
			existe = jdbcTemplate.queryForObject(QUERY_SOLICITUD_BAJA, Integer.class, numEmp, tipoSolA, estSolA, tipoSolB, estSolB, tipoSolC, estSolC);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- NO EXISTE SOL --");
			existe = 0;
		}
		return existe;
	}
	
	public Integer existeSolicitudBajaDep(Integer numEmp, Integer tipoSolA, Integer estSolA, Integer estSolB, 
															Integer tipoSolB, Integer estSolC, Integer estSolD){
		Integer existe = 0;
		try {
			existe = jdbcTemplate.queryForObject(QUERY_SOLICITUD_BAJA_DEP, Integer.class, numEmp, 
															tipoSolA, estSolA, 
															tipoSolA, estSolB, 
															tipoSolB, estSolC, 
															tipoSolB, estSolD);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- NO EXISTE SOL --");
			existe = 0;
		}
		return existe;
	}	
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}