package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AdminSolicitudDao;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.FechaTO;
import mx.com.orbita.to.SolicitudAdminTO;
import mx.com.orbita.to.SolicitudTO;

@Component
public class AdminSolicitudDaoImpl implements AdminSolicitudDao {
	
	private static final Logger logger = Logger.getLogger(AdminSolicitudDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_SOLICITUD_EXISTE = "select count(*) from solicitud where id_empleado = ?";
	
	public static final String QUERY_SOLICITUD = "SELECT S.ID_SOLICITUD, E.ID_EMPLEADO, E.A_PATERNO, " +
			"E.A_MATERNO, E.NOMBRE, to_char(E.FECHA_NACIMIENTO,'dd/MM/yyyy'), R.CLAVE, E.ID_TIPO_EMPLEADO, " +
			"TE.DESCRIPCION, S.COSTO_EMPLEADO, S.COSTO_DEPENDIENTES,COSTO_COB_MEMORIAL,S.COSTO_TOTAL, " +
			"TO_CHAR(S.FECHA_AUTORIZACION,'dd/MM/yyyy') AS FECHA_ALTA, " +
			"S.ID_TIPO_SOL, CTS.DESCRIPCION TIPO_SOL, S.ID_ESTATUS, ES.DESCRIPCION AS ESTATUS_SOL, " +
			"TO_CHAR(S.FECHA_CANCELACION_AUT,'dd/MM/yyyy') AS FECHA_BAJA, S.COSTO_AMPLIACION, S.ID_TIPO_COB, " +
			"TC.DESCRIPCION AS COBERTURA,M.CLAVE,S.COMENTARIO, "+ 
			"(SELECT DISTINCT D.ID_PARENTESCO FROM DEPENDIENTE D WHERE D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ? " +
			"AND D.ID_PARENTESCO IN (?,?) AND D.ID_SOLICITUD = S.ID_SOLICITUD) " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN CAT_REGION R ON E.ID_REGION = R.ID_REGION " +
			"INNER JOIN CAT_TIPO_EMPLEADO TE ON E.ID_TIPO_EMPLEADO = TE.ID_TIPO_EMPLEADO " +
			"INNER JOIN CAT_TIPO_COBERTURA TC ON S.ID_TIPO_COB = TC.ID_TIPO_COB " +
			"INNER JOIN CAT_ESTATUS_SOL ES ON S.ID_ESTATUS = ES.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_SOL CTS ON S.ID_TIPO_SOL = CTS.ID_TIPO_SOL " +
			"INNER JOIN CAT_TIPO_COBERTURA_MEMORIAL M ON M.ID_TIPO_COB_MEM = S.ID_TIPO_COB_MEM " +
			"WHERE E.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS <> ?";
		
	public static final String QUERY_SOLICITUD_LISTA = "SELECT S.ID_SOLICITUD, " +
			"E.ID_EMPLEADO, E.A_PATERNO, E.A_MATERNO, E.NOMBRE, " +
			"TO_CHAR(E.FECHA_NACIMIENTO,'dd/MM/yyyy')," +
			"TO_CHAR(S.FECHA_ELABORACION,'dd/MM/yyyy'), " +
			"TO_CHAR(S.FECHA_AUTORIZACION,'dd/MM/yyyy') AS FECHA_ALTA, " +
			"TO_CHAR(S.FECHA_CANCELACION_AUT,'dd/MM/yyyy') AS FECHA_BAJA, " +
			"TO_CHAR(S.FECHA_RENOVACION,'dd/MM/yyyy') AS FECHA_RENOV, " +
			"R.CLAVE, E.ID_TIPO_EMPLEADO, TE.DESCRIPCION, S.COSTO_EMPLEADO, " +
			"S.COSTO_DEPENDIENTES,COSTO_COB_MEMORIAL,S.COSTO_TOTAL, CTS.DESCRIPCION TIPO_SOL, " +
			"ES.DESCRIPCION AS ESTATUS_SOL, S.COSTO_AMPL_IND,S.COSTO_AMPLIACION, " +
			"S.COSTO_EMPLEADO+S.COSTO_DEPENDIENTES COSTO_COB_BAS, " +
			"S.COSTO_AMPLIACION-S.COSTO_AMPL_IND COSTO_AMP_DEP, S.ID_TIPO_COB, " +
			"TC.DESCRIPCION AS COBERTURA, M.CLAVE, S.COMENTARIO " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN CAT_REGION R ON E.ID_REGION = R.ID_REGION " +
			"INNER JOIN CAT_TIPO_EMPLEADO TE ON E.ID_TIPO_EMPLEADO = TE.ID_TIPO_EMPLEADO " +
			"INNER JOIN CAT_TIPO_COBERTURA TC ON S.ID_TIPO_COB = TC.ID_TIPO_COB " +
			"INNER JOIN CAT_ESTATUS_SOL ES ON S.ID_ESTATUS = ES.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_SOL CTS ON S.ID_TIPO_SOL = CTS.ID_TIPO_SOL " +
			"INNER JOIN CAT_TIPO_COBERTURA_MEMORIAL M ON M.ID_TIPO_COB_MEM = S.ID_TIPO_COB_MEM " +
			"WHERE E.ID_EMPLEADO = ? ORDER BY S.ID_SOLICITUD DESC";

	public static final String QUERY_SOLICITUD_TOTAL = "SELECT ID_SOLICITUD,COSTO_EMPLEADO,COSTO_AMPL_IND,ID_TIPO_COB, " +
			"TO_CHAR(FECHA_AUTORIZACION,'DD') AS DIA, TO_CHAR(FECHA_AUTORIZACION,'MM') AS MES, " +
			"TO_CHAR(FECHA_AUTORIZACION,'YYYY') AS ANIO, " +
			"TO_CHAR(FECHA_AUTORIZACION,'DD/MM/YYYY')  AS FECHA_AUTORIZACION, "+ 
			"TO_CHAR(FECHA_RENOVACION,'DD') AS DIA_REN, " +
			"TO_CHAR(FECHA_RENOVACION,'MM') AS MES_REN, " +
			"TO_CHAR(FECHA_RENOVACION,'YYYY') AS ANIO_REN, "+ 
			"TO_CHAR(FECHA_RENOVACION,'DD/MM/YYYY') AS FECHA_RENOVACION " +
			"FROM SOLICITUD WHERE ID_EMPLEADO = ? AND ID_TIPO_SOL = ? AND ID_ESTATUS = ?";
	
	public static final String QUERY_FECHA_ALTA_SOL = "SELECT TO_CHAR(FECHA_AUTORIZACION,'DD') AS DIA, " +
			"TO_CHAR(FECHA_AUTORIZACION,'MM') AS MES " +
			"FROM SOLICITUD WHERE ID_SOLICITUD = ?";
		
	public static final String QUERY_DEPENDIENTE = "SELECT S.ID_SOLICITUD,D.ID_DEPENDIENTE, " +
			"D.ID_PARENTESCO, CP.PARENTESCO, D.A_PATERNO, D.A_MATERNO, D.NOMBRE, CS.SEXO, " +
			"TO_CHAR(D.FECHA_NACIMIENTO,'dd/MM/yyyy'), " +
			"TO_CHAR(D.FECHA_ALTA,'dd/MM/yyyy'), " +
			"TO_CHAR(D.FECHA_ALTA_AUT,'dd/MM/yyyy')AS FECHA_ALTA, " +
			"TO_CHAR(D.FECHA_BAJA_AUT,'dd/MM/yyyy')AS FECHA_BAJA, " +
			"TO_CHAR(D.FECHA_RENOVACION,'dd/MM/yyyy')AS FECHA_RENOV, " +
			"D.COSTO_DEPENDIENTE,D.COSTO_AMPLIACION, " +
			"CASE D.COSTO_CANCELACION WHEN NULL THEN '-' ELSE D.COSTO_CANCELACION||'' END, " +
			"CTS.DESCRIPCION TIPO_SOL, D.ID_ESTATUS, ES.DESCRIPCION AS ESTATUS_SOL, D.COMENTARIO " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_PARENTESCO CP ON D.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_ESTATUS_SOL ES ON D.ID_ESTATUS = ES.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_SOL CTS ON D.ID_TIPO_SOL = CTS.ID_TIPO_SOL "+ 
			"INNER JOIN CAT_SEXO CS on D.ID_SEXO = CS.ID_SEXO " +
			"WHERE E.ID_EMPLEADO = ? ORDER BY S.ID_SOLICITUD DESC ";
	
	public static final String QUERY_DEP_COSTOS = "SELECT D.ID_DEPENDIENTE,D.COSTO_DEPENDIENTE,D.COSTO_AMPLIACION " +
			"FROM DEPENDIENTE D " +
			"WHERE D.ID_DEPENDIENTE = ? ";
	
	public static final String QUERY_DEPENDIENTE_AUTORIZADO = "SELECT E.ID_EMPLEADO, D.ID_DEPENDIENTE, " +
			"D.ID_PARENTESCO, D.ID_SEXO, CT.SEXO, to_char(D.FECHA_NACIMIENTO,'yyyy-mm-dd'), " +
			"TO_CHAR(FECHA_ALTA_AUT,'YYYY') AS ANIO, TO_CHAR(FECHA_ALTA_AUT,'MM') AS MES, " +
			"TO_CHAR(FECHA_ALTA_AUT,'DD') AS DIA, COSTO_DEPENDIENTE " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_SEXO CT ON D.ID_SEXO = CT.ID_SEXO " +
			"WHERE E.ID_EMPLEADO = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS = ?";
	
	public static final String QUERY_DEPENDIENTE_EDITADO = "SELECT E.ID_EMPLEADO, D.ID_DEPENDIENTE, " +
			"D.ID_PARENTESCO, D.ID_SEXO, CT.SEXO, to_char(D.FECHA_NACIMIENTO,'yyyy-mm-dd'), " +
			"TO_CHAR(D.FECHA_ALTA_AUT,'YYYY') AS ANIO, " +
			"TO_CHAR(D.FECHA_ALTA_AUT,'MM') AS MES, TO_CHAR(D.FECHA_ALTA_AUT,'DD') AS DIA, " +
			"TO_CHAR(D.FECHA_RENOVACION,'YYYY') AS ANIO_RENOV, " +
			"TO_CHAR(D.FECHA_RENOVACION,'MM') AS MES_RENOV, " +
			"TO_CHAR(D.FECHA_RENOVACION,'DD') AS DIA_RENOV, COSTO_DEPENDIENTE " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_SEXO CT ON D.ID_SEXO = CT.ID_SEXO " +
			"WHERE E.ID_EMPLEADO = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS = ? " +
			"AND D.ID_DEPENDIENTE <> ?";
	
	public static final String QUERY_DEPENDIENTE_CON_AUTORIZADO = "SELECT D.ID_CONSULMED " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN CONSULMED D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_SEXO CT ON D.ID_SEXO = CT.ID_SEXO " +
			"WHERE E.ID_EMPLEADO = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS = ?";

	public static final String QUERY_COSTO_DEP = "SELECT NVL(SUM(COSTO_DEPENDIENTE),0) " +
			"FROM DEPENDIENTE WHERE ID_SOLICITUD = ? " +
			"AND ID_TIPO_SOL = ? AND ID_ESTATUS = ? AND ID_DEPENDIENTE <> ?";
	
	public static final String QUERY_TOTAL_DEP = "SELECT COUNT(*) " +
			"FROM DEPENDIENTE WHERE ID_SOLICITUD = ? AND ID_TIPO_SOL = ? " +
			"AND ID_ESTATUS = ? AND ID_DEPENDIENTE <> ?";
	
	public static final String QUERY_FECHA_ALTA_DEP = "SELECT TO_CHAR(FECHA_ALTA_AUT,'DD') AS DIA, " +
			"TO_CHAR(FECHA_ALTA_AUT,'MM') AS MES " +
			"FROM DEPENDIENTE WHERE ID_SOLICITUD = ? " +
			"AND ID_TIPO_SOL = ? AND ID_ESTATUS = ? AND ID_DEPENDIENTE <> ?";
		
	public static final String QUERY_CONSULMED = "SELECT S.ID_SOLICITUD, C.ID_CONSULMED, " +
			"C.ID_PARENTESCO, CP.PARENTESCO, C.A_PATERNO, C.A_MATERNO, C.NOMBRE, " +
			"TO_CHAR(C.FECHA_NACIMIENTO,'dd/MM/yyyy'), " +
			"TO_CHAR(C.FECHA_ALTA,'dd/MM/yyyy'), " +
			"TO_CHAR(C.FECHA_ALTA_AUT,'dd/MM/yyyy') AS FECHA_ALTA, " +
			"TO_CHAR(C.FECHA_BAJA_AUT,'dd/MM/yyyy') AS FECHA_BAJA, "+
			"TO_CHAR(C.FECHA_RENOVACION,'dd/MM/yyyy') AS FECHA_RENOV, " +
			"C.COSTO_CONSULMED, CTS.DESCRIPCION TIPO_SOL,C.ID_ESTATUS,ES.DESCRIPCION AS ESTATUS_SOL," +
			"C.COMENTARIO " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"INNER JOIN CAT_PARENTESCO CP ON C.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_ESTATUS_SOL ES ON C.ID_ESTATUS = ES.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_SOL CTS ON C.ID_TIPO_SOL = CTS.ID_TIPO_SOL " +
			"WHERE E.ID_EMPLEADO = ? ORDER BY S.ID_SOLICITUD DESC";
	
	public static final String QUERY_MEMORIAL = "SELECT S.ID_SOLICITUD, M.ID_MEMORIAL, M.ID_PARENTESCO," +
			"CP.PARENTESCO, M.A_PATERNO, M.A_MATERNO, M.NOMBRE," +
			"TO_CHAR(M.FECHA_NACIMIENTO,'dd/MM/yyyy'), " +
			"TO_CHAR(M.FECHA_ALTA,'dd/MM/yyyy')," +
			"TO_CHAR(M.FECHA_ALTA_AUT,'dd/MM/yyyy') AS FECHA_ALTA," +
			"TO_CHAR(M.FECHA_BAJA_AUT,'dd/MM/yyyy') AS FECHA_BAJA," +
			"TO_CHAR(M.FECHA_RENOVACION,'dd/MM/yyyy') AS FECHA_RENOV," +
			"M.COSTO_MEMORIAL, CTS.DESCRIPCION TIPO_SOL,M.ID_ESTATUS,ES.DESCRIPCION AS ESTATUS_SOL,M.COMENTARIO " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO " +
			"INNER JOIN MEMORIAL M ON S.ID_SOLICITUD = M.ID_SOLICITUD " +
			"INNER JOIN CAT_PARENTESCO CP ON M.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_ESTATUS_SOL ES ON M.ID_ESTATUS = ES.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_SOL CTS ON M.ID_TIPO_SOL = CTS.ID_TIPO_SOL " +
			"WHERE E.ID_EMPLEADO = ? ORDER BY S.ID_SOLICITUD DESC";
				
	public static final String UPDATE_SOLICITUD = "UPDATE SOLICITUD SET FECHA_AUTORIZACION = ? " +
			"WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_SOLICITUD_COSTO_TOTAL = "UPDATE SOLICITUD SET COSTO_EMPLEADO = ?, "
			+ "COSTO_AMPL_IND = ?, "
			+ "COSTO_DEPENDIENTES = ?, "
			+ "COSTO_AMPLIACION = ?, "
			+ "COSTO_TOTAL = ? "
			+ "WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_SOLICITUD_COSTO_TOTAL_X_DEP = "UPDATE SOLICITUD SET COSTO_DEPENDIENTES = ?, "
			+ "COSTO_AMPLIACION = ?, "
			+ "COSTO_TOTAL = ? "
			+ "WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_DEPENDIENTE = "UPDATE DEPENDIENTE SET ID_PARENTESCO = ?, " +
			"A_PATERNO = ?, A_MATERNO = ?, NOMBRE = ?, FECHA_NACIMIENTO = ?, " +
			"ID_TIPO_SOL = ?, ID_ESTATUS = ?, COSTO_CANCELACION = ?, FECHA_BAJA_AUT = ? " +
			"WHERE ID_DEPENDIENTE = ?";
	
	public static final String UPDATE_DEPENDIENTE_COSTO = "UPDATE DEPENDIENTE SET COSTO_DEPENDIENTE = ?, " +
			"COSTO_AMPLIACION = ? WHERE ID_DEPENDIENTE = ?";
	
	public static final String UPDATE_CONSULMED = "UPDATE CONSULMED SET ID_PARENTESCO = ?, A_PATERNO = ?, " +
			"A_MATERNO = ?, NOMBRE = ?, FECHA_NACIMIENTO = ?, FECHA_ALTA_AUT = ?, ID_ESTATUS = ? " +
			"WHERE ID_CONSULMED = ?";

	public static final String UPDATE_CONSULMED_ALTA = "UPDATE CONSULMED SET FECHA_ALTA_AUT = ? " +
			"WHERE ID_CONSULMED = ?";

	
	public Integer getSolicitudExiste(Integer numEmp){
		Integer existeSol = 0;
		try {
			existeSol = jdbcTemplate.queryForObject(QUERY_SOLICITUD_EXISTE, Integer.class, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			existeSol = 0;
		}
		return existeSol;
	}	
	
	public SolicitudAdminTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol, 
								Integer idParentescoA, Integer idParentescoB, Integer estatusRechazado){
		SolicitudAdminTO solicitudTO = new SolicitudAdminTO();
		try {
			solicitudTO = jdbcTemplate.queryForObject(QUERY_SOLICITUD, new RowMapper<SolicitudAdminTO>(){
				public SolicitudAdminTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudAdminTO solicitud = new SolicitudAdminTO();
					solicitud.setIdSolicitud(rs.getInt(1));
					solicitud.setNumEmpSolicitado(rs.getInt(2));
					solicitud.setApellidoP(rs.getString(3));
					solicitud.setApellidoM(rs.getString(4));
					solicitud.setNombre(rs.getString(5));
					solicitud.setFechaNac(rs.getString(6));
					solicitud.setRegion(rs.getString(7));
					solicitud.setIdTipoEmpleado(rs.getInt(8));
					solicitud.setTipoEmpleado(rs.getString(9));
					solicitud.setCosto(rs.getDouble(10));
					solicitud.setCostoTotalDep(rs.getDouble(11));
					solicitud.setCostoMemorial(rs.getDouble(12));
					solicitud.setCostoTotal(rs.getDouble(13));
					solicitud.setFechaAlta(rs.getString(14));
					solicitud.setIdTipoSol(rs.getInt(15));
					solicitud.setTipoSol(rs.getString(16));
					solicitud.setIdEstatulSol(rs.getInt(17));
					solicitud.setEstatulSol(rs.getString(18));
					solicitud.setFechaBaja(rs.getString(19));
					solicitud.setCostoAmpliacion(rs.getDouble(20));
					solicitud.setIdCobertura(rs.getInt(21));
					solicitud.setCobertura(rs.getString(22));
					solicitud.setTipoMemorial(rs.getString(23));
					solicitud.setComentario(rs.getString(24));
					solicitud.setIdConyuge(rs.getInt(25));
					return solicitud;
				}
			},tipoSol, estatusSol, idParentescoA, idParentescoB, numEmp,tipoSol, estatusRechazado);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			solicitudTO.setNumEmpSolicitado(0);
		}
		return solicitudTO;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SolicitudAdminTO> getSolicitudes(Integer numEmp){
		List<SolicitudAdminTO> lista = new ArrayList<SolicitudAdminTO>();
		lista = jdbcTemplate.query(QUERY_SOLICITUD_LISTA, new RowMapper(){
			public SolicitudAdminTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				SolicitudAdminTO solicitud = new SolicitudAdminTO();
				solicitud.setIdSolicitud(rs.getInt(1));
				solicitud.setNumEmpSolicitado(rs.getInt(2));
				solicitud.setApellidoP(rs.getString(3));
				solicitud.setApellidoM(rs.getString(4));
				solicitud.setNombre(rs.getString(5));
				solicitud.setFechaNac(rs.getString(6));
				solicitud.setFechaElab(rs.getString(7));
				solicitud.setFechaAlta(rs.getString(8));
				solicitud.setFechaBaja(rs.getString(9));
				solicitud.setFechaRenov(rs.getString(10));
				solicitud.setRegion(rs.getString(11));
				solicitud.setIdTipoEmpleado(rs.getInt(12));
				solicitud.setTipoEmpleado(rs.getString(13));
				solicitud.setCosto(rs.getDouble(14));
				solicitud.setCostoTotalDep(rs.getDouble(15));
				solicitud.setCostoMemorial(rs.getDouble(16));
				solicitud.setCostoTotal(rs.getDouble(17));
				solicitud.setTipoSol(rs.getString(18));
				solicitud.setEstatulSol(rs.getString(19));
				solicitud.setCostoAmpliacionInd(rs.getDouble(20));
				solicitud.setCostoAmpliacion(rs.getDouble(21));
				solicitud.setCostoCobBasica(rs.getDouble(22));
				solicitud.setCostoAmpliacionDep(rs.getDouble(23));
				solicitud.setIdCobertura(rs.getInt(24));
				solicitud.setCobertura(rs.getString(25));
				solicitud.setTipoMemorial(rs.getString(26));
				solicitud.setComentario(rs.getString(27));
				return solicitud;
		}}, numEmp);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SolicitudAdminTO> getDependiente(Integer numEmp){
		List<SolicitudAdminTO> lista = new ArrayList<SolicitudAdminTO>();
		lista = jdbcTemplate.query(QUERY_DEPENDIENTE, new RowMapper(){
			public SolicitudAdminTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				SolicitudAdminTO dependiente = new SolicitudAdminTO();
				dependiente.setIdSolicitud(rs.getInt(1));
				dependiente.setIdDependiente(rs.getInt(2));
				dependiente.setIdParentesco(rs.getInt(3));
				dependiente.setParentesco(rs.getString(4));
				dependiente.setApellidoP(rs.getString(5));
				dependiente.setApellidoM(rs.getString(6));
				dependiente.setNombre(rs.getString(7));
				dependiente.setSexo(rs.getString(8));
				dependiente.setFechaNac(rs.getString(9));
				dependiente.setFechaElab(rs.getString(10));
				dependiente.setFechaAlta(rs.getString(11));
				dependiente.setFechaBaja(rs.getString(12));
				dependiente.setFechaRenov(rs.getString(13));
				dependiente.setCosto(rs.getDouble(14));
				dependiente.setCostoAmpliacionInd(rs.getDouble(15));
				dependiente.setCostoCancelacion(rs.getString(16));
				dependiente.setTipoSol(rs.getString(17));
				dependiente.setIdEstatulSol(rs.getInt(18));
				dependiente.setEstatulSol(rs.getString(19));
				dependiente.setComentario(rs.getString(20));
				return dependiente;
		}}, numEmp);
		return lista;
	}
		
	public DependienteTO getDepCostos(Integer idDep){
		DependienteTO datos = new DependienteTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_DEP_COSTOS, new RowMapper<DependienteTO>(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					DependienteTO dependienteTO = new DependienteTO();
					dependienteTO.setIdDependiente(rs.getInt(1));
					dependienteTO.setCostoDep(rs.getDouble(2));
					dependienteTO.setCostoAmpDep(rs.getDouble(3));
					return dependienteTO;
				}
			}, idDep);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			datos.setIdDependiente(0);
		}
		return datos;
	}	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE_AUTORIZADO, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(2));
					dependiente.setIdParentesco(rs.getInt(3));
					dependiente.setIdSexo(rs.getInt(4));
					dependiente.setSexo(rs.getString(5));
					dependiente.setFechaNac(rs.getString(6));
					dependiente.setAnioAut(rs.getInt(7));
					dependiente.setMesAut(rs.getInt(8));
					dependiente.setDiaAut(rs.getInt(9));
					dependiente.setCostoDep(rs.getDouble(10));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependienteEditado(Integer numEmp, Integer tipoSol, 
													Integer estatusSol, Integer idDep) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE_EDITADO, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(2));
					dependiente.setIdParentesco(rs.getInt(3));
					dependiente.setIdSexo(rs.getInt(4));
					dependiente.setSexo(rs.getString(5));
					dependiente.setFechaNac(rs.getString(6));
					dependiente.setAnioAut(rs.getInt(7));
					dependiente.setMesAut(rs.getInt(8));
					dependiente.setDiaAut(rs.getInt(9));
					dependiente.setAnioRenov(rs.getInt(10));
					dependiente.setMesRenov(rs.getInt(11));
					dependiente.setDiaRenov(rs.getInt(12));
					dependiente.setCostoDep(rs.getDouble(13));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol, idDep);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}

	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ConsulmedTO> getConsulmed(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		List<ConsulmedTO> lista = new ArrayList<ConsulmedTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE_CON_AUTORIZADO, new RowMapper(){
				public ConsulmedTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ConsulmedTO dependiente = new ConsulmedTO();
					dependiente.setIdConsulmed(rs.getInt(1));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	public Double getDependienteCosto(Integer idSol, Integer tipoSol, Integer estSol, Integer idDep){
		Double costo = 0.0;
		try {
			costo = jdbcTemplate.queryForObject(QUERY_COSTO_DEP, Double.class, idSol, tipoSol, estSol, idDep);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			costo = 0.0;
		} 
		logger.info("  Costo: "+costo);
		return costo;
	}	
	
	public Integer getDependienteTotal(Integer idSol, Integer tipoSol, Integer estSol, Integer idDep){
		Integer total = 0;
		try {
			total = jdbcTemplate.queryForObject(QUERY_TOTAL_DEP, Integer.class, idSol, tipoSol, estSol, idDep);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			total = 0;
		}
		logger.info("  Total: "+total);
		return total;
	}	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SolicitudAdminTO> getConsulmed(Integer numEmp){
		List<SolicitudAdminTO> lista = new ArrayList<SolicitudAdminTO>();
		lista = jdbcTemplate.query(QUERY_CONSULMED, new RowMapper(){
			public SolicitudAdminTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				SolicitudAdminTO consulmed = new SolicitudAdminTO();
				consulmed.setIdSolicitud(rs.getInt(1));
				consulmed.setIdConsulmed(rs.getInt(2));
				consulmed.setIdParentesco(rs.getInt(3));
				consulmed.setParentesco(rs.getString(4));
				consulmed.setApellidoP(rs.getString(5));
				consulmed.setApellidoM(rs.getString(6));
				consulmed.setNombre(rs.getString(7));
				consulmed.setFechaNac(rs.getString(8));
				consulmed.setFechaElab(rs.getString(9));
				consulmed.setFechaAlta(rs.getString(10));
				consulmed.setFechaBaja(rs.getString(11));
				consulmed.setFechaRenov(rs.getString(12));
				consulmed.setCosto(rs.getDouble(13));
				consulmed.setTipoSol(rs.getString(14));
				consulmed.setIdEstatulSol(rs.getInt(15));
				consulmed.setEstatulSol(rs.getString(16));
				consulmed.setComentario(rs.getString(17));
				return consulmed;
		}}, numEmp);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SolicitudAdminTO> getMemorial(Integer numEmp){
		List<SolicitudAdminTO> lista = new ArrayList<SolicitudAdminTO>();
		lista = jdbcTemplate.query(QUERY_MEMORIAL, new RowMapper(){
			public SolicitudAdminTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				SolicitudAdminTO consulmed = new SolicitudAdminTO();
				consulmed.setIdSolicitud(rs.getInt(1));
				consulmed.setIdConsulmed(rs.getInt(2));
				consulmed.setIdParentesco(rs.getInt(3));
				consulmed.setParentesco(rs.getString(4));
				consulmed.setApellidoP(rs.getString(5));
				consulmed.setApellidoM(rs.getString(6));
				consulmed.setNombre(rs.getString(7));
				consulmed.setFechaNac(rs.getString(8));
				consulmed.setFechaElab(rs.getString(9));
				consulmed.setFechaAlta(rs.getString(10));
				consulmed.setFechaBaja(rs.getString(11));
				consulmed.setFechaRenov(rs.getString(12));
				consulmed.setCosto(rs.getDouble(13));
				consulmed.setTipoSol(rs.getString(14));
				consulmed.setIdEstatulSol(rs.getInt(15));
				consulmed.setEstatulSol(rs.getString(16));
				consulmed.setComentario(rs.getString(17));
				return consulmed;
		}}, numEmp);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<FechaTO> getFechaSol(Integer idSol){
		List<FechaTO> lista = new ArrayList<FechaTO>();
		lista = jdbcTemplate.query(QUERY_FECHA_ALTA_SOL, new RowMapper(){
			public FechaTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				FechaTO fecha = new FechaTO();
				fecha.setDia(rs.getInt(1));
				fecha.setMes(rs.getInt(2));
				return fecha;
		}}, idSol);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<FechaTO> getFechaSol(Integer idSol, Integer tipoSol, Integer idEst, Integer idDep){
		List<FechaTO> lista = new ArrayList<FechaTO>();
		lista = jdbcTemplate.query(QUERY_FECHA_ALTA_DEP, new RowMapper(){
			public FechaTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				FechaTO fecha = new FechaTO();
				fecha.setDia(rs.getInt(1));
				fecha.setMes(rs.getInt(2));
				return fecha;
		}}, idSol, tipoSol, idEst, idDep);
		return lista;
	}
	
	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		SolicitudTO datos = new SolicitudTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_SOLICITUD_TOTAL, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitudTO = new SolicitudTO();
					solicitudTO.setIdSol(rs.getInt(1));
					solicitudTO.setCostoEmpleado(rs.getDouble(2));
					solicitudTO.setCostoAmpliacion(rs.getDouble(3));
					solicitudTO.setIdTipoCobertura(rs.getInt(4));
					solicitudTO.setDiaAut(rs.getInt(5));
					solicitudTO.setMesAut(rs.getInt(6));
					solicitudTO.setAnioAut(rs.getInt(7));
					solicitudTO.setFechaAutorizacion(rs.getString(8));
					solicitudTO.setDiaRenov(rs.getInt(9));
					solicitudTO.setMesRenov(rs.getInt(10));
					solicitudTO.setAnioRenov(rs.getInt(11));
					solicitudTO.setFechaRenovacion(rs.getString(12));
					return solicitudTO;
				}
			}, numEmp, tipoSol,estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			datos.setIdSol(0);
		}
		return datos;
	}

	public void setSolicitud(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_SOLICITUD,solicitudAdminTO.getFechaAltaDate(), 
													solicitudAdminTO.getIdSolicitud());
	}
	
	public void setSolicitudCosto(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_SOLICITUD_COSTO_TOTAL,
				solicitudAdminTO.getCostoEmp(),
				solicitudAdminTO.getCostoAmpliacionInd(),
				solicitudAdminTO.getCostoTotalDep(),
				solicitudAdminTO.getCostoAmpliacion(),
				solicitudAdminTO.getCostoTotal(),
				solicitudAdminTO.getIdSolicitud());
	}
	
	public void setSolicitudCostoxDep(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_SOLICITUD_COSTO_TOTAL_X_DEP,
				solicitudAdminTO.getCostoTotalDep(),
				solicitudAdminTO.getCostoAmpliacion(),
				solicitudAdminTO.getCostoTotal(),
				solicitudAdminTO.getIdSolicitud());
	}
	
	public void setDependiente(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_DEPENDIENTE,solicitudAdminTO.getIdParentesco(),
												solicitudAdminTO.getApellidoP().toUpperCase(),
												solicitudAdminTO.getApellidoM().toUpperCase(),
												solicitudAdminTO.getNombre().toUpperCase(), 
												solicitudAdminTO.getFechaNacDate(), 
												solicitudAdminTO.getIdTipoSol(),
												solicitudAdminTO.getIdEstatulSolNvo(),
												solicitudAdminTO.getCostoCancelacion(), 
												solicitudAdminTO.getFechaBajaDate(),
												solicitudAdminTO.getIdDependiente());
	}
	
	public void setDependienteCosto(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_DEPENDIENTE_COSTO,solicitudAdminTO.getCosto(),
				solicitudAdminTO.getCostoAmpliacionInd(),solicitudAdminTO.getIdDependiente());
	}
		
	public void setCondulmed(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_CONSULMED,solicitudAdminTO.getIdParentesco(),solicitudAdminTO.getApellidoP().toUpperCase(),
				solicitudAdminTO.getApellidoM().toUpperCase(),solicitudAdminTO.getNombre().toUpperCase(), solicitudAdminTO.getFechaNacDate(), 
				solicitudAdminTO.getFechaAltaDate(), solicitudAdminTO.getIdEstatulSol(),solicitudAdminTO.getIdConsulmed());			
	}
	
	public void setCondulmedFechaAlta(SolicitudAdminTO solicitudAdminTO){
		jdbcTemplate.update(UPDATE_CONSULMED_ALTA,solicitudAdminTO.getFechaAltaDate(), solicitudAdminTO.getIdConsulmed());			
	}
		
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}