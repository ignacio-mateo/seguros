package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AutorizarSolDao;
import mx.com.orbita.to.AutorizacionTO;
import mx.com.orbita.to.DependienteTO;

@Component
public class AutorizarSolDaoImpl implements AutorizarSolDao {
		
	public static final Logger logger = Logger.getLogger(AutorizarSolDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_SOLICITUD = "SELECT S.ID_SOLICITUD, ? IDOPERACION, ? SOL, " +
			"S.ID_EMPLEADO, E.NOMBRE || ' ' ||E.A_PATERNO || ' '  || E.A_MATERNO AS NOMBRE, " +
			"TO_CHAR(S.FECHA_ELABORACION,'DD-MM-yyyy') AS FECHA, " +
			"TO_CHAR(S.FECHA_CANCELACION,'DD-MM-yyyy') AS FECHA_CAN, E.ID_TIPO_EMPLEADO, CT.SEXO, " +
			"TO_CHAR(E.FECHA_NACIMIENTO,'DD-MM-yyyy'), S.COSTO_EMPLEADO,S.COSTO_AMPLIACION, " +
			"S.ID_TIPO_COB,S.ID_TIPO_COB_MEM,COSTO_AMPL_IND " +
			"FROM SOLICITUD S INNER JOIN EMPLEADO E ON S.ID_EMPLEADO = E.ID_EMPLEADO "+ 
			"INNER JOIN CAT_SEXO CT on E.ID_SEXO = CT.ID_SEXO WHERE S.ID_ESTATUS = ? " +
			"ORDER BY S.ID_SOLICITUD ";	
	
	public static final String QUERY_SOLICITUD_BAJA = "SELECT S.ID_SOLICITUD, ? IDOPERACION, ? SOL, " +
			"S.ID_EMPLEADO, E.NOMBRE || ' ' ||E.A_PATERNO || ' '  || E.A_MATERNO AS NOMBRE, " +
			"TO_CHAR(S.FECHA_ELABORACION,'DD-MM-yyyy') AS FECHA, " +
			"TO_CHAR(S.FECHA_CANCELACION,'DD-MM-yyyy') AS FECHA_CAN, E.ID_TIPO_EMPLEADO, CT.SEXO, " +
			"TO_CHAR(E.FECHA_NACIMIENTO,'DD-MM-yyyy'), S.COSTO_EMPLEADO,S.COSTO_AMPLIACION, " +
			"S.ID_TIPO_COB, COSTO_AMPL_IND " +
			"FROM SOLICITUD S INNER JOIN EMPLEADO E ON S.ID_EMPLEADO = E.ID_EMPLEADO "+ 
			"INNER JOIN CAT_SEXO CT on E.ID_SEXO = CT.ID_SEXO WHERE S.ID_ESTATUS = ? " +
			"ORDER BY S.FECHA_CANCELACION ";	
	
	public static final String QUERY_DEPENDIENTE = "SELECT S.ID_SOLICITUD, D.ID_DEPENDIENTE, ? IDOPERACION, ? DEP, " +
			"S.ID_EMPLEADO, E.ID_TIPO_EMPLEADO, E.NOMBRE || ' ' ||E.A_PATERNO || ' '  || E.A_MATERNO AS NOMBRE, " +
			"TO_CHAR(D.FECHA_ALTA,'DD-MM-yyyy') AS FECHA, TO_CHAR(D.FECHA_BAJA,'DD-MM-yyyy') AS FECHA_BAJ, "+ 
			"S.ID_TIPO_COB, S.COSTO_EMPLEADO, D.COSTO_DEPENDIENTE, S.COSTO_TOTAL, S.COSTO_AMPLIACION, " +
			"D.ID_PARENTESCO, case D.ID_SEXO when 1 then 'M' else 'F' end, TO_CHAR(D.FECHA_NACIMIENTO,'yyyy-MM-dd') "+ 
			"FROM SOLICITUD S " +
			"INNER JOIN EMPLEADO E ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"WHERE S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ? AND D.ID_ESTATUS = ? ORDER BY D.ID_DEPENDIENTE";
	
	public static final String QUERY_DEPENDIENTE_BAJA = "SELECT S.ID_SOLICITUD, D.ID_DEPENDIENTE, ? IDOPERACION, ? DEP, " +
			"S.ID_EMPLEADO, E.ID_TIPO_EMPLEADO, E.NOMBRE || ' ' ||E.A_PATERNO || ' '  || E.A_MATERNO AS NOMBRE, " +
			"TO_CHAR(D.FECHA_ALTA,'DD-MM-yyyy') AS FECHA, TO_CHAR(D.FECHA_BAJA,'DD-MM-yyyy') AS FECHA_BAJ, "+ 
			"S.ID_TIPO_COB, S.COSTO_EMPLEADO, D.COSTO_DEPENDIENTE, S.COSTO_TOTAL, S.COSTO_AMPLIACION, " +
			"D.ID_PARENTESCO, case D.ID_SEXO when 1 then 'M' else 'F' end, TO_CHAR(D.FECHA_NACIMIENTO,'yyyy-MM-dd') "+ 
			"FROM SOLICITUD S " +
			"INNER JOIN EMPLEADO E ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"WHERE S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ? AND D.ID_ESTATUS = ? ORDER BY D.FECHA_BAJA";

	public static final String QUERY_CONSULMED = "SELECT C.ID_CONSULMED, ? IDOPERACION, ? CON, S.ID_EMPLEADO, " +
			"E.NOMBRE || ' ' ||E.A_PATERNO || ' '  || E.A_MATERNO AS NOMBRE, " +
			"TO_CHAR(C.FECHA_ALTA,'DD-MM-yyyy') AS FECHA, TO_CHAR(C.FECHA_BAJA,'DD-MM-yyyy') AS FECHA_BAJ " +
			"FROM SOLICITUD S " +
			"INNER JOIN EMPLEADO E ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"WHERE S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ? AND C.ID_ESTATUS = ? ORDER BY C.ID_CONSULMED";
	
	public static final String QUERY_CONSULMED_BAJA = "SELECT C.ID_CONSULMED, ? IDOPERACION, ? CON, S.ID_EMPLEADO, " +
			"E.NOMBRE || ' ' ||E.A_PATERNO || ' '  || E.A_MATERNO AS NOMBRE, " +
			"TO_CHAR(C.FECHA_ALTA,'DD-MM-yyyy') AS FECHA, TO_CHAR(C.FECHA_BAJA,'DD-MM-yyyy') AS FECHA_BAJ " +
			"FROM SOLICITUD S " +
			"INNER JOIN EMPLEADO E ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"WHERE S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ? AND C.ID_ESTATUS = ? ORDER BY C.FECHA_BAJA";
	
	public static final String QUERY_TOTAL_DEP = "SELECT COUNT(*) FROM DEPENDIENTE WHERE ID_SOLICITUD = ? " +
			"AND ID_TIPO_SOL = ? AND ID_ESTATUS = ?";
	
	public static final String QUERY_TOTAL_DEP_SIN = "SELECT COUNT(*) FROM DEPENDIENTE WHERE ID_SOLICITUD = ?" +
			" AND ID_TIPO_SOL = ? AND ID_ESTATUS IN (?,?)";
	
	public static final String QUERY_COSTO_TOT_DEP = "SELECT COSTO_DEPENDIENTES FROM SOLICITUD WHERE ID_SOLICITUD = ?";
	
	public static final String QUERY_COSTO_AMPLIACION = "SELECT COSTO_AMPLIACION FROM SOLICITUD WHERE ID_SOLICITUD = ?";
		
	public static final String QUERY_COSTO_AMP_EMP = "SELECT COALESCE(S.COSTO_AMPL_IND,0) FROM SOLICITUD S WHERE S.ID_SOLICITUD = ?";
			
	public static final String QUERY_COSTO_AMP_DEP = "SELECT COALESCE(COSTO_AMPLIACION,0) FROM DEPENDIENTE WHERE ID_DEPENDIENTE = ?";
	
	public static final String QUERY_COSTO_AMP_DEP_TOTAL = "SELECT COALESCE(SUM(COSTO_AMPLIACION),0) FROM DEPENDIENTE " +
			"WHERE ID_SOLICITUD = ? AND ID_TIPO_SOL = ? AND ID_ESTATUS = ? AND ID_DEPENDIENTE <> ?";
	
	public static final String QUERY_FECHA_ALTA = "select to_char(fecha_autorizacion,'DD.MM.YYYY') from solicitud where id_solicitud = ?";
		
	public static final String QUERY_DEP_DATOS = "SELECT D.ID_DEPENDIENTE, D.ID_PARENTESCO, D.ID_SEXO, " +
			"CT.SEXO, TO_CHAR(D.FECHA_NACIMIENTO,'yyyy-mm-dd'), D.COSTO_DEPENDIENTE, " +
			"D.COSTO_AMPLIACION, TO_CHAR(FECHA_ALTA_AUT,'YYYY') AS ANIO, TO_CHAR(FECHA_ALTA_AUT,'MM') AS MES, " +
			"TO_CHAR(FECHA_ALTA_AUT,'DD') AS DIA " +
			"FROM DEPENDIENTE D INNER JOIN CAT_SEXO CT " +
			"ON D.ID_SEXO = CT.ID_SEXO " +
			"WHERE D.ID_SOLICITUD = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS = ?";
	
	public static final String QUERY_DEPS_FECHA = "select id_dependiente, TO_CHAR(FECHA_ALTA_AUT,'YYYY') AS ANIO, "
			+ "TO_CHAR(FECHA_ALTA_AUT,'MM') AS MES, TO_CHAR(FECHA_ALTA_AUT,'DD') AS DIA "
			+ "from dependiente where id_solicitud = ? and id_tipo_sol = ? and id_estatus = ?";
	
	public static final String QUERY_DEP_FECHA = "select id_dependiente, TO_CHAR(FECHA_ALTA_AUT,'YYYY') AS ANIO, "
			+ "TO_CHAR(FECHA_ALTA_AUT,'MM') AS MES, TO_CHAR(FECHA_ALTA_AUT,'DD') AS DIA "
			+ "from dependiente where id_dependiente = ?";
		
	public static final String QUERY_DEP_DATO = "SELECT D.ID_DEPENDIENTE, D.ID_PARENTESCO, D.ID_SEXO, CT.SEXO, " +
			"to_char(D.FECHA_NACIMIENTO,'yyyy-mm-dd') FROM DEPENDIENTE D " +
			"INNER JOIN CAT_SEXO CT ON D.ID_SEXO = CT.ID_SEXO WHERE D.ID_DEPENDIENTE = ?";
		
	public static final String UPDATE_SOLICITUD_ALTA = "UPDATE SOLICITUD SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_ALTA = ?, ID_ESTATUS = ?, FECHA_AUTORIZACION = ?, COMENTARIO = ? " +
			"WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_SOLICITUD_BAJA = "UPDATE SOLICITUD SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_BAJA = ?, ID_ESTATUS = ?, FECHA_CANCELACION_AUT = ?, COMENTARIO = ? " +
			"WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_DEPENDIENTE_ALTA = "UPDATE DEPENDIENTE SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_ALTA = ?, ID_ESTATUS = ?, FECHA_ALTA_AUT = ?, COMENTARIO = ? " +
			"WHERE ID_DEPENDIENTE = ?";
	
	public static final String UPDATE_DEPENDIENTE_BAJA = "UPDATE DEPENDIENTE SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_BAJA = ?, ID_ESTATUS = ?, FECHA_BAJA_AUT = ?, COMENTARIO = ? " +
			"WHERE ID_DEPENDIENTE = ?";

	public static final String UPDATE_SOL_DEP = "UPDATE SOLICITUD SET COSTO_TOTAL = ?, " +
			"COSTO_DEPENDIENTES = ?, COSTO_AMPLIACION = ? " +
			"WHERE ID_SOLICITUD = ?";

	public static final String UPDATE_COSTO_CANCELACION = "UPDATE DEPENDIENTE SET COSTO_DEPENDIENTE = ?, " +
			"COSTO_CANCELACION = ?, COSTO_AMPLIACION = ? " +
			"WHERE ID_DEPENDIENTE = ?";
	
	public static final String UPDATE_DEPENDIENTEXSOL_ALTA = "UPDATE DEPENDIENTE SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_ALTA = ?, ID_ESTATUS = ?, FECHA_ALTA_AUT = ? " +
			"WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_DEPENDIENTEXSOL_BAJA = "UPDATE DEPENDIENTE SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_BAJA = ?, ID_ESTATUS = ?, FECHA_BAJA_AUT = ? " +
			"WHERE ID_SOLICITUD = ? AND ID_ESTATUS <> ?";
	
	public static final String UPDATE_CONSULMED_ALTA = "UPDATE CONSULMED SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_ALTA = ?, ID_ESTATUS = ?, FECHA_ALTA_AUT = ?, COMENTARIO = ? " +
			"WHERE ID_CONSULMED = ?";
	
	public static final String UPDATE_CONSULMED_BAJA = "UPDATE CONSULMED SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_BAJA = ?, ID_ESTATUS = ?, FECHA_BAJA_AUT = ?, COMENTARIO = ? " +
			"WHERE ID_CONSULMED = ?";
	
	public static final String UPDATE_CONSULMEDXSOL_ALTA = "UPDATE CONSULMED SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_ALTA = ?, ID_ESTATUS = ?, FECHA_ALTA_AUT = ? " +
			"WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_MEMORIALXSOL_ALTA = "UPDATE MEMORIAL SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_ALTA = ?, ID_ESTATUS = ?, FECHA_ALTA_AUT = ? " +
			"WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_CONSULMEDXSOL_BAJA = "UPDATE CONSULMED SET ID_TIPO_SOL = ?, " +
			"ID_ADMIN_AUT_BAJA = ?, ID_ESTATUS = ?, FECHA_BAJA_AUT = ? " +
			"WHERE ID_SOLICITUD = ? AND ID_ESTATUS <> ?";
		
	public static final String UPDATE_SOLICITUD_MONTOS = "UPDATE SOLICITUD SET COSTO_EMPLEADO = ?, " +
			"COSTO_AMPL_IND = ?, COSTO_DEPENDIENTES = ?, COSTO_TOTAL = ?, COSTO_AMPLIACION = ?, COSTO_COB_MEMORIAL = ? " +
			"WHERE ID_SOLICITUD = ?";

	public static final String UPDATE_DEPENDIENTE_MONTO = "UPDATE DEPENDIENTE SET COSTO_DEPENDIENTE = ?, " +
			"COSTO_AMPLIACION = ? " +
			"WHERE ID_DEPENDIENTE = ?";
	
	/**
	 * Obtener solicitudes para autorizar
	 * @param idOperacion
	 * @param tipoSol
	 * @param estatusSol
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AutorizacionTO> getProcesarSolicitudes(Integer idOperacion, Integer tipoSol, Integer estatusSol) {
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = jdbcTemplate.query(QUERY_SOLICITUD, new RowMapper(){
			public AutorizacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AutorizacionTO autorizacion = new AutorizacionTO();
				autorizacion.setIdSolPrimaria(rs.getInt(1));
				autorizacion.setIdOperacion(rs.getInt(2));
				autorizacion.setIdTipoSolicitud(rs.getInt(3));
				autorizacion.setIdEmpleado(rs.getInt(4));
				autorizacion.setNombre(rs.getString(5));
				autorizacion.setFechaElaboracion(rs.getString(6));
				autorizacion.setFechaCancelacion(rs.getString(7));
				autorizacion.setIdTipoEmp(rs.getInt(8));
				autorizacion.setSexo(rs.getString(9));
				autorizacion.setFechaNac(rs.getString(10));
				autorizacion.setCostoEmp(rs.getDouble(11));
				autorizacion.setCostoAmpl(rs.getDouble(12));
				autorizacion.setIdTipoCob(rs.getInt(13));
				autorizacion.setIdTipoCobMem(rs.getInt(14));
				autorizacion.setCostoAmplInd(rs.getDouble(15));
				return autorizacion;
		}},idOperacion,tipoSol,estatusSol);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AutorizacionTO> getProcesarSolicitudesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol) {
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = jdbcTemplate.query(QUERY_SOLICITUD_BAJA, new RowMapper(){
			public AutorizacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AutorizacionTO autorizacion = new AutorizacionTO();
				autorizacion.setIdSolPrimaria(rs.getInt(1));
				autorizacion.setIdOperacion(rs.getInt(2));
				autorizacion.setIdTipoSolicitud(rs.getInt(3));
				autorizacion.setIdEmpleado(rs.getInt(4));
				autorizacion.setNombre(rs.getString(5));
				autorizacion.setFechaElaboracion(rs.getString(6));
				autorizacion.setFechaCancelacion(rs.getString(7));
				autorizacion.setIdTipoEmp(rs.getInt(8));
				autorizacion.setSexo(rs.getString(9));
				autorizacion.setFechaNac(rs.getString(10));
				autorizacion.setCostoEmp(rs.getDouble(11));
				autorizacion.setCostoAmpl(rs.getDouble(12));
				autorizacion.setIdTipoCob(rs.getInt(13));
				autorizacion.setCostoAmplInd(rs.getDouble(14));
				return autorizacion;
		}},idOperacion,tipoSol,estatusSol);
		return lista;
	}
		
	/**
	 * Obtener solicitudes de adicion para autorizar
	 * @param idOperacion
	 * @param tipoSol
	 * @param estatusSol
	 * @param estatusCicloSol
	 * @param estatusDep
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AutorizacionTO> getProcesarDependientes(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
															Integer estatusCicloSol, Integer estatusDep) {
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = jdbcTemplate.query(QUERY_DEPENDIENTE, new RowMapper(){
			public AutorizacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AutorizacionTO autorizacion = new AutorizacionTO();
				autorizacion.setIdSolSecundaria(rs.getInt(1));
				autorizacion.setIdSolPrimaria(rs.getInt(2));
				autorizacion.setIdOperacion(rs.getInt(3));
				autorizacion.setIdTipoSolicitud(rs.getInt(4));
				autorizacion.setIdEmpleado(rs.getInt(5));
				autorizacion.setIdTipoEmp(rs.getInt(6));
				autorizacion.setNombre(rs.getString(7));
				autorizacion.setFechaElaboracion(rs.getString(8));
				autorizacion.setFechaCancelacion(rs.getString(9));
				autorizacion.setIdTipoCob(rs.getInt(10));
				autorizacion.setCostoEmp(rs.getDouble(11));
				autorizacion.setCostoDep(rs.getDouble(12));
				autorizacion.setCostoTotal(rs.getDouble(13));
				autorizacion.setCostoAmpl(rs.getDouble(14));
				autorizacion.setIdParentesco(rs.getInt(15));
				autorizacion.setSexo(rs.getString(16));
				autorizacion.setFechaNac(rs.getString(17));
				return autorizacion;
		}},idOperacion,tipoSol,estatusSol,estatusCicloSol,estatusDep);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AutorizacionTO> getProcesarDependientesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
															Integer estatusCicloSol, Integer estatusDep) {
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = jdbcTemplate.query(QUERY_DEPENDIENTE_BAJA, new RowMapper(){
			public AutorizacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AutorizacionTO autorizacion = new AutorizacionTO();
				autorizacion.setIdSolSecundaria(rs.getInt(1));
				autorizacion.setIdSolPrimaria(rs.getInt(2));
				autorizacion.setIdOperacion(rs.getInt(3));
				autorizacion.setIdTipoSolicitud(rs.getInt(4));
				autorizacion.setIdEmpleado(rs.getInt(5));
				autorizacion.setIdTipoEmp(rs.getInt(6));
				autorizacion.setNombre(rs.getString(7));
				autorizacion.setFechaElaboracion(rs.getString(8));
				autorizacion.setFechaCancelacion(rs.getString(9));
				autorizacion.setIdTipoCob(rs.getInt(10));
				autorizacion.setCostoEmp(rs.getDouble(11));
				autorizacion.setCostoDep(rs.getDouble(12));
				autorizacion.setCostoTotal(rs.getDouble(13));
				autorizacion.setCostoAmpl(rs.getDouble(14));
				autorizacion.setIdParentesco(rs.getInt(15));
				autorizacion.setSexo(rs.getString(16));
				autorizacion.setFechaNac(rs.getString(17));
				return autorizacion;
		}},idOperacion,tipoSol,estatusSol,estatusCicloSol,estatusDep);
		return lista;
	}
	
	
	
	/**
	 * Obtener solicitudes de consulmed para autorizar
	 * @param idOperacion
	 * @param tipoSol
	 * @param estatusSol
	 * @param estatusCicloSol
	 * @param estatusDep
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AutorizacionTO> getProcesarConsulmed(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
														Integer estatusCicloSol, Integer estatusDep) {
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = jdbcTemplate.query(QUERY_CONSULMED, new RowMapper(){
			public AutorizacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AutorizacionTO autorizacion = new AutorizacionTO();
				autorizacion.setIdSolPrimaria(rs.getInt(1));
				autorizacion.setIdOperacion(rs.getInt(2));
				autorizacion.setIdTipoSolicitud(rs.getInt(3));
				autorizacion.setIdEmpleado(rs.getInt(4));
				autorizacion.setNombre(rs.getString(5));
				autorizacion.setFechaElaboracion(rs.getString(6));
				autorizacion.setFechaCancelacion(rs.getString(7));
				return autorizacion;
		}},idOperacion,tipoSol,estatusSol,estatusCicloSol,estatusDep);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AutorizacionTO> getProcesarConsulmedBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
														Integer estatusCicloSol, Integer estatusDep) {
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = jdbcTemplate.query(QUERY_CONSULMED_BAJA, new RowMapper(){
			public AutorizacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AutorizacionTO autorizacion = new AutorizacionTO();
				autorizacion.setIdSolPrimaria(rs.getInt(1));
				autorizacion.setIdOperacion(rs.getInt(2));
				autorizacion.setIdTipoSolicitud(rs.getInt(3));
				autorizacion.setIdEmpleado(rs.getInt(4));
				autorizacion.setNombre(rs.getString(5));
				autorizacion.setFechaElaboracion(rs.getString(6));
				autorizacion.setFechaCancelacion(rs.getString(7));
				return autorizacion;
		}},idOperacion,tipoSol,estatusSol,estatusCicloSol,estatusDep);
		return lista;
	}
			
	public Integer getTotalDep(Integer idSolDep, Integer tipoSol, Integer estatusSol){
		Integer total = 0;
		try {
			total = jdbcTemplate.queryForObject(QUERY_TOTAL_DEP, Integer.class, idSolDep, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ No existen dependientes ] ---");
			total = 0;
		}
		return total;
	}	
	
	public Integer getTotalDep(Integer idSolDep,Integer tipoSol,Integer estatusSolAut,Integer estatusSol){
		Integer total = 0;
		try {
			total = jdbcTemplate.queryForObject(QUERY_TOTAL_DEP_SIN, Integer.class, idSolDep, tipoSol,estatusSolAut, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ No existen dependientes ] ---");
			total = 0;
		}
		return total;
	}
	
	public Double getCostoTotDep(Integer idSol){
		Double costoTotalDep = 0.0;
		try {
			costoTotalDep = jdbcTemplate.queryForObject(QUERY_COSTO_TOT_DEP, Double.class, idSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ Costo 0 ] ---");
			costoTotalDep = 0.0;
		}
		return costoTotalDep;
	}	
	
	public Double getCostoTotAmpliacion(Integer idSol){
		Double costoAmpliacion = 0.0;
		try {
			costoAmpliacion = jdbcTemplate.queryForObject(QUERY_COSTO_AMPLIACION, Double.class, idSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ Costo 0 ] ---");
			costoAmpliacion = 0.0;
		}
		return costoAmpliacion;
	}	

		
	public Double getCostoAmpliacionEmp(Integer idSol){
		Double costoAmpliacion = 0.0;
		try {
			costoAmpliacion = jdbcTemplate.queryForObject(QUERY_COSTO_AMP_EMP, Double.class, idSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ Costo 0 ] ---");
			costoAmpliacion = 0.0;
		}
		return costoAmpliacion;
	}	
	
	public Double getCostoAmpliacionDep(Integer idSol){
		Double costoAmpliacion = 0.0;
		try {
			costoAmpliacion = jdbcTemplate.queryForObject(QUERY_COSTO_AMP_DEP, Double.class, idSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ Costo 0 ] ---");
			costoAmpliacion = 0.0;
		}
		return costoAmpliacion;
	}	
	
	public Double getCostoAmpliacionDepTot(Integer idSol, Integer tipoSol, Integer idEst, Integer idDep){
		Double costoAmpliacion = 0.0;
		try {
			costoAmpliacion = jdbcTemplate.queryForObject(QUERY_COSTO_AMP_DEP_TOTAL, Double.class, idSol, tipoSol, idEst, idDep);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ Costo 0 ] ---");
			costoAmpliacion = 0.0;
		}
		return costoAmpliacion;
	}	
	
	public String getFechaAlta(Integer idSol){
		String fechaAlta = "";
		try {
			fechaAlta = jdbcTemplate.queryForObject(QUERY_FECHA_ALTA, String.class, idSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ Fecha vacia ] ---");
			fechaAlta = "";
		}
		return fechaAlta;
	}	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer idEst) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEP_DATOS, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setIdParentesco(rs.getInt(2));
					dependiente.setIdSexo(rs.getInt(3));
					dependiente.setSexo(rs.getString(4));
					dependiente.setFechaNac(rs.getString(5));
					dependiente.setCostoDep(rs.getDouble(6));
					dependiente.setCostoAmpDep(rs.getDouble(7));
					dependiente.setAnioAut(rs.getInt(8));
					dependiente.setMesAut(rs.getInt(9));
					dependiente.setDiaAut(rs.getInt(10));
					return dependiente;
			}}, idSolicitud, tipoSol, idEst);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ No existen dependientes ] ---");
		}
		return lista;
	}
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependienteFechas(Integer idSolicitud, Integer tipoSol, Integer idEst) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPS_FECHA, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setAnioAut(rs.getInt(2));
					dependiente.setMesAut(rs.getInt(3));
					dependiente.setDiaAut(rs.getInt(4));
					return dependiente;
			}}, idSolicitud, tipoSol, idEst);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ No existen dependientes ] ---");
		}
		return lista;
	}
	
	public DependienteTO getDependienteFecha(Integer idSolicitud) {
		DependienteTO datos = new DependienteTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_DEP_FECHA, new RowMapper<DependienteTO>(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setAnioAut(rs.getInt(2));
					dependiente.setMesAut(rs.getInt(3));
					dependiente.setDiaAut(rs.getInt(4));
					return dependiente;
				}
			}, idSolicitud);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [ No existen dependiente ] ---");		
		}
		return datos;
	}
	
	public void setProcesarSolicitudesAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_SOLICITUD_ALTA, idOperacion, usuarioAdmin, estatusSol, new Date(), comentario, idSolicitud);
	}
	
	public void setProcesarSolicitudesBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Date fecha, String comentario, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_SOLICITUD_BAJA, idOperacion, usuarioAdmin, estatusSol, fecha, comentario, idSolicitud);
	}
	
	public void setProcesarDependientesAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_DEPENDIENTE_ALTA, idOperacion, usuarioAdmin, estatusSol, new Date(), comentario, idSolicitud);
	}
	
	public void setProcesarDependientesBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_DEPENDIENTE_BAJA, idOperacion, usuarioAdmin, estatusSol, new Date(), comentario, idSolicitud);
	}
		
	public void setProcesarSolDependientes(Double costoTotal, Double costoDependientes, Double costoAmpliacion, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_SOL_DEP, costoTotal, costoDependientes, costoAmpliacion, idSolicitud);
	}
	
	public void setProcesarCostoCancelacion(Double costo, Double costoAmp, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_COSTO_CANCELACION, costo, costo, costoAmp, idSolicitud);
	}
	
	public void setProcesarDependientesXsolAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_DEPENDIENTEXSOL_ALTA, idOperacion, usuarioAdmin, estatusSol, new Date(), idSolicitud);
	}
	
	public void setProcesarDependientesXsolBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Date fecha, Integer idSolicitud, Integer estRechazado){
		jdbcTemplate.update(UPDATE_DEPENDIENTEXSOL_BAJA, idOperacion, usuarioAdmin, estatusSol, fecha, idSolicitud, estRechazado);
	}
		
	public void setProcesarConsulmedAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_CONSULMED_ALTA, idOperacion, usuarioAdmin, estatusSol, new Date(), comentario, idSolicitud);
	}
	
	public void setProcesarConsulmedBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, String comentario, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_CONSULMED_BAJA, idOperacion, usuarioAdmin, estatusSol, new Date(), comentario, idSolicitud);
	}
		
	public void setProcesarConsulmedXsolAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_CONSULMEDXSOL_ALTA, idOperacion, usuarioAdmin, estatusSol, new Date(), idSolicitud);
	}
	
	public void setProcesarConsulmedXsolBaja(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Date fecha, Integer idSolicitud, Integer estRechazado){
		jdbcTemplate.update(UPDATE_CONSULMEDXSOL_BAJA, idOperacion, usuarioAdmin, estatusSol, fecha, idSolicitud, estRechazado);
	}
	
	public void setProcesarMemorialXsolAlta(Integer idOperacion, Integer usuarioAdmin, Integer estatusSol, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_MEMORIALXSOL_ALTA, idOperacion, usuarioAdmin, estatusSol, new Date(), idSolicitud);
	}
	
	public void setUpdateSol(Double costoEmp, Double costoAmpInd, Double costoDep, Double costoTotal, Double costoAmpl, Double costoMem, Integer idSolicitud){
		jdbcTemplate.update(UPDATE_SOLICITUD_MONTOS, costoEmp,costoAmpInd,costoDep,costoTotal,costoAmpl,costoMem,idSolicitud);
	}
	
	public void setUpdateSolDep(Double costoDep,Double costoAmpl,Integer idSolicitud){
		jdbcTemplate.update(UPDATE_DEPENDIENTE_MONTO, costoDep,costoAmpl,idSolicitud);
	}
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}