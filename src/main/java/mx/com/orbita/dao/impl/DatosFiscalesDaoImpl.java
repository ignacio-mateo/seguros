package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.DatosFiscalesDao;
import mx.com.orbita.to.DatFiscalesTO;

@Component
public class DatosFiscalesDaoImpl implements DatosFiscalesDao{
	
	private static final Logger logger = Logger.getLogger(DatosFiscalesDaoImpl.class);
	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_DATOS_FISCALES = "SELECT * FROM DATOS_FISCALES WHERE ID_EMPLEADO = ?";
	
	public static final String MERGE_DATOS_FISCALES = "MERGE INTO DATOS_FISCALES D "
			+ "USING (SELECT ? ID_EMPLEADO, ? RFC,? CALLE, ? NUMERO, ? COLONIA, ? CP, ? DELEGACION_MUNICIPIO, ? ENTIDAD FROM DUAL) F "
			+ "ON (D.ID_EMPLEADO = F.ID_EMPLEADO) "
			+ "WHEN MATCHED THEN "
			+ "		UPDATE SET D.RFC = F.RFC," 
			+"					D.CALLE = F.CALLE, "
			+ "					D.NUMERO = F.NUMERO, "
			+ "					D.COLONIA = F.COLONIA, "
			+ "					D.CP = F.CP, "
			+ "					D.DELEGACION_MUNICIPIO = F.DELEGACION_MUNICIPIO, "
			+ "					D.ENTIDAD = F.ENTIDAD "
			+ " WHEN NOT MATCHED THEN " 
			+ "		INSERT(D.ID_EMPLEADO,D.RFC,D.CALLE,D.NUMERO,D.COLONIA,D.CP,D.DELEGACION_MUNICIPIO,D.ENTIDAD) " 
			+ "     VALUES(F.ID_EMPLEADO,F.RFC,F.CALLE,F.NUMERO,F.COLONIA,F.CP,F.DELEGACION_MUNICIPIO,F.ENTIDAD) ";
	
	public DatFiscalesTO getDatosFiscales(Integer numEmp) {
		DatFiscalesTO datos = new DatFiscalesTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_DATOS_FISCALES, new RowMapper<DatFiscalesTO>(){
				public DatFiscalesTO mapRow(ResultSet result, int rowNum) throws SQLException{
					DatFiscalesTO datFiscalesTO = new DatFiscalesTO();
					datFiscalesTO.setNum_empleado(result.getInt(1));
					datFiscalesTO.setRfc(result.getString(2));
					datFiscalesTO.setCalle(result.getString(3));
					datFiscalesTO.setNumero(result.getString(4));
					datFiscalesTO.setColonia(result.getString(5));
					datFiscalesTO.setCp(result.getString(6));
					datFiscalesTO.setDeleg_mun(result.getString(7));
					datFiscalesTO.setEntidad(result.getString(8));
					return datFiscalesTO;
				}
			}, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existen datos fiscales en la tabla se regresa vacio el objeto "+datos.getExisteDatosFis());
			datos.setExisteDatosFis(true);			
		}
		return datos;
	}
		
	public void setDatosFiscales(DatFiscalesTO datos){
		jdbcTemplate.update(MERGE_DATOS_FISCALES,
				datos.getNum_empleado(),
				datos.getRfc(),
				datos.getCalle().toUpperCase(),
				datos.getNumero(),
				datos.getColonia().toUpperCase(),
				datos.getCp(),
				datos.getDeleg_mun().toUpperCase(),
				datos.getEntidad().toUpperCase());
	}
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}