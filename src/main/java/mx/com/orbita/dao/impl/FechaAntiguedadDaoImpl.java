package mx.com.orbita.dao.impl;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.FechaAntiguedadDao;

@Component
public class FechaAntiguedadDaoImpl implements FechaAntiguedadDao {
		
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_SOLICITUD = "SELECT ID_SOLICITUD " +
			"FROM SOLICITUD " +
			"WHERE ID_EMPLEADO = ? " +
			"AND ID_TIPO_SOL = ? " +
			"AND ID_ESTATUS = ?";
	
	public static final String UPDATE_SOLICITUD = "UPDATE SOLICITUD " +
			"SET FECHA_ELABORACION = ?, FECHA_AUTORIZACION = ? " +
			"WHERE ID_EMPLEADO = ?";
	
	public static final String UPDATE_DEPENDIENTE = "UPDATE DEPENDIENTE " +
			"SET FECHA_ALTA = ?, FECHA_ALTA_AUT = ? "+
			"WHERE ID_DEPENDIENTE = ?";
	
	public Integer existeSolicitud(Integer numEmp, Integer tipoSol,Integer estatusSol){
		Integer existe = 0;
		try {
			existe = jdbcTemplate.queryForObject(QUERY_SOLICITUD, Integer.class, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			existe = 0;
		}
		return existe;
	}
	
	public void setSolicitud(Date fechaElab,Date fechaAut,Integer numEmp){
		jdbcTemplate.update(UPDATE_SOLICITUD,fechaElab,fechaAut,numEmp);
	}
	
	public void setDependiente(Date fechaAlta,Date fechaAltaAut,Integer idDependiente){
		jdbcTemplate.update(UPDATE_DEPENDIENTE,fechaAlta,fechaAltaAut,idDependiente);
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}
