package mx.com.orbita.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.ReembolsoDao;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.ReembolsoConceptoTO;
import mx.com.orbita.to.ReembolsoRelacionTO;

@Component
public class ReembolsoDaoImpl implements ReembolsoDao {
	
	private static final Logger logger = Logger.getLogger(ReembolsoDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public static final String INSERT_REEMBOLSO = "INSERT INTO REEMBOLSO(Id_Empleado,Id_Tipo_Reembolso,Afectado,Parent_Afectado,Descripcion_Reembolso,Observaciones) "
			+ "VALUES(?,?,?,?,?,?)";
	
	public static final String INSERT_REEMBOLSO_CONCEPTO = "INSERT INTO REEMBOLSO_CONCEPTO(No_Documento,Nombre,Concepto,Importe,Id_Reembolso) "
			+ "VALUES(?,?,?,?,?)";
	
	public static final String QUERY_REEMBOLSO_ID_LIST = "SELECT R.ID_REEMBOLSO FROM REEMBOLSO R WHERE R.ID_EMPLEADO = ?";
	
	public static final String QUERY_REEMBOLSO_ID = "SELECT R.ID_REEMBOLSO FROM REEMBOLSO R "
			+ "WHERE R.ID_EMPLEADO = ? ORDER BY R.ID_REEMBOLSO DESC LIMIT 1";
	
	public static final String QUERY_REEMBOLSO = "SELECT R.ID_REEMBOLSO, R.ID_TIPO_REEMBOLSO, R.ID_ESTATUS, R.AFECTADO, R.PARENT_AFECTADO, "
			+ "R.DESCRIPCION_REEMBOLSO, R.OBSERVACIONES, SUM(RC.IMPORTE) AS IMPORTE_TOTAL "
			+ "FROM REEMBOLSO R "
			+ "INNER JOIN REEMBOLSO_CONCEPTO RC ON R.ID_REEMBOLSO = RC.ID_REEMBOLSO "
			+ "WHERE R.ID_EMPLEADO = ? "
			+ "AND R.ID_REEMBOLSO = ? "
			+ "GROUP BY R.ID_REEMBOLSO, R.ID_TIPO_REEMBOLSO, R.ID_ESTATUS, R.AFECTADO, R.PARENT_AFECTADO, R.DESCRIPCION_REEMBOLSO, R.OBSERVACIONES";
			
	public static final String QUERY_REEMBOLSO_CONCEPTO = "SELECT RC.ID_CONCEPTO, RC.NO_DOCUMENTO, RC.NOMBRE, RC.CONCEPTO, RC.IMPORTE "
			+ "FROM REEMBOLSO_CONCEPTO RC WHERE RC.ID_REEMBOLSO = ?";
	
	public static final String QUERY_DEPENDIENTES = "SELECT E.NOMBRE ||' '|| E.A_PATERNO||' '|| E.A_MATERNO NOMBRE, "
			+ "TO_CHAR(E.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA_NAC, TO_CHAR(S.FECHA_AUTORIZACION,'dd-MM-yyyy') FECHA_ALTA, "
			+ "CS.DESCRIPCION, 'TITULAR' PARENTESCO FROM EMPLEADO E "
			+ "INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO "
			+ "INNER JOIN CAT_SEXO CS ON E.ID_SEXO = CS.ID_SEXO WHERE E.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ? "
			+ "UNION "
			+ "SELECT D.NOMBRE||' '||D.A_PATERNO||' '||D.A_MATERNO NOMBRE, TO_CHAR(D.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA_NAC, "
			+ "TO_CHAR(D.FECHA_ALTA_AUT,'dd-MM-yyyy') FECHA_ALTA, CS.DESCRIPCION, CP.PARENTESCO FROM EMPLEADO E "
			+ "INNER JOIN SOLICITUD S ON E.ID_EMPLEADO = S.ID_EMPLEADO "
			+ "INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD "
			+ "INNER JOIN CAT_SEXO CS ON D.ID_SEXO = CS.ID_SEXO "
			+ "INNER JOIN CAT_PARENTESCO CP ON D.ID_PARENTESCO = CP.ID_PARENTESCO "
			+ "WHERE E.ID_EMPLEADO = ? "
			+ "AND D.ID_TIPO_SOL = ? "
			+ "AND D.ID_ESTATUS = ?";
		
	public ReembolsoRelacionTO getReembolso(Integer numEmp, Integer idReembolso){
		ReembolsoRelacionTO datos = new ReembolsoRelacionTO();
		datos = jdbcTemplate.queryForObject(QUERY_REEMBOLSO, new RowMapper<ReembolsoRelacionTO>(){
			public ReembolsoRelacionTO mapRow(ResultSet rs, int rowNum) throws SQLException{
				ReembolsoRelacionTO datosTO = new ReembolsoRelacionTO();
				datosTO.setIdReembolso(rs.getInt(1));
				datosTO.setTipoReembolso(rs.getInt(2));
				datosTO.setIdEstatus(rs.getInt(3));
				datosTO.setNombreAfectado(rs.getString(4));
				datosTO.setParentescoTitular(rs.getString(5));
				datosTO.setDescripcionEnfermedad(rs.getString(6));
				datosTO.setObservaciones(rs.getString(7));
				datosTO.setImporteTotal(rs.getDouble(8));
				return datosTO;
			}
		}, numEmp,idReembolso);
		return datos;
	}
	
	public Integer getReembolsoID(Integer numEmp){
		Integer idReembolso = 0;
		try {
			idReembolso = jdbcTemplate.queryForObject(QUERY_REEMBOLSO_ID, Integer.class, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existe Solicitud");
			idReembolso = 0;
		}
		return idReembolso;
	}
	
	public List<Integer> getReembolsoListID(Integer numEmp){
		List<Integer> idReembolsos = new ArrayList<Integer>();	
		idReembolsos = jdbcTemplate.queryForList(QUERY_REEMBOLSO_ID_LIST, Integer.class, numEmp);
		logger.info("  idDependiente: "+idReembolsos.toString());
		return idReembolsos;	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependientes(Integer numEmp, Integer tipoSol, Integer estatusSol){
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		lista = jdbcTemplate.query(QUERY_DEPENDIENTES, new RowMapper(){
			public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				DependienteTO dependiente = new DependienteTO();
				dependiente.setNombre(rs.getString(1));
				dependiente.setFechaNac(rs.getString(2));
				dependiente.setFechaAltaAut(rs.getString(3));
				dependiente.setSexo(rs.getString(4));
				dependiente.setParentesco(rs.getString(5));
				return dependiente;
		}}, numEmp, tipoSol, estatusSol, numEmp, tipoSol, estatusSol);
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ReembolsoConceptoTO> getReembolsoConceptos(Integer idReembolso){
		List<ReembolsoConceptoTO> lista = new ArrayList<ReembolsoConceptoTO>();
		lista = jdbcTemplate.query(QUERY_REEMBOLSO_CONCEPTO, new RowMapper(){
			public ReembolsoConceptoTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				ReembolsoConceptoTO reembolso = new ReembolsoConceptoTO();
				reembolso.setIdReembolso(rs.getInt(1));
				reembolso.setNoDocumento(rs.getString(2));
				reembolso.setNombreExpediente(rs.getString(3));
				reembolso.setConcepto(rs.getString(4));
				reembolso.setImporte(rs.getDouble(5));
					
				return reembolso;
		}}, idReembolso);
		logger.info("  ListaReembolsos: "+lista.size());
		return lista;
	}
	
	public Integer setReembolso(ReembolsoRelacionTO reembolsoRelacionTO, Integer numEmp){
		final Integer numEmpInsert = numEmp;
		final Integer tipoReembolso = reembolsoRelacionTO.getTipoReembolso();
		final String nombreAfectado = reembolsoRelacionTO.getNombreAfectado();
		final String parentAfectado = reembolsoRelacionTO.getParentescoTitular();
		final String descEnfermedad = reembolsoRelacionTO.getDescripcionEnfermedad();
		final String observaciones = reembolsoRelacionTO.getObservaciones();
				
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				// Otra alternativa es Statement.RETURN_GENERATED_KEYS en lugar del param "Id_Reembolso"
				PreparedStatement ps = con.prepareStatement(INSERT_REEMBOLSO,new String[] {"Id_Reembolso"});
                ps.setInt(1, numEmpInsert);
                ps.setInt(2, tipoReembolso);
                ps.setString(3, nombreAfectado);
                ps.setString(4, parentAfectado);
                ps.setString(5, descEnfermedad);
                ps.setString(6, observaciones);
                return ps;
				}
			}, keyHolder);		
		logger.info("  getKey: "+keyHolder.getKey());
		Integer id = keyHolder.getKey().intValue();
		logger.info("  getKey: "+id);
		return id;
	}
	
	public Integer setReembolsoConcepto(ReembolsoConceptoTO reembolsoConceptoTO){
		Integer resultado = jdbcTemplate.update(INSERT_REEMBOLSO_CONCEPTO, 
				reembolsoConceptoTO.getNoDocumento(),
				reembolsoConceptoTO.getNombreExpediente(),
				reembolsoConceptoTO.getConcepto(),
				reembolsoConceptoTO.getImporte(),
				reembolsoConceptoTO.getIdReembolso());
		return resultado;
	}
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}