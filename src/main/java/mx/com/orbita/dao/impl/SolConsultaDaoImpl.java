package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SolConsultaDao;
import mx.com.orbita.to.AmpliacionTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

@Component
public class SolConsultaDaoImpl implements SolConsultaDao{

	private static final Logger logger = Logger.getLogger(SolConsultaDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_EMPLEADO = "SELECT E.ID_EMPLEADO, E.NOMBRE, E.A_PATERNO, E.A_MATERNO, E.PUESTO, E.FECHA_NACIMIENTO, "
			+ "E.FECHA_INGRESO, CT.SEXO, CR.CLAVE, E.ID_TIPO_EMPLEADO, (SELECT D.RFC FROM DATOS_FISCALES D WHERE D.ID_EMPLEADO = ?), TE.DESCRIPCION TIPOEMP "
			+ "FROM EMPLEADO E "
			+ "INNER JOIN CAT_SEXO CT on E.ID_SEXO = CT.ID_SEXO "
			+ "INNER JOIN CAT_REGION CR on E.ID_REGION = CR.ID_REGION " 
			+ "INNER JOIN CAT_TIPO_EMPLEADO TE ON E.ID_TIPO_EMPLEADO = TE.ID_TIPO_EMPLEADO "
			+ "WHERE E.ID_EMPLEADO = ?";
	
	public static final String QUERY_SOLICITUD_ID = "SELECT S.ID_SOLICITUD FROM SOLICITUD S " +
			"WHERE S.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS IN (?,?)";
	
	public static final String QUERY_SOLICITUD_A3 = "SELECT S.ID_SOLICITUD, S.COSTO_EMPLEADO, S.COSTO_DEPENDIENTES, " +
			"S.COSTO_AMPLIACION, S.COSTO_TOTAL, S.ID_TIPO_COB, T.DESCRIPCION " +
			"FROM SOLICITUD S INNER JOIN CAT_TIPO_COBERTURA T ON S.ID_TIPO_COB = T.ID_TIPO_COB " +
			"WHERE S.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?"; 
	
	public static final String QUERY_SOLICITUD_ALTA = "SELECT S.ID_SOLICITUD, S.COSTO_EMPLEADO, S.COSTO_DEPENDIENTES, " +
			"S.COSTO_AMPLIACION,S.COSTO_COB_MEMORIAL,S.COSTO_TOTAL, TO_CHAR(S.FECHA_ELABORACION,'dd Mon yyyy'), TO_CHAR(S.FECHA_AUTORIZACION,'dd Mon yyyy'), " +
			"(SELECT A.NOMBRE ||' '||A.A_PATERNO ||' '||A.A_MATERNO FROM ADMINISTRADOR A WHERE A.ID_EMPLEADO = S.ID_ADMIN_AUT_ALTA), " +
			"S.ID_ESTATUS, CE.DESCRIPCION, CASE TC.DESCRIPCION WHEN 'N' THEN ' ' ELSE TC.DESCRIPCION END, TCM.CLAVE " +
			"FROM SOLICITUD S INNER JOIN CAT_ESTATUS_SOL CE ON S.ID_ESTATUS = CE.ID_ESTATUS " +
			"INNER JOIN CAT_TIPO_COBERTURA TC ON S.ID_TIPO_COB = TC.ID_TIPO_COB " +
			"INNER JOIN CAT_TIPO_COBERTURA_MEMORIAL TCM ON S.ID_TIPO_COB_MEM = TCM.ID_TIPO_COB_MEM "+ 
			"WHERE S.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS IN (?,?)"; 
	
	public static final String QUERY_SOLICITUD_COB = "SELECT S.ID_SOLICITUD,S.ID_TIPO_COB,C.DESCRIPCION " +
			"FROM SOLICITUD S INNER JOIN CAT_TIPO_COBERTURA C ON S.ID_TIPO_COB = C.ID_TIPO_COB " +
			"WHERE S.ID_EMPLEADO = ? AND S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ?";
	
	public static final String QUERY_TIPO_COB = "SELECT ID_TIPO_COB,DESCRIPCION FROM CAT_TIPO_COBERTURA ORDER BY ID_TIPO_COB ASC";
	
	public static final String QUERY_TIPO_COB_MEM = "SELECT ID_TIPO_COB_MEM,DESCRIPCION FROM CAT_TIPO_COBERTURA_MEMORIAL ORDER BY ID_TIPO_COB_MEM ASC";
	
	public static final String QUERY_TIPO_COB_ID = "SELECT DESCRIPCION FROM CAT_TIPO_COBERTURA WHERE ID_TIPO_COB = ?";
	
	public static final String QUERY_SOLICITUD_BAJA = "SELECT S.ID_SOLICITUD, S.COSTO_EMPLEADO, S.COSTO_DEPENDIENTES, S.COSTO_AMPLIACION, "+
			"S.COSTO_TOTAL, TO_CHAR(S.FECHA_CANCELACION,'dd Mon yyyy'), TO_CHAR(S.FECHA_CANCELACION_AUT,'dd Mon yyyy'), " +
			"(SELECT A.NOMBRE ||' '||A.A_PATERNO ||' '||A.A_MATERNO FROM ADMINISTRADOR A WHERE A.ID_EMPLEADO = S.ID_ADMIN_AUT_BAJA), CE.DESCRIPCION "+
			"FROM SOLICITUD S INNER JOIN CAT_ESTATUS_SOL CE ON S.ID_ESTATUS = CE.ID_ESTATUS "+
			"WHERE S.ID_EMPLEADO = ? AND ((S.ID_TIPO_SOL IN (?) AND S.ID_ESTATUS IN (?)) OR (S.ID_TIPO_SOL IN (?) AND S.ID_ESTATUS IN (?,?,?)))"; 
	
	public static final String QUERY_DEPENDIENTE_ALTA = "SELECT D.ID_DEPENDIENTE, D.ID_PARENTESCO, CP.PARENTESCO, D.A_PATERNO, D.A_MATERNO," +
			" D.NOMBRE, TO_CHAR(D.FECHA_NACIMIENTO,'DD-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO, D.COSTO_DEPENDIENTE " +
			"FROM DEPENDIENTE D " +
			"INNER JOIN CAT_PARENTESCO CP ON D.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_SEXO CS ON D.ID_SEXO = CS.ID_SEXO " +
			"WHERE D.ID_SOLICITUD = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS IN (?,?)";
	
	public static final String QUERY_DEPENDIENTE_ALTA_RENOV = "SELECT D.ID_DEPENDIENTE, D.ID_PARENTESCO, CP.PARENTESCO, D.A_PATERNO, D.A_MATERNO," +
			"D.NOMBRE, " +
			"TO_CHAR(D.FECHA_NACIMIENTO,'yyyy-MM-dd') FECHA_NACIMIENTO," +
			"TO_CHAR(D.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA," +
			"CS.SEXO, " +
			"D.COSTO_DEPENDIENTE " +
			"FROM DEPENDIENTE D " +
			"INNER JOIN CAT_PARENTESCO CP ON D.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_SEXO CS ON D.ID_SEXO = CS.ID_SEXO " +
			"WHERE D.ID_SOLICITUD = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS IN (?,?)";
	
	public static final String QUERY_DEPENDIENTE_ALTA_X_ID = "select D.ID_DEPENDIENTE from DEPENDIENTE D where D.ID_SOLICITUD = ? " +
			"AND D.ID_TIPO_SOL = ? AND D.ID_ESTATUS IN (?,?,?) order by D.ID_DEPENDIENTE desc";
	
	public static final String QUERY_DEPENDIENTE_DETALLE_ALTA ="SELECT D.ID_DEPENDIENTE, CT.PARENTESCO, D.A_PATERNO, D.A_MATERNO, D.NOMBRE, "+ 
			"TO_CHAR(D.FECHA_NACIMIENTO,'DD-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO,D.COSTO_DEPENDIENTE, " +
			"CE.DESCRIPCION, (SELECT A.NOMBRE ||' '||A.A_PATERNO ||' '||A.A_MATERNO FROM ADMINISTRADOR A WHERE A.ID_EMPLEADO = D.ID_ADMIN_AUT_ALTA), " +
			"TO_CHAR(D.FECHA_ALTA_AUT,'dd Mon yyyy'), TO_CHAR(D.FECHA_ALTA,'dd Mon yyyy') "+ 
			"FROM DEPENDIENTE D "+ 
			"INNER JOIN CAT_PARENTESCO CT ON D.ID_PARENTESCO = CT.ID_PARENTESCO "+ 
			"INNER JOIN CAT_SEXO CS ON D.ID_SEXO = CS.ID_SEXO "+ 
			"INNER JOIN CAT_ESTATUS_SOL CE ON D.ID_ESTATUS = CE.ID_ESTATUS "+ 
			"WHERE D.ID_DEPENDIENTE = ?";
	
	public static final String QUERY_DEPENDIENTE_DETALLE_BAJA ="select D.ID_DEPENDIENTE, CT.PARENTESCO, D.A_PATERNO, D.A_MATERNO, D.NOMBRE, "+ 
			"TO_CHAR(D.FECHA_NACIMIENTO,'DD-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO,D.COSTO_DEPENDIENTE, " +
			"CE.DESCRIPCION, (SELECT A.NOMBRE ||' '||A.A_PATERNO ||' '||A.A_MATERNO FROM ADMINISTRADOR A WHERE A.ID_EMPLEADO = D.ID_ADMIN_AUT_BAJA), " +
			"TO_CHAR(D.FECHA_BAJA_AUT,'dd Mon yyyy'), TO_CHAR(D.FECHA_BAJA,'dd Mon yyyy') "+ 
			"from DEPENDIENTE D "+ 
			"inner join CAT_PARENTESCO CT on D.ID_PARENTESCO = CT.ID_PARENTESCO "+ 
			"inner join CAT_SEXO CS on D.ID_SEXO = CS.ID_SEXO "+ 
			"inner join CAT_ESTATUS_SOL CE on D.ID_ESTATUS = CE.ID_ESTATUS "+ 
			"where D.ID_DEPENDIENTE = ?";	
	
	public static final String QUERY_DEPENDIENTE_BAJA = "SELECT D.ID_DEPENDIENTE, CP.PARENTESCO, D.A_PATERNO, D.A_MATERNO, "+
			"D.NOMBRE, TO_CHAR(D.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO, D.COSTO_DEPENDIENTE "+
			"FROM DEPENDIENTE D "+
			"INNER JOIN CAT_PARENTESCO CP ON D.ID_PARENTESCO = CP.ID_PARENTESCO "+
			"INNER JOIN CAT_SEXO CS ON D.ID_SEXO = CS.ID_SEXO "+
			"WHERE D.ID_SOLICITUD = ? AND D.ID_TIPO_SOL IN(?,?) AND D.ID_ESTATUS IN(?,?,?)";
	
	public static final String QUERY_DEPENDIENTE_BAJA_X_ID = "SELECT D.ID_DEPENDIENTE FROM DEPENDIENTE D "+ 
			"WHERE D.ID_SOLICITUD = ? AND ((D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?) " +
			"OR (D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?) " +
			"OR (D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?) " +
			"OR (D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?) " +
			"OR (D.ID_TIPO_SOL = ? AND D.ID_ESTATUS = ?)) " + 
			"order by D.ID_DEPENDIENTE desc";
	
	public static final String QUERY_CONSULMED_ALTA = "SELECT C.ID_CONSULMED,C.ID_PARENTESCO,CP.PARENTESCO,C.A_PATERNO, C.A_MATERNO, " +
			"C.NOMBRE, TO_CHAR(C.FECHA_NACIMIENTO,'DD/MM/yyyy') FECHA_NACIMIENTO, CS.SEXO, C.COSTO_CONSULMED " +
			"FROM CONSULMED C " +
			"INNER JOIN CAT_PARENTESCO CP ON C.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_SEXO CS ON C.ID_SEXO = CS.ID_SEXO " +
			"WHERE C.ID_SOLICITUD = ? " +
			"AND C.ID_TIPO_SOL = ? " +
			"AND C.ID_ESTATUS IN (?,?)";
	
	public static final String QUERY_CONSULMED_ALTA_X_ID = "select C.ID_CONSULMED from CONSULMED C where C.ID_SOLICITUD = ? " +
			"AND C.ID_TIPO_SOL = ? AND C.ID_ESTATUS IN (?,?,?) order by C.ID_CONSULMED desc";
	
	public static final String QUERY_CONSULMED_DETALLE_ALTA = "SELECT C.ID_CONSULMED, CT.PARENTESCO, C.A_PATERNO, C.A_MATERNO, C.NOMBRE, "+ 
			"TO_CHAR(C.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO, C.COSTO_CONSULMED, CE.DESCRIPCION, " +
			"(SELECT A.NOMBRE ||' '||A.A_PATERNO ||' '||A.A_MATERNO FROM ADMINISTRADOR A WHERE A.ID_EMPLEADO = C.ID_ADMIN_AUT_ALTA), " +
			"TO_CHAR(C.FECHA_ALTA_AUT,'dd Mon yyyy'), TO_CHAR(C.FECHA_ALTA,'dd Mon yyyy') "+ 
			"FROM CONSULMED C "+ 
			"INNER JOIN CAT_PARENTESCO CT ON C.ID_PARENTESCO = CT.ID_PARENTESCO "+ 
			"INNER JOIN CAT_SEXO CS ON C.ID_SEXO = CS.ID_SEXO "+ 
			"INNER JOIN CAT_ESTATUS_SOL CE ON C.ID_ESTATUS = CE.ID_ESTATUS "+ 
			"WHERE C.ID_CONSULMED = ?";
	
	public static final String QUERY_CONSULMED_DETALLE_BAJA = "select C.ID_CONSULMED, CT.PARENTESCO, C.A_PATERNO, C.A_MATERNO, C.NOMBRE, "+ 
			"TO_CHAR(C.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO, C.COSTO_CONSULMED, CE.DESCRIPCION, " +
			"(SELECT A.NOMBRE ||' '||A.A_PATERNO ||' '||A.A_MATERNO FROM ADMINISTRADOR A WHERE A.ID_EMPLEADO = C.ID_ADMIN_AUT_BAJA), " +
			"TO_CHAR(C.FECHA_BAJA_AUT,'dd Mon yyyy'), TO_CHAR(C.FECHA_BAJA,'dd Mon yyyy') "+ 
			"from CONSULMED C "+ 
			"inner join CAT_PARENTESCO CT on C.ID_PARENTESCO = CT.ID_PARENTESCO "+ 
			"inner join CAT_SEXO CS on C.ID_SEXO = CS.ID_SEXO "+ 
			"inner join CAT_ESTATUS_SOL CE on C.ID_ESTATUS = CE.ID_ESTATUS "+ 
			"where C.ID_CONSULMED = ?";
	
	public static final String QUERY_CONSULMED_BAJA = "SELECT C.ID_CONSULMED, CP.PARENTESCO,C.A_PATERNO, C.A_MATERNO, "+
			"C.NOMBRE, TO_CHAR(C.FECHA_NACIMIENTO,'dd-MM-yyyy') FECHA_NACIMIENTO, CS.SEXO,C.COSTO_CONSULMED "+
			"FROM CONSULMED C "+
			"INNER JOIN CAT_PARENTESCO CP ON C.ID_PARENTESCO = CP.ID_PARENTESCO "+
			"INNER JOIN CAT_SEXO CS ON C.ID_SEXO = CS.ID_SEXO "+
			"WHERE C.ID_SOLICITUD = ? AND C.ID_TIPO_SOL IN(?,?)  AND C.ID_ESTATUS IN(?,?,?)";
	
	public static final String QUERY_CONSULMED_BAJA_X_ID = "SELECT C.ID_CONSULMED FROM CONSULMED C "+ 
			"WHERE C.ID_SOLICITUD = ? AND ((C.ID_TIPO_SOL = ? AND C.ID_ESTATUS = ?) " +
			"OR (C.ID_TIPO_SOL = ? AND C.ID_ESTATUS = ?) " +
			"OR (C.ID_TIPO_SOL = ? AND C.ID_ESTATUS = ?) " +
			"OR (C.ID_TIPO_SOL = ? AND C.ID_ESTATUS = ?) " +
			"OR (C.ID_TIPO_SOL = ? AND C.ID_ESTATUS = ?)) " +
			"order by C.ID_CONSULMED desc";
	
	public static final String QUERY_CONSULMED_ID_PARENTESCO = "SELECT C.ID_PARENTESCO FROM SOLICITUD S " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"WHERE S.ID_EMPLEADO = ? " +
			"AND S.ID_TIPO_SOL = ? " +
			"AND S.ID_ESTATUS IN (?,?) " +
			"AND C.ID_ESTATUS NOT IN (?,?,?,?,?,?)";
	
	public static final String QUERY_CONSULMED_TOTAL = "SELECT COUNT(C.ID_CONSULMED) FROM CONSULMED C " +
			 "WHERE C.ID_SOLICITUD = (SELECT S.ID_SOLICITUD FROM SOLICITUD S WHERE S.ID_TIPO_SOL = ? AND S.ID_ESTATUS = ? AND S.ID_EMPLEADO = ?) " +
			 "AND C.ID_TIPO_SOL = ? AND C.ID_ESTATUS IN (?,?)";
	
	public static final String QUERY_MEMORIAL_ALTA = "SELECT M.ID_MEMORIAL,M.ID_PARENTESCO,CP.PARENTESCO,M.A_PATERNO,M.A_MATERNO," +
			"M.NOMBRE, TO_CHAR(M.FECHA_NACIMIENTO,'DD/MM/yyyy') FECHA_NACIMIENTO, CS.SEXO " +
			"FROM MEMORIAL M " +
			"INNER JOIN CAT_PARENTESCO CP ON M.ID_PARENTESCO = CP.ID_PARENTESCO " +
			"INNER JOIN CAT_SEXO CS ON M.ID_SEXO = CS.ID_SEXO " +
			"WHERE M.ID_SOLICITUD = ?";
		
	// EMPLEADO

	/**
	 * Metodo que retorna un empleado
	 */
	public EmpleadoTO getEmpleadoBD(Integer numEmp){
		EmpleadoTO datos = new EmpleadoTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_EMPLEADO, new RowMapper<EmpleadoTO>(){
				public EmpleadoTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					EmpleadoTO empleadoTO = new EmpleadoTO();
					empleadoTO.setNumEmp(rs.getInt(1));
					empleadoTO.setNombre(rs.getString(2));
					empleadoTO.setApellidoP(rs.getString(3));
					empleadoTO.setApellidoM(rs.getString(4));
					empleadoTO.setPuesto(rs.getString(5));
					empleadoTO.setFechaNac(rs.getDate(6));
					empleadoTO.setFechaIng(rs.getDate(7));
					empleadoTO.setSexo(rs.getString(8));
					empleadoTO.setRegion(rs.getString(9));
					empleadoTO.setTipoEmp(rs.getInt(10));
					empleadoTO.setRfc(rs.getString(11));
					empleadoTO.setDescTipoEmp(rs.getString(12));
					return empleadoTO;
				}
			}, numEmp, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			datos.setNumEmp(0);
		}
		return datos;
	}
		
	// SOLICITUD
	
	/**
	 * Metodo que retorna el Id de la solicitud
	 */
	public Integer getSolicitudId(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB){
		Integer existeSol = 0;
		try {
			existeSol = jdbcTemplate.queryForObject(QUERY_SOLICITUD_ID, Integer.class, numEmp, tipoSol, estatusSolA, estatusSolB);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- Excepcion [No existe sol.] -- "+e.getMessage());
			existeSol = 0;
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.info("  -- Excepcion [Sol. duplicada] -- "+e.getMessage());
			existeSol = -1;
		}
		return existeSol;
	}	
	
	/**
	 * Metodo que retorna una solicitud en cualquier estatus
	 */
	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		SolicitudTO solicitudTO = new SolicitudTO();
		try {
			solicitudTO = jdbcTemplate.queryForObject(QUERY_SOLICITUD_A3, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitud = new SolicitudTO();
					solicitud.setIdSol(rs.getInt(1));
					solicitud.setCostoEmpleado(rs.getDouble(2));
					solicitud.setCostoDependiente(rs.getDouble(3));
					solicitud.setCostoAmpliacion(rs.getDouble(4));
					solicitud.setCostoTotal(rs.getDouble(5));
					solicitud.setIdTipoCobertura(rs.getInt(6));
					solicitud.setTipoCobertura(rs.getString(7));
					return solicitud;
				}
			}, numEmp,tipoSol,estatusSol);
		} catch (EmptyResultDataAccessException e) {
			solicitudTO.setIdSol(0);
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return solicitudTO;
	}
	
	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB) {
		SolicitudTO solicitudTO = new SolicitudTO();
		try {
			solicitudTO = jdbcTemplate.queryForObject(QUERY_SOLICITUD_ALTA, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitud = new SolicitudTO();
					solicitud.setIdSol(rs.getInt(1));
					solicitud.setCostoEmpleado(rs.getDouble(2));
					solicitud.setCostoDependiente(rs.getDouble(3));
					solicitud.setCostoAmpliacion(rs.getDouble(4));
						solicitud.setCostoMemorial(rs.getDouble(5));
					solicitud.setCostoTotal(rs.getDouble(6));
					solicitud.setFechaElaboracion(rs.getString(7));
					solicitud.setFechaAutorizacion(rs.getString(8));
					solicitud.setAdminAutAlta(rs.getString(9));
					solicitud.setIdEstatusSol(rs.getInt(10));
					solicitud.setEstatusSol(rs.getString(11));	
					solicitud.setTipoCobertura(rs.getString(12));
						solicitud.setTipoMemorial(rs.getString(13));
					return solicitud;
				}
			}, numEmp, tipoSol, estatusSolA,estatusSolB);
		} catch (EmptyResultDataAccessException e) {
			solicitudTO.setIdSol(0);
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return solicitudTO;
	}
	
	public SolicitudTO getSolicitudCob(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		SolicitudTO solicitudTO = new SolicitudTO();
		try {
			solicitudTO = jdbcTemplate.queryForObject(QUERY_SOLICITUD_COB, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitud = new SolicitudTO();
					solicitud.setIdSol(rs.getInt(1));
					solicitud.setIdTipoCobertura(rs.getInt(2));
					solicitud.setTipoCobertura(rs.getString(3));
					return solicitud;
				}
			}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			solicitudTO.setIdSol(0);
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return solicitudTO;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AmpliacionTO> getTipoCob() {
		List<AmpliacionTO> lista = new ArrayList<AmpliacionTO>();
		lista = jdbcTemplate.query(QUERY_TIPO_COB, new RowMapper(){
			public AmpliacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AmpliacionTO cobertura = new AmpliacionTO();
				cobertura.setIdTipoCob(rs.getInt(1));
				cobertura.setDescripcion(rs.getString(2));
				return cobertura;
			}});
		return lista;	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AmpliacionTO> getTipoCobMem() {
		List<AmpliacionTO> lista = new ArrayList<AmpliacionTO>();
		lista = jdbcTemplate.query(QUERY_TIPO_COB_MEM, new RowMapper(){
			public AmpliacionTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				AmpliacionTO cobertura = new AmpliacionTO();
		        cobertura.setIdTipoCob(rs.getInt(1));
		        cobertura.setDescripcion(rs.getString(2));
		        return cobertura;
			}});
		return lista;
	}
	
	public String getTipoCob(Integer idTipoCob){
		String cobertura = jdbcTemplate.queryForObject(QUERY_TIPO_COB_ID, String.class, idTipoCob);
		return cobertura;
	}
	
		
	/**
	 * Metodo que retorna una solicitud de acuerdo a un estatus
	 */
	public SolicitudTO getSolicitudBaja(Integer numEmp, Integer tipoSolA, Integer estatusSolA, 
			Integer tipoSolB, Integer estatusSolB, Integer estatusSolC, Integer estatusSolD) {
		SolicitudTO solicitudTO = new SolicitudTO();
		try {
			solicitudTO = jdbcTemplate.queryForObject(QUERY_SOLICITUD_BAJA, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitud = new SolicitudTO();
					solicitud.setIdSol(rs.getInt(1));
					solicitud.setCostoEmpleado(rs.getDouble(2));
					solicitud.setCostoDependiente(rs.getDouble(3));
					solicitud.setCostoAmpliacion(rs.getDouble(4));
					solicitud.setCostoTotal(rs.getDouble(5));
					solicitud.setFechaElaboracion(rs.getString(6));
					solicitud.setFechaCancelacionAut(rs.getString(7));
					solicitud.setAdminAutBaja(rs.getString(8));
					solicitud.setEstatusSol(rs.getString(9));			
					return solicitud;
				}
			}, numEmp, tipoSolA, estatusSolA, tipoSolB, estatusSolB, estatusSolC, estatusSolD);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return solicitudTO;
	}
	
	// DEPENDIENTE
	
	/**
	 * Metodo que retorna una Lista de Dependientes
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE_ALTA, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setIdParentesco(rs.getInt(2));
					dependiente.setParentesco(rs.getString(3));
					dependiente.setApellidoP(rs.getString(4));
					dependiente.setApellidoM(rs.getString(5));
					dependiente.setNombre(rs.getString(6));
					dependiente.setFechaNac(rs.getString(7));
					dependiente.setSexo(rs.getString(8));
					dependiente.setCostoDep(rs.getDouble(9));
					return dependiente;
			}}, idSolicitud, tipoSol, estatusSolA, estatusSolB);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	/**
	 * Metodo que retorna una Lista de Dependientes
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependienteRenov(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE_ALTA_RENOV, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setIdParentesco(rs.getInt(2));
					dependiente.setParentesco(rs.getString(3));
					dependiente.setApellidoP(rs.getString(4));
					dependiente.setApellidoM(rs.getString(5));
					dependiente.setNombre(rs.getString(6));
					dependiente.setFechaNac(rs.getString(7));
					dependiente.setFechaAlta(rs.getString(8));
					dependiente.setSexo(rs.getString(9));
					dependiente.setCostoDep(rs.getDouble(10));
					return dependiente;
			}}, idSolicitud, tipoSol, estatusSolA, estatusSolB);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	/**
	 * Metodo que retorna una lista de Dependientes por Id
	 */
	public List<Integer> getIdDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB, Integer estatusSolC){
		List<Integer> idDependiente = new ArrayList<Integer>();	
		idDependiente = jdbcTemplate.queryForList(QUERY_DEPENDIENTE_ALTA_X_ID, Integer.class, idSolicitud, tipoSol, estatusSolA, estatusSolB, estatusSolC);
		return idDependiente;
	}
	
	/**
	 * Metodo que retorna el detalle del Alta del Dependiente
	 */
	public DependienteTO getDependienteAlta(Integer idDependiente) {
		DependienteTO datos = new DependienteTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_DEPENDIENTE_DETALLE_ALTA, new RowMapper<DependienteTO>(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					DependienteTO datosTO = new DependienteTO();
					datosTO.setIdDependiente(rs.getInt(1));
					datosTO.setParentesco(rs.getString(2));
					datosTO.setApellidoP(rs.getString(3));
					datosTO.setApellidoM(rs.getString(4));
					datosTO.setNombre(rs.getString(5));
					datosTO.setFechaNac(rs.getString(6));
					datosTO.setSexo(rs.getString(7));
					datosTO.setCostoDep(rs.getDouble(8));
					datosTO.setEstatusDep(rs.getString(9));
					datosTO.setAdminAutAlta(rs.getString(10));
					datosTO.setFechaAltaAut(rs.getString(11));
					datosTO.setFechaAlta(rs.getString(12));
					return datosTO;
				}
			}, idDependiente);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return datos;
	}
	
	/**
	 * Metodo que retorna el detalle de Baja del Dependiente
	 */
	public DependienteTO getDependienteBaja(Integer idDependiente) {
		DependienteTO datos = new DependienteTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_DEPENDIENTE_DETALLE_BAJA, new RowMapper<DependienteTO>(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					DependienteTO datosTO = new DependienteTO();
					datosTO.setIdDependiente(rs.getInt(1));
					datosTO.setParentesco(rs.getString(2));
					datosTO.setApellidoP(rs.getString(3));
					datosTO.setApellidoM(rs.getString(4));
					datosTO.setNombre(rs.getString(5));
					datosTO.setFechaNac(rs.getString(6));
					datosTO.setSexo(rs.getString(7));
					datosTO.setCostoDep(rs.getDouble(8));
					datosTO.setEstatusDep(rs.getString(9));
					datosTO.setAdminAutBaja(rs.getString(10));
					datosTO.setFechaBajaAut(rs.getString(11));
					datosTO.setFechaAlta(rs.getString(12));
					return datosTO;
				}
			}, idDependiente);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return datos;
	}
	
	/**
	 * Metodo que retorna los dependientes en procesos de baja
	 * @param idSolicitud
	 * @param tipoSol
	 * @param estatusSol
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependienteBaja(Integer idSolicitud, Integer tipoSolA, Integer tipoSolB, 
			Integer estatusSolA, Integer estatusSolB, Integer estatusSolC) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE_BAJA, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(1));
					dependiente.setParentesco(rs.getString(2));
					dependiente.setApellidoP(rs.getString(3));
					dependiente.setApellidoM(rs.getString(4));
					dependiente.setNombre(rs.getString(5));
					dependiente.setFechaNac(rs.getString(6));
					dependiente.setSexo(rs.getString(7));
					dependiente.setCostoDep(rs.getDouble(8));
					return dependiente;
			}}, idSolicitud, tipoSolA, tipoSolB, estatusSolA, estatusSolB, estatusSolC);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	/**
	 * Metodo que retorna una Lista de Dependientes por Id
	 */
	public List<Integer> getDependienteBajaxId(Integer idSolicitud, Integer tipoSolA, Integer estatusSolA, 
			Integer tipoSolB, Integer estatusSolB, Integer estatusSolC, Integer estatusSolD, Integer estatusSolE){
		List<Integer> idDependiente = new ArrayList<Integer>();	
		idDependiente = jdbcTemplate.queryForList(QUERY_DEPENDIENTE_BAJA_X_ID, Integer.class, idSolicitud, tipoSolA, estatusSolA, 
				tipoSolB, estatusSolB, tipoSolB, estatusSolC, tipoSolB, estatusSolD, tipoSolB, estatusSolE);
		return idDependiente;
	}
	
	// CONSULMED
	
	/**
	 * Metodo que obtiene los Dependientes Consulmed Autorizados
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ConsulmedTO> getConsulmed(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB) {
		List<ConsulmedTO> lista = new ArrayList<ConsulmedTO>();
		try {
			lista = jdbcTemplate.query(QUERY_CONSULMED_ALTA, new RowMapper(){
				public ConsulmedTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ConsulmedTO consulmed = new ConsulmedTO();
					consulmed.setIdConsulmed(rs.getInt(1));
					consulmed.setIdParentesco(rs.getInt(2));
					consulmed.setParentesco(rs.getString(3));
					consulmed.setApellidoP(rs.getString(4));
					consulmed.setApellidoM(rs.getString(5));
					consulmed.setNombre(rs.getString(6));
					consulmed.setFechaNac(rs.getString(7));
					consulmed.setSexo(rs.getString(8));
					consulmed.setCostoConsul(rs.getDouble(9));
					return consulmed;
			}}, idSolicitud, tipoSol, estatusSolA, estatusSolB);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	/**
	 * Metodo que retorna una Lista de Altas Dependientes Consulmed por Id
	 */
	public List<Integer> getIdConsulmed(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB, Integer estatusSolC){
		List<Integer> idDependiente = new ArrayList<Integer>();	
		idDependiente = jdbcTemplate.queryForList(QUERY_CONSULMED_ALTA_X_ID, Integer.class, idSolicitud, tipoSol, estatusSolA, estatusSolB, estatusSolC);
		return idDependiente;
	}
	
	/**
	 * Metodo que retorna el detalle de un Alta de Dependiente Consulmed 
	 */
	public ConsulmedTO getConsulmedAlta(Integer idDependiente) {
		ConsulmedTO consulmedTO = new ConsulmedTO();
		try {
			consulmedTO = jdbcTemplate.queryForObject(QUERY_CONSULMED_DETALLE_ALTA, new RowMapper<ConsulmedTO>(){
				public ConsulmedTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					ConsulmedTO consulmed = new ConsulmedTO();
					consulmed.setIdConsulmed(rs.getInt(1));
					consulmed.setParentesco(rs.getString(2));
					consulmed.setApellidoP(rs.getString(3));
					consulmed.setApellidoM(rs.getString(4));
					consulmed.setNombre(rs.getString(5));
					consulmed.setFechaNac(rs.getString(6));
					consulmed.setSexo(rs.getString(7));
					consulmed.setCostoConsul(rs.getDouble(8));
					consulmed.setEstatusDep(rs.getString(9));
					consulmed.setAdminAutAlta(rs.getString(10));
					consulmed.setFechaAltaAut(rs.getString(11));
					consulmed.setFechaAlta(rs.getString(12));
					return consulmed;
				}
			}, idDependiente);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return consulmedTO;
	}
	
	/**
	 * Metodo que retorna el detalle de una Baja de Dependiente Consulmed 
	 */
	public ConsulmedTO getConsulmedBaja(Integer idDependiente) {
		ConsulmedTO consulmedTO = new ConsulmedTO();
		try {
			consulmedTO = jdbcTemplate.queryForObject(QUERY_CONSULMED_DETALLE_BAJA, new RowMapper<ConsulmedTO>(){
				public ConsulmedTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					ConsulmedTO consulmed = new ConsulmedTO();
					consulmed.setIdConsulmed(rs.getInt(1));
					consulmed.setParentesco(rs.getString(2));
					consulmed.setApellidoP(rs.getString(3));
					consulmed.setApellidoM(rs.getString(4));
					consulmed.setNombre(rs.getString(5));
					consulmed.setFechaNac(rs.getString(6));
					consulmed.setSexo(rs.getString(7));
					consulmed.setCostoConsul(rs.getDouble(8));
					consulmed.setEstatusDep(rs.getString(9));
					consulmed.setAdminAutBaja(rs.getString(10));
					consulmed.setFechaBajaAut(rs.getString(11));
					consulmed.setFechaAlta(rs.getString(12));
					return consulmed;
				}
			}, idDependiente);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return consulmedTO;
	}
		
	/**
	 * Metodo que obtiene los Dependientes Consulmed en proceso de baja
	 * @param idSolicitud
	 * @param tipoSol
	 * @param estatusSol
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ConsulmedTO> getConsulmedBaja(Integer idSolicitud, Integer tipoSolA, Integer tipoSolB, 
			Integer estatusSolA, Integer estatusSolB, Integer estatusSolC) {
		List<ConsulmedTO> lista = new ArrayList<ConsulmedTO>();
		try {
			lista = jdbcTemplate.query(QUERY_CONSULMED_BAJA, new RowMapper(){
				public ConsulmedTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ConsulmedTO consulmed = new ConsulmedTO();
					consulmed.setIdConsulmed(rs.getInt(1));
					consulmed.setParentesco(rs.getString(2));
					consulmed.setApellidoP(rs.getString(3));
					consulmed.setApellidoM(rs.getString(4));
					consulmed.setNombre(rs.getString(5));
					consulmed.setFechaNac(rs.getString(6));
					consulmed.setSexo(rs.getString(7));
					consulmed.setCostoConsul(rs.getDouble(8));
					return consulmed;
			}}, idSolicitud, tipoSolA, tipoSolB, estatusSolA, estatusSolB, estatusSolC);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	/**
	 * Metodo que retorna una Lista de Dependientes Consulmed por Id
	 */
	public List<Integer> getConsulmedBajaxId(Integer idSolicitud, Integer tipoSolA, Integer estatusSolA, 
			Integer tipoSolB, Integer estatusSolB, Integer estatusSolC, Integer estatusSolD, Integer estatusSolE){
		List<Integer> idDependiente = new ArrayList<Integer>();	
		idDependiente = jdbcTemplate.queryForList(QUERY_CONSULMED_BAJA_X_ID, Integer.class, idSolicitud, tipoSolA, estatusSolA, 
				tipoSolB, estatusSolB, tipoSolB, estatusSolC, tipoSolB, estatusSolD, tipoSolB, estatusSolE);
		return idDependiente;
	}

	
	/**
	 * Metodo que retorno los parentescos de los Dependientes Consulmed dados de alta
	 * @param tipoSol
	 * @param numEmp
	 * @return
	 */
	public List<Integer> getConsulmedIdParentesco(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB,
			Integer estatusConA, Integer estatusConB, Integer estatusConC, Integer estatusConD, Integer estatusConE, Integer estatusConF){
		List<Integer> parientes = jdbcTemplate.queryForList(QUERY_CONSULMED_ID_PARENTESCO, Integer.class, numEmp, tipoSol, estatusSolA, estatusSolB,
				estatusConA,estatusConB,estatusConC,estatusConD,estatusConE,estatusConF);
		return parientes;
	}
	
	/**
	 * Metodo que retorna el total de Dependientes Consulmed
	 * @param tipoSol
	 * @param numEmp
	 * @return
	 */
	public Integer getConsulmedTotal(Integer tipoSol, Integer estatusSol, Integer numEmp, Integer estatusDepA, Integer estatusDepB){
		Integer existeSol = 0;
		existeSol = jdbcTemplate.queryForObject(QUERY_CONSULMED_TOTAL, Integer.class, tipoSol, estatusSol, numEmp, tipoSol, estatusDepA, estatusDepB);
		return existeSol;
	}
		
	
	// MEMORIAL
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ConsulmedTO> getMemorial(Integer idSolicitud) {
		List<ConsulmedTO> lista = new ArrayList<ConsulmedTO>();
		try {
			lista = jdbcTemplate.query(QUERY_MEMORIAL_ALTA, new RowMapper(){
				public ConsulmedTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ConsulmedTO consulmed = new ConsulmedTO();
					consulmed.setIdConsulmed(rs.getInt(1));
					consulmed.setIdParentesco(rs.getInt(2));
					consulmed.setParentesco(rs.getString(3));
					consulmed.setApellidoP(rs.getString(4));
					consulmed.setApellidoM(rs.getString(5));
					consulmed.setNombre(rs.getString(6));
					consulmed.setFechaNac(rs.getString(7));
					consulmed.setSexo(rs.getString(8));
					return consulmed;
			}}, idSolicitud);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}