package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.FiniquitoDao;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.FiniquitoTO;
import mx.com.orbita.to.SolicitudTO;

@Component
public class FiniquitoDaoImpl implements FiniquitoDao {
	
	private static final Logger logger = Logger.getLogger(FiniquitoDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public static final String QUERY_DEPENDIENTE = "SELECT E.ID_EMPLEADO, D.ID_DEPENDIENTE, D.ID_PARENTESCO, " +
			"D.ID_SEXO, CT.SEXO, to_char(D.FECHA_NACIMIENTO,'yyyy-mm-dd'), " +
			"TO_CHAR(D.FECHA_ALTA_AUT,'YYYY') AS ANIO, " +
			"TO_CHAR(D.FECHA_ALTA_AUT,'MM') AS MES, TO_CHAR(D.FECHA_ALTA_AUT,'DD') AS DIA, " +
			"TO_CHAR(D.FECHA_RENOVACION,'YYYY') AS ANIO_RENOV, TO_CHAR(D.FECHA_RENOVACION,'MM') AS MES_RENOV, " +
			"TO_CHAR(D.FECHA_RENOVACION,'DD') AS DIA_RENOV, D.COSTO_DEPENDIENTE " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"INNER JOIN CAT_SEXO CT ON D.ID_SEXO = CT.ID_SEXO " +
			"WHERE E.ID_EMPLEADO = ? " +
			"AND D.ID_TIPO_SOL = ? " +
			"AND D.ID_ESTATUS = ?";
	
	public static final String QUERY_TIPO_COBERTURA = "SELECT S.ID_TIPO_COB FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO WHERE E.ID_EMPLEADO = ?";
	
	public static final String QUERY_COSTO_TOTAL = "SELECT S.COSTO_TOTAL FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO WHERE E.ID_EMPLEADO = ?";
	
	public static final String QUERY_SOLICITUD = "SELECT ID_SOLICITUD FROM SOLICITUD " +
			"WHERE ID_EMPLEADO = ? AND ID_TIPO_SOL = ? AND ID_ESTATUS = ?";
	
	public static final String QUERY_SOLICITUD_TOTAL = "SELECT ID_SOLICITUD, COSTO_EMPLEADO, " +
			"ID_TIPO_COB, TO_CHAR(FECHA_AUTORIZACION,'DD') AS DIA, " +
			"TO_CHAR(FECHA_AUTORIZACION,'MM') AS MES, " +
			"TO_CHAR(FECHA_AUTORIZACION,'YYYY') AS ANIO, " +
			"TO_CHAR(FECHA_AUTORIZACION,'DD/MM/YYYY') AS FECHA_AUTORIZACION, "+ 
			"TO_CHAR(FECHA_RENOVACION,'DD') AS DIA_REN," +
			"TO_CHAR(FECHA_RENOVACION,'MM') AS MES_REN, " +
			"TO_CHAR(FECHA_RENOVACION,'YYYY') AS ANIO_REN, "+ 
			"TO_CHAR(FECHA_RENOVACION,'DD/MM/YYYY') AS FECHA_RENOVACION " +
			"FROM SOLICITUD WHERE ID_EMPLEADO = ? AND ID_TIPO_SOL = ? AND ID_ESTATUS = ?";
	
	public static final String QUERY_DEP_ID_SOL = "SELECT S.ID_SOLICITUD, D.ID_DEPENDIENTE " +
			"FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN DEPENDIENTE D ON S.ID_SOLICITUD = D.ID_SOLICITUD " +
			"WHERE E.ID_EMPLEADO = ? " +
			"AND S.ID_TIPO_SOL = ? " +
			"AND S.ID_ESTATUS = ?";
	
	public static final String QUERY_CON_ID_SOL = "SELECT S.ID_SOLICITUD, " +
			"C.ID_CONSULMED FROM EMPLEADO E " +
			"INNER JOIN SOLICITUD S ON S.ID_EMPLEADO = E.ID_EMPLEADO " +
			"INNER JOIN CONSULMED C ON S.ID_SOLICITUD = C.ID_SOLICITUD " +
			"WHERE E.ID_EMPLEADO = ? " +
			"AND S.ID_TIPO_SOL = ? " +
			"AND S.ID_ESTATUS = ?";
	
	public static final String QUERY_FINIQUITO = "SELECT F.ID_EMPLEADO, " +
			"E.NOMBRE||' '||E.A_PATERNO||' '||E.A_MATERNO,F.COSTO_EMP, F.COSTO_EMP_AMP, "+ 
			"F.COSTO_DEPS, F.COSTO_DEPS_AMP, F.COSTO_FINIQUITO, F.ID_ADMIN_AUT, " +
			"TO_CHAR(F.FECHA_FINIQUITO,'DD/MM/YYYY') "+ 
			"FROM EMPLEADO E " +
			"INNER JOIN FINIQUITO F ON E.ID_EMPLEADO = F.ID_EMPLEADO " +
			"WHERE F.ID_EMPLEADO = ?";
	
	public static final String UPDATE_SOLICITUD = "UPDATE SOLICITUD SET ID_TIPO_SOL = ?, " +
			"ID_ESTATUS = ?, COSTO_EMPLEADO = ?, COSTO_AMPL_IND = ?, COSTO_DEPENDIENTES = ?, " +
			"COSTO_AMPLIACION = ?, COSTO_TOTAL = ?, FECHA_CANCELACION = ?, " +
			"FECHA_CANCELACION_AUT = ? " +
			"WHERE ID_EMPLEADO = ?";
	
	public static final String INSERT_FINIQUITO = "INSERT INTO FINIQUITO VALUES(?,?,?,?,?,?,?,?)";
	
	public static final String UPDATE_DEPENDIENTE = "UPDATE DEPENDIENTE SET ID_TIPO_SOL = ?, " +
			"ID_ESTATUS = ?, FECHA_BAJA = ?, FECHA_BAJA_AUT = ?  WHERE ID_SOLICITUD = ?";
	
	public static final String UPDATE_CONSULMED = "UPDATE CONSULMED SET ID_TIPO_SOL = ?, ID_ESTATUS = ?, " +
			"FECHA_BAJA = ?, FECHA_BAJA_AUT = ? WHERE ID_SOLICITUD = ?";	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEPENDIENTE, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdDependiente(rs.getInt(2));
					dependiente.setIdParentesco(rs.getInt(3));
					dependiente.setIdSexo(rs.getInt(4));
					dependiente.setSexo(rs.getString(5));
					dependiente.setFechaNac(rs.getString(6));
					dependiente.setAnioAut(rs.getInt(7));
					dependiente.setMesAut(rs.getInt(8));
					dependiente.setDiaAut(rs.getInt(9));
					dependiente.setAnioRenov(rs.getInt(10));
					dependiente.setMesRenov(rs.getInt(11));
					dependiente.setDiaRenov(rs.getInt(12));
					dependiente.setCostoDep(rs.getDouble(13));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getDependienteSol(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_DEP_ID_SOL, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdSolicitud(rs.getInt(1));
					dependiente.setIdDependiente(rs.getInt(2));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DependienteTO> getConsulmedSol(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		List<DependienteTO> lista = new ArrayList<DependienteTO>();
		try {
			lista = jdbcTemplate.query(QUERY_CON_ID_SOL, new RowMapper(){
				public DependienteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DependienteTO dependiente = new DependienteTO();
					dependiente.setIdSolicitud(rs.getInt(1));
					dependiente.setIdDependiente(rs.getInt(2));
					return dependiente;
			}}, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return lista;
	}
		
	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		SolicitudTO datos = new SolicitudTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_SOLICITUD_TOTAL, new RowMapper<SolicitudTO>(){
				public SolicitudTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					SolicitudTO solicitudTO = new SolicitudTO();
					solicitudTO.setIdSol(rs.getInt(1));
					solicitudTO.setCostoEmpleado(rs.getDouble(2));
					solicitudTO.setIdTipoCobertura(rs.getInt(3));
					solicitudTO.setDiaAut(rs.getInt(4));
					solicitudTO.setMesAut(rs.getInt(5));
					solicitudTO.setAnioAut(rs.getInt(6));
					solicitudTO.setFechaAutorizacion(rs.getString(7));
					solicitudTO.setDiaRenov(rs.getInt(8));
					solicitudTO.setMesRenov(rs.getInt(9));
					solicitudTO.setAnioRenov(rs.getInt(10));
					solicitudTO.setFechaRenovacion(rs.getString(11));
					return solicitudTO;
				}
			}, numEmp, tipoSol,estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
		}
		return datos;
	}
	
	public FiniquitoTO getFiniquito(Integer numEmp){
		FiniquitoTO datos = new FiniquitoTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_FINIQUITO, new RowMapper<FiniquitoTO>(){
				public FiniquitoTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					FiniquitoTO finiquitoTO = new FiniquitoTO();
					finiquitoTO.setNumEmpSolicitado(rs.getInt(1));
					finiquitoTO.setNombreEmpSolicitado(rs.getString(2));
					finiquitoTO.setMontoEmp(rs.getDouble(3));
					finiquitoTO.setMontoEmpAmp(rs.getDouble(4));
					finiquitoTO.setMontoDep(rs.getDouble(5));
					finiquitoTO.setMontoDepAmp(rs.getDouble(6));
					finiquitoTO.setMontoFiniquito(rs.getDouble(7));
					finiquitoTO.setNumEmp(rs.getInt(8));
					finiquitoTO.setFechaFiniq(rs.getString(9));
					return finiquitoTO;
				}
			}, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  -- EmptyResultDataAccessException -- "+e.getMessage());
			datos.setNumEmpSolicitado(0);
		}
		return datos;
	}
	
	public Integer getTipoCobertura(Integer numEmp){
		Integer cobertura = -1;
		try {
			cobertura = jdbcTemplate.queryForObject(QUERY_TIPO_COBERTURA, Integer.class, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			cobertura = -1;
		}
		logger.info("  Id Tipo Cobertura: "+cobertura);
		return cobertura;
	}	
	
	public Double getCostoTotal(Integer numEmp){
		Double costo = 0.0;
		try {
			costo = jdbcTemplate.queryForObject(QUERY_COSTO_TOTAL, Double.class, numEmp);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			costo = 0.0;
		}
		logger.info("  Id Tipo Cobertura: "+costo);
		return costo;
	}	
	
	public Integer getIdSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		Integer sol = 0;
		try {
			sol = jdbcTemplate.queryForObject(QUERY_SOLICITUD, Integer.class, numEmp, tipoSol, estatusSol);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  Excepcion: "+e.getMessage());
			sol = 0;
		}
		logger.info("  Id Tipo Cobertura: "+sol);
		return sol;
	}	
	
	public Integer setSolicitud(FiniquitoTO finiquitoTO, Integer tipoSol, Integer estatusSol){
		Integer resultado = jdbcTemplate.update(UPDATE_SOLICITUD, tipoSol,estatusSol,finiquitoTO.getMontoEmp(),
				finiquitoTO.getMontoEmpAmp(),finiquitoTO.getMontoDep(),finiquitoTO.getMontoEmpAmp()+finiquitoTO.getMontoDepAmp(),
				finiquitoTO.getMontoFiniquito(),finiquitoTO.getFechaFiniquito(),finiquitoTO.getFechaFiniquito(),finiquitoTO.getNumEmpSolicitado());
		return resultado;
	}
	
	public Integer setFiniquito(FiniquitoTO finiquitoTO){
		Integer resultado = jdbcTemplate.update(INSERT_FINIQUITO, 
				finiquitoTO.getNumEmpSolicitado(),
				finiquitoTO.getMontoEmp(),
				finiquitoTO.getMontoEmpAmp(),
				finiquitoTO.getMontoDep(),
				finiquitoTO.getMontoDepAmp(),
				finiquitoTO.getMontoFiniquito(),
				finiquitoTO.getNumEmp(),
				finiquitoTO.getFechaFiniquito());
		return resultado;
	}
	
	public Integer setDependiente(Integer idSol, Integer tipoSol, Integer estatusSol,Date fechaFiniquito){
		Integer resultado = jdbcTemplate.update(UPDATE_DEPENDIENTE,tipoSol,estatusSol,fechaFiniquito,fechaFiniquito,idSol);
		return resultado;
	}
	
	public Integer setConsulmed(Integer idSol, Integer tipoSol, Integer estatusSol,Date fechaFiniquito){
		Integer resultado = jdbcTemplate.update(UPDATE_CONSULMED, tipoSol,estatusSol,fechaFiniquito,fechaFiniquito,idSol);
		return resultado;
	}
	
	
	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}	
	
}