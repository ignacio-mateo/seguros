package mx.com.orbita.dao;

import java.util.Date;

public interface FechaAntiguedadDao {
	
	Integer existeSolicitud(Integer numEmp, Integer tipoSol,Integer estatusSol);
	void setSolicitud(Date fechaElab,Date fechaAut,Integer numEmp);
	void setDependiente(Date fechaAlta,Date fechaAltaAut,Integer idDependiente);
	
}
