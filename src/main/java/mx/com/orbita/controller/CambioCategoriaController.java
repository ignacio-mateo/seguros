package mx.com.orbita.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.controller.form.CategoriaForm;
import mx.com.orbita.service.CambioCategoriaService;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Redondeos;

@Controller
public class CambioCategoriaController {

	private final static Logger logger = Logger.getLogger(CambioCategoriaController.class);

	private CambioCategoriaService cambioCategoriaService;

	@RequestMapping(value="inicioCambioCategoria.htm")
	protected ModelAndView inicioCambioCategoria(EmpTO empTO){
		logger.info("CambioCategoriaController.getCambioCategoria");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/categoria/categoriaInicio");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="consultaCambioCategoria.htm")
	protected ModelAndView getCambioCategoria(EmpTO empTO, @RequestParam Integer mes, @RequestParam Integer dia){
		logger.info("CambioCategoriaController.getCambioCategoria");
		logger.info("  Parametros de Peticion numEmp: ["+empTO.getNumEmpSolicitado()+"] mes: ["+mes+"] dia: ["+dia+"]");
		ModelAndView modelAndView = new ModelAndView();
		CategoriaForm categoriaActual = new CategoriaForm();
		CategoriaForm categoriaTransit = new CategoriaForm();
		CategoriaForm categoriaNueva = new CategoriaForm();
		CategoriaForm categoriaTotal = new CategoriaForm();
		double montoTotalEmp = 0.0;
		double montoTotal = 0.0;
		double montoTotalAnt = 0.0;
		double montoDep = 0.0;
		double montoTotalDep = 0.0;
		double montoTotalAmp = 0.0;

		Integer solicitud = cambioCategoriaService.getSolicitudExiste(empTO.getNumEmpSolicitado(),
				Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);

		if(solicitud.equals(0)){
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.EMPLEADO_NO_ENCONTRADO);
			modelAndView.setViewName("administrador/categoria/categoriaInicio");
		}else{

			// Datos Actuales
			logger.info(" Categoria Actual");
			categoriaActual = cambioCategoriaService.getCategoriaActual(empTO.getNumEmpSolicitado());

			// Datos Prorrateo
			logger.info(" Categoria Actual a la fecha de hoy");
			categoriaTransit = cambioCategoriaService.getCategoriaTransitoria(empTO.getNumEmpSolicitado(), mes, dia);
			SolicitudTO solicitudTransit = categoriaTransit.getSolicitudTO();
			List<DependienteTO> listTransit = categoriaTransit.getListDep();

			// Datos Nuevos
			logger.info(" Categoria Nueva");
			categoriaNueva = cambioCategoriaService.getCambioCategoria(empTO.getNumEmpSolicitado(),mes,dia);

			// Datos Totales
			logger.info(" Datos Actuales");
			categoriaTotal = cambioCategoriaService.getCambioCategoria(empTO.getNumEmpSolicitado(),mes,dia);
			SolicitudTO solicitudNueva = categoriaTotal.getSolicitudTO();
			List<DependienteTO> listNuevo = categoriaTotal.getListDep();

			logger.info(" Datos Finales");
			montoTotalAnt = categoriaActual.getSolicitudTO().getCostoTotal();
			montoTotal = solicitudNueva.getCostoTotal() + solicitudTransit.getCostoTotal();
			montoTotalAmp = solicitudNueva.getCostoAmpliacion() + solicitudTransit.getCostoAmpliacion();
			solicitudNueva.setCostoTotal(Redondeos.roundNum(montoTotal));
			solicitudNueva.setCostoAmpliacion(Redondeos.roundNum(montoTotalAmp));

			EmpleadoTO empleadoNuevo = categoriaTotal.getEmpleadoTO();
			montoTotalEmp = empleadoNuevo.getMontoEmpProrateado() + solicitudTransit.getCostoEmpleado();
			empleadoNuevo.setMontoEmpProrateado(Redondeos.roundNum(montoTotalEmp));

			int i = 0;
			for (DependienteTO dependienteTO : listNuevo) {
				montoDep = 0.0;
				montoDep = dependienteTO.getCostoDep() + listTransit.get(i).getCostoDep();
				logger.info("  costoDep: ["+dependienteTO.getCostoDep()
							+"] costoDepTransit: ["+listTransit.get(i).getCostoDep()
							+"] costoDepTotal: "+montoDep+"]");
				dependienteTO.setCostoDep(Redondeos.roundNum(montoDep));
				montoTotalDep = montoTotalDep + montoDep;
				i++;
			}
			logger.info("  montoTotalEmp: ["+montoTotalEmp
							+"] montoTotalAnt: ["+montoTotalAnt
							+"] montoTotalDep: ["+montoTotalDep
							+"] montoTotal: ["+montoTotal
							+"] montoTotalAmp: ["+montoTotalAmp);

			solicitudNueva.setCostoTotalAnt(montoTotalAnt);
			solicitudNueva.setCostoDependiente(montoTotalDep);
			categoriaTotal.setEmpTO(empTO);
			categoriaTotal.setEmpleadoTO(empleadoNuevo);
			categoriaTotal.setSolicitudTO(solicitudNueva);
			categoriaTotal.setListDep(listNuevo);

			// Dependientes en proceso
			Integer dependientesProceso = cambioCategoriaService.getDependienteProceso(empTO.getNumEmpSolicitado());

			if(dependientesProceso>=1){
				modelAndView.addObject("mensaje1", Codigo.DEPENDIENTES_PROCESO);
			}

			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("categoriaActual", categoriaActual);
			modelAndView.addObject("categoriaTransit", categoriaTransit);
			modelAndView.addObject("categoriaNueva", categoriaNueva);
			modelAndView.addObject("categoriaTotal", categoriaTotal);
			modelAndView.setViewName("administrador/categoria/categoriaConsulta");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="actualizaCambioCategoria.htm", method = RequestMethod.POST)
	protected ModelAndView setCambioCategoria(@ModelAttribute("categoriaTotal") CategoriaForm categoriaTotal){
		logger.info("CambioCategoriaController.setCambioCategoria");
		ModelAndView modelAndView = new ModelAndView();
		EmpleadoTO empleadoTO = categoriaTotal.getEmpleadoTO();
		SolicitudTO solicitudTO = categoriaTotal.getSolicitudTO();
		List<DependienteTO> listDep = categoriaTotal.getListDep();
		EmpTO empTO = categoriaTotal.getEmpTO();
		cambioCategoriaService.setCambioCategoria(empleadoTO, empTO, solicitudTO, listDep);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@Autowired
	public void setCambioCategoriaService(CambioCategoriaService cambioCategoriaService) {
		this.cambioCategoriaService = cambioCategoriaService;
	}

}
