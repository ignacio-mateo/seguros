package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DatFiscalesTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public class CircularForm {
	
	private DatFiscalesTO datosFiscales;
	private EmpleadoTO empleadoTO;
	private SolicitudTO solicitudTO;
	private List<DependienteTO> listDep;
	private List<ConsulmedTO> listCon;
	private List<ConsulmedTO> listConAlta;
	private EmpTO empTO;
	private String estatusRenov;
	private String estatusAmpl;
	private Integer numDep;
	private Double montoAmplAnterior;
	private Double montoAmplNuevo;
	private Double montoTotal;
	
	public DatFiscalesTO getDatosFiscales() {
		return datosFiscales;
	}
	public void setDatosFiscales(DatFiscalesTO datosFiscales) {
		this.datosFiscales = datosFiscales;
	}
	public EmpleadoTO getEmpleadoTO() {
		return empleadoTO;
	}
	public void setEmpleadoTO(EmpleadoTO empleadoTO) {
		this.empleadoTO = empleadoTO;
	}
	public SolicitudTO getSolicitudTO() {
		return solicitudTO;
	}
	public void setSolicitudTO(SolicitudTO solicitudTO) {
		this.solicitudTO = solicitudTO;
	}
	public List<DependienteTO> getListDep() {
		return listDep;
	}
	public void setListDep(List<DependienteTO> listDep) {
		this.listDep = listDep;
	}
	public List<ConsulmedTO> getListCon() {
		return listCon;
	}
	public void setListCon(List<ConsulmedTO> listCon) {
		this.listCon = listCon;
	}
	public String getEstatusRenov() {
		return estatusRenov;
	}
	public void setEstatusRenov(String estatusRenov) {
		this.estatusRenov = estatusRenov;
	}
	public String getEstatusAmpl() {
		return estatusAmpl;
	}
	public void setEstatusAmpl(String estatusAmpl) {
		this.estatusAmpl = estatusAmpl;
	}
	public Integer getNumDep() {
		return numDep;
	}
	public void setNumDep(Integer numDep) {
		this.numDep = numDep;
	}
	public Double getMontoAmplNuevo() {
		return montoAmplNuevo;
	}
	public void setMontoAmplNuevo(Double montoAmplNuevo) {
		this.montoAmplNuevo = montoAmplNuevo;
	}
	public Double getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}
	public Double getMontoAmplAnterior() {
		return montoAmplAnterior;
	}
	public void setMontoAmplAnterior(Double montoAmplAnterior) {
		this.montoAmplAnterior = montoAmplAnterior;
	}
	public EmpTO getEmpTO() {
		return empTO;
	}
	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}
	public List<ConsulmedTO> getListConAlta() {
		return listConAlta;
	}
	public void setListConAlta(List<ConsulmedTO> listConAlta) {
		this.listConAlta = listConAlta;
	}
}
