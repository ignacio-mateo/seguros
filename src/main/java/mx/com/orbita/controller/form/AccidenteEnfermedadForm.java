package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoEnfermedadTO;

public class AccidenteEnfermedadForm {
	
	private ReembolsoEnfermedadTO reembolsoTO;
	private EmpleadoTO empleadoTO;
	private List<DependienteTO> listDep;
	private EmpTO empTO;
	
	public ReembolsoEnfermedadTO getReembolsoTO() {
		return reembolsoTO;
	}
	public void setReembolsoTO(ReembolsoEnfermedadTO reembolsoTO) {
		this.reembolsoTO = reembolsoTO;
	}
	public EmpleadoTO getEmpleadoTO() {
		return empleadoTO;
	}
	public void setEmpleadoTO(EmpleadoTO empleadoTO) {
		this.empleadoTO = empleadoTO;
	}
	public List<DependienteTO> getListDep() {
		return listDep;
	}
	public void setListDep(List<DependienteTO> listDep) {
		this.listDep = listDep;
	}
	public EmpTO getEmpTO() {
		return empTO;
	}
	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}
	

}
