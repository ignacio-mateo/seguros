package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public class SolicitudForm {
	
	private List<DependienteTO> listDep;
	private List<DependienteTO> listDepAlta;
	private List<ConsulmedTO> listCon;
	private List<ConsulmedTO> listConAlta;
	private List<ConsulmedTO> listMem;
	private EmpleadoTO empleadoTO;
	private SolicitudTO solicitudTO;
	private EmpTO empTO;
	
	public List<DependienteTO> getListDep() {
		return listDep;
	}
	public void setListDep(List<DependienteTO> listDep) {
		this.listDep = listDep;
	}
	public List<DependienteTO> getListDepAlta() {
		return listDepAlta;
	}
	public void setListDepAlta(List<DependienteTO> listDepAlta) {
		this.listDepAlta = listDepAlta;
	}
	public List<ConsulmedTO> getListCon() {
		return listCon;
	}
	public void setListCon(List<ConsulmedTO> listCon) {
		this.listCon = listCon;
	}
	public List<ConsulmedTO> getListConAlta() {
		return listConAlta;
	}
	public void setListConAlta(List<ConsulmedTO> listConAlta) {
		this.listConAlta = listConAlta;
	}
	public EmpleadoTO getEmpleadoTO() {
		return empleadoTO;
	}
	public void setEmpleadoTO(EmpleadoTO empleadoTO) {
		this.empleadoTO = empleadoTO;
	}
	public SolicitudTO getSolicitudTO() {
		return solicitudTO;
	}
	public void setSolicitudTO(SolicitudTO solicitudTO) {
		this.solicitudTO = solicitudTO;
	}
	public EmpTO getEmpTO() {
		return empTO;
	}
	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}
	public List<ConsulmedTO> getListMem() {
		return listMem;
	}
	public void setListMem(List<ConsulmedTO> listMem) {
		this.listMem = listMem;
	}
	
}
