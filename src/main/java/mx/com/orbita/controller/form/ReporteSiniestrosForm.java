package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoReporteTO;

public class ReporteSiniestrosForm {
	
	private ReembolsoReporteTO reembolsoReporteTO;
	private List<DependienteTO> listDep;
	private EmpleadoTO empleadoTO;
	private EmpTO empTO;
	
	public ReembolsoReporteTO getReembolsoReporteTO() {
		return reembolsoReporteTO;
	}
	public void setReembolsoReporteTO(ReembolsoReporteTO reembolsoReporteTO) {
		this.reembolsoReporteTO = reembolsoReporteTO;
	}
	public List<DependienteTO> getListDep() {
		return listDep;
	}
	public void setListDep(List<DependienteTO> listDep) {
		this.listDep = listDep;
	}
	public EmpleadoTO getEmpleadoTO() {
		return empleadoTO;
	}
	public void setEmpleadoTO(EmpleadoTO empleadoTO) {
		this.empleadoTO = empleadoTO;
	}
	public EmpTO getEmpTO() {
		return empTO;
	}
	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}

}
