package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public class CategoriaForm {
	
	private EmpleadoTO empleadoTO;
	private SolicitudTO solicitudTO;
	private List<DependienteTO>  listDep;
	private EmpTO empTO;
	
	public EmpleadoTO getEmpleadoTO() {
		return empleadoTO;
	}
	public void setEmpleadoTO(EmpleadoTO empleadoTO) {
		this.empleadoTO = empleadoTO;
	}
	public SolicitudTO getSolicitudTO() {
		return solicitudTO;
	}
	public void setSolicitudTO(SolicitudTO solicitudTO) {
		this.solicitudTO = solicitudTO;
	}
	public List<DependienteTO> getListDep() {
		return listDep;
	}
	public void setListDep(List<DependienteTO> listDep) {
		this.listDep = listDep;
	}
	public EmpTO getEmpTO() {
		return empTO;
	}
	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}
	

}
