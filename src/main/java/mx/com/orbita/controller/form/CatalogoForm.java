package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.CatalogoTO;

public class CatalogoForm {
	
	private List<CatalogoTO> lista;

	public List<CatalogoTO> getLista() {
		return lista;
	}

	public void setLista(List<CatalogoTO> lista) {
		this.lista = lista;
	}

}
