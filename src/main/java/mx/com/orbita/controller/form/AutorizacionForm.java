package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.AutorizacionTO;
import mx.com.orbita.to.EmpTO;

public class AutorizacionForm {
	
	private List<AutorizacionTO> listSol;
	private Integer tamListSol;
	private EmpTO empTO;

	public List<AutorizacionTO> getListSol() {
		return listSol;
	}

	public void setListSol(List<AutorizacionTO> listSol) {
		this.listSol = listSol;
	}

	public Integer getTamListSol() {
		return tamListSol;
	}

	public void setTamListSol(Integer tamListSol) {
		this.tamListSol = tamListSol;
	}

	public EmpTO getEmpTO() {
		return empTO;
	}

	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}
	
}
