package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoConceptoTO;
import mx.com.orbita.to.ReembolsoRelacionTO;

public class ReembolsoForm {
	
	private List<ReembolsoConceptoTO> listReembolso;
	List<DependienteTO> listDep;
	private ReembolsoRelacionTO reembolsoRelacionTO;
	private EmpleadoTO empleadoTO;
	private EmpTO empTO;
	
	public List<ReembolsoConceptoTO> getListReembolso() {
		return listReembolso;
	}
	public void setListReembolso(List<ReembolsoConceptoTO> listReembolso) {
		this.listReembolso = listReembolso;
	}
	public List<DependienteTO> getListDep() {
		return listDep;
	}
	public void setListDep(List<DependienteTO> listDep) {
		this.listDep = listDep;
	}
	public ReembolsoRelacionTO getReembolsoRelacionTO() {
		return reembolsoRelacionTO;
	}
	public void setReembolsoRelacionTO(ReembolsoRelacionTO reembolsoRelacionTO) {
		this.reembolsoRelacionTO = reembolsoRelacionTO;
	}
	public EmpleadoTO getEmpleadoTO() {
		return empleadoTO;
	}
	public void setEmpleadoTO(EmpleadoTO empleadoTO) {
		this.empleadoTO = empleadoTO;
	}
	public EmpTO getEmpTO() {
		return empTO;
	}
	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}

}
