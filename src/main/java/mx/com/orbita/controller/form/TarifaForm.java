package mx.com.orbita.controller.form;

import java.util.List;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.TarifaTO;

public class TarifaForm {
	
	private List<TarifaTO> listTarifa;
	private EmpTO empTO;

	public List<TarifaTO> getListTarifa() {
		return listTarifa;
	}

	public void setListTarifa(List<TarifaTO> listTarifa) {
		this.listTarifa = listTarifa;
	}

	public EmpTO getEmpTO() {
		return empTO;
	}

	public void setEmpTO(EmpTO empTO) {
		this.empTO = empTO;
	}
	
}
