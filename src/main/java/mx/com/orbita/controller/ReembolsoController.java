package mx.com.orbita.controller;

import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.business.EmpleadoBusiness;
import mx.com.orbita.controller.form.AccidenteEnfermedadForm;
import mx.com.orbita.controller.form.ReembolsoForm;
import mx.com.orbita.controller.form.ReporteSiniestrosForm;
import mx.com.orbita.service.ReembolsoService;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoAdrisaTO;
import mx.com.orbita.to.ReembolsoConceptoTO;
import mx.com.orbita.to.ReembolsoEnfermedadTO;
import mx.com.orbita.to.ReembolsoRelacionTO;
import mx.com.orbita.to.ReembolsoReporteTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Encriptacion;
import mx.com.orbita.util.Fechas;
import mx.com.orbita.util.Redondeos;

@Controller
public class ReembolsoController {
	
	private static final Logger logger = Logger.getLogger(ReembolsoController.class);
	
	private SolConsultaService solConsultaService;
	private ReembolsoService reembolsoService;
	private SistemaService sistemaService;
	private EmpleadoBusiness empleadoBusiness;
	
	@RequestMapping(value="inicioReembolso.htm")
	protected ModelAndView getInicioReembolso(EmpTO empTO){
		logger.info("ReembolsoController.getInicioReembolso");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){
			modelAndView.addObject("empTO", empTO);
			modelAndView.setViewName("reembolsos/reembolsoInicioFormatos");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="guiasReembolso.htm")
	protected ModelAndView getGuiasReembolso(EmpTO empTO){
		logger.info("ReembolsoController.getGuiasReembolso");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){
			modelAndView.addObject("empTO", empTO);
			modelAndView.setViewName("reembolsos/reembolsoGuiaFormatos");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
			
	/**
	 * Formato de Relacion de Documentos
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="relacionDocumentos.htm")
	protected ModelAndView getRelacionDocumentos(EmpTO empTO){
		logger.info("ReembolsoController.getRelacionDocumentos");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		ReembolsoForm reembolsoForm = new ReembolsoForm();
			
		ReembolsoRelacionTO reembolsoRelacionTO = new ReembolsoRelacionTO();
		// Para tener el valor por default del tipo de reembolso reembolsoRelacionTO.setTipoReembolso(1);
		EmpleadoTO empleadoTO = new EmpleadoTO();
		empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
		String nombre = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();
		empleadoTO.setNombre(nombre);
		List<ReembolsoConceptoTO> listReembolso = reembolsoService.getListaConceptosReembolsos();
		
		// Dependientes
		List<DependienteTO> listDep = reembolsoService.getDependientes(empTO.getNumEmp(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
		
		reembolsoForm.setListDep(listDep);
		reembolsoForm.setReembolsoRelacionTO(reembolsoRelacionTO);
		reembolsoForm.setEmpleadoTO(empleadoTO);
		reembolsoForm.setListReembolso(listReembolso);
		reembolsoForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("reembolsoForm",reembolsoForm);
		modelAndView.setViewName("reembolsos/reembolsoRelacionDocumentos");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	// Guardar Formato de Relacion de Documentos
	@RequestMapping(value="guardarRelacionDocumentos.htm")
	protected ModelAndView setRelacionDocumentos(@ModelAttribute("reembolsoForm") ReembolsoForm reembolso){
		logger.info("ReembolsoController.setRelacionDocumentos");
		ModelAndView modelAndView = new ModelAndView();
		ReembolsoForm reembolsoForm = new ReembolsoForm();
		Double suma = 0.0;
		
		List<ReembolsoConceptoTO> listReembolso = reembolso.getListReembolso();
		ReembolsoRelacionTO reembolsoRelacionTO = reembolso.getReembolsoRelacionTO();
		EmpTO empTO = reembolso.getEmpTO();
		EmpleadoTO empleadoTO = reembolso.getEmpleadoTO();
		
		for (ReembolsoConceptoTO reembolsoConceptoTO : listReembolso) {
			if(reembolsoConceptoTO.getImporte()!=null && !reembolsoConceptoTO.getImporte().equals("")){
				reembolsoConceptoTO.setImporteFormat(Redondeos.formateadorMiles(reembolsoConceptoTO.getImporte()));
				suma = suma + reembolsoConceptoTO.getImporte();
			}
		}
		reembolsoRelacionTO.setImporteTotalFormat(Redondeos.formateadorMiles(suma));
		
		//reembolsoService.setReembolso(listReembolso, reembolsoRelacionTO, empTO.getNumEmp());
		reembolsoForm.setListReembolso(listReembolso);
		reembolsoForm.setReembolsoRelacionTO(reembolsoRelacionTO);
		reembolsoForm.setEmpleadoTO(empleadoTO);
		reembolsoForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("reembolsoForm", reembolsoForm);
		modelAndView.setViewName("reembolsos/reembolsoRelacionDocumentosLleno");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	// Consulta Lista Folio de Relacion de Documentos
	@RequestMapping(value="consultaFolioReembolso.htm")
	protected ModelAndView getFolioReembolso(EmpTO empTO){
		logger.info("ReembolsoController.getFolioReembolso");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){
			List<Integer> listaFolios = reembolsoService.getReembolsoListID(empTO.getNumEmp());
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("listaFolios", listaFolios);
			if(listaFolios.size()==0){modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_FOL_REEMB);}
			modelAndView.setViewName("reembolsos/reembolsoConsultaIdReembolso");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}	
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	// Consulta Detalle Formato de Relacion de Documentos
	@RequestMapping(value="consultaDetalleReembolso.htm")
	protected ModelAndView getDetalleReembolso(@RequestParam Integer numEmp, @RequestParam String nombreEmp, 
			@RequestParam Integer idReembolso, @RequestParam Integer perfilEmp){
		logger.info("ReembolsoController.getDetalleReembolso");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+"]");
		ModelAndView modelAndView = new ModelAndView();
		ReembolsoForm reembolsoForm = new ReembolsoForm();
		EmpTO empTO = new EmpTO();
		// Empleado
		EmpleadoTO empleadoTO = new EmpleadoTO();
		empleadoTO = solConsultaService.getEmpleadoBD(numEmp);
		String nombre = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();
		empleadoTO.setNombre(nombre);
		// Reembolso
		ReembolsoRelacionTO reembolsoRelacionTO = new ReembolsoRelacionTO();
		reembolsoRelacionTO = reembolsoService.getReembolso(numEmp,idReembolso);
		// Conceptos de Reembolsos
		List<ReembolsoConceptoTO> listReembolso = reembolsoService.getReembolsoConceptos(idReembolso);
		reembolsoForm.setReembolsoRelacionTO(reembolsoRelacionTO);
		reembolsoForm.setEmpleadoTO(empleadoTO);
		reembolsoForm.setListReembolso(listReembolso);
		empTO.setNumEmp(numEmp);
		empTO.setNombreEmp(nombreEmp);
		empTO.setPerfilEmp(perfilEmp);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("reembolsoForm",reembolsoForm);
		modelAndView.setViewName("reembolsos/reembolsoConsultaIdReembolsoDetalle");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
				
	/**
	 * Formato de Aviso y/o Enfermedad
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="formatoAccidenteEnfermedad.htm")
	protected ModelAndView getFormatoAccidenteEnfermedad(EmpTO empTO){
		logger.info("ReembolsoController.getFormatoAccidenteEnfermedad");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		AccidenteEnfermedadForm accidenteEnfermedadForm = new AccidenteEnfermedadForm();
		
		ReembolsoEnfermedadTO reembolsoTO = new ReembolsoEnfermedadTO(); 
		reembolsoTO.setRazonSocial(Codigo.RAZON_SOCIAL);
		reembolsoTO.setMismoPadecimiento(2);
		reembolsoTO.setSeguroAuto(2);
		// Datos del Empleado
		EmpleadoTO empleadoTO = new EmpleadoTO();
		empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
		String nombre = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();
		reembolsoTO.setNumEmp(empTO.getNumEmp().toString());
		reembolsoTO.setNombreTitular(nombre);
		reembolsoTO.setRfcTitular(empleadoTO.getRfc());
				
		if(empleadoTO.getTipoEmp().equals(1)){
			reembolsoTO.setNumPoliza(Codigo.NO_POLIZA_CONF_AVISO_ACCIDENTE);
		}else{
			reembolsoTO.setNumPoliza(Codigo.NO_POLIZA_SIND_AVISO_ACCIDENTE);
		}
				
		// Dependientes
		List<DependienteTO> listDep= reembolsoService.getDependientes(empTO.getNumEmp(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
		
		accidenteEnfermedadForm.setReembolsoTO(reembolsoTO);
		accidenteEnfermedadForm.setEmpleadoTO(empleadoTO);
		accidenteEnfermedadForm.setEmpTO(empTO);
		accidenteEnfermedadForm.setListDep(listDep);
		
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("accidenteEnfermedadForm",accidenteEnfermedadForm);
		modelAndView.setViewName("reembolsos/reembolsoFormatoAccidenteEnfermedad");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="formatoAccidenteEnfermedadLleno.htm")
	protected ModelAndView setFormatoAccidenteEnfermedad(@ModelAttribute("accidenteEnfermedadForm") AccidenteEnfermedadForm accidenteForm){
		logger.info("ReembolsoController.setFormatoAccidenteEnfermedad");
		ModelAndView modelAndView = new ModelAndView();
		AccidenteEnfermedadForm accidenteEnfermedadForm = new AccidenteEnfermedadForm();
		ReembolsoEnfermedadTO reembolsoTO = accidenteForm.getReembolsoTO();
		EmpTO empTO = accidenteForm.getEmpTO();
		reembolsoTO.setFechaAltaN(reembolsoTO.getFechaAlta());
		reembolsoTO.setFechaNacN(reembolsoTO.getFechaNac());
		reembolsoTO.setFechaAltaSiniestroN(reembolsoTO.getFechaAltaSiniestro());
		reembolsoTO.setFechaSintomasN(reembolsoTO.getFechaSintomas());
		reembolsoTO.setFechaVisitaMedN(reembolsoTO.getFechaVisitaMed());
		reembolsoTO.setFechaMedicoConN(reembolsoTO.getFechaMedicoCon());
		accidenteEnfermedadForm.setReembolsoTO(reembolsoTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("accidenteEnfermedadForm",accidenteEnfermedadForm);
		modelAndView.setViewName("reembolsos/reembolsoFormatoAccidenteEnfermedadLleno");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Formato de Reporte de Siniestros
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="reporteGastosMedicos.htm")
	protected ModelAndView getReporteGastosMedicos(EmpTO empTO){
		logger.info("ReembolsoController.getReporteGastosMedicos");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		ReporteSiniestrosForm reporteSiniestrosForm = new ReporteSiniestrosForm();
		ReembolsoReporteTO reembolsoReporteTO = new ReembolsoReporteTO();
		Fechas fechas = new Fechas();
		
		// Datos del Empleado
		EmpleadoTO empleadoTO = new EmpleadoTO();
		empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
				
		// Dependientes
		List<DependienteTO> listDep = reembolsoService.getDependientes(empTO.getNumEmp(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
				
		reembolsoReporteTO.setPoliza(Codigo.POLIZA);
		reembolsoReporteTO.setContratante(Codigo.CONTRATANTE);
		reembolsoReporteTO.setEmisor(Codigo.EMISOR);
		reembolsoReporteTO.setAsegurado(empTO.getNombreEmp());
		reembolsoReporteTO.setRfc(empleadoTO.getRfc());
		reembolsoReporteTO.setFechaReclamacion(fechas.formatoFechas(new Date(),"dd/MM/yyyy").toUpperCase());
				
		if(empleadoTO.getTipoEmp().equals(1)){
			reembolsoReporteTO.setNumPoliza(Codigo.NO_POLIZA_CONF_REPORTE_SINIESTROS);
			reembolsoReporteTO.setPlan(Codigo.PLAN_CONF);
		}else{
			reembolsoReporteTO.setNumPoliza(Codigo.NO_POLIZA_SIND_REPORTE_SINIESTROS);
			reembolsoReporteTO.setPlan(Codigo.PLAN_SIND);
		}
		
		reporteSiniestrosForm.setEmpleadoTO(empleadoTO);
		reporteSiniestrosForm.setEmpTO(empTO);
		reporteSiniestrosForm.setReembolsoReporteTO(reembolsoReporteTO);
		reporteSiniestrosForm.setListDep(listDep);
		modelAndView.addObject("reporteSiniestrosForm", reporteSiniestrosForm);
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("reembolsos/reembolsoReporteGastosMedicos");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteGastosMedicosLleno.htm")
	protected ModelAndView setReporteGastosMedicos(@ModelAttribute("reporteSiniestrosForm") ReporteSiniestrosForm reporteForm){
		logger.info("ReembolsoController.setReporteGastosMedicos");
		ModelAndView modelAndView = new ModelAndView();
		ReporteSiniestrosForm reporteSiniestrosForm = new ReporteSiniestrosForm();
		ReembolsoReporteTO reembolsoReporteTO = reporteForm.getReembolsoReporteTO();
		reembolsoReporteTO.setFechaNacN(reembolsoReporteTO.getFechaNac());
		reembolsoReporteTO.setFechaReclamacionN(reembolsoReporteTO.getFechaReclamacion());
		
		reembolsoReporteTO.setDesglose1F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose1()));
		reembolsoReporteTO.setDesglose2F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose2()));
		reembolsoReporteTO.setDesglose3F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose3()));
		reembolsoReporteTO.setDesglose4F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose4()));
		reembolsoReporteTO.setDesglose5F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose5()));
		reembolsoReporteTO.setDesglose6F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose6()));
		reembolsoReporteTO.setDesglose7F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose7()));
		reembolsoReporteTO.setDesglose8F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose8()));
		reembolsoReporteTO.setDesglose9F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose9()));
		reembolsoReporteTO.setDesglose10F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose10()));
		reembolsoReporteTO.setDesglose11F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose11()));
		reembolsoReporteTO.setDesglose12F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose12()));
		reembolsoReporteTO.setDesglose13F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose13()));
		reembolsoReporteTO.setDesglose14F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose14()));
		reembolsoReporteTO.setDesglose15F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose15()));
		reembolsoReporteTO.setDesglose16F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose16()));
		reembolsoReporteTO.setDesglose17F(Redondeos.formateadorMiles(reembolsoReporteTO.getDesglose17()));
		reembolsoReporteTO.setDesgloseTotalF(Redondeos.formateadorMiles(reembolsoReporteTO.getDesgloseTotal()));
			
		
		EmpTO empTO = reporteForm.getEmpTO();
		EmpleadoTO empleadoTO = reporteForm.getEmpleadoTO();
		reporteSiniestrosForm.setEmpleadoTO(empleadoTO);
		reporteSiniestrosForm.setReembolsoReporteTO(reembolsoReporteTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("reporteSiniestrosForm", reporteSiniestrosForm);
		modelAndView.setViewName("reembolsos/reembolsoReporteGastosMedicosLleno");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	// Enviar reembolso a Adrisa
	@RequestMapping(value="enviaReembolso.htm")
	protected ModelAndView getEnviaReembolso(EmpTO empTO){
		logger.info("ReembolsoController.getInicioReembolso");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		ReembolsoAdrisaTO reembolsoAdrisaTO = new ReembolsoAdrisaTO();
		Encriptacion encriptacion = new Encriptacion();
		String url_uno = "";
		String url_dos = "";
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){
			/*
			reembolsoAdrisaTO = empleadoBusiness.datosEmpleadoReembolso(empTO.getNumEmp());

			String liga_uno = Codigo.LIGA_REEMBOLSOS_UNO + encriptacion.getURLEncriptada(reembolsoAdrisaTO);
			url_uno = "<a href=\""+liga_uno+"\" target=\"_blank\" style=\"font-size:14px;\">Envio de Tramite de Reembolso y Carta Pase</a>";
			logger.info("  URL 1: "+url_uno);
						
			String liga_dos = Codigo.LIGA_REEMBOLSOS_DOS + encriptacion.getURLEncriptada(reembolsoAdrisaTO);
			url_dos = "<a href=\""+liga_dos+"\" target=\"_blank\" style=\"font-size:14px;\">Envio de Tramite de Reembolso y Carta Pase</a>";
			logger.info("  URL 1: "+url_dos);
			*/
			modelAndView.addObject("url_uno", url_uno);
			modelAndView.addObject("url_dos", url_dos);
			modelAndView.addObject("empTO", empTO);
			modelAndView.setViewName("reembolsos/reembolsoEnviaAdrisa");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	
	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}

	@Autowired
	public void setReembolsoService(ReembolsoService reembolsoService) {
		this.reembolsoService = reembolsoService;
	}
	
	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}

	@Autowired
	public void setEmpleadoBusiness(EmpleadoBusiness empleadoBusiness) {
		this.empleadoBusiness = empleadoBusiness;
	}

	
}
