package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;

@Controller
public class SolConsultaController {
	
	private static final Logger logger = Logger.getLogger(SolConsultaController.class);
	private SolConsultaService solConsultaService;
	private SistemaService sistemaService;
	
	
	// CONSULTA DE ALTAS
	
	/**
	 * Consulta Solicitud
	 * @param numEmp
	 * @return
	 */
	@RequestMapping(value="consultaAltaSolicitud.htm")
	protected ModelAndView getAltaSolicitud(EmpTO empTO){
		logger.info("SolConsultaController.getAltaSolicitud");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
				
		if((sistemaService.existeSolicitudAlta(empTO.getNumEmp()))!=0){		
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA,
																		Codigo.EST_SOL_ALTA_EN_PROCESO, 
																		Codigo.EST_SOL_ALTA_AUTORIZADA);
			List<DependienteTO> listaDep = new ArrayList<DependienteTO>();
			List<DependienteTO> listaDepAux = new ArrayList<DependienteTO>();
			List<ConsulmedTO> listaConsulmed  = new ArrayList<ConsulmedTO>();
			List<ConsulmedTO> listaConsulmedAux  = new ArrayList<ConsulmedTO>();
			List<ConsulmedTO> listaMemorial  = new ArrayList<ConsulmedTO>();
			
			if(solicitudTO.getIdEstatusSol().equals(1)){
				listaDep = solConsultaService.getDependiente(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_EN_PROCESO);
				listaConsulmed = solConsultaService.getConsulmed(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_EN_PROCESO);
			}else{
				listaDep = solConsultaService.getDependiente(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA);
				listaDepAux = solConsultaService.getDependiente(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_BAJA_EN_PROCESO, Codigo.EST_SOL_BAJA_EN_PROCESO);
				listaDep.addAll(listaDepAux);
				listaConsulmed = solConsultaService.getConsulmed(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA);
				listaConsulmedAux = solConsultaService.getConsulmed(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_BAJA_EN_PROCESO, Codigo.EST_SOL_BAJA_EN_PROCESO);
				listaConsulmed.addAll(listaConsulmedAux);
			}
			
			listaMemorial = solConsultaService.getMemorial(solicitudTO.getIdSol());

			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject(empleadoTO);
			modelAndView.addObject(solicitudTO);
			modelAndView.addObject("listaDep",listaDep);
			modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA);
			modelAndView.addObject("consentimientoLlenado", Codigo.CONSENTIMIENTO_LLENADO);
			modelAndView.addObject("consentimientoFirma", Codigo.CONSENTIMIENTO_FIRMA);
			modelAndView.addObject("consentimientoExt", Codigo.CONSENTIMIENTO_EXT);
			modelAndView.addObject("listaConsulmed",listaConsulmed);
			modelAndView.addObject("listaMemorial",listaMemorial);
			modelAndView.setViewName("solConsulta/solConsultaAlta");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta Lista de Dependientes
	 * @param numEmp
	 * @return
	 */
	@RequestMapping(value="consultaAltaIdDependiente.htm")
	protected ModelAndView getAltaIdDependiente(EmpTO empTO){
		logger.info("SolConsultaController.getAltaIdDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){	
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA,
																		Codigo.EST_SOL_ALTA_EN_PROCESO, 
																		Codigo.EST_SOL_ALTA_AUTORIZADA);
			List<Integer> listaDep = solConsultaService.getIdDependiente(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA);
			modelAndView.addObject(empleadoTO);
			modelAndView.addObject(solicitudTO);	
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("listaDep",listaDep);
			if(listaDep.size()==0){modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_NO_DEP);}
			modelAndView.setViewName("solConsulta/solConsultaAltaDependienteId");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta Detalle del Dependiente
	 * @param numEmp
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaAltaDependiente.htm")
	protected ModelAndView getAltaDependiente(@RequestParam Integer numEmp,@RequestParam String nombreEmp,@RequestParam Integer perfilEmp, @RequestParam Integer idDependiente){
		logger.info("SolConsultaController.getAltaDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmp);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmp, 
																	Codigo.TIPO_SOL_ALTA,
																	Codigo.EST_SOL_ALTA_EN_PROCESO,
																	Codigo.EST_SOL_ALTA_AUTORIZADA);
		DependienteTO dependienteTO = solConsultaService.getDependienteAltaDetalle(idDependiente);
		empTO.setNumEmp(numEmp);
		empTO.setNombreEmp(nombreEmp);
		empTO.setPerfilEmp(perfilEmp);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("empleadoTO",empleadoTO);
		modelAndView.addObject("solicitudTO",solicitudTO);
		modelAndView.addObject("dependienteTO",dependienteTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA_DEPENDIENTE);
		modelAndView.addObject("consentimientoLlenado", Codigo.CONSENTIMIENTO_LLENADO);
		modelAndView.addObject("consentimientoFirma", Codigo.CONSENTIMIENTO_FIRMA);
		modelAndView.addObject("consentimientoExt", Codigo.CONSENTIMIENTO_EXT);
		modelAndView.setViewName("solConsulta/solConsultaAltaDependiente");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta Lista de Dependientes Consulmed
	 * @param numEmp
	 * @return
	 */
	@RequestMapping(value="consultaAltaIdConsulmed.htm")
	protected ModelAndView getAltaIdConsulmed(EmpTO empTO){
		logger.info("SolConsultaController.getAltaIdConsulmed");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){	
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA,
																		Codigo.EST_SOL_ALTA_EN_PROCESO, 
																		Codigo.EST_SOL_ALTA_AUTORIZADA);
			List<Integer> listaConsulmed = solConsultaService.getIdConsulmed(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA);
			modelAndView.addObject(empleadoTO);
			modelAndView.addObject(solicitudTO);
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("listaConsulmed",listaConsulmed);
			if(listaConsulmed.size()==0){modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_NO_DEP);}
			modelAndView.setViewName("solConsulta/solConsultaAltaConsulmedId");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta de Detalle del Dependiente Consulmed
	 * @param numEmp
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaAltaConsulmed.htm")
	protected ModelAndView getAltaConsulmed(@RequestParam Integer numEmp,@RequestParam String nombreEmp,@RequestParam Integer perfilEmp, @RequestParam Integer idDependiente){
		logger.info("SolConsultaController.getAltaConsulmed");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmp);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmp, 
																	Codigo.TIPO_SOL_ALTA,
																	Codigo.EST_SOL_ALTA_EN_PROCESO, 
																	Codigo.EST_SOL_ALTA_AUTORIZADA);
		ConsulmedTO consulmedTO = solConsultaService.getConsulmedAltaDetalle(idDependiente);
		empTO.setNumEmp(numEmp);
		empTO.setNombreEmp(nombreEmp);
		empTO.setPerfilEmp(perfilEmp);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject(empleadoTO);
		modelAndView.addObject(solicitudTO);
		modelAndView.addObject("consulmedTO",consulmedTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA_CONSULMED);
		modelAndView.setViewName("solConsulta/solConsultaAltaConsulmed");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	// CONSULTAS DE BAJAS
	
	/**
	 * Consulta Solicitud
	 * @param numEmp
	 * @return
	 */
	@RequestMapping(value="consultaBajaSolicitud.htm")
	protected ModelAndView getBajaSolicitud(EmpTO empTO){
		logger.info("SolConsultaController.getBajaSolicitud");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
				
		if((sistemaService.existeSolicitudBaja(empTO.getNumEmp()))!=0){		
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitudBaja(empTO.getNumEmp());
			List<DependienteTO> listaDep = solConsultaService.getDependienteConsultaBaja(solicitudTO.getIdSol());
			List<ConsulmedTO> listaConsulmed = solConsultaService.getConsulmedConsultaBaja(solicitudTO.getIdSol());
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject(empleadoTO);
			modelAndView.addObject(solicitudTO);
			modelAndView.addObject("listaDep",listaDep);
			modelAndView.addObject("listaConsulmed",listaConsulmed);
			modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA);
			modelAndView.addObject("consentimientoLlenado", Codigo.CONSENTIMIENTO_LLENADO);
			modelAndView.addObject("consentimientoFirma", Codigo.CONSENTIMIENTO_FIRMA);
			modelAndView.addObject("consentimientoExt", Codigo.CONSENTIMIENTO_EXT);
			modelAndView.setViewName("solConsulta/solConsultaBaja");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_BAJA_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta Lista de Dependientes
	 * @param numEmp
	 * @return
	 */
	@RequestMapping(value="consultaBajaIdDependiente.htm")
	protected ModelAndView getBajaIdDependiente(EmpTO empTO){
		logger.info("SolConsultaController.getBajaIdDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitudBajaDep(empTO.getNumEmp()))!=0){	
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA,
																		Codigo.EST_SOL_ALTA_AUTORIZADA, 
																		Codigo.EST_SOL_BAJA_EN_PROCESO);
			
			if(solicitudTO.getIdSol().equals(0)){
				solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
							Codigo.TIPO_SOL_BAJA,
							Codigo.EST_SOL_BAJA_AUTORIZADA, 
							Codigo.EST_SOL_BAJA_AUTORIZADA);
			}
			
			List<Integer> listaDep = solConsultaService.getDependienteBajaxId(solicitudTO.getIdSol());
			modelAndView.addObject(empleadoTO);
			modelAndView.addObject(solicitudTO);
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("listaDep",listaDep);
			if(listaDep.size()==0){modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_NO_DEP);}
			modelAndView.setViewName("solConsulta/solConsultaBajaDependienteId");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	/**
	 * Consulta el Detalle del Dependiente
	 * @param numEmp
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaBajaDependiente.htm")
	protected ModelAndView getBajaDependiente(@RequestParam Integer numEmp,@RequestParam String nombreEmp,@RequestParam Integer perfilEmp, @RequestParam Integer idDependiente){
		logger.info("SolConsultaController.getBajaDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmp);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmp, 
																	Codigo.TIPO_SOL_ALTA,
																	Codigo.EST_SOL_ALTA_AUTORIZADA, 
																	Codigo.EST_SOL_ALTA_AUTORIZADA);
		DependienteTO dependienteTO = solConsultaService.getDependienteBajaDetalle(idDependiente);
		empTO.setNumEmp(numEmp);
		empTO.setNombreEmp(nombreEmp);
		empTO.setPerfilEmp(perfilEmp);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("empleadoTO",empleadoTO);
		modelAndView.addObject("solicitudTO",solicitudTO);
		modelAndView.addObject("dependienteTO",dependienteTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA_DEPENDIENTE);
		modelAndView.addObject("consentimientoLlenado", Codigo.CONSENTIMIENTO_LLENADO);
		modelAndView.addObject("consentimientoFirma", Codigo.CONSENTIMIENTO_FIRMA);
		modelAndView.addObject("consentimientoExt", Codigo.CONSENTIMIENTO_EXT);
		modelAndView.setViewName("solConsulta/solConsultaBajaDependiente");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta la lista de Dependientes Consulmed	
	 * @param numEmp
	 * @return
	 */
	@RequestMapping(value="consultaBajaIdConsulmed.htm")
	protected ModelAndView getBajaIdConsulmed(EmpTO empTO){
		logger.info("SolConsultaController.getBajaIdConsulmed");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if((sistemaService.existeSolicitudBajaDep(empTO.getNumEmp()))!=0){	
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA,
																		Codigo.EST_SOL_ALTA_AUTORIZADA, 
																		Codigo.EST_SOL_BAJA_EN_PROCESO);
			
			if(solicitudTO.getIdSol().equals(0)){
				solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
						Codigo.TIPO_SOL_BAJA,
						Codigo.EST_SOL_BAJA_AUTORIZADA, 
						Codigo.EST_SOL_BAJA_AUTORIZADA);
			}
						
			List<Integer> listaConsulmed = solConsultaService.getConsulmedBajaxId(solicitudTO.getIdSol());
			modelAndView.addObject(empleadoTO);
			modelAndView.addObject(solicitudTO);
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("listaConsulmed",listaConsulmed);
			if(listaConsulmed.size()==0){modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_NO_DEP);}
			modelAndView.setViewName("solConsulta/solConsultaBajaConsulmedId");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta el Detalle del Dependiente Consulmed
	 * @param numEmp
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaBajaConsulmed.htm")
	protected ModelAndView getBajaConsulmed(@RequestParam Integer numEmp,@RequestParam String nombreEmp,@RequestParam Integer perfilEmp, @RequestParam Integer idDependiente){
		logger.info("SolConsultaController.getBajaConsulmed");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmp);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmp, 
																	Codigo.TIPO_SOL_ALTA,
																	Codigo.EST_SOL_ALTA_AUTORIZADA, 
																	Codigo.EST_SOL_ALTA_AUTORIZADA);
		ConsulmedTO consulmedTO = solConsultaService.getConsulmedBajaDetalle(idDependiente);
		empTO.setNumEmp(numEmp);
		empTO.setNombreEmp(nombreEmp);
		empTO.setPerfilEmp(perfilEmp);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject(empleadoTO);
		modelAndView.addObject(solicitudTO);
		modelAndView.addObject("consulmedTO",consulmedTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA_CONSULMED);
		modelAndView.setViewName("solConsulta/solConsultaBajaConsulmed");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	
	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}

	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}
	
}
