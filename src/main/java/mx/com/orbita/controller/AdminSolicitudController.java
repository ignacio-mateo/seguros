package mx.com.orbita.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.AdminSolicitudService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudAdminTO;
import mx.com.orbita.util.Codigo;

@Controller
public class AdminSolicitudController {
	
	private static final Logger logger = Logger.getLogger(AdminSolicitudController.class);
	
	AdminSolicitudService adminSolicitudService;
	SolAltaService solAltaService;

	/**
	 * Metodo de carga incial
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="consultaSolicitudAdmin.htm")
	protected ModelAndView getSolicitudAdmin(EmpTO empTO){
		logger.info("AdminSolicitudController.getSolicitudAdmin");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/solicitud/solicitudAdminBusqueda");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Metodo para buscar solicitudes  
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="consultaSolicitudAdminUsuario.htm")
	protected ModelAndView getSolicitudAdminUsuario(EmpTO empTO){
		logger.info("AdminSolicitudController.getSolicitudAdminUsuario");
		ModelAndView modelAndView = new ModelAndView();
		Integer solicitud = adminSolicitudService.getSolicitudExiste(empTO.getNumEmpSolicitado());
		if(solicitud.equals(0)){
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.EMPLEADO_NO_ENCONTRADO);
			modelAndView.setViewName("administrador/solicitud/solicitudAdminBusqueda");
		}else{
			SolicitudAdminTO solicitudAdminTO = adminSolicitudService.getSolicitud(empTO.getNumEmpSolicitado());
			if(solicitudAdminTO.getNumEmpSolicitado().equals(0)){
				List<SolicitudAdminTO> listSolicitud = adminSolicitudService.getSolicitudes(empTO.getNumEmpSolicitado());
				List<SolicitudAdminTO> listDependiente = adminSolicitudService.getDependiente(empTO.getNumEmpSolicitado());
				List<SolicitudAdminTO> listConsulmed = adminSolicitudService.getConsulmed(empTO.getNumEmpSolicitado());
				List<SolicitudAdminTO> listMemorial = adminSolicitudService.getMemorial(empTO.getNumEmpSolicitado());
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("listSolicitud", listSolicitud);
				modelAndView.addObject("listDependiente", listDependiente);
				modelAndView.addObject("listConsulmed", listConsulmed);
				modelAndView.addObject("listMemorial", listMemorial);
				modelAndView.setViewName("administrador/solicitud/solicitudAdminDetalleBaja");
			}else{
				List<SolicitudAdminTO> listSolicitud = adminSolicitudService.getSolicitudes(empTO.getNumEmpSolicitado());
				List<SolicitudAdminTO> listDependiente = adminSolicitudService.getDependiente(empTO.getNumEmpSolicitado());
				List<SolicitudAdminTO> listConsulmed = adminSolicitudService.getConsulmed(empTO.getNumEmpSolicitado());
				List<SolicitudAdminTO> listMemorial = adminSolicitudService.getMemorial(empTO.getNumEmpSolicitado());
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("solicitudAdminTO", solicitudAdminTO);
				modelAndView.addObject("listSolicitud", listSolicitud);
				modelAndView.addObject("listDependiente", listDependiente);
				modelAndView.addObject("listConsulmed", listConsulmed);
				modelAndView.addObject("listMemorial", listMemorial);
				modelAndView.setViewName("administrador/solicitud/solicitudAdminDetalle");
			}
		}	
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	@RequestMapping(value="editarSolicitudAdmin.htm")
	protected ModelAndView getSolicitudEdicion(SolicitudAdminTO solicitudAdminTO){
		logger.info("AdminSolicitudController.getSolicitudEdicion");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(solicitudAdminTO.getNumEmp());
		empTO.setNombreEmp(solicitudAdminTO.getNombreEmp());
		empTO.setPerfilEmp(solicitudAdminTO.getPerfilEmp());
		
		EmpleadoTO empleadoTO = new EmpleadoTO();
		empleadoTO = solAltaService.getEmpleadoWS(solicitudAdminTO.getNumEmpSolicitado());
		solicitudAdminTO.setNombre(empleadoTO.getNombre());
		solicitudAdminTO.setApellidoP(empleadoTO.getApellidoP());
		solicitudAdminTO.setApellidoM(empleadoTO.getApellidoM());
		
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("solicitudAdminTO", solicitudAdminTO);
		modelAndView.setViewName("administrador/solicitud/solicitudAdminEditarSol");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="editarDependienteAdmin.htm")
	protected ModelAndView getDependienteEdicion(SolicitudAdminTO solicitudAdminTO){
		logger.info("AdminSolicitudController.getDependienteEdicion");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(solicitudAdminTO.getNumEmp());
		empTO.setNombreEmp(solicitudAdminTO.getNombreEmp());
		empTO.setPerfilEmp(solicitudAdminTO.getPerfilEmp());
		empTO.setNumEmpSolicitado(solicitudAdminTO.getNumEmpSolicitado());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("solicitudAdminTO", solicitudAdminTO);
		modelAndView.setViewName("administrador/solicitud/solicitudAdminEditarDep");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	@RequestMapping(value="editarConsumlmedAdmin.htm")
	protected ModelAndView getConsulmedEdicion(SolicitudAdminTO solicitudAdminTO){
		logger.info("AdminSolicitudController.getConsulmedEdicion");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(solicitudAdminTO.getNumEmp());
		empTO.setNombreEmp(solicitudAdminTO.getNombreEmp());
		empTO.setPerfilEmp(solicitudAdminTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("solicitudAdminTO", solicitudAdminTO);
		modelAndView.setViewName("administrador/solicitud/solicitudAdminEditarCon");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="guardarSolicitudAdmin.htm")
	protected ModelAndView setSolicitudEdicion(SolicitudAdminTO solicitudAdminTO){
		logger.info("AdminSolicitudController.setSolicitudEdicion");
		logger.info("  Parametros de Peticion [idSolicitud: "+solicitudAdminTO.getIdSolicitud()
						+" numEmp: "+solicitudAdminTO.getNumEmpSolicitado()+" fecha: "+solicitudAdminTO.getFechaAlta()+"]");
		adminSolicitudService.setSolicitud(solicitudAdminTO);
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(solicitudAdminTO.getNumEmp());
		empTO.setNombreEmp(solicitudAdminTO.getNombreEmp());
		empTO.setPerfilEmp(solicitudAdminTO.getPerfilEmp());
		empTO.setNumEmpSolicitado(solicitudAdminTO.getNumEmpSolicitado());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="guardarDependienteAdmin.htm")
	protected ModelAndView setDependienteEdicion(SolicitudAdminTO solicitudAdminTO){
		logger.info("AdminSolicitudController.setDependienteEdicion");
		logger.info("  Parametros de Peticion [idDependiente: "+solicitudAdminTO.getIdDependiente()
						+" numEmp: "+solicitudAdminTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(solicitudAdminTO.getNumEmp());
		empTO.setNombreEmp(solicitudAdminTO.getNombreEmp());
		empTO.setPerfilEmp(solicitudAdminTO.getPerfilEmp());
		adminSolicitudService.setDependiente(solicitudAdminTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="guardarConsulmedAdmin.htm")
	protected ModelAndView setConsulmedEdicion(SolicitudAdminTO solicitudAdminTO){
		logger.info("AdminSolicitudController.setConsulmedEdicion");
		logger.info("  Parametros de Peticion [idConsulmed: "+solicitudAdminTO.getIdConsulmed()
						+" numEmp: "+solicitudAdminTO.getNumEmpSolicitado()+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(solicitudAdminTO.getNumEmp());
		empTO.setNombreEmp(solicitudAdminTO.getNombreEmp());
		empTO.setPerfilEmp(solicitudAdminTO.getPerfilEmp());
		adminSolicitudService.setConsulmed(solicitudAdminTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
		
	@Autowired
	public void setAdminSolicitudService(AdminSolicitudService adminSolicitudService) {
		this.adminSolicitudService = adminSolicitudService;
	}

	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}
	
}