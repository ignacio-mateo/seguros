package mx.com.orbita.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import mx.com.orbita.service.DatosFiscalesService;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.to.DatFiscalesTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.util.Codigo;

@Controller
public class DatosFiscalesController {
	
	private static final Logger logger = Logger.getLogger(DatosFiscalesController.class);
	
	private DatosFiscalesService datosFiscalesService;
	private SistemaService sistemaService;
	
	@RequestMapping(value="consultaDatosFiscales.htm")
	protected ModelAndView getDatosFiscales(EmpTO empTO){
		logger.info("DatosFiscalesController.getDatosFiscales");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		if(Codigo.FLAG_DATOS_FISCALES){
			modelAndView.addObject("mensaje1", Codigo.AVISO_DATOS_FISCALES);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}else{
			if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){
				DatFiscalesTO datFiscalesTO = new DatFiscalesTO(); 
				datFiscalesTO = datosFiscalesService.getDatosFiscales(empTO.getNumEmp());
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("datFiscalesTO", datFiscalesTO);
				modelAndView.setViewName("datosFiscales/datosFiscalesRegistro");
			}else{
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
				modelAndView.addObject("mensaje3", Codigo.EXTENSIONES);
				modelAndView.setViewName("solEstatus/solEstatus");
			}
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value ="registroDatosFiscales.htm")
	public ModelAndView setDatosFiscales(DatFiscalesTO datFiscalesTO) {
		logger.info("DatosFiscalesController.setDatosFiscales");
		logger.info("  Entidad: "+datFiscalesTO.getEntidad());
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		datosFiscalesService.setDatosFiscales(datFiscalesTO);
		empTO.setNumEmp(datFiscalesTO.getNum_empleado());
		empTO.setNombreEmp(datFiscalesTO.getNombre());
		empTO.setPerfilEmp(datFiscalesTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("datosFiscales/datosFiscalesExito");
		logger.info("  * Fin Peticion");
		return modelAndView;
	 }
	
	@RequestMapping(value ="registroDatosFiscalesSol.htm")
	public String setDatosFiscalesSol(DatFiscalesTO datFiscalesTO, RedirectAttributes atributos) {
		logger.info("DatosFiscalesController.setDatosFiscalesSol");
		EmpTO empTO = new EmpTO();
		//datosFiscalesService.setDatosFiscales(datFiscalesTO);
		empTO.setNumEmp(datFiscalesTO.getNum_empleado());
		empTO.setNombreEmp(datFiscalesTO.getNombre());
		empTO.setPerfilEmp(datFiscalesTO.getPerfilEmp());
		atributos.addFlashAttribute("empTO", empTO);
		return "redirect:solicitud.htm";
	 }
	
	@Autowired
	public void setDatosFiscalesService(DatosFiscalesService datosFiscalesService) {
		this.datosFiscalesService = datosFiscalesService;
	}

	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}
	
}
