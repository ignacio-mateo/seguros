package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.ContactoService;
import mx.com.orbita.to.ContactoTO;
import mx.com.orbita.to.EmpTO;

@Controller
public class ContactoController {

	private static final Logger logger = Logger.getLogger(ContactoController.class);

	private ContactoService contactoService;

	@RequestMapping(value="/contactos.htm", method=RequestMethod.POST)
	protected ModelAndView getContactos(EmpTO empTO){
		logger.info("ContactoController.getContactos");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		List<ContactoTO> contactos = new ArrayList<ContactoTO>();
		contactos = contactoService.getContacto();
		logger.info("  ListaContactos: "+contactos.size());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("contactos", contactos);
		modelAndView.setViewName("contacto/contactos");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="/documentos.htm", method=RequestMethod.POST)
	protected ModelAndView getDocumentos(EmpTO empTO){
		logger.info("ContactoController.getDocumentos");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("contacto/documentos");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}



	@Autowired
	public void setContactoService(ContactoService contactoService) {
		this.contactoService = contactoService;
	}


}
