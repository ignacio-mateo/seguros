package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.controller.form.AutorizacionForm;
import mx.com.orbita.service.AutorizarSolService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.AutorizacionTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;

@Controller
public class AutorizarSolController{

	public static final Logger logger = Logger.getLogger(AutorizarSolController.class);
	
	private AutorizarSolService autorizarSolService;
	private SolConsultaService solConsultaService;
	
	
	/**
	 * Consulta Solicitudes de Alta 
	 * @param empTO
	 * 
	 */
	@RequestMapping(value="consultaAltaSolitudesxAut.htm")
	protected ModelAndView getAltaSolitudesxAut(EmpTO empTO){
		logger.info("AutorizarSolController.getAltaSolitudesxAut");
		ModelAndView modelAndView = new ModelAndView();
		AutorizacionForm autorizacionForm = new AutorizacionForm();
		List<AutorizacionTO> listSol = new ArrayList<AutorizacionTO>();
		logger.info("  idOperacion: ["+Codigo.TIPO_SOL_ALTA+"] tipoSol: ["+Codigo.TIPO_SOL_ALTA+"] estatusSol: ["+Codigo.EST_SOL_ALTA_EN_PROCESO+"]");
		listSol = autorizarSolService.getProcesarSolicitudes(Codigo.TIPO_SOL_ALTA, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO);
		logger.info("  listaSol: ["+listSol.size()+"]");
		autorizacionForm.setListSol(listSol);
		autorizacionForm.setTamListSol(listSol.size());
		autorizacionForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo",Codigo.TITULO_ALTA_AUTORIZAR_SOL);
		modelAndView.addObject("autorizacionForm",autorizacionForm);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionSolicitudAlta");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta detalle de la solicitud
	 * @param paramNumEmp
	 * @param paramNombreEmp
	 * @param paramPerfilEmp
	 * @param numEmpSolicitado
	 * @return
	 */
	@RequestMapping(value="consultaDetalleSolicitudAlta.htm")
	protected ModelAndView getDetalleSolicitudAlta(@RequestParam Integer paramNumEmp, 
													@RequestParam String paramNombreEmp,
													@RequestParam Integer paramPerfilEmp, 
													@RequestParam Integer numEmpSolicitado){
		logger.info("AutorizarSolController.getDetalleSolicitudAlta");
		logger.info("  Parametros de Peticion [numEmp: "+numEmpSolicitado+" numEmp: "+paramNumEmp+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmpSolicitado);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmpSolicitado, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_EN_PROCESO);
		List<DependienteTO> listaDep = solConsultaService.getDependiente(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_RECHAZADA);
		List<ConsulmedTO> listaConsulmed = solConsultaService.getConsulmed(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_RECHAZADA);
		List<ConsulmedTO> listaMemorial = solConsultaService.getMemorial(solicitudTO.getIdSol());
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idSol: ["+solicitudTO.getIdSol()+"] listDep: ["+listaDep.size()+"] listCon: ["+listaConsulmed.size()+"] listMem: ["+listaMemorial.size()+"]");
		empTO.setNumEmp(paramNumEmp);
		empTO.setNombreEmp(paramNombreEmp);
		empTO.setPerfilEmp(paramPerfilEmp);
		modelAndView.addObject(empleadoTO);
		modelAndView.addObject(solicitudTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("listaDep",listaDep);
		modelAndView.addObject("listaMemorial",listaMemorial);
		modelAndView.addObject("listaConsulmed",listaConsulmed);
		modelAndView.addObject("titulo",Codigo.TITULO_SOL_ALTA);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionVerSolicitudAlta");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta Solicitudes de Baja 
	 * @param empTO
	 * 
	 */
	@RequestMapping(value="consultaBajaSolitudesxAut.htm")
	protected ModelAndView getBajaSolitudesxAut(EmpTO empTO){
		logger.info("AutorizarSolController.getBajaSolitudesxAut");
		ModelAndView modelAndView = new ModelAndView();
		AutorizacionForm autorizacionForm = new AutorizacionForm();
		List<AutorizacionTO> listSol = new ArrayList<AutorizacionTO>();
		logger.info("  idOperacion: ["+Codigo.ID_OPERACION_BAJA+"] tipoSol: ["+Codigo.TIPO_SOL_ALTA+"] estatusSol: ["+Codigo.EST_SOL_BAJA_EN_PROCESO+"]");
		listSol = autorizarSolService.getProcesarSolicitudesBaja(Codigo.ID_OPERACION_BAJA, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_BAJA_EN_PROCESO);
		logger.info("  listaSol: ["+listSol.size()+"]");
		autorizacionForm.setTamListSol(listSol.size());
		autorizacionForm.setListSol(listSol);
		autorizacionForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo",Codigo.TITULO_BAJA_AUTORIZAR_SOL);
		modelAndView.addObject("autorizacionForm",autorizacionForm);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionSolicitudBaja");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta el detalle de la solicitud de baja
	 * @param paramNumEmp
	 * @param paramNombreEmp
	 * @param paramPerfilEmp
	 * @param numEmpSolicitado
	 * @return
	 */
	@RequestMapping(value="consultaDetalleSolicitudBaja.htm")
	protected ModelAndView getDetalleSolicitudBaja(@RequestParam Integer paramNumEmp,
													@RequestParam String paramNombreEmp,
													@RequestParam Integer paramPerfilEmp, 
													@RequestParam Integer numEmpSolicitado){
		logger.info("AutorizarSolController.getDetalleSolicitudBaja");
		logger.info("  Parametros de Peticion [numEmp: "+numEmpSolicitado+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmpSolicitado);
		SolicitudTO solicitudTO = solConsultaService.getSolicitudBaja(numEmpSolicitado);
		List<DependienteTO> listaDep = solConsultaService.getDependienteConsultaBaja(solicitudTO.getIdSol());
		List<ConsulmedTO> listaConsulmed = solConsultaService.getConsulmedConsultaBaja(solicitudTO.getIdSol());
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idSol: ["+solicitudTO.getIdSol()+"] listDep: ["+listaDep.size()+"] listCon: ["+listaConsulmed.size()+"]");
		empTO.setNumEmp(paramNumEmp);
		empTO.setNombreEmp(paramNombreEmp);
		empTO.setPerfilEmp(paramPerfilEmp);
		modelAndView.addObject(empleadoTO);
		modelAndView.addObject(solicitudTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("listaDep",listaDep);
		modelAndView.addObject("titulo",Codigo.TITULO_SOL_BAJA);
		modelAndView.addObject("listaConsulmed",listaConsulmed);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionVerSolicitudBaja");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Actualizaciones de Alta de Solicitudes
	 * @param autorizacionForm
	 * @return modelAndView
	 */
	@RequestMapping(value="autorizarSolAlta.htm")
	protected ModelAndView setProcesarSolAlta(@ModelAttribute("autorizacionForm") AutorizacionForm autorizacionForm){
		logger.info("AutorizarSolController.setProcesarSolAlta");
		logger.info("  Parametros de Peticion [#Solicitudes: "+autorizacionForm.getListSol().size()+"]");
		ModelAndView modelAndView = new ModelAndView();
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = autorizacionForm.getListSol();
		EmpTO empTO = autorizacionForm.getEmpTO();
		autorizarSolService.setProcesarSolAlta(lista, empTO.getNumEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;		
	}
	
	/**
	 * Actualizaciones de Bajas de Solicitudes
	 * @param autorizacionForm
	 * @return
	 */
	@RequestMapping(value="autorizarSolBaja.htm")
	protected ModelAndView setProcesarSolBaja(@ModelAttribute("autorizacionForm") AutorizacionForm autorizacionForm){
		logger.info("AutorizarSolController.setProcesarSolBaja");
		logger.info("  Parametros de Peticion [#Solicitudes: "+autorizacionForm.getListSol().size()+"]");
		ModelAndView modelAndView = new ModelAndView();
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		lista = autorizacionForm.getListSol();
		EmpTO empTO = autorizacionForm.getEmpTO();
		autorizarSolService.setProcesarSolBaja(lista, empTO.getNumEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;		
	}
	
	
	
	// DEPENDIENTES
	/**
	 * Consulta altas de adiciones
	 * @param empTO 
	 * 
	 */
	@RequestMapping(value="consultaAltaDependientesxAut.htm")
	protected ModelAndView getAltaDependientesxAut(EmpTO empTO){
		logger.info("AutorizarSolController.getAltaDependientesxAut");
		ModelAndView modelAndView = new ModelAndView();
		AutorizacionForm autorizacionForm = new AutorizacionForm();
		List<AutorizacionTO> listSol = new ArrayList<AutorizacionTO>();
		logger.info("  idOperacion: ["+Codigo.ID_OPERACION_ALTA+"] tipoSol: ["+Codigo.ID_DEPENDIENTE+"] " +
						"estatusSol: ["+Codigo.TIPO_SOL_ALTA+"] estatusCicloSol: ["+Codigo.EST_SOL_ALTA_AUTORIZADA+"] " +
						"estatusDep: ["+Codigo.EST_SOL_ALTA_EN_PROCESO+"]");
		listSol = autorizarSolService.getProcesarDependientes(Codigo.ID_OPERACION_ALTA, 
																Codigo.ID_DEPENDIENTE, 
																Codigo.TIPO_SOL_ALTA, 
																Codigo.EST_SOL_ALTA_AUTORIZADA, 
																Codigo.EST_SOL_ALTA_EN_PROCESO);
		logger.info("  listSol: ["+listSol.size()+"]");
		autorizacionForm.setTamListSol(listSol.size());
		autorizacionForm.setListSol(listSol);
		autorizacionForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo",Codigo.TITULO_ALTA_AUTORIZAR_DEP);
		modelAndView.addObject("autorizacionForm",autorizacionForm);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionDependienteAlta");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta el detalle de las altas de adiciones
	 * @param paramNumEmp
	 * @param paramNombreEmp
	 * @param paramPerfilEmp
	 * @param numEmpSolicitado
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaDetalleDependienteAlta.htm")
	protected ModelAndView getDetalleDependienteAlta(@RequestParam Integer paramNumEmp,
														@RequestParam String paramNombreEmp,
														@RequestParam Integer paramPerfilEmp, 
														@RequestParam Integer numEmpSolicitado, 
														@RequestParam Integer idDependiente){
		logger.info("AutorizarSolController.getDetalleDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+numEmpSolicitado+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmpSolicitado);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmpSolicitado, 
																	Codigo.TIPO_SOL_ALTA, 
																	Codigo.EST_SOL_ALTA_AUTORIZADA,
																	Codigo.EST_SOL_ALTA_AUTORIZADA);
		DependienteTO dependienteTO = solConsultaService.getDependienteAltaDetalle(idDependiente);
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idSol: ["+solicitudTO.getIdSol()+"] idDep: ["+dependienteTO.getIdDependiente()+"]");
		empTO.setNumEmp(paramNumEmp);
		empTO.setNombreEmp(paramNombreEmp);
		empTO.setPerfilEmp(paramPerfilEmp);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("empleadoTO",empleadoTO);
		modelAndView.addObject("solicitudTO",solicitudTO);
		modelAndView.addObject("dependienteTO",dependienteTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA_DEPENDIENTE);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionVerDependienteAlta");
		logger.info("  * Fin Peticion");
		return modelAndView;		
	}
	
	/**
	 * Consulta bajas de adiciones
	 * @param empTO  
	 * 
	 */
	@RequestMapping(value="consultaBajaDependientesxAut.htm")
	protected ModelAndView getBajaDependientesxAut(EmpTO empTO){
		logger.info("AutorizarSolController.getBajaDependientesxAut");
		ModelAndView modelAndView = new ModelAndView();
		AutorizacionForm autorizacionForm = new AutorizacionForm();
		List<AutorizacionTO> listSol = new ArrayList<AutorizacionTO>();
		logger.info("  idOperacion: ["+Codigo.ID_OPERACION_BAJA+"] tipoSol: ["+Codigo.ID_DEPENDIENTE+"] " +
						"estatusSol: ["+Codigo.TIPO_SOL_ALTA+"] estatusCicloSol: ["+Codigo.EST_SOL_ALTA_AUTORIZADA+"] " +
						"estatusDep: ["+Codigo.EST_SOL_BAJA_EN_PROCESO+"]");
		listSol = autorizarSolService.getProcesarDependientesBaja(Codigo.ID_OPERACION_BAJA, 
																Codigo.ID_DEPENDIENTE, 
																Codigo.TIPO_SOL_ALTA, 
																Codigo.EST_SOL_ALTA_AUTORIZADA, 
																Codigo.EST_SOL_BAJA_EN_PROCESO);
		logger.info("  listSol: ["+listSol.size()+"]");
		autorizacionForm.setTamListSol(listSol.size());
		autorizacionForm.setListSol(listSol);
		autorizacionForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo",Codigo.TITULO_BAJA_AUTORIZAR_DEP);
		modelAndView.addObject("autorizacionForm",autorizacionForm);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionDependienteBaja");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	/**
	 * Consulta el detalle de las bajas de adiciones
	 * @param paramNumEmp
	 * @param paramNombreEmp
	 * @param paramPerfilEmp
	 * @param numEmpSolicitado
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaDetalleDependienteBaja.htm")
	protected ModelAndView getDetalleDependienteBaja(@RequestParam Integer paramNumEmp,
														@RequestParam String paramNombreEmp,
														@RequestParam Integer paramPerfilEmp, 
														@RequestParam Integer numEmpSolicitado, 
														@RequestParam Integer idDependiente){
		logger.info("AutorizarSolController.getDetalleDependienteBaja");
		logger.info("  Parametros de Peticion [numEmp: "+numEmpSolicitado+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmpSolicitado);
		SolicitudTO solicitudTO = solConsultaService.getSolicitudBaja(numEmpSolicitado);
		DependienteTO dependienteTO = solConsultaService.getDependienteBajaDetalle(idDependiente);
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idCon: ["+solicitudTO.getIdSol()+"] idDep: "+dependienteTO.getIdDependiente()+"]");
		empTO.setNumEmp(paramNumEmp);
		empTO.setNombreEmp(paramNombreEmp);
		empTO.setPerfilEmp(paramPerfilEmp);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("empleadoTO",empleadoTO);
		modelAndView.addObject("solicitudTO",solicitudTO);
		modelAndView.addObject("dependienteTO",dependienteTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA_DEPENDIENTE);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionVerDependienteBaja");
		logger.info("  * Fin Peticion");
		return modelAndView;		
	}
		
	/**
	 * Actualizaciones de Alta de Adiciones
	 * @param autorizacionForm
	 * @return
	 */
	@RequestMapping(value="autorizarDepAlta.htm")
	protected ModelAndView setProcesarDepAlta(@ModelAttribute("autorizacionForm") AutorizacionForm autorizacionForm){
		logger.info("AutorizarSolController.setProcesarDepAlta");
		logger.info("  Parametros de Peticion [#Solicitudes: "+autorizacionForm.getListSol().size()+"]");
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		ModelAndView modelAndView = new ModelAndView();
		lista = autorizacionForm.getListSol();
		EmpTO empTO = autorizacionForm.getEmpTO();
		autorizarSolService.setProcesarDepAlta(lista, empTO.getNumEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Actualizaciones de Bajas de Adiciones
	 * @param autorizacionForm
	 * @return
	 */
	@RequestMapping(value="autorizarDepBaja.htm")
	protected ModelAndView setProcesarDepBaja(@ModelAttribute("autorizacionForm") AutorizacionForm autorizacionForm){
		logger.info("AutorizarSolController.setProcesarDepBaja");
		logger.info("  Parametros de Peticion [#Solicitudes: "+autorizacionForm.getListSol().size()+"]");
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		ModelAndView modelAndView = new ModelAndView();
		lista = autorizacionForm.getListSol();
		EmpTO empTO = autorizacionForm.getEmpTO();
		autorizarSolService.setProcesarDepBaja(lista, empTO.getNumEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	
	// CONSULMED
	/**
	 * Consulta Altas Consulmed 
	 * 
	 */
	@RequestMapping(value="consultaAltaConsulmedxAut.htm")
	protected ModelAndView getAltaConxAutorizar(EmpTO empTO){
		logger.info("AutorizarSolController.getAltaConxAutorizar");
		ModelAndView modelAndView = new ModelAndView();
		AutorizacionForm autorizacionForm = new AutorizacionForm();
		List<AutorizacionTO> listSol = new ArrayList<AutorizacionTO>();
		logger.info("  idOperacion: ["+Codigo.ID_OPERACION_ALTA+"] tipoSol: ["+Codigo.ID_CONSULMED+"] " +
				"estatusSol: ["+Codigo.TIPO_SOL_ALTA+"] estatusCicloSol: ["+Codigo.EST_SOL_ALTA_AUTORIZADA+"] " +
				"estatusDep: ["+Codigo.EST_SOL_ALTA_EN_PROCESO+"]");
		listSol = autorizarSolService.getProcesarConsulmed(Codigo.ID_OPERACION_ALTA, 
															Codigo.ID_CONSULMED, 
															Codigo.TIPO_SOL_ALTA, 
															Codigo.EST_SOL_ALTA_AUTORIZADA, 
															Codigo.EST_SOL_ALTA_EN_PROCESO);
		logger.info("  listSol: ["+listSol.size()+"]");		
		autorizacionForm.setTamListSol(listSol.size());
		autorizacionForm.setListSol(listSol);
		autorizacionForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo",Codigo.TITULO_ALTA_AUTORIZAR_CON);
		modelAndView.addObject("autorizacionForm",autorizacionForm);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionConsulmedAlta");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta el detalle de las bajas consulmed
	 * @param paramNumEmp
	 * @param paramNombreEmp
	 * @param paramPerfilEmp
	 * @param numEmpSolicitado
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaDetalleConsulmedAlta.htm")
	protected ModelAndView getDetalleConsulmedAlta(@RequestParam Integer paramNumEmp,
													@RequestParam String paramNombreEmp,
													@RequestParam Integer paramPerfilEmp, 
													@RequestParam Integer numEmpSolicitado, 
													@RequestParam Integer idDependiente){
		logger.info("AutorizarSolController.getDetalleConsulmedAlta");
		logger.info("  Parametros de Peticion [numEmp: "+numEmpSolicitado+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmpSolicitado);
		SolicitudTO solicitudTO = solConsultaService.getSolicitud(numEmpSolicitado, 
																	Codigo.TIPO_SOL_ALTA, 
																	Codigo.EST_SOL_ALTA_AUTORIZADA,
																	Codigo.EST_SOL_ALTA_AUTORIZADA);
		ConsulmedTO consulmedTO = solConsultaService.getConsulmedAltaDetalle(idDependiente);
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idSol: ["+solicitudTO.getIdSol()+"] idCon: ["+consulmedTO.getIdSolicitud()+"]");
		empTO.setNumEmp(paramNumEmp);
		empTO.setNombreEmp(paramNombreEmp);
		empTO.setPerfilEmp(paramPerfilEmp);
		modelAndView.addObject(empleadoTO);
		modelAndView.addObject(solicitudTO);
		modelAndView.addObject("consulmedTO",consulmedTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA_CONSULMED);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionVerConsulmedAlta");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta Bajas Consulmed 
	 * @param empTO
	 * 
	 */
	@RequestMapping(value="consultaBajaConsulmedxAut.htm")
	protected ModelAndView getBajaConxAutorizar(EmpTO empTO){
		logger.info("AutorizarSolController.getBajaConxAutorizar");
		ModelAndView modelAndView = new ModelAndView();
		AutorizacionForm autorizacionForm = new AutorizacionForm();
		List<AutorizacionTO> listSol = new ArrayList<AutorizacionTO>();
		logger.info("  idOperacion: ["+Codigo.ID_OPERACION_BAJA+"] tipoSol: ["+Codigo.ID_CONSULMED+"] " +
						"estatusSol: ["+Codigo.TIPO_SOL_ALTA+"] estatusCicloSol: ["+Codigo.EST_SOL_ALTA_AUTORIZADA+"] " +
						"estatusDep: ["+Codigo.EST_SOL_BAJA_EN_PROCESO+"]");
		listSol = autorizarSolService.getProcesarConsulmedBaja(Codigo.ID_OPERACION_BAJA, 
															Codigo.ID_CONSULMED, 
															Codigo.TIPO_SOL_ALTA, 
															Codigo.EST_SOL_ALTA_AUTORIZADA, 
															Codigo.EST_SOL_BAJA_EN_PROCESO);
		logger.info("  listSol: ["+listSol.size()+"]");
		autorizacionForm.setTamListSol(listSol.size());
		autorizacionForm.setListSol(listSol);
		autorizacionForm.setEmpTO(empTO);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo",Codigo.TITULO_BAJAA_AUTORIZAR_CON);
		modelAndView.addObject("autorizacionForm",autorizacionForm);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionConsulmedBaja");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta el detalle de las bajas consulmed
	 * @param paramNumEmp
	 * @param paramNombreEmp
	 * @param paramPerfilEmp
	 * @param numEmpSolicitado
	 * @param idDependiente
	 * @return
	 */
	@RequestMapping(value="consultaDetalleConsulmedBaja.htm")
	protected ModelAndView getDetalleConsulmedBaja(@RequestParam Integer paramNumEmp,
													@RequestParam String paramNombreEmp,
													@RequestParam Integer paramPerfilEmp, 
													@RequestParam Integer numEmpSolicitado, 
													@RequestParam Integer idDependiente){
		logger.info("AutorizarSolController.getDetalleConsulmedBaja");
		logger.info("  Parametros de Peticion [numEmp: "+numEmpSolicitado+" idDependiente: "+idDependiente+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmpSolicitado);
		SolicitudTO solicitudTO = solConsultaService.getSolicitudBaja(numEmpSolicitado);
		ConsulmedTO consulmedTO = solConsultaService.getConsulmedBajaDetalle(idDependiente);
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idCon: ["+solicitudTO.getIdSol()+"] idDep: "+consulmedTO.getIdConsulmed()+"]");
		empTO.setNumEmp(paramNumEmp);
		empTO.setNombreEmp(paramNombreEmp);
		empTO.setPerfilEmp(paramPerfilEmp);
		modelAndView.addObject(empleadoTO);
		modelAndView.addObject(solicitudTO);
		modelAndView.addObject("consulmedTO",consulmedTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA_CONSULMED);
		modelAndView.setViewName("/administrador/autorizacion/autorizacionVerConsulmedBaja");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Actualizaciones de Altas Consulmed
	 * @param autorizacionForm
	 * @return
	 */
	@RequestMapping(value="autorizarConsulmedAlta.htm")
	protected ModelAndView setProcesarConsulmedAlta(@ModelAttribute("autorizacionForm") AutorizacionForm autorizacionForm){
		logger.info("AutorizarSolController.setProcesarConsulmedAlta");
		logger.info("  Parametros de Peticion [#Solicitudes: "+autorizacionForm.getListSol().size()+"]");
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		ModelAndView modelAndView = new ModelAndView();
		lista = autorizacionForm.getListSol();
		EmpTO empTO = autorizacionForm.getEmpTO();
		autorizarSolService.setProcesarConAlta(lista, empTO.getNumEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Actualizaciones de Bajas de Consulmed
	 * @param autorizacionForm
	 * @return
	 */
	@RequestMapping(value="autorizarConsulmedBaja.htm")
	protected ModelAndView setProcesarConsulmedBaja(@ModelAttribute("autorizacionForm") AutorizacionForm autorizacionForm){
		logger.info("AutorizarSolController.setProcesarConsulmedBaja");
		logger.info("  Parametros de Peticion [#Solicitudes: "+autorizacionForm.getListSol().size()+"]");
		List<AutorizacionTO> lista = new ArrayList<AutorizacionTO>();
		ModelAndView modelAndView = new ModelAndView();
		lista = autorizacionForm.getListSol();
		EmpTO empTO = autorizacionForm.getEmpTO();
		autorizarSolService.setProcesarConBaja(lista, empTO.getNumEmp());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	@Autowired
	public void setAutorizarSolService(AutorizarSolService autorizarSolService) {
		this.autorizarSolService = autorizarSolService;
	}

	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}
		
}