package mx.com.orbita.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.controller.form.SolicitudForm;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.AmpliacionTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DatFiscalesTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;

@Controller
public class SolAltaController{

	private static final Logger logger = Logger.getLogger(SolAltaController.class);
	
	private SolAltaService solAltaService; 
	private SolConsultaService solConsultaService;
	private SistemaService sistemaService;
		
	/**
	 * Calcula el costo de cada dependiente agregado, se manda llamar por medio de AJAX
	 * @param tipoEmp
	 * @param parentesco
	 * @param fecha
	 * @param sexo
	 * @return
	 */
	@RequestMapping(value="/getMontoDependiente.htm", method = RequestMethod.GET)
	protected @ResponseBody String getMontoDependiente(@RequestParam Integer tipoEmp,
														@RequestParam Integer parentesco,
														@RequestParam String fecha,
														@RequestParam String sexo){
		logger.info("SolAltaController.getMontoDependiente");
		logger.info("  Parametros de Peticion [tipoEmp: "+tipoEmp+" parentesco: "+parentesco+" fecha: "+fecha+" sexo:"+sexo+"]");
		Double mensaje = solAltaService.getMontoDependiente(tipoEmp, parentesco, fecha, sexo);
		logger.info("  CostoDependiente: "+mensaje);
		logger.info("  * Fin Peticion");
		return mensaje.toString();
	}
	
	/**
	 * Calcula el costo de la ampliacion, se manda a llamar por medio de AJAX
	 * @param arg0
	 * @param arg1
	 * @param numDep
	 * @param tipoAmpl
	 * @return
	 */
	@RequestMapping(value="/getMontoAmpliacion.htm", method = RequestMethod.GET)
	protected @ResponseBody String getMontoAmpliacion(@RequestParam Integer numDep,@RequestParam Integer tipoAmpl){
		logger.info("SolAltaController.getMontoAmpliacion");
		logger.info("  Parametros de Peticion [numDep: "+numDep+" tipoAmpl: "+tipoAmpl+"]");
		Double mensaje = solAltaService.getMontoAmpliacion(tipoAmpl, numDep);
		logger.info("  CostoAmpliacion: "+mensaje);
		logger.info("  * Fin Peticion");
		return mensaje.toString();
	}
	
	@RequestMapping(value="/getMontoMemorial.htm", method = RequestMethod.GET)
	protected @ResponseBody String getMontoMemorial(@RequestParam Integer idTipoMemorial){
		logger.info("SolAltaController.getMontoMemorial");
	    logger.info("  Parametros de Peticion [tipoMemorial: "+idTipoMemorial+" ]");
	 	Double mensaje = solAltaService.getMontoMemorial(idTipoMemorial);
	 	logger.info("  CostoMemorial: "+mensaje);
	 	logger.info("  * Fin Peticion");
	 	return mensaje.toString();
	}
	
	/**
	 * Consulta una cotizacion
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="/cotizacion.htm", method = RequestMethod.POST)
	protected ModelAndView getCotizacion(EmpTO empTO){
		logger.info("SolAltaController.getCotizacion");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+" nombreEmp: "+empTO.getNombreEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		SolicitudForm solicitudForm = new SolicitudForm();
		Integer existeConyuge = 0;
		
		EmpleadoTO empleadoTO = new EmpleadoTO();
		empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmp());
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] tipoEmp: ["+empleadoTO.getTipoEmp()+"] fechaNac: ["
				+empleadoTO.getFechaNac()+"] sexo:["+empleadoTO.getSexo()+"]");
		empleadoTO = solAltaService.getCotizacion(empleadoTO);
		logger.info("  montoEmp: ["+empleadoTO.getMontoEmp()+"] montoEmpProrra: ["+empleadoTO.getMontoEmpProrateado()+"]");
		
		SolicitudTO solicitudTO = new SolicitudTO();
		solicitudTO.setCostoEmpleado(empleadoTO.getMontoEmpProrateado());
		solicitudTO.setCostoTotal(empleadoTO.getMontoEmpProrateado());
		solicitudTO.setIdTipoSolicitud(1);
		
		List<DependienteTO> listDep = solAltaService.getListaDependiente();
		List<ConsulmedTO> listCon = solAltaService.getListaConsulmed();
		
		List<AmpliacionTO> listaCob = solConsultaService.getTipoCob();
		Map<String,String> mapCob = new HashMap<String, String>();
		for (AmpliacionTO ampliacionTO : listaCob) {
			if(!ampliacionTO.getIdTipoCob().equals(0)){
				mapCob.put(ampliacionTO.getIdTipoCob().toString(), ampliacionTO.getDescripcion().toString());
			}
		}
		
		solicitudForm.setEmpleadoTO(empleadoTO);
		solicitudForm.setSolicitudTO(solicitudTO);
		solicitudForm.setListCon(listCon);
		solicitudForm.setListDep(listDep);
		
		modelAndView.addObject("mapCob", mapCob);
		modelAndView.addObject("existeConyuge", existeConyuge);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_COTIZACION);
		modelAndView.addObject("solicitudForm", solicitudForm);
		modelAndView.setViewName("solAlta/solAltaCotizacion");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	/**
	 * Captuta datos fiscales antes de generar una solicitud de gastos medicos
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="solDatosFiscales.htm", method=RequestMethod.POST)
	protected ModelAndView getSolDatosFiscales(EmpTO empTO){
		logger.info("SolAltaController.getSolDatosFiscales");
		ModelAndView modelAndView = new ModelAndView();
		
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))==0){
			DatFiscalesTO datFiscalesTO = new DatFiscalesTO(); 
			EmpleadoTO empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmp());
			datFiscalesTO.setNum_empleado(empleadoTO.getNumEmp());
			datFiscalesTO.setRfc(empleadoTO.getRfc());
			datFiscalesTO.setNombre(empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM());
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("datFiscalesTO", datFiscalesTO);
			modelAndView.setViewName("datosFiscales/datosFiscalesRegistroSol");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_AUTORIZADA);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta una cotizacion y permite generar una solicitud 
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="solicitud.htm")
	protected ModelAndView getSolicitud(EmpTO empTO){
		logger.info("SolAltaController.getSolicitud");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		SolicitudForm solicitudForm = new SolicitudForm();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		Integer existeConyuge = 0;
				
		empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmp());
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] tipoEmp: ["+empleadoTO.getTipoEmp()+"] fechaNac: ["
								+empleadoTO.getFechaNac()+"] sexo:["+empleadoTO.getSexo()+"]");
		empleadoTO = solAltaService.getCotizacion(empleadoTO);
		logger.info("  montoEmp: ["+empleadoTO.getMontoEmp()+"] montoEmpProrra: ["+empleadoTO.getMontoEmpProrateado()+"]");
			
		SolicitudTO solicitudTO = new SolicitudTO();
		solicitudTO.setCostoEmpleado(empleadoTO.getMontoEmpProrateado());
		solicitudTO.setCostoTotal(empleadoTO.getMontoEmpProrateado());
		solicitudTO.setIdTipoSolicitud(Codigo.TIPO_SOL_ALTA);
			
		List<DependienteTO> listDep = solAltaService.getListaDependiente();
		List<ConsulmedTO> listCon = solAltaService.getListaConsulmed();
		List<ConsulmedTO> listMem = solAltaService.getListaConsulmed();
		
		List<AmpliacionTO> listaCob = solConsultaService.getTipoCob();
		Map<String,String> mapCob = new HashMap<String, String>();
		for (AmpliacionTO ampliacionTO : listaCob) {
			if(!ampliacionTO.getIdTipoCob().equals(0)){
				mapCob.put(ampliacionTO.getIdTipoCob().toString(), ampliacionTO.getDescripcion().toString());
			}
		}
		
		List<AmpliacionTO> listaCobMem = solConsultaService.getTipoCobMem();
		Map<String,String> mapCobMem = new HashMap<String, String>();
		for (AmpliacionTO ampliacionTO : listaCobMem) {
			if(!ampliacionTO.getIdTipoCob().equals(0)){
				mapCobMem.put(ampliacionTO.getIdTipoCob().toString(), ampliacionTO.getDescripcion().toString());
			}
		}
				
		solicitudForm.setEmpleadoTO(empleadoTO);
		solicitudForm.setSolicitudTO(solicitudTO);
		solicitudForm.setListCon(listCon);
		solicitudForm.setListMem(listMem);
		solicitudForm.setListDep(listDep);
		solicitudForm.setEmpTO(empTO);
			
		modelAndView.addObject("mapCob", mapCob);
		modelAndView.addObject("mapCobMem", mapCobMem);
		modelAndView.addObject("existeConyuge", existeConyuge);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA);
		modelAndView.addObject("solicitudForm", solicitudForm);
		modelAndView.setViewName("solAlta/solAltaRegistro");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta una cotizacion de adicion
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="altaDependiente.htm", method=RequestMethod.POST)
	protected ModelAndView getDependiente(EmpTO empTO){
		logger.info("SolAltaController.getDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		SolicitudForm solicitudForm = new SolicitudForm();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		Integer existeConyuge = 0;
		
		if((sistemaService.existeSolicitudAlta(empTO.getNumEmp()))!=0){	
			empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA, 
																		Codigo.EST_SOL_ALTA_EN_PROCESO, 
																		Codigo.EST_SOL_ALTA_AUTORIZADA);
			
			// Lista de Dependientes dados de Alta
			List<DependienteTO> listDepAlta = solConsultaService.getDependiente(solicitudTO.getIdSol(), 
																					Codigo.TIPO_SOL_ALTA, 
																					Codigo.EST_SOL_ALTA_EN_PROCESO, 
																					Codigo.EST_SOL_ALTA_AUTORIZADA);
			
			List<DependienteTO> listDepAltaAux = solConsultaService.getDependiente(solicitudTO.getIdSol(), 
					Codigo.TIPO_SOL_ALTA, 
					Codigo.EST_SOL_BAJA_EN_PROCESO, 
					Codigo.EST_SOL_BAJA_EN_PROCESO);
			listDepAlta.addAll(listDepAltaAux);
			
			logger.info("  numSol: ["+solicitudTO.getIdSol()+"]"+" dependAlta: ["+listDepAlta.size()+"] dependEnProceso: ["+listDepAltaAux.size()+"]");
			
			for (DependienteTO dependienteTO : listDepAlta) {
				if(dependienteTO.getIdParentesco()==3 || dependienteTO.getIdParentesco()==4){
					existeConyuge = 1;
				}
			}
			
			// Lista de Dependientes Disponibles, por default son 10 casillas
			List<DependienteTO> listDep = solAltaService.getListaDependiente();
					
			solicitudForm.setEmpleadoTO(empleadoTO);	
			solicitudForm.setSolicitudTO(solicitudTO);
			solicitudForm.setListDepAlta(listDepAlta);
			solicitudForm.setListDep(listDep);
			solicitudForm.setEmpTO(empTO);
			
			modelAndView.addObject("existeConyuge", existeConyuge);
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA_DEPENDIENTE);
			modelAndView.addObject("solicitudForm", solicitudForm);
			modelAndView.setViewName("solAlta/solAltaDepRegistro");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Consulta una cotizacion de consulmed
	 * @param empTO
	 * @return
	 */
	@RequestMapping(value="altaConsulmed.htm", method=RequestMethod.POST)
	protected ModelAndView getConsulmed(EmpTO empTO){
		logger.info("SolAltaController.getConsulmed");	
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		SolicitudForm solicitudForm = new SolicitudForm();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		
		if((sistemaService.existeSolicitudAlta(empTO.getNumEmp()))!=0){
			empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA, 
																		Codigo.EST_SOL_ALTA_EN_PROCESO, 
																		Codigo.EST_SOL_ALTA_AUTORIZADA);
						
			List<ConsulmedTO> listConAlta = solConsultaService.getConsulmed(solicitudTO.getIdSol(), 
																				Codigo.TIPO_SOL_ALTA, 
																				Codigo.EST_SOL_ALTA_EN_PROCESO, 
																				Codigo.EST_SOL_ALTA_AUTORIZADA);
			
			List<ConsulmedTO> listConAltaAux = solConsultaService.getConsulmed(solicitudTO.getIdSol(), 
					Codigo.TIPO_SOL_ALTA, 
					Codigo.EST_SOL_BAJA_EN_PROCESO, 
					Codigo.EST_SOL_BAJA_EN_PROCESO);
			
			listConAlta.addAll(listConAltaAux);
			logger.info("  numSol: ["+solicitudTO.getIdSol()+"]"+" dependAlta: ["+listConAlta.size()+"] dependEnProceso: ["+listConAltaAux.size()+"]");
						
			List<ConsulmedTO> listCon = solConsultaService.getConsulmedDisponibles(empTO.getNumEmp());		
			solicitudForm.setEmpleadoTO(empleadoTO);
			solicitudForm.setSolicitudTO(solicitudTO);
			if(listCon.size()>0){	solicitudForm.setListCon(listCon);		}
			if(listConAlta.size()>0){	solicitudForm.setListConAlta(listConAlta);	}
			solicitudForm.setEmpTO(empTO);
			
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("titulo", Codigo.TITULO_SOL_ALTA_CONSULMED);
			modelAndView.addObject("solicitudForm", solicitudForm);
			modelAndView.setViewName("solAlta/solAltaConsulmedRegistro");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje3", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
			
	/**
	 * Genera una solicitud de Gastos Medicos
	 * @param solicitud
	 * @return
	 */
	@RequestMapping(value="/guardaSolicitud.htm", method = RequestMethod.POST)
	protected ModelAndView setSolicitud(@ModelAttribute("solicitudForm") SolicitudForm solicitud){
		logger.info("SolAltaController.setSolicitud");
		ModelAndView modelAndView = new ModelAndView();
		EmpleadoTO empleadoTO = solicitud.getEmpleadoTO();		
		SolicitudTO solicitudTO = solicitud.getSolicitudTO();	
		List<DependienteTO> listDep = solicitud.getListDep();
		List<ConsulmedTO> listCon = solicitud.getListCon();
		List<ConsulmedTO> listMem = solicitud.getListMem();
		solAltaService.setSolicitud(empleadoTO,solicitudTO,listDep,listCon,listMem);
		EmpTO empTO = solicitud.getEmpTO();
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM()+" "+Codigo.MSJ_PROCESANDO_SOL);
		modelAndView.addObject("mensaje2", Codigo.MSJ_IMPRIME_SOL);
		modelAndView.addObject("mensaje3", Codigo.MSJ_ALTA_SOL);
		modelAndView.addObject("mensaje4", Codigo.LINK_ALTA);
		modelAndView.setViewName("solEstatus/solEstatus");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Genera una solicitud de adicion
	 * @param dependienteForm
	 * @return
	 */
	@RequestMapping(value="guardaDependiente.htm")
	protected ModelAndView setDependiente(@ModelAttribute("solicitudForm") SolicitudForm dependienteForm){
		logger.info("SolAltaController.setDependiente");
		ModelAndView modelAndView = new ModelAndView();
		EmpleadoTO empleadoTO = dependienteForm.getEmpleadoTO();
		SolicitudTO solicitudTO = dependienteForm.getSolicitudTO();
		List<DependienteTO> listDep = dependienteForm.getListDep();
		solAltaService.setDependiente(listDep, solicitudTO.getIdSol());
		EmpTO empTO = dependienteForm.getEmpTO();
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM()+" "+Codigo.MSJ_PROCESANDO_SOL);
		modelAndView.addObject("mensaje2", Codigo.MSJ_IMPRIME_SOL);
		modelAndView.addObject("mensaje3", Codigo.MSJ_ALTA_DEP);
		modelAndView.addObject("mensaje4", Codigo.SOL_MENSAJE_DEP_VER_ALTA);
		modelAndView.addObject("mensaje5", Codigo.LINK_ALTA_DEP);
		modelAndView.setViewName("solEstatus/solEstatus");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	/**
	 * Genera una solicitud de consulmed
	 * @param consulmed
	 * @return
	 */
	@RequestMapping(value="/guardarConsulmed.htm", method = RequestMethod.POST)
	protected ModelAndView setConsulmed(@ModelAttribute("solicitudForm") SolicitudForm consulmed){
		logger.info("SolAltaController.setConsulmed");
		ModelAndView modelAndView = new ModelAndView();
		EmpleadoTO empleadoTO = consulmed.getEmpleadoTO();
		SolicitudTO solicitudTO = consulmed.getSolicitudTO();
		List<ConsulmedTO> listCon = consulmed.getListCon();
		solAltaService.setConsulmed(listCon, solicitudTO.getIdSol());
		EmpTO empTO = consulmed.getEmpTO();
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1",empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM()+" "+Codigo.MSJ_PROCESANDO_SOL);
		modelAndView.setViewName("solEstatus/solEstatus");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
		
	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}
	
	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}

	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}	
	
}