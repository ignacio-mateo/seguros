package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.controller.form.TarifaForm;
import mx.com.orbita.service.AdminTarifaService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.TarifaTO;
import mx.com.orbita.util.Codigo;

@Controller
public class AdminTarifaController {

	private static final Logger logger = Logger.getLogger(AdminTarifaController.class);
	
	private AdminTarifaService adminTarifaService;

	@RequestMapping(value="/getTarifaConf.htm")
	protected ModelAndView getTarifaConfianza(EmpTO empTO){
		logger.info("AdminTarifaController.getTarifaConfianza");
		ModelAndView modelAndView = new ModelAndView(); 
		TarifaForm tarifaForm = new TarifaForm();
		List<TarifaTO> listTarifa = new ArrayList<TarifaTO>();
		listTarifa = adminTarifaService.getTarifaConfianza();
		logger.info("  ListaTarifa Confianza: "+listTarifa.size());
		tarifaForm.setListTarifa(listTarifa);
		tarifaForm.setEmpTO(empTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("tarifaForm",tarifaForm);
		modelAndView.setViewName("administrador/tarifas/tarifaConfianza");
		return modelAndView;
	}
	
	@RequestMapping(value="/getTarifaSind.htm")
	protected ModelAndView getTarifaSind(EmpTO empTO){
		logger.info("AdminTarifaController.getTarifaSind");
		ModelAndView modelAndView = new ModelAndView();
		TarifaForm tarifaForm = new TarifaForm();
		List<TarifaTO> listTarifa = new ArrayList<TarifaTO>();
		listTarifa = adminTarifaService.getTarifaSindicalizado();
		logger.info("  ListaTarifa Sindicalizado: "+listTarifa.size());
		tarifaForm.setListTarifa(listTarifa);
		tarifaForm.setEmpTO(empTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("tarifaForm",tarifaForm);
		modelAndView.setViewName("administrador/tarifas/tarifaSindicalizado");
		return modelAndView;
	}
	
	@RequestMapping(value="/getTarifaAmplia.htm")
	protected ModelAndView getTarifaAmplia(EmpTO empTO){
		logger.info("AdminTarifaController.getTarifaAmplia");
		ModelAndView modelAndView = new ModelAndView();
		TarifaForm tarifaForm = new TarifaForm();
		List<TarifaTO> listTarifa = new ArrayList<TarifaTO>();
		listTarifa = adminTarifaService.getTarifaAmplia();
		logger.info("  ListaTarifa Amplia: "+listTarifa.size());
		tarifaForm.setListTarifa(listTarifa);
		tarifaForm.setEmpTO(empTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("tarifaForm",tarifaForm);
		modelAndView.setViewName("administrador/tarifas/tarifaAmplia");
		return modelAndView;
	}
	
	@RequestMapping(value="/setTarifaConf.htm", method = RequestMethod.POST)
	protected ModelAndView setTarifaConfianza(@ModelAttribute("tarifaForm") TarifaForm tarifaForm){
		logger.info("AdminTarifaController.setTarifaConfianza "+tarifaForm+" - "+tarifaForm.getListTarifa().size());
		ModelAndView modelAndView = new ModelAndView();
		List<TarifaTO> lista= tarifaForm.getListTarifa();
		EmpTO empTO = tarifaForm.getEmpTO();
		adminTarifaService.setTarifaConfianza(lista);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		return modelAndView;
	}

	@RequestMapping(value="/setTarifaSind.htm", method = RequestMethod.POST)
	protected ModelAndView setTarifaSind(@ModelAttribute("tarifaForm") TarifaForm tarifaForm){
		logger.info("AdminTarifaController.setTarifaSind "+tarifaForm+" - "+tarifaForm.getListTarifa().size());
		ModelAndView modelAndView = new ModelAndView();
		List<TarifaTO> lista= tarifaForm.getListTarifa();
		EmpTO empTO = tarifaForm.getEmpTO();
		adminTarifaService.setTarifaSindicalizado(lista);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		return modelAndView;
	}
	
	@RequestMapping(value="/setTarifaAmplia.htm", method = RequestMethod.POST)
	protected ModelAndView setTarifaAmplia(@ModelAttribute("tarifaForm") TarifaForm tarifaForm){
		logger.info("AdminTarifaController.setTarifaAmplia "+tarifaForm+" - "+tarifaForm.getListTarifa().size());
		ModelAndView modelAndView = new ModelAndView();
		List<TarifaTO> lista= tarifaForm.getListTarifa();
		EmpTO empTO = tarifaForm.getEmpTO();
		adminTarifaService.setTarifaAmplia(lista);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		return modelAndView;
	}
	
	
	@Autowired
	public void setAdminTarifaService(AdminTarifaService adminTarifaService) {
		this.adminTarifaService = adminTarifaService;
	}
	
}
