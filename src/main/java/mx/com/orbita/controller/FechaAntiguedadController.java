package mx.com.orbita.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.FechaAntiguedadService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.FechaAntiguedadTO;
import mx.com.orbita.util.Codigo;

@Controller
public class FechaAntiguedadController {
	
	private static final Logger logger = Logger.getLogger(FechaAntiguedadController.class);
	
	private FechaAntiguedadService fechaAntiguedadService;
		
	@RequestMapping(value="inicioFechaAntiguedad.htm")
	public ModelAndView inicio(EmpTO empTO){
		logger.info("FechaAntiguedadController.inicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/fechaAntiguedad/fechaAntiguedadInicio");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	@RequestMapping(value="actualizarFechaAntiguedad.htm")
	public ModelAndView setFechaAntiguedad(FechaAntiguedadTO fechaAntiguedadTO){
		logger.info("  Parametros de Peticion numEmp: ["+fechaAntiguedadTO.getNumEmpSolicitado()
				+"] onSeleccion: ["+fechaAntiguedadTO.getOnSeleccion()
				+"] fechaElabTit: ["+fechaAntiguedadTO.getFechaElabTit()
				+"] fechaAltaTit: ["+fechaAntiguedadTO.getFechaAltaTit()
				+"] fechaElabDep: ["+fechaAntiguedadTO.getFechaElabDep()
				+"] fechaAltaDep: ["+fechaAntiguedadTO.getFechaAltaDep()
				+"] idDep: ["+fechaAntiguedadTO.getIdDep()+"]");
		
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(fechaAntiguedadTO.getNumEmp());
		empTO.setNombreEmp(fechaAntiguedadTO.getNombreEmp());
		empTO.setPerfilEmp(fechaAntiguedadTO.getPerfilEmp());
				
		Integer existeSol = fechaAntiguedadService.existeSolicitud(fechaAntiguedadTO.getNumEmpSolicitado()); 
		logger.info(" existeSol: ["+existeSol+"]");
		if(existeSol.equals(0)){
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.EMPLEADO_NO_ENCONTRADO);
			modelAndView.setViewName("administrador/fechaAntiguedad/fechaAntiguedadInicio");
		}else{
			fechaAntiguedadService.setFechaAntiguedad(fechaAntiguedadTO);
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
			modelAndView.setViewName("solEstatus/solEstatusAdmin");
		}
				
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@Autowired
	public void setFechaAntiguedadService(FechaAntiguedadService fechaAntiguedadService) {
		this.fechaAntiguedadService = fechaAntiguedadService;
	}

}