package mx.com.orbita.controller;

import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.CambioCoberturaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.AmpliacionTO;
import mx.com.orbita.to.CoberturaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Redondeos;

@Controller
public class CambioCoberturaController {

	private static final Logger logger = Logger .getLogger(CambioCoberturaController.class);

	private CambioCoberturaService cambioCoberturaService;
	private SolConsultaService solConsultaService;

	@RequestMapping(value="inicioCambioCobertura.htm")
	protected ModelAndView inicioCambioCobertura(EmpTO empTO){
		logger.info("CambioCoberturaController.inicioCambioCobertura");
		ModelAndView modelAndView = new ModelAndView();

		List<AmpliacionTO> listaCob = solConsultaService.getTipoCob();
		modelAndView.addObject("idTipoAmpl", listaCob);
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/cobertura/coberturaInicio");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="consultaCambioCobertura.htm")
	protected ModelAndView getCambioCobertura(EmpTO empTO,
												@RequestParam Integer idTipoAmpl,
												@RequestParam Integer mes,
												@RequestParam Integer dia){
		logger.info("CambioCoberturaController.getCambioCobertura");
		logger.info("  Parametros de Peticion numEmp: ["+empTO.getNumEmpSolicitado()
										+"] idTipoAmpl: ["+idTipoAmpl
										+"] mes: ["+mes+"] dia: ["+dia+"]");

		SolicitudTO solicitudTO = cambioCoberturaService.getSolicitud(empTO.getNumEmpSolicitado(),
												Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);

		ModelAndView modelAndView = new ModelAndView();
		Calendar cal= Calendar.getInstance();
		int anio= cal.get(Calendar.YEAR);
		String fecha = dia+"/"+mes+"/"+anio;
		double montoTotalDep = 0.0;
		double montoAmpliacion = 0.0;
		double montoAmpliacionInd = 0.0;
		double montoAmpAct = 0.0;
		double montoAmpActInd = 0.0;
		double montoAmpProrr = 0.0;
		double montoAmpNvo = 0.0;
		double montoAmpNvoInd = 0.0;
		String tipoPoliza = "";

		if(!solicitudTO.getIdSol().equals(0)){
			EmpleadoTO empleadoTO = cambioCoberturaService.getEmpleado(empTO.getNumEmpSolicitado());
			List<DependienteTO> listDep = cambioCoberturaService.getDependiente(empTO.getNumEmpSolicitado());
			Integer dependientesProceso = cambioCoberturaService.getDependienteProceso(empTO.getNumEmpSolicitado());

			// Suma Dependientes
			for (DependienteTO dependienteTO : listDep) {
				montoTotalDep+=dependienteTO.getCostoDep();
				dependienteTO.setFechaAlta(fecha);
			}

			// Obtener monto de ampliacion prorraeado de la ampliacion original
			if(solicitudTO.getIdTipoCobertura()!=0){
				montoAmpAct = cambioCoberturaService.getCambioCobertura(solicitudTO.getIdTipoCobertura(), listDep.size(),mes,dia);
				montoAmpActInd = cambioCoberturaService.getCambioCoberturaIndiv(solicitudTO.getIdTipoCobertura(),
						Codigo.DEP_INDIVIDUAL_AMPLIACION, mes, dia);
			}
			montoAmpProrr = solicitudTO.getCostoAmpliacion() - montoAmpAct;

			// Obtener monto de ampliacion
			if(idTipoAmpl!=0){
				montoAmpNvo = cambioCoberturaService.getCambioCobertura(idTipoAmpl, listDep.size(),mes,dia);
				montoAmpNvoInd = cambioCoberturaService.getCambioCoberturaIndiv(idTipoAmpl,
						Codigo.DEP_INDIVIDUAL_AMPLIACION, mes, dia);
			}

			tipoPoliza = solConsultaService.getTipoCob(idTipoAmpl);
			solicitudTO.setFechaElaboracion(fecha);

			montoAmpliacion = montoAmpNvo + montoAmpProrr;
			montoAmpliacionInd = montoAmpliacion / (listDep.size()+1);

			// Suma Total
			double montoTotalParcial = solicitudTO.getCostoEmpleado() + montoTotalDep + montoAmpNvo;
			double montoTotal = solicitudTO.getCostoEmpleado() + montoTotalDep + montoAmpliacion;
			logger.info("  montoTotal: ["+montoTotal
							+"] montoAmpInd: ["+montoAmpliacionInd
							+"] montoAmp: ["+montoAmpliacion
							+"] montoAmpProrr: ["+Redondeos.roundNum(montoAmpProrr)
							+"] montoTotalDep: ["+montoTotalDep
							+"] listDep: ["+listDep.size()
							+"] depProceso: ["+dependientesProceso
							+"] tipoAmpNvo: ["+tipoPoliza+"]");

			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("empleadoTO", empleadoTO);
			modelAndView.addObject("solicitudTO", solicitudTO);
			modelAndView.addObject("listDep", listDep);
			modelAndView.addObject("montoAmpNvoInd", montoAmpNvoInd);
			modelAndView.addObject("montoAmpNvo", montoAmpNvo);
			modelAndView.addObject("montoAmpProrr", Redondeos.roundNum(montoAmpProrr));
			modelAndView.addObject("montoTotalDep", montoTotalDep);
			modelAndView.addObject("montoTotalParcial", Redondeos.roundNum(montoTotalParcial));
			modelAndView.addObject("montoTotal", Redondeos.roundNum(montoTotal));
			modelAndView.addObject("idTipoAmpl",idTipoAmpl);
			modelAndView.addObject("tipoPoliza",tipoPoliza);
			modelAndView.addObject("montoAmpliacion", Redondeos.roundNum(montoAmpliacion));
			modelAndView.addObject("montoAmpliacionInd", montoAmpliacionInd);
			if(dependientesProceso>=1){	modelAndView.addObject("mensaje1", Codigo.DEPENDIENTES_PROCESO);	}
			modelAndView.setViewName("administrador/cobertura/coberturaConsulta");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1",Codigo.EMPLEADO_NO_ENCONTRADO);
			modelAndView.setViewName("administrador/cobertura/coberturaInicio");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="actualizacionCambioCobertura.htm")
	protected ModelAndView setCambioCobertura(CoberturaTO coberturaTO){
		logger.info("CambioCoberturaController.setCambioCobertura");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(coberturaTO.getNumEmp());
		empTO.setNombreEmp(coberturaTO.getNombreEmp());
		empTO.setPerfilEmp(coberturaTO.getPerfilEmp());
		cambioCoberturaService.setCambioCobertura(coberturaTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}


	@Autowired
	public void setCambioCoberturaService(CambioCoberturaService cambioCoberturaService) {
		this.cambioCoberturaService = cambioCoberturaService;
	}

	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}

}
