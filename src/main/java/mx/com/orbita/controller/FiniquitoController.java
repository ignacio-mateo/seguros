package mx.com.orbita.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.FiniquitoService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.FiniquitoTO;
import mx.com.orbita.util.Codigo;

@Controller
public class FiniquitoController {
		
	private static final Logger logger = Logger.getLogger(FiniquitoController.class);
	
	private FiniquitoService finiquitoService;
		
	@RequestMapping(value="generarFiniquitoInicio.htm")
	protected ModelAndView getGeneracionFiniquitoInicio(EmpTO empTO){
		logger.info("FiniquitoController.getGeneracionFiniquitoInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/finiquitos/finiquitoGeneracion");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="consultaFiniquitoInicio.htm")
	protected ModelAndView getConsultaFiniquitoInicio(EmpTO empTO){
		logger.info("FiniquitoController.getConsultaFiniquitoInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/finiquitos/finiquitoConsulta");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	@RequestMapping(value="generaFiniquito.htm")
	protected ModelAndView getGeneracionFiniquito(EmpTO empTO, @RequestParam Integer mes, @RequestParam Integer dia, @RequestParam String fechaFiniq){
		logger.info("FiniquitoController.getGeneracionFiniquitoInicio");
		logger.info("  Parametros de Peticion [numEmpSolicitado: "+empTO.getNumEmpSolicitado()+" mes: "+mes+" dia: "+dia+"]");
		ModelAndView modelAndView = new ModelAndView();
		FiniquitoTO finiquitoTO = new FiniquitoTO();
		Integer existeSol = finiquitoService.getIdSolicitud(empTO.getNumEmpSolicitado());
		if(existeSol!=0){
			// Se cotiza empleado
			finiquitoTO = finiquitoService.calcularFiniquito(empTO.getNumEmpSolicitado(), mes, dia);
			finiquitoTO.setFechaFiniq(fechaFiniq);
			modelAndView.addObject("empTO",empTO);
			modelAndView.addObject("finiquitoTO",finiquitoTO);
			modelAndView.setViewName("administrador/finiquitos/finiquitoGeneraCotizacion");
		}else{
			// Empleado no encontrado
			modelAndView.addObject("empTO",empTO);
			modelAndView.addObject("mensaje1",Codigo.EMPLEADO_NO_ENCONTRADO);
			modelAndView.addObject("finiquitoTO",finiquitoTO);
			modelAndView.setViewName("administrador/finiquitos/finiquitoGeneracion");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="consultaFiniquito.htm")
	protected ModelAndView getConsultaFiniquito(EmpTO empTO){
		logger.info("FiniquitoController.getConsultaFiniquito");
		logger.info("  Parametros de Peticion [numEmpSolicitado: "+empTO.getNumEmpSolicitado()+"]");
		ModelAndView modelAndView = new ModelAndView();
		FiniquitoTO finiquitoTO = new FiniquitoTO();
		FiniquitoTO finiquitoDatosTO = new FiniquitoTO();
		finiquitoDatosTO = finiquitoService.getFiniquito(empTO.getNumEmpSolicitado());
		if(!finiquitoDatosTO.getNumEmpSolicitado().equals(0)){
			// Empleado ya finiquitado
			modelAndView.addObject("mensaje1",Codigo.EMPLEADO_FINIQUITADO);
			modelAndView.addObject("empTO",empTO);
			modelAndView.addObject("finiquitoTO",finiquitoDatosTO);
			modelAndView.setViewName("administrador/finiquitos/finiquitoVisualizaCotizacion");
		}else{
			// Empleado no encontrado
			modelAndView.addObject("empTO",empTO);
			modelAndView.addObject("mensaje1",Codigo.EMPLEADO_NO_FINIQUITADO);
			modelAndView.addObject("finiquitoTO",finiquitoTO);
			modelAndView.setViewName("administrador/finiquitos/finiquitoConsulta");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
		
	@RequestMapping(value="guardarFiniquito.htm")
	protected ModelAndView setFiniquito(FiniquitoTO finiquitoTO){
		logger.info("FiniquitoController.setFiniquito");
		logger.info("  Parametros de Peticion [numDep: "+finiquitoTO.getNumEmpSolicitado()+" idSol: "+finiquitoTO.getIdSol()+" Monto Finiquito: "+finiquitoTO.getMontoFiniquito()+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(finiquitoTO.getNumEmp());
		empTO.setNombreEmp(finiquitoTO.getNombreEmp());
		empTO.setPerfilEmp(finiquitoTO.getPerfilEmp());
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha = new Date();
		try {
			fecha = formatoDelTexto.parse(finiquitoTO.getFechaFiniq());
			finiquitoTO.setFechaFiniquito(fecha);
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage()); 
		}
		finiquitoService.setFiniquito(finiquitoTO);
		modelAndView.addObject("empTO",empTO);
		modelAndView.addObject("mensaje1",Codigo.SOL_MENSAJE_ACT);
		modelAndView.setViewName("solEstatus/solEstatusAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	

	@Autowired
	public void setFiniquitoService(FiniquitoService finiquitoService) {
		this.finiquitoService = finiquitoService;
	}

}