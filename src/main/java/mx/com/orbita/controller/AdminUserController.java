package mx.com.orbita.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.business.EmpleadoBusiness;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.util.Codigo;

@Controller
public class AdminUserController {

	private static final Logger logger = Logger.getLogger(AdminUserController.class);

	private EmpleadoBusiness empleadoBusiness;
	private SistemaService sistemaService;

	// Entrar como Administrador
	@RequestMapping(value="entrarAdmin.htm")
	protected ModelAndView setAdmin(EmpTO empTO){
		logger.info("AdminUserController.getUsuario");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("inicioAdmin");
		return modelAndView;
	}

	// Entrar como Administrador
	@RequestMapping(value="salirAdmin.htm")
	protected ModelAndView setAdminExit(EmpTO empTO){
		logger.info("AdminUserController.setAdminExit");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("inicio");
		return modelAndView;
	}

	// Entrar como un Usuario
	@RequestMapping(value="entrarUsuario.htm")
	protected ModelAndView getUsuario(EmpTO empTO){
		logger.info("AdminUserController.getUsuario");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("administrador/login/loginUser");
		return modelAndView;
	}

	// Autenticar como un Usuario
	@RequestMapping(value="autenticarUsuario.htm", method = RequestMethod.POST)
	protected ModelAndView setUsuario(EmpTO empTO){
		logger.info("AdminUserControllerr.setUsuario");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		Integer idUsuario = empTO.getNumEmpSolicitado();
		EmpleadoTO empleadoTO = empleadoBusiness.consultaEmpleado(idUsuario);
		Integer idPerfil = sistemaService.getPerfilAdmin(idUsuario);

		if(!empleadoTO.getNumEmp().equals(0)){
			empTO.setNumEmp(empleadoTO.getNumEmp());
			empTO.setNombreEmp(empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM());
			empTO.setPerfilEmp(idPerfil);
			modelAndView.addObject("empTO", empTO);
			modelAndView.setViewName("inicio");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.EMPLEADO_NO_ENCONTRADO);
			modelAndView.setViewName("administrador/login/loginUser");
		}
		return modelAndView;
	}

	@Autowired
	public void setEmpleadoBusiness(EmpleadoBusiness empleadoBusiness) {
		this.empleadoBusiness = empleadoBusiness;
	}

	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}

}
