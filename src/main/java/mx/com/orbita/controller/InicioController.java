package mx.com.orbita.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.business.EmpleadoBusiness;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.util.Codigo;

@Controller
public class InicioController {
	
	private static final Logger logger = Logger.getLogger(InicioController.class);
	
	private EmpleadoBusiness empleadoBusiness;
	private SistemaService sistemaService;
	
	@RequestMapping(value="inicio.htm", method = RequestMethod.POST)
	protected ModelAndView login(@RequestParam String numEmp,@RequestParam String passwd){
		logger.info("InicioController.login");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+" passwd: "+passwd+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
						
		Integer empleado = Integer.parseInt(numEmp);
		Integer idPerfil = sistemaService.getPerfilAdmin(empleado);
				
		/*
		 * perfil = '0' usuario normal, '1' administrador
		 * bandera = 'true' bloquear acceso al sistema (RENOV), 'false' acceso al sistema (NORMAL)
		 * 
		 * perfil 0 y bandera true(1)  PERIODO DE RENOVACION Y USER NORMAL
		 * perfil 1 y bandera true(1)  PERIODO NORMAL
		 * 
		 * perfil 0 y bandera false(0)  PERIODO NORMAL
		 * perfil 1 y bandera false(0) PERIODO NORMAL
		 * 
		 */
		if(idPerfil.equals(0) && Codigo.FLAG_PERIODO_RENOVACION){
			logger.info("  PERIODO RENOVACION Y USUARIO NORMAL ");
			modelAndView.addObject("mensaje1", Codigo.AVISO_RENOVACION);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("renovacion/renovacionAviso");
		}else{
			EmpleadoTO empleadoTO = new EmpleadoTO();
			/*
			if(Codigo.LDAP_ACTIVAR){
				logger.info("  PERIODO CON AUTENTICACION ");
				LDAP autenticar = new LDAP();
				WsEmployeeCatalogDOAUTHENTICATEResponse resultado = autenticar.getAutenticacion(numEmp, passwd);
				if(!resultado.getStauthenticate().getCodigo().equals("1")){
					modelAndView.addObject("mensaje1", resultado.getStauthenticate().getDescripcion());
					modelAndView.setViewName("indexInicio");
				}else{
					empleadoTO = empleadoBusiness.consultaEmpleado(empleado);
					empTO.setNumEmp(empleadoTO.getNumEmp());
					empTO.setNombreEmp(empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM());
					empTO.setPerfilEmp(idPerfil);
					logger.info(" Datos: "+empTO.getNumEmp()+" - "+empTO.getPerfilEmp()+" - "+empTO.getNombreEmp());
					modelAndView.addObject("empTO", empTO);
					modelAndView.setViewName("inicio");
				}
			}else{*/
				logger.info("  PERIODO SIN AUTENTICACION ");
				empleadoTO = empleadoBusiness.consultaEmpleado(empleado);
				empTO.setNumEmp(empleadoTO.getNumEmp());
				empTO.setNombreEmp(empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM());
				empTO.setPerfilEmp(idPerfil);
				logger.info(" Datos: "+empTO.getNumEmp()+" - "+empTO.getPerfilEmp()+" - "+empTO.getNombreEmp());
				modelAndView.addObject("empTO", empTO);
				modelAndView.setViewName("inicio");
			//}
		}
		return modelAndView;
	}

	@Autowired
	public void setEmpleadoBusiness(EmpleadoBusiness empleadoBusiness) {
		this.empleadoBusiness = empleadoBusiness;
	}

	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}

	
}