package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.AdminConstanciaService;
import mx.com.orbita.to.ConstanciaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.util.Codigo;

@Controller
public class AdminConstanciaController {
		
	private static final Logger logger = Logger.getLogger(AdminConstanciaController.class);
	
	private AdminConstanciaService adminConstanciaService;

	@RequestMapping(value="initConstancia.htm")
	protected ModelAndView initConstancia(EmpTO empTO){
		logger.info("AdminConstanciaController.initConstancia");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/constancias/constanciaInicio");
		return modelAndView;
	}
			
	@RequestMapping(value="reporteActualGeneraPdf.htm")
	protected ModelAndView getReporteActualPdf(HttpServletResponse response,EmpTO empTO,
			@RequestParam Integer tipoConstancia,@RequestParam String fecha, @RequestParam String comentario){
		logger.info("AdminConstanciaController.reporteActualGeneraPdf");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		ConstanciaTO constanciaTO = new ConstanciaTO();
		constanciaTO = adminConstanciaService.getEmpleado(empTO.getNumEmpSolicitado());
		
		if(tipoConstancia.equals(1) && constanciaTO.getIdTipoSol().equals(1) && constanciaTO.getIdEstatus().equals(2)){
			List<DependienteTO> lista = new ArrayList<DependienteTO>();
			lista = adminConstanciaService.getDependiente(empTO.getNumEmpSolicitado(),Codigo.ID_OPERACION_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
			modelAndView.addObject("tipoConstancia", tipoConstancia);
			modelAndView.addObject("fecha", fecha);
			modelAndView.addObject("comentario", comentario);
			modelAndView.addObject("lista", lista);
			modelAndView.addObject("constanciaTO", constanciaTO);
			response.setHeader("Content-type", "application/pdf");
	        response.setHeader("Content-Disposition","attachment; filename=\"ConstanciaGastosMedicos.pdf\"");  
			modelAndView.setViewName("pdfView");
		}else if(tipoConstancia.equals(2) && constanciaTO.getIdTipoSol().equals(2) && (constanciaTO.getIdEstatus().equals(8) || constanciaTO.getIdEstatus().equals(9))){
			List<DependienteTO> lista = new ArrayList<DependienteTO>();
			lista = adminConstanciaService.getDependiente(empTO.getNumEmpSolicitado(),Codigo.ID_OPERACION_BAJA,Codigo.EST_SOL_BAJA_X_FINIQUITO);
			modelAndView.addObject("tipoConstancia", tipoConstancia);
			modelAndView.addObject("fecha", fecha);
			modelAndView.addObject("comentario", comentario);
			modelAndView.addObject("lista", lista);
			modelAndView.addObject("constanciaTO", constanciaTO);
			response.setHeader("Content-type", "application/pdf");
	        response.setHeader("Content-Disposition","attachment; filename=\"ConstanciaGastosMedicos.pdf\"");
			modelAndView.setViewName("pdfView");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.CONS_ERROR);
			modelAndView.setViewName("administrador/constancias/constanciaInicio");
		}
		return modelAndView;
	}
	
	
	@Autowired
	public void setAdminConstanciaService(AdminConstanciaService adminConstanciaService) {
		this.adminConstanciaService = adminConstanciaService;
	}
	
}
