package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import mx.com.orbita.controller.form.CircularForm;
import mx.com.orbita.service.CircularService;
import mx.com.orbita.service.DatosFiscalesService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.CircularTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DatFiscalesTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Fechas;
import mx.com.orbita.util.LDAP;

@Controller
public class CircularController {
	
	private static final Logger logger = Logger.getLogger(CircularController.class);
	
	private SolAltaService solAltaService; 
	private DatosFiscalesService datosFiscalesService;
	private SolConsultaService solConsultaService;
	private CircularService circularService;

	// Circular Usuario

	@RequestMapping(value="inicioCircular.htm")
	protected ModelAndView inicioCircular(){
		logger.info("CircularController.inicioCircular");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("renovacion/renovacionIndex");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="loginCircular.htm", method = RequestMethod.POST)
	protected String login(@RequestParam Integer numEmp,@RequestParam String passwd, RedirectAttributes atributos){
		logger.info("CircularController.login");
		logger.info("  Parametros de Peticion [numEmp: "+numEmp+" passwd: "+passwd+"]");
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(numEmp);

		if(Codigo.FLAG_ACCESO_CIRCULAR){
			return "redirect:mensaje.htm";
		}else{
			if(Codigo.LDAP_ACTIVAR){
				logger.info("  Periodo Circular con autenticacion ");
				atributos.addFlashAttribute("empTO", empTO);
				return "redirect:consultaCircular.htm";
			}else{
				logger.info("  Periodo Circular sin autenticacion ");
				atributos.addFlashAttribute("empTO", empTO);
				return "redirect:consultaCircular.htm";
			}
		} // Fin del if del periodo circular
	}

	// Si se quita el login, es necesario agregarle la anotacion @RequestParam String numEmp en el parametro
	@RequestMapping(value="consultaCircular.htm")
	protected ModelAndView getCircular(EmpTO empTO){
		logger.info("CircularController.getCircular");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();

		CircularForm circularForm = new CircularForm();
		DatFiscalesTO datosFiscales = new DatFiscalesTO();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		List<DependienteTO> listDep = new ArrayList<DependienteTO>(0);
		List<ConsulmedTO> listCon = new ArrayList<ConsulmedTO>(0);
		Fechas formatoFechas = new Fechas();

		try {
			// Datos del Empleado
			empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmp());

			// Cotizacion del Empleado
			empleadoTO = solAltaService.getCotizacion(empleadoTO, Codigo.MES_RENOVACION, Codigo.DIA_RENOVACION);
			String nombreCompleto = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();

			// Datos Fiscales
			datosFiscales = datosFiscalesService.getDatosFiscales(empTO.getNumEmp());

			// Datos de la Solicitud
			SolicitudTO solicitudTO = solConsultaService.getSolicitudCob(empTO.getNumEmp(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);

			listCon = solConsultaService.getConsulmed(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA);
			listDep = solConsultaService.getDependienteRenov(solicitudTO.getIdSol(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA);

			double costoTotalDep = 0.0;
			if(listDep.size()>0){
				for (DependienteTO dependiente : listDep) {
					dependiente.setCostoDep(solAltaService.getMontoDependiente(empleadoTO.getTipoEmp(), dependiente.getIdParentesco(),
							dependiente.getFechaNac(), dependiente.getSexo(), Codigo.MES_RENOVACION, Codigo.DIA_RENOVACION));
					costoTotalDep = costoTotalDep + dependiente.getCostoDep();
				}
			}

			// Ampliacion
			if(solicitudTO.getIdTipoCobertura()!=0){
				double montoAmpl = solAltaService.getMontoAmpliacion(solicitudTO.getIdTipoCobertura(),
						listDep.size(),
						Codigo.MES_RENOVACION,
						Codigo.DIA_RENOVACION);
				double costoTotal = empleadoTO.getMontoEmpProrateado() + costoTotalDep + montoAmpl;
				double costoTotalAct = costoTotalDep + empleadoTO.getMontoEmpProrateado();
				solicitudTO.setCostoAmpliacion(montoAmpl);
				solicitudTO.setCostoTotalAct(costoTotalAct);
				solicitudTO.setCostoTotal(costoTotal);
			}else{
				double costoTotalAct = costoTotalDep + empleadoTO.getMontoEmpProrateado();
				double costoTotal = empleadoTO.getMontoEmpProrateado() + costoTotalDep;
				solicitudTO.setCostoAmpliacion(0.0);
				solicitudTO.setCostoTotalAct(costoTotalAct);
				solicitudTO.setCostoTotal(costoTotal);
			}

			empleadoTO.setNombre(nombreCompleto);
			empleadoTO.setDescEmpleado("TITULAR");
			empleadoTO.setFechaNacF(formatoFechas.formatoFechas(empleadoTO.getFechaNac(), "dd-MM-yyyy"));
			circularForm.setDatosFiscales(datosFiscales);
			circularForm.setDatosFiscales(datosFiscales);
			circularForm.setSolicitudTO(solicitudTO);
			circularForm.setEmpleadoTO(empleadoTO);
			circularForm.setListDep(listDep);
			circularForm.setListCon(listCon);
			circularForm.setNumDep(listDep.size());

			modelAndView.addObject("circularForm", circularForm);
			modelAndView.setViewName("renovacion/renovacionCircular");
		} catch (Exception e){
			logger.info(" Error 1: "+e.getMessage());
			e.getStackTrace();
			modelAndView.setViewName("renovacion/renovacionIndex");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	} // Fin del metodo


	// Circular Admin

	@RequestMapping(value="inicioCircularAdmin.htm")
	protected ModelAndView inicioCoberturaAdmin(EmpTO empTO){
		logger.info("CircularController.inicioCoberturaAdmin");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("renovacion/renovacionIndexAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="consultaCircularAdmin.htm")
	protected ModelAndView getCircularA3Admin(EmpTO empTO){
		logger.info("CircularController.getCircular");
		logger.info("  Parametros de Peticion [admin: "+empTO.getNumEmpSolicitado()+"]");
		ModelAndView modelAndView = new ModelAndView();

		CircularForm circularForm = new CircularForm();
		DatFiscalesTO datosFiscales = new DatFiscalesTO();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		List<DependienteTO> listDep = new ArrayList<DependienteTO>(0);
		List<ConsulmedTO> listCon = new ArrayList<ConsulmedTO>(0);
		Fechas formatoFechas = new Fechas();

		try {
			// Datos del Empleado
			empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmpSolicitado());

			// Cotizacion del Empleado
			empleadoTO = solAltaService.getCotizacion(empleadoTO, Codigo.MES_RENOVACION, Codigo.DIA_RENOVACION);
			String nombreCompleto = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();

			// Datos Fiscales
			datosFiscales = datosFiscalesService.getDatosFiscales(empTO.getNumEmpSolicitado());

			// Datos de la Solicitud
			SolicitudTO solicitudTO = solConsultaService.getSolicitudCob(empTO.getNumEmpSolicitado(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);

			listCon = solConsultaService.getConsulmed(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA);
			listDep = solConsultaService.getDependienteRenov(solicitudTO.getIdSol(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA);

			double costoTotalDep = 0.0;
			if(listDep.size()>0){
				for (DependienteTO dependiente : listDep) {
					dependiente.setCostoDep(solAltaService.getMontoDependiente(empleadoTO.getTipoEmp(), dependiente.getIdParentesco(),
							dependiente.getFechaNac(), dependiente.getSexo(), Codigo.MES_RENOVACION, Codigo.DIA_RENOVACION));
					costoTotalDep = costoTotalDep + dependiente.getCostoDep();
				}
			}

			// Ampliacion
			if(solicitudTO.getIdTipoCobertura()!=0){
				double montoAmpl = solAltaService.getMontoAmpliacion(solicitudTO.getIdTipoCobertura(),
						listDep.size(),
						Codigo.MES_RENOVACION,
						Codigo.DIA_RENOVACION);
				double costoTotal = empleadoTO.getMontoEmpProrateado() + costoTotalDep + montoAmpl;
				double costoTotalAct = costoTotalDep + empleadoTO.getMontoEmpProrateado();
				solicitudTO.setCostoAmpliacion(montoAmpl);
				solicitudTO.setCostoTotalAct(costoTotalAct);
				solicitudTO.setCostoTotal(costoTotal);
			}else{
				double costoTotalAct = costoTotalDep + empleadoTO.getMontoEmpProrateado();
				double costoTotal = empleadoTO.getMontoEmpProrateado() + costoTotalDep;
				solicitudTO.setCostoAmpliacion(0.0);
				solicitudTO.setCostoTotalAct(costoTotalAct);
				solicitudTO.setCostoTotal(costoTotal);
			}

			empleadoTO.setNombre(nombreCompleto);
			empleadoTO.setDescEmpleado("TITULAR");
			empleadoTO.setFechaNacF(formatoFechas.formatoFechas(empleadoTO.getFechaNac(), "dd-MM-yyyy"));
			circularForm.setDatosFiscales(datosFiscales);
			circularForm.setDatosFiscales(datosFiscales);
			circularForm.setSolicitudTO(solicitudTO);
			circularForm.setEmpleadoTO(empleadoTO);
			circularForm.setListDep(listDep);
			circularForm.setListCon(listCon);
			circularForm.setNumDep(listDep.size());

			modelAndView.addObject("circularForm", circularForm);
			modelAndView.setViewName("renovacion/renovacionCircularAdmin");
		} catch (Exception e){
			logger.info(" Error 1: "+e.getMessage());
			e.getStackTrace();
			modelAndView.setViewName("renovacion/renovacionIndexAdmin");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	} // Fin del metodo


	// Ambos

	@RequestMapping(value="mensaje.htm")
	protected ModelAndView mensaje(){
		logger.info("CircularController.mensaje");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("mensaje1", Codigo.AVISO_CIRCULAR);
		modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
		modelAndView.setViewName("renovacion/renovacionAviso");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="getMontoAmpliacionCircular.htm", method = RequestMethod.GET)
	protected @ResponseBody String getMontoAmpliacion(@RequestParam Integer numDep,@RequestParam Integer tipoAmpl){
		logger.info("CircularController.getMontoAmpliacion");
		logger.info("  Parametros de Peticion [numDep: "+numDep+" tipoAmpl: "+tipoAmpl+"]");
		Double mensaje = solAltaService.getMontoAmpliacion(tipoAmpl, numDep, Codigo.MES_RENOVACION, Codigo.DIA_RENOVACION);
		logger.info("  CostoAmpliacion: "+mensaje);
		logger.info("  * Fin Peticion");
		return mensaje.toString();
	}

	@RequestMapping(value="guardarCircular.htm")
	protected ModelAndView setCircular(@ModelAttribute("circularForm") CircularForm circularForm){
		logger.info("CircularController.setCircular");
		ModelAndView modelAndView = new ModelAndView();
		DatFiscalesTO datosFiscales = circularForm.getDatosFiscales();
		EmpleadoTO empleadoTO = circularForm.getEmpleadoTO();
		String estatusRenov = circularForm.getEstatusRenov();
		String estatusAmpl = circularForm.getEstatusAmpl();
		CircularTO circularTO = new CircularTO();

		logger.info("Num. Emp.:"+empleadoTO.getNumEmp());
		logger.info("RFC: "+datosFiscales.getRfc());
		logger.info("Estatus Renovacion: "+estatusRenov);
		logger.info("Estatus Ampliacion: "+estatusAmpl);
		logger.info("Calle: "+datosFiscales.getCalle());
		logger.info("Numero: "+datosFiscales.getNumero());
		logger.info("Colonia: "+datosFiscales.getColonia());
		logger.info("Cp: "+datosFiscales.getCp());
		logger.info("Delegacion: "+datosFiscales.getDeleg_mun());
		logger.info("Entidad: "+datosFiscales.getEntidad());

		datosFiscales.setNum_empleado(empleadoTO.getNumEmp());
		circularTO.setNumEmp(empleadoTO.getNumEmp());
		circularTO.setEstRenov(Codigo.ESTATUS_RENOV_PENDIENTE);
		circularTO.setRespRenov(Integer.parseInt(estatusRenov));
		circularTO.setFechaResp(new Date());

		if(estatusRenov == null || estatusRenov.equals("2")){
			circularTO.setTipoCob(Codigo.ID_TIPO_COB_N);
		}else{
			circularTO.setTipoCob(Integer.parseInt(estatusAmpl));
		}

		datosFiscalesService.setDatosFiscales(datosFiscales);
		circularService.setRenovacion(circularTO);

		modelAndView.addObject("mensaje1", Codigo.AVISO_RENOVACION_EXITOSA);
		modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
		modelAndView.setViewName("renovacion/renovacionAviso");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}


	// Memorial Usuario
	@RequestMapping(value="inicioContracionMemorial.htm")
	protected ModelAndView getContracionMemorial(EmpTO empTO){
		logger.info("CircularController.getContracionMemorial");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"] perfil: ["+empTO.getPerfilEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		CircularForm circularForm = new CircularForm();

		// Datos del Empleado
		empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmp());
		SolicitudTO solicitudTO = solConsultaService.getSolicitudCob(empTO.getNumEmp(),
				Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);

		String nombreCompleto = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();

		List<ConsulmedTO> listConAlta = solConsultaService.getConsulmed(solicitudTO.getIdSol(),
				Codigo.TIPO_SOL_ALTA,
				Codigo.EST_SOL_ALTA_EN_PROCESO,
				Codigo.EST_SOL_ALTA_AUTORIZADA);

		List<ConsulmedTO> listConAltaAux = solConsultaService.getConsulmed(solicitudTO.getIdSol(),
				Codigo.TIPO_SOL_ALTA,
				Codigo.EST_SOL_BAJA_EN_PROCESO,
				Codigo.EST_SOL_BAJA_EN_PROCESO);

		listConAlta.addAll(listConAltaAux);
		logger.info("  numSol: ["+solicitudTO.getIdSol()+"]"+" dependAlta: ["+listConAlta.size()+"] dependEnProceso: ["+listConAltaAux.size()+"]");

		List<ConsulmedTO> listCon = solConsultaService.getConsulmedDisponibles(empTO.getNumEmp());

		if(listConAlta.size()>0){
			circularForm.setListConAlta(listConAlta);
		}
		if(listCon.size()>0){
			circularForm.setListCon(listCon);
		}

		empleadoTO.setNombre(nombreCompleto);
		circularForm.setEmpTO(empTO);
		circularForm.setSolicitudTO(solicitudTO);
		circularForm.setEmpleadoTO(empleadoTO);
		modelAndView.addObject("circularForm", circularForm);
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("renovacion/renovacionMemorial");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="guardarMemorial.htm", method = RequestMethod.POST)
	protected ModelAndView setContracionMemorial(@ModelAttribute("circularForm") CircularForm circularForm){
		logger.info("CircularController.setContracionMemorial");
		ModelAndView modelAndView = new ModelAndView();

		EmpTO empTO = circularForm.getEmpTO();
		EmpleadoTO empleadoTO = circularForm.getEmpleadoTO();
		List<ConsulmedTO> listCon = circularForm.getListCon();
		List<ConsulmedTO> listConAlta = circularForm.getListConAlta();
		SolicitudTO solicitudTO = circularForm.getSolicitudTO();
		CircularTO circularTO = new CircularTO();

		logger.info("numEmp: ["+empTO.getNumEmp()
				+"] perfil: ["+empTO.getPerfilEmp()
				+"] respuesta: ["+circularForm.getEstatusRenov()+"]\n");

		circularTO.setNumEmp(empleadoTO.getNumEmp());
		circularTO.setRespRenov(Integer.parseInt(circularForm.getEstatusRenov()));
		circularTO.setFechaResp(new Date());
		circularService.setMemorial(circularTO);

		if(listConAlta != null){
			logger.info("lista actuales: "+listConAlta.size());
			for (ConsulmedTO consulmedTO : listConAlta) {
				consulmedTO.setIdSolicitud(solicitudTO.getIdSol());
				logger.info("id: ["+consulmedTO.getIdConsulmed()
						+"] nombre: ["+consulmedTO.getNombre()
						+"] aP: ["+consulmedTO.getApellidoP()
						+"] aM: ["+consulmedTO.getApellidoM()
						+"] idParentesco: ["+consulmedTO.getIdParentesco()
						+"] fechaNac: ["+consulmedTO.getFechaNac()
						+"] idSol: ["+consulmedTO.getIdSolicitud()
						+"] sexo: ["+consulmedTO.getSexo()+"]");
			}
		}

		if(listCon != null){
			logger.info("lista nuevos: "+listCon.size());
			for (ConsulmedTO consulmedTO2 : listCon) {
				consulmedTO2.setIdSolicitud(solicitudTO.getIdSol());
				logger.info("id: ["+consulmedTO2.getIdConsulmed()
						+"] nombre: ["+consulmedTO2.getNombre()
						+"] aP: ["+consulmedTO2.getApellidoP()
						+"] aM: ["+consulmedTO2.getApellidoM()
						+"] idParentesco: ["+consulmedTO2.getIdParentesco()
						+"] fechaNac: ["+consulmedTO2.getFechaNac()
						+"] idSol: ["+consulmedTO2.getIdSolicitud()
						+"] sexo: ["+consulmedTO2.getSexo()+"]");
			}
		}

		if(listConAlta != null){
			if(listConAlta.size()>0){
				logger.info("lista total: "+listConAlta.size());
				if(listCon != null && listCon.size() > 0){
					listConAlta.addAll(listCon);
				}
				circularService.setDependienteMemorial(listConAlta);
			}
		}else{
			logger.info("lista total: "+listCon.size());
			circularService.setDependienteMemorial(listCon);
		}

		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1",empleadoTO.getNombre()+" "+Codigo.MSJ_MEMORIAL_EXITOSO);
		modelAndView.setViewName("renovacion/renovacionExito");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}


	// Memorial Admin

	@RequestMapping(value="inicioContratacionMemorialAdmin.htm")
	protected ModelAndView inicioContratacionMemorialAdmin(EmpTO empTO){
		logger.info("CircularController.inicioContratacionMemorialAdmin");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("renovacion/renovacionMemorialIndexAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="getContratacionMemorialAdmin.htm")
	protected ModelAndView getContratacionMemorialAdmin(EmpTO empTO){
		logger.info("CircularController.getContratacionMemorialAdmin");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmpSolicitado()+"] perfil: ["+empTO.getPerfilEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpleadoTO empleadoTO = new EmpleadoTO();
		CircularForm circularForm = new CircularForm();

		// Datos del Empleado
		empleadoTO = solAltaService.getEmpleadoWS(empTO.getNumEmpSolicitado());
		SolicitudTO solicitudTO = solConsultaService.getSolicitudCob(empTO.getNumEmpSolicitado(),
				Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);

		String nombreCompleto = empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM();

		List<ConsulmedTO> listConAlta = solConsultaService.getConsulmed(solicitudTO.getIdSol(),
				Codigo.TIPO_SOL_ALTA,
				Codigo.EST_SOL_ALTA_EN_PROCESO,
				Codigo.EST_SOL_ALTA_AUTORIZADA);

		List<ConsulmedTO> listConAltaAux = solConsultaService.getConsulmed(solicitudTO.getIdSol(),
				Codigo.TIPO_SOL_ALTA,
				Codigo.EST_SOL_BAJA_EN_PROCESO,
				Codigo.EST_SOL_BAJA_EN_PROCESO);

		listConAlta.addAll(listConAltaAux);
		logger.info("  numSol: ["+solicitudTO.getIdSol()+"]"+" dependAlta: ["+listConAlta.size()+"] dependEnProceso: ["+listConAltaAux.size()+"]");

		List<ConsulmedTO> listCon = solConsultaService.getConsulmedDisponibles(empTO.getNumEmpSolicitado());

		if(listConAlta.size()>0){
			circularForm.setNumDep(listConAlta.size());
			circularForm.setListConAlta(listConAlta);
		}
		if(listCon.size()>0){
			circularForm.setListCon(listCon);
		}

		empleadoTO.setNombre(nombreCompleto);
		circularForm.setEmpTO(empTO);
		circularForm.setSolicitudTO(solicitudTO);
		circularForm.setEmpleadoTO(empleadoTO);
		modelAndView.addObject("circularForm", circularForm);
		modelAndView.addObject("empTO", empTO);
		modelAndView.setViewName("renovacion/renovacionMemorialAdmin");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}



	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}
	
	@Autowired
	public void setDatosFiscalesService(DatosFiscalesService datosFiscalesService) {
		this.datosFiscalesService = datosFiscalesService;
	}

	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}

	@Autowired
	public void setCircularService(CircularService circularService) {
		this.circularService = circularService;
	}
	
}
