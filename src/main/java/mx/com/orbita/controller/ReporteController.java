package mx.com.orbita.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.ReporteService;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.ReporteParamsTO;
import mx.com.orbita.to.ReporteTO;

@Controller
public class ReporteController {
	
	private static final Logger logger = Logger.getLogger(ReporteController.class);
	
	private ReporteService reporteService;
		
	/**
	 * Metodo Reporte Cambio de Categoria
	 * @return
	 */
	@RequestMapping(value="reporteCategoria.htm")
	protected ModelAndView getReporteCategoriaInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteCategoriaInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteCategoriaInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteCategoriaGenera.htm")
	protected ModelAndView getReporteCategoria(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteCategoria");
		logger.info("  Parametros de Peticion [consulta: "+reporteParamsTO.getOpcionConsulta()+" tipoEmp: "+reporteParamsTO.getTipoEmp()+" fechaInicio: "+reporteParamsTO.getFechaIni()+" fechaFinal: "+reporteParamsTO.getFechaFin()+"]");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getCambioCategoria(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.setViewName("administrador/reportes/reporteCategoria");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteCambioCategoria.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		return modelAndView;
	}
	
	/**
	 * Metodo Reporte Nomina
	 * @return
	 */
	@RequestMapping(value="reporteNomina.htm")
	protected ModelAndView getReporteNominaInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteNominaInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteNominaInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteNominaGenera.htm")
	protected ModelAndView getReporteNomina(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteNomina");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getNomina(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteNomina");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteNomina.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="reporteNominaGeneraTxt.htm")
	protected @ResponseBody String getReporteNominaTxt(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		response.setContentType("application/octet-stream");
		response.setHeader("Content-type", "text/plain");
		response.setHeader("Content-Disposition", "attachment; filename=reporteNomina.txt");
		List<ReporteTO> lista = reporteService.getNomina(reporteParamsTO);
		String listaNomina = "";
		int i = 1;
		for (ReporteTO reporteTO : lista) {
			String listaTemp = i+" | "+reporteTO.getIdSolicitud()
								+" | "+reporteTO.getNumEmp()
								+" | "+reporteTO.getApellidoP()
								+" | "+reporteTO.getApellidoM()
								+" | "+reporteTO.getNombre()
								+" | "+reporteTO.getRegion()
								+" | "+reporteTO.getFechaAlta()
								+" | "+reporteTO.getCobertura()
								+" | "+reporteTO.getCostoEmp()+"\n";
			listaNomina=listaNomina+listaTemp;
			i++;
		}
		return listaNomina;
	}
	
	/**
	 * Metodo Reporte Actual
	 * @return
	 */
	@RequestMapping(value="reporteActual.htm")
	protected ModelAndView getReporteActualInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteActualInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteActualInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteActualGenera.htm")
	protected ModelAndView getReporteActual(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.reporteActualGenera");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getReporteActual(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteActual");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteActual.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}
	
	/**
	 * Metodo Reporte Solo Consulmed
	 * @return
	 */
	@RequestMapping(value="reporteConsulmed.htm")
	protected ModelAndView getReporteConsulmedInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteDependienteInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteConsulmedInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteConsulmedGenera.htm")
	protected ModelAndView getReporteConsulmed(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteConsulmed");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getConsulmed(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteConsulmed");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteConsulmed.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}
		
	@RequestMapping(value="reporteDependienteGeneraTxt.htm")
	protected @ResponseBody String getReporteConsulmedTxt(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		response.setContentType("application/octet-stream");
		response.setHeader("Content-type", "text/plain");
		response.setHeader("Content-Disposition", "attachment; filename=reporteDependiente.txt");
		List<ReporteTO> lista = reporteService.getConsulmed(reporteParamsTO);
		String listaDep = "";
		int i = 1;
		for (ReporteTO reporteTO : lista) {
			String listaTemp = i+" | "+reporteTO.getNumEmp()+" | "
									+reporteTO.getApellidoP()+" | "
									+reporteTO.getApellidoM()+" | "
									+reporteTO.getNombre()+" | "
									+reporteTO.getParentesco()+" | "
									+reporteTO.getFechaNac()+" | "
									+reporteTO.getRegion()+" | "
									+reporteTO.getCentro()+" | "
									+reporteTO.getFechaAlta()+" | "
									+reporteTO.getCobertura()+"\n";
			i++;
			listaDep = listaDep + listaTemp;
		}
		return listaDep;
	}
	
	/**
	 * Metodo Reporte Consulmed Total
	 * @return
	 */
	@RequestMapping(value="reporteConsulmedTotal.htm")
	protected ModelAndView getReporteDependienteTotalInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteDependienteInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteConsulmedTotalInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteConsulmedTotalGenera.htm")
	protected ModelAndView getReporteConsulmedTotal(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteConsulmed");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getConsulmedTotal(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteConsulmed");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteConsulmedTotal.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}	
	
	/**
	 * Metodo Reporte Datos Fiscales
	 * @return
	 */
	@RequestMapping(value="reporteDatosFiscales.htm")
	protected ModelAndView getReporteDatosFiscalesInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteDatosFiscalesInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteDatosFiscalesInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteDatosFiscalesGenera.htm")
	protected ModelAndView getReporteDatosFiscales(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteDatosFiscales");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getDatosFiscales(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteDatosFiscales");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteDatosFiscales.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}
	
	/**
	 * Metodo Reporte Ampliaciones Dependientes
	 * @return
	 */
	@RequestMapping(value="reporteAmpliacionesDep.htm")
	protected ModelAndView getReporteAmpliacionesDepInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteAmpliacionesDepInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteAmpliacionesDepInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteAmpliacionesDepGenera.htm")
	protected ModelAndView getReporteAmpliacionesDep(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteAmpliaciones");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getAmpliacionesDep(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteAmpliacionesDep");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteAmplicionDependientes.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="reporteAmpliacionesDepGeneraTxt.htm")
	protected @ResponseBody String getReporteAmpliacionesDepTxt(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		response.setContentType("application/octet-stream");
		response.setHeader("Content-type", "text/plain");
		response.setHeader("Content-Disposition", "attachment; filename=reporteAmpliacionesDep.txt");
		List<ReporteTO> lista = reporteService.getAmpliacionesDep(reporteParamsTO);
		String listaAmpl = "";
		int i = 1;
		for (ReporteTO reporteTO : lista) {
			String listaTemp = i+" | "+reporteTO.getNumEmp()
							+" | "+reporteTO.getApellidoP()
							+" | "+reporteTO.getApellidoM()
							+" | "+reporteTO.getNombre()
							+" | "+reporteTO.getFechaAlta()
							+" | "+reporteTO.getParentesco()
							+" | "+reporteTO.getSexo()
							+" | "+reporteTO.getFechaNac()
							+" | "+reporteTO.getCobertura()
							+" | "+reporteTO.getTipoEmpleado()+"\n";
			i++;
			listaAmpl = listaAmpl + listaTemp;
		}
		return listaAmpl;
	}
	
	/**
	 * Metodo Reporte de Ampliciones
	 * @return
	 */
	@RequestMapping(value="reporteAmpliaciones.htm")
	protected ModelAndView getReporteAmpliacionesInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteAmpliacionesInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteAmpliacionesInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteAmpliacionesGenera.htm")
	protected ModelAndView getReporteAmpliaciones(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteAmpliaciones");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getAmpliaciones(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		if(reporteParamsTO.getOpcionConsulta()==1){
			modelAndView.addObject("lista", lista);
			modelAndView.setViewName("administrador/reportes/reporteAmpliaciones");
		}else if(reporteParamsTO.getOpcionConsulta()==2){
			response.setHeader("Content-type", "application/vnd.ms-excel");
	        response.setHeader("Content-Disposition","attachment; filename=\"reporteAmpliacionTotal.xls\"");        		
	        modelAndView.setViewName("reporteExcelView");
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="reporteAmpliacionesGeneraTxt.htm")
	protected @ResponseBody String getReporteAmpliacionesTxt(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		response.setContentType("application/octet-stream");
		response.setHeader("Content-type", "text/plain");
		response.setHeader("Content-Disposition", "attachment; filename=reporteAmpliacionesTotal.txt");
		List<ReporteTO> lista = reporteService.getAmpliaciones(reporteParamsTO);
		String listaAmpl = "";
		int i = 1;
		for (ReporteTO reporteTO : lista) {
			String listaTemp = i+" | "+reporteTO.getIdSolicitud()
					+" | "+reporteTO.getNumEmp()
					+" | "+reporteTO.getApellidoP()
					+" | "+reporteTO.getApellidoM()
					+" | "+reporteTO.getNombre()
					+" | "+reporteTO.getFechaAlta()
					+" | "+reporteTO.getTipoEmpleado()
					+" | "+reporteTO.getCobertura()
					+" | "+reporteTO.getCostoEmp()
					+" | "+reporteTO.getCostoAmp()
					+" | "+reporteTO.getCostoTot()
					+" | "+reporteTO.getParentesco()
					+" | "+reporteTO.getFechaNac()
					+" | "+reporteTO.getSexo()+"\n";
			i++;
			listaAmpl = listaAmpl + listaTemp;
		}
		return listaAmpl;
	}
		
	/**
	 * Metodo Reporte Descargas de Archivos
	 * @return
	 */
	@RequestMapping(value="reporteDescargas.htm")
	protected ModelAndView getDescargaInicio(EmpTO empTO){
		logger.info("ReporteController.getDescargaInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteDescargasInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteDescargasGeneraTxt.htm")
	protected @ResponseBody String getDescargas(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		response.setContentType("application/octet-stream");
		response.setHeader("Content-type", "text/plain");
		response.setHeader("Content-Disposition", "attachment; filename=reporteDescargas.txt");
		List<ReporteTO> lista = reporteService.getDescargaSol(reporteParamsTO);
		String listaAmpl = "";
		int i = 1;
		for (ReporteTO reporteTO : lista) {
			String listaTemp = i+"|"+reporteTO.getNumEmp()
					+"|"+reporteTO.getNombre()
					+"|"+reporteTO.getApellidoP()
					+"|"+reporteTO.getApellidoM()
					+"|"+reporteTO.getTipoInbursa()
					+"|"+reporteTO.getSexo()
					+"|"+reporteTO.getFechaNac()
					+"|"+reporteTO.getFechaAlta()
					+"|"+reporteTO.getCuenta()
					+"|"+reporteTO.getCobertura()
					+"\n";
			i++;
			listaAmpl = listaAmpl + listaTemp;
		}
		return listaAmpl;
	}
	
	/**
	 * Metodo Reporte de Renovacione
	 * @return
	 */
	@RequestMapping(value="reporteRenov.htm")
	protected ModelAndView getReporteRenovInicio(EmpTO empTO){
		logger.info("ReporteController.getReporteRenovInicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("empTO",empTO);
		modelAndView.setViewName("administrador/reportes/reporteRenovacionInicio");
		return modelAndView;
	}
	
	@RequestMapping(value="reporteRenovGenera.htm")
	protected ModelAndView getReporteRenov(ReporteParamsTO reporteParamsTO, HttpServletResponse response){
		logger.info("ReporteController.getReporteRenov");
		ModelAndView modelAndView = new ModelAndView();
		EmpTO empTO = new EmpTO();
		empTO.setNumEmp(reporteParamsTO.getNumEmp());
		empTO.setNombreEmp(reporteParamsTO.getNombreEmp());
		empTO.setPerfilEmp(reporteParamsTO.getPerfilEmp());
		modelAndView.addObject("empTO", empTO);
		
		List<ReporteTO> lista = reporteService.getRenovacion(reporteParamsTO);
		modelAndView.addObject("lista", lista);
		modelAndView.addObject("reporteParamsTO", reporteParamsTO);
		
		response.setHeader("Content-type", "application/vnd.ms-excel");
	    response.setHeader("Content-Disposition","attachment; filename=\"reporteRenovaciones.xls\"");        		
	    modelAndView.setViewName("reporteExcelView");
		return modelAndView;
	}
	
	
	@Autowired
	public void setReporteService(ReporteService reporteService) {
		this.reporteService = reporteService;
	}
	
}