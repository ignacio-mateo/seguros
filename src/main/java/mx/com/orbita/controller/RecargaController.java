package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.controller.form.CatalogoForm;
import mx.com.orbita.to.CatalogoTO;

@Controller
public class RecargaController {

	
	public static final Logger logger = Logger.getLogger(RecargaController.class);
	
	@RequestMapping(value="recarga.htm")
	protected ModelAndView getRecarga(){
		logger.info("  RecargaController.getRecarga");
		ModelAndView modelAndView = new ModelAndView();
		CatalogoForm catalogoForm = new CatalogoForm();
		List<CatalogoTO> lista = new ArrayList<CatalogoTO>();
		CatalogoTO catalogoTO = new CatalogoTO();
		catalogoTO.setIdCampo(1);
		catalogoTO.setDescripcion("RENOVACION DATOS FISCALES");
		catalogoTO.setCampoActual("false");
		catalogoTO.setCampoNuevo("");
		lista.add(catalogoTO);
		catalogoForm.setLista(lista);
		modelAndView.addObject("catalogoForm", catalogoForm);
		modelAndView.setViewName("configuracion/configuracionInicio");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	
	@RequestMapping(value="actualizarRecarga.htm")
	protected ModelAndView setRecarga(@ModelAttribute("catalogoForm") CatalogoForm catalogoForm){
		logger.info("  RecargaController.setRecarga");
		ModelAndView modelAndView = new ModelAndView();
		
		List<CatalogoTO> lista = catalogoForm.getLista();
		for (CatalogoTO catalogoTO : lista) {
			logger.info("  Id: "+catalogoTO.getIdCampo());
			logger.info("  Descripcion: "+catalogoTO.getDescripcion());
			logger.info("  Campo Actual: "+catalogoTO.getCampoActual());
			logger.info("  Campo Nuevo: "+catalogoTO.getCampoNuevo());
		}
		
		
		
		modelAndView.setViewName("configuracion/configuracionInicio");
		return modelAndView;
	}
	
}
