package mx.com.orbita.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.controller.form.SolicitudForm;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.service.SolBajaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;

@Controller
public class SolBajaController {
	
	private static final Logger logger = Logger.getLogger(SolBajaController.class);
	
	private SolConsultaService solConsultaService;
	private SolBajaService solBajaService;
	private SistemaService sistemaService;
		
	
	@RequestMapping(value="bajaSolicitud.htm")
	protected ModelAndView getBajaSolicitud(EmpTO empTO){
		logger.info("SolBajaController.getBajaSolicitud");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		
		Integer folio = sistemaService.existeSolicitudAut(empTO.getNumEmp());
		if(folio!=0){
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			modelAndView.addObject("folio", folio);
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("empleadoTO", empleadoTO);
			modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA);
			modelAndView.setViewName("solBaja/solBajaRegistro");
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_BAJA_TOT);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		return modelAndView;
	}
	
	@RequestMapping(value="bajaDependiente.htm")
	protected ModelAndView getBajaDependiente(EmpTO empTO){
		logger.info("SolBajaController.getBajaDependiente");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
			
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){	
			SolicitudForm solicitudForm = new SolicitudForm();
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			SolicitudTO solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
																		Codigo.TIPO_SOL_ALTA, 
																		Codigo.EST_SOL_ALTA_AUTORIZADA,
																		Codigo.EST_SOL_ALTA_AUTORIZADA);
			
			List<DependienteTO> listDep = new ArrayList<DependienteTO>();
			listDep = solConsultaService.getDependienteBaja(solicitudTO.getIdSol());
			
			
			if(listDep.size()!=0){
				solicitudForm.setEmpleadoTO(empleadoTO);
				solicitudForm.setSolicitudTO(solicitudTO);
				solicitudForm.setListDep(listDep);
				solicitudForm.setEmpTO(empTO);
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA_DEPENDIENTE);
				modelAndView.addObject("solicitudForm", solicitudForm);
				modelAndView.setViewName("solBaja/solBajaDepRegistro");
			}else{
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_NO_DEP);
				modelAndView.setViewName("solEstatus/solEstatus");
			}
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje2", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="bajaConsulmed.htm")
	protected ModelAndView getBajaConsulmed(EmpTO empTO){
		logger.info("SolBajaController.getBajaConsulmed");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
				
		if((sistemaService.existeSolicitud(empTO.getNumEmp()))!=0){	
			SolicitudForm solicitudForm = new SolicitudForm();	
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(empTO.getNumEmp());
			logger.info("  Num.Empleado: "+empleadoTO.getNumEmp());
			SolicitudTO solicitudTO = new SolicitudTO();
			solicitudTO = solConsultaService.getSolicitud(empTO.getNumEmp(), 
															Codigo.TIPO_SOL_ALTA, 
															Codigo.EST_SOL_ALTA_AUTORIZADA,
															Codigo.EST_SOL_ALTA_AUTORIZADA);
			
			List<ConsulmedTO> listCon = new ArrayList<ConsulmedTO>();
			listCon = solConsultaService.getConsulmedBaja(solicitudTO.getIdSol());
			
			if(listCon.size()!=0){
				solicitudForm.setEmpleadoTO(empleadoTO);
				solicitudForm.setSolicitudTO(solicitudTO);
				solicitudForm.setListCon(listCon);
				solicitudForm.setEmpTO(empTO);
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("solicitudForm", solicitudForm);
				modelAndView.addObject("titulo", Codigo.TITULO_SOL_BAJA_CONSULMED);
				modelAndView.setViewName("solBaja/solBajaConsulmedRegistro");
			}else{
				modelAndView.addObject("empTO", empTO);
				modelAndView.addObject("mensaje1", Codigo.SOL_MENSAJE_NO_DEP);
				modelAndView.setViewName("solEstatus/solEstatus");
			}
		}else{
			modelAndView.addObject("empTO", empTO);
			modelAndView.addObject("mensaje1", Codigo.SOL_NO_EXISTE);
			modelAndView.addObject("mensaje3", Codigo.EXTENSIONES);
			modelAndView.setViewName("solEstatus/solEstatus");
		}
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="registrarBajaSolicitud.htm")
	protected ModelAndView setBajaSolicitud(EmpTO empTO){
		logger.info("SolBajaController.setBajaSolicitud");
		logger.info("  Parametros de Peticion [numEmp: "+empTO.getNumEmp()+"]");
		ModelAndView modelAndView = new ModelAndView();
		solBajaService.setSolicitud(empTO.getNumEmp(), empTO.getParametro());
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", empTO.getNombreEmp()+" "+Codigo.MSJ_PROCESANDO_SOL);
		modelAndView.addObject("mensaje2", Codigo.MSJ_IMPRIME_SOL);
		modelAndView.addObject("mensaje3", Codigo.MSJ_BAJA_SOL);
		modelAndView.addObject("mensaje4", Codigo.LINK_BAJA);
		modelAndView.setViewName("solEstatus/solEstatus");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="registrarBajaDependiente.htm")
	protected ModelAndView setBajaDependiente(@ModelAttribute("solicitudForm") SolicitudForm solicitudForm){
		logger.info("SolBajaController.setBajaDependiente");
		ModelAndView modelAndView = new ModelAndView();
		List<DependienteTO> listaBajas = solicitudForm.getListDep();
		EmpTO empTO = solicitudForm.getEmpTO();
		solBajaService.setDependiente(listaBajas);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", empTO.getNombreEmp()+" "+Codigo.MSJ_PROCESANDO_SOL);
		modelAndView.addObject("mensaje2", Codigo.MSJ_IMPRIME_SOL);
		modelAndView.addObject("mensaje3", Codigo.MSJ_BAJA_DEP);
		modelAndView.addObject("mensaje4", Codigo.SOL_MENSAJE_DEP_VER_BAJA);
		modelAndView.addObject("mensaje5", Codigo.LINK_BAJA_DEP);
		modelAndView.setViewName("solEstatus/solEstatus");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	@RequestMapping(value="registrarBajaConsulmed.htm")
	protected ModelAndView setBajaConsulmed(@ModelAttribute("solicitudForm") SolicitudForm solicitudForm){
		logger.info("SolBajaController.setBajaConsulmed");
		ModelAndView modelAndView = new ModelAndView();
		List<ConsulmedTO> listaBajas = solicitudForm.getListCon();
		EmpTO empTO = solicitudForm.getEmpTO();
		solBajaService.setConsulmed(listaBajas);
		modelAndView.addObject("empTO", empTO);
		modelAndView.addObject("mensaje1", empTO.getNombreEmp()+" "+Codigo.MSJ_PROCESANDO_SOL);
		modelAndView.setViewName("solEstatus/solEstatus");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}
	
	
	@Autowired
	public void setSolBajaService(SolBajaService solBajaService) {
		this.solBajaService = solBajaService;
	}

	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}
	
	@Autowired
	public void setSistemaService(SistemaService sistemaService) {
		this.sistemaService = sistemaService;
	}

	
	
}
