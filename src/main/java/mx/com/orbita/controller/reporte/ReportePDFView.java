package mx.com.orbita.controller.reporte;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import mx.com.orbita.to.ConstanciaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Fechas;

public class ReportePDFView extends AbstractITextPdfView {
	
	@Override
    protected void buildPdfDocument(Map<String, Object> model, 
    								Document doc, 
    								PdfWriter writer, 
    								HttpServletRequest request, 
    								HttpServletResponse response) throws Exception {
		
		// Recuperar parametros
		List<DependienteTO> lista = (List<DependienteTO>) model.get("lista");
		ConstanciaTO constanciaTO = (ConstanciaTO) model.get("constanciaTO");
        Integer tipoConstancia = (Integer) model.get("tipoConstancia");
        String fecha = (String) model.get("fecha");
        String comentario = (String) model.get("comentario");
        
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaTemp = new Date();
        try {
        	if(tipoConstancia==2){
        		fechaTemp = formatoDelTexto.parse(fecha);
        	}
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage()); 
		}
		
		Paragraph parrafo1 = new Paragraph();
		parrafo1.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.ITALIC));
		parrafo1.add(new Fechas().formatoFechas(new Date(),"dd MMMM, yyyy").toUpperCase());
		parrafo1.setAlignment(Element.ALIGN_RIGHT);
		doc.add(new Phrase(Chunk.NEWLINE));
		doc.add(new Phrase(Chunk.NEWLINE));
		doc.add(new Phrase(Chunk.NEWLINE));
		doc.add(new Phrase(Chunk.NEWLINE));
		doc.add(new Phrase(Chunk.NEWLINE));
        doc.add(parrafo1);
        doc.add(new Phrase(Chunk.NEWLINE));
        		
        Paragraph parrafo2 = new Paragraph();
        parrafo2.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.BOLD));
        parrafo2.add("A quien corresponda:");
        doc.add(parrafo2);
        doc.add(Chunk.NEWLINE);
       
		Paragraph parrafo3 = new Paragraph();
		parrafo3.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.NORMAL));
		parrafo3.add("A solicitud de ");
		parrafo3.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.BOLD));
		parrafo3.add(constanciaTO.getApellidoP()+" "+constanciaTO.getApellidoM()+" "+constanciaTO.getNombre());
		
		if(tipoConstancia.equals(1)){		// Actual
			parrafo3.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.NORMAL));
			parrafo3.add(", se expide la presente, certificando que se encuentra amparado");
		}else if(tipoConstancia.equals(2)){ // Finiquito
			parrafo3.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.NORMAL));
			parrafo3.add(", se expide la presente certificando que se encontr� amparado hasta el ");
			parrafo3.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.UNDERLINE));
			String fechaFiniquito = new Fechas().formatoFechas(fechaTemp,"dd/MMMM/yyyy").toUpperCase();
			parrafo3.add(fechaFiniquito.replace("/", " DE "));
		}
		
		parrafo3.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.NORMAL));
		parrafo3.add(", en la poliza de Seguro de Gastos Medicos Mayores del grupo de empleados y funcionarios "
				+ "en la empresa Radiom�vil Dipsa, S.A. de C.V., teniendo las siguientes coberturas:");
        doc.add(parrafo3);
        doc.add(Chunk.NEWLINE);
                
        PdfPTable parrafoBorde1 = new PdfPTable(2);
        parrafoBorde1.setWidthPercentage(100.0f);
        parrafoBorde1.setWidths(new float[] {2.0f,8.0f});
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),11,Font.ITALIC);
        PdfPCell parrafoBorde1Cell1 = new PdfPCell(new Phrase("Aseguradora:",font));
        PdfPCell parrafoBorde1Cell2 = new PdfPCell(new Phrase(Codigo.CONS_ASEGURADORA,font));
        PdfPCell parrafoBorde1Cell3 = new PdfPCell(new Phrase("Domicilio:",font));
        PdfPCell parrafoBorde1Cell4 = new PdfPCell(new Phrase(Codigo.CONS_DOMICILIO,font));
        PdfPCell parrafoBorde1Cell5 = new PdfPCell(new Phrase("RFC:",font));
        PdfPCell parrafoBorde1Cell6 = new PdfPCell(new Phrase(Codigo.CONS_RFC,font));
        PdfPCell parrafoBorde1Cell7 = new PdfPCell(new Phrase("Poliza:",font));
        PdfPCell parrafoBorde1Cell8 = new PdfPCell(new Phrase(constanciaTO.getIdTipoEmp().equals(1)?Codigo.CONS_POLIZA_CONF:Codigo.CONS_POLIZA_SIND,font));
        PdfPCell parrafoBorde1Cell9 = new PdfPCell(new Phrase("Certificado No.:",font));
        PdfPCell parrafoBorde1Cell10 = new PdfPCell(new Phrase(constanciaTO.getNumEmp()+" (Cobertura Nacional) MR0"+constanciaTO.getIdRegion()+" "+constanciaTO.getTipoCobertura(),font));
        parrafoBorde1Cell1.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell2.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell3.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell4.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell5.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell6.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell7.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell8.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell9.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1Cell10.setBorder(Rectangle.NO_BORDER);
        parrafoBorde1.addCell(parrafoBorde1Cell1);
        parrafoBorde1.addCell(parrafoBorde1Cell2);
        parrafoBorde1.addCell(parrafoBorde1Cell3);
        parrafoBorde1.addCell(parrafoBorde1Cell4);
        parrafoBorde1.addCell(parrafoBorde1Cell5);
        parrafoBorde1.addCell(parrafoBorde1Cell6);
        parrafoBorde1.addCell(parrafoBorde1Cell7);
        parrafoBorde1.addCell(parrafoBorde1Cell8);
        parrafoBorde1.addCell(parrafoBorde1Cell9);
        parrafoBorde1.addCell(parrafoBorde1Cell10);
		doc.add(parrafoBorde1);
		doc.add(Chunk.NEWLINE);
                
        PdfPTable parrafoBorde2 = new PdfPTable(2);
        parrafoBorde2.setWidthPercentage(100.0f);
        parrafoBorde2.setWidths(new float[] {2.0f,8.0f});
        PdfPCell parrafoBorde2Cell1 = new PdfPCell(new Phrase("Vigencia:",font));
        PdfPCell parrafoBorde2Cell2 = new PdfPCell(new Phrase(Codigo.CONS_VIGENCIA,font));
        PdfPCell parrafoBorde2Cell3 = new PdfPCell(new Phrase("Fecha de Alta:",font));
        PdfPCell parrafoBorde2Cell4 = new PdfPCell(new Phrase(new Fechas().formatoFechas(constanciaTO.getFechaAut(),"MMM dd yyyy").toUpperCase(),font));
        PdfPCell parrafoBorde2Cell5 = new PdfPCell(new Phrase("Suma Asegurada:",font));
        PdfPCell parrafoBorde2Cell6 = new PdfPCell();
        
        if(constanciaTO.getTipoCobertura().equals("N")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_BASICA,font));
        }else if(constanciaTO.getTipoCobertura().equals("A1")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A1,font));
        }else if(constanciaTO.getTipoCobertura().equals("A2 N")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A2_N,font));
        }else if(constanciaTO.getTipoCobertura().equals("A2 E")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A2_E,font));
        }else if(constanciaTO.getTipoCobertura().equals("A3 N")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A3_N,font));
        }else if(constanciaTO.getTipoCobertura().equals("A3 E")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A3_E,font));
        }else if(constanciaTO.getTipoCobertura().equals("A4 N")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A4_N,font));
        }else if(constanciaTO.getTipoCobertura().equals("A4 E")){
        	parrafoBorde2Cell6 = new PdfPCell(new Phrase(Codigo.CONS_SUMA_ASEG_A4_E,font));
        }        
                
        PdfPCell parrafoBorde2Cell7 = new PdfPCell(new Phrase("Deducible:",font));
        PdfPCell parrafoBorde2Cell8 = new PdfPCell(new Phrase(constanciaTO.getIdTipoEmp().equals(1)?Codigo.CONS_DEDUCIBLE_CONF:Codigo.CONS_DEDUCIBLE_SIND,font));
        PdfPCell parrafoBorde2Cell9 = new PdfPCell(new Phrase("Coaseguro:",font));
        PdfPCell parrafoBorde2Cell10 = new PdfPCell(new Phrase(Codigo.CONS_COASEGURO,font));
        PdfPCell parrafoBorde2Cell11 = new PdfPCell(new Phrase("Cesarea:",font));
        PdfPCell parrafoBorde2Cell12 = new PdfPCell(new Phrase(constanciaTO.getIdTipoEmp().equals(1)?Codigo.CONS_CESAREA_CONF:Codigo.CONS_CESAREA_SIND,font));
        PdfPCell parrafoBorde2Cell13 = new PdfPCell(new Phrase("Parto Natural:",font));
        PdfPCell parrafoBorde2Cell14 = new PdfPCell(new Phrase(constanciaTO.getIdTipoEmp().equals(1)?Codigo.CONS_PARTO_NAT_CONF:Codigo.CONS_PARTO_NAT_SIND,font));
        parrafoBorde2Cell1.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell2.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell3.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell4.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell5.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell6.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell7.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell8.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell9.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell10.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell11.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell12.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell13.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2Cell14.setBorder(Rectangle.NO_BORDER);
        parrafoBorde2.addCell(parrafoBorde2Cell1);
        parrafoBorde2.addCell(parrafoBorde2Cell2);
        parrafoBorde2.addCell(parrafoBorde2Cell3);
        parrafoBorde2.addCell(parrafoBorde2Cell4);
        parrafoBorde2.addCell(parrafoBorde2Cell5);
        parrafoBorde2.addCell(parrafoBorde2Cell6);
        parrafoBorde2.addCell(parrafoBorde2Cell7);
        parrafoBorde2.addCell(parrafoBorde2Cell8);
        parrafoBorde2.addCell(parrafoBorde2Cell9);
        parrafoBorde2.addCell(parrafoBorde2Cell10);
        parrafoBorde2.addCell(parrafoBorde2Cell11);
        parrafoBorde2.addCell(parrafoBorde2Cell12);
        parrafoBorde2.addCell(parrafoBorde2Cell13);
        parrafoBorde2.addCell(parrafoBorde2Cell14);
		doc.add(parrafoBorde2);
		doc.add(Chunk.NEWLINE);
                       
        PdfPTable parrafoBorde3 = new PdfPTable(3);
        parrafoBorde3.setWidthPercentage(100.0f);
        parrafoBorde3.setWidths(new float[] {1.0f,4.0f,4.0f});
        Font fontDep = FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),10,Font.ITALIC);
        PdfPCell parrafoBorde3Cell1 = new PdfPCell(new Phrase("Titular:",fontDep));
        PdfPCell parrafoBorde3Cell2 = new PdfPCell(new Phrase(constanciaTO.getApellidoP()+" "+constanciaTO.getApellidoM()+" "+constanciaTO.getNombre(),fontDep));
        PdfPCell parrafoBorde3Cell3 = new PdfPCell(new Phrase("("+constanciaTO.getTipoEmp()+")",fontDep));      
        parrafoBorde3Cell1.setBorder(Rectangle.NO_BORDER);
        parrafoBorde3Cell2.setBorder(Rectangle.NO_BORDER);
        parrafoBorde3Cell3.setBorder(Rectangle.NO_BORDER);
        parrafoBorde3.addCell(parrafoBorde3Cell1);
        parrafoBorde3.addCell(parrafoBorde3Cell2);
        parrafoBorde3.addCell(parrafoBorde3Cell3);
		doc.add(parrafoBorde3);
		
		if(lista.size()>=1){
			PdfPTable parrafoBorde4 = new PdfPTable(3);
	        parrafoBorde4.setWidthPercentage(100.0f);
	        parrafoBorde4.setWidths(new float[] {1.0f,4.0f,4.0f});
	        for (DependienteTO elemento : lista) {
	        	parrafoBorde4.addCell(new PdfPCell(new Phrase(" ",fontDep))).setBorder(Rectangle.NO_BORDER);
	        	parrafoBorde4.addCell(new PdfPCell(new Phrase(elemento.getApellidoP()+" "+elemento.getApellidoM()
	        			+" "+elemento.getNombre(),fontDep))).setBorder(Rectangle.NO_BORDER);
	        	parrafoBorde4.addCell(new PdfPCell(new Phrase(elemento.getParentesco()+" ALTA "
	        			+new Fechas().formatoFechas(elemento.getFecha(),"MMM dd yyyy").toUpperCase(),fontDep))).setBorder(Rectangle.NO_BORDER);
	        }
	        doc.add(parrafoBorde4);
		}
		doc.add(Chunk.NEWLINE);
		
		Paragraph parrafo4 = new Paragraph();
	    parrafo4.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.NORMAL));
	    parrafo4.add(comentario);
	    doc.add(parrafo4);
	    doc.add(Chunk.NEWLINE);
	    
		Paragraph parrafo5 = new Paragraph();
		parrafo5.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.NORMAL));
		parrafo5.add("Sin otro particular, me encuentro a sus ordenes para cualquier duda o aclaracion al respecto.");
        doc.add(parrafo5);
        doc.add(Chunk.NEWLINE);
        doc.add(Chunk.NEWLINE);
        				
		Paragraph parrafo6 = new Paragraph();
		parrafo6.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.BOLD));
		parrafo6.add("A t e n t a m e n t e,");
        doc.add(parrafo6);
        doc.add(Chunk.NEWLINE);
        doc.add(Chunk.NEWLINE);
        doc.add(Chunk.NEWLINE);
					             		
        Paragraph parrafo7 = new Paragraph();
        parrafo7.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.BOLD));
        parrafo7.add("Nombre ApellidoPaterno ApellidoMaterno");
        doc.add(parrafo7);
        				
        Paragraph parrafo8 = new Paragraph();
        parrafo8.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.BOLD));
        parrafo8.add("Administrador");
        doc.add(parrafo8);
        		
        Paragraph parrafo9 = new Paragraph();
        parrafo9.setFont(FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(),12,Font.BOLD));
        parrafo9.add("Seguros");
        doc.add(parrafo9);
		 
    }

}
