package mx.com.orbita.controller.reporte;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import mx.com.orbita.to.ReporteParamsTO;
import mx.com.orbita.to.ReporteTO;
import mx.com.orbita.util.Fechas;

public class ReporteExcelView extends AbstractExcelView{
	
	public static final Logger loger = Logger.getLogger(ReporteExcelView.class);

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		loger.info("CategoriaExcelView.buildExcelDocument");
		
		 // Recuperar parametros
		List<ReporteTO> lista = (List<ReporteTO>) model.get("lista");
		ReporteParamsTO reporteParamsTO = (ReporteParamsTO) model.get("reporteParamsTO");
		
		// Definir Variables
		Fechas fechas = new Fechas();
		String fechaActual = "";
		fechaActual = fechas.formatoFechas(new Date(),"EEE, MMM dd yyyy").toUpperCase();
        
		if(reporteParamsTO.getReporte()==1){
			generaReporteCategoria(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==2){
			generaReporteNomina(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==3){
			generaReporteActual(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==4){
			generaReporteConsulmed(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==5){
			generaReporteConsulmedTotal(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==6){
			generaReporteDatosFiscales(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==7){
			generaReporteAmpliacionDep(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==8){
			generaReporteAmpliacion(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==9){
			generaReporteEncuesta(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==10){
			generaReporteRenovacionSi(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==11){
			generaReporteRenovacionNo(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==12){
			generaReporteRenovacionNoContesto(workbook, lista, reporteParamsTO, fechaActual);
		}else if(reporteParamsTO.getReporte()==13){
            generaReporteMemorialEncuesta(workbook, lista, reporteParamsTO, fechaActual);
        }else if(reporteParamsTO.getReporte()==14){
            generaReporteMemorialDependiente(workbook, lista, reporteParamsTO, fechaActual);
    }
        		
	}
	
	
	private void generaReporteCategoria(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteCategoria");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Categorias");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,5));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion de Cambio de Categoria");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,5));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Rango Fechas: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(reporteParamsTO.getFechaIni()+" - "+reporteParamsTO.getFechaFin());
        encabezado.getCell(1).setCellStyle(style);
        encabezado.createCell(3).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(3).setCellStyle(style);
        encabezado.createCell(4).setCellValue(fechaActual);
        encabezado.getCell(4).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("Num. Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Nombre");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Cambio");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Region");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Monto Total Anterior");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Monto Total Nuevo");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Fecha Cambio");
        celda.getCell(6).setCellStyle(style);
        
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getApellidoP()+" "+reporteCategoriaTO.getApellidoM()+" "+reporteCategoriaTO.getNombre());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getCambio());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getRegion());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getCostoTotAnt());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getCostoTot());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getFechaCambio());
		}
	}

	private void generaReporteNomina(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteNomina");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Nomina");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,5));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion de Nominas");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,5));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Rango Fechas: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(reporteParamsTO.getFechaIni()+" - "+reporteParamsTO.getFechaFin());
        encabezado.getCell(1).setCellStyle(style);
        encabezado.createCell(3).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(3).setCellStyle(style);
        encabezado.createCell(4).setCellValue(fechaActual);
        encabezado.getCell(4).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("Folio");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Empleado");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Nombre");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Tipo Empleado");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Region");
        celda.getCell(4).setCellStyle(style);
        
        if(reporteParamsTO.getTipo()==1 || reporteParamsTO.getTipo()==3){
        	celda.createCell(5).setCellValue("Fecha Alta");
            celda.getCell(5).setCellStyle(style);
        }else if(reporteParamsTO.getTipo()==2 || reporteParamsTO.getTipo()==4){
        	celda.createCell(5).setCellValue("Fecha Baja");
            celda.getCell(5).setCellStyle(style);
        }
        
        celda.createCell(6).setCellValue("Tipo Poliza");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Importe");
        celda.getCell(7).setCellStyle(style);
        
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getIdSolicitud());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getApellidoP()+" "+reporteCategoriaTO.getApellidoM()+" "+reporteCategoriaTO.getNombre());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getRegion());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getCostoTot());
		}
	}
	
	private void generaReporteActual(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteActual");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Actual SGMM");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,7));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Actual SGMM");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,7));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(fechaActual);
        encabezado.getCell(1).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("No.");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Solicitud");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Empleado");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Nombre");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Region");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Fecha Alta");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Costo");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Tipo Empleado");
        celda.getCell(7).setCellStyle(style);
        celda.createCell(8).setCellValue("Cobertura");
        celda.getCell(8).setCellStyle(style);
        
        int rowCount = 7;
        int i=0;
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(i);
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getIdSolicitud());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getApellidoP()+" "+reporteCategoriaTO.getApellidoM()+" "+reporteCategoriaTO.getNombre());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getRegion());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getCostoTot());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(8).setCellValue(reporteCategoriaTO.getCobertura());
        	i++;
		}
	}
	
	private void generaReporteConsulmed(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteDependiente");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Consulmed");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,7));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Consulmed");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,7));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Rango Fechas: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(reporteParamsTO.getFechaIni()+" - "+reporteParamsTO.getFechaFin());
        encabezado.getCell(1).setCellStyle(style);
        encabezado.createCell(3).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(3).setCellStyle(style);
        encabezado.createCell(4).setCellValue(fechaActual);
        encabezado.getCell(4).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("A. Paterno");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("A. Materno");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Nombre");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Parentesco");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Fecha Nacimiento");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Region");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Ubicacion");
        celda.getCell(7).setCellStyle(style);
        
        if(reporteParamsTO.getTipo()==1 || reporteParamsTO.getTipo()==2){
        	celda.createCell(8).setCellValue("Fecha Alta");
            celda.getCell(8).setCellStyle(style);
        }else if(reporteParamsTO.getTipo()==3 || reporteParamsTO.getTipo()==4){
        	celda.createCell(8).setCellValue("Fecha Baja");
            celda.getCell(8).setCellStyle(style);
        }
       
        celda.createCell(9).setCellValue("Cobertura");
        celda.getCell(9).setCellStyle(style);
        
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getApellidoP());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getApellidoM());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getNombre());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getParentesco());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getFechaNac());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getRegion());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getCentro());
        	datos.createCell(8).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(9).setCellValue(reporteCategoriaTO.getCobertura());
		}
	}
	
	private void generaReporteConsulmedTotal(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteDependienteTotal");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Consulmed Total");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,7));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Consulmed Total");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,7));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("A. Paterno");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("A. Materno");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Nombre");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Tipo Empleado");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Parentesco");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Fecha Nacimiento");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Fecha Alta");
        celda.getCell(7).setCellStyle(style);
        celda.createCell(8).setCellValue("Monto");
        celda.getCell(8).setCellStyle(style);
        celda.createCell(9).setCellValue("Monto Ampl");
        celda.getCell(9).setCellStyle(style);
        celda.createCell(10).setCellValue("Cobertura");
        celda.getCell(10).setCellStyle(style);
        
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getApellidoP());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getApellidoM());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getNombre());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getParentesco());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getFechaNac());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(8).setCellValue(reporteCategoriaTO.getCostoDep());   
        	datos.createCell(9).setCellValue(reporteCategoriaTO.getCostoAmp());   
        	datos.createCell(10).setCellValue(reporteCategoriaTO.getCobertura());
		}
	}
	
	private void generaReporteDatosFiscales(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteDatosFiscales");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Datos Fiscales");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,14));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Datos Fiscales");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,14));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Rango Fechas: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(reporteParamsTO.getFechaIni()+" - "+reporteParamsTO.getFechaFin());
        encabezado.getCell(1).setCellStyle(style);
        encabezado.createCell(3).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(3).setCellStyle(style);
        encabezado.createCell(4).setCellValue(fechaActual);
        encabezado.getCell(4).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("POLIZA");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("CATEGORIA");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("TIPO");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("A. PATERNO");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("A. MATERNO");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("NOMBRE");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("NOMBRE COMPLETO");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("EMISOR");
        celda.getCell(7).setCellStyle(style);
        celda.createCell(8).setCellValue("ANIO");
        celda.getCell(8).setCellStyle(style);
        celda.createCell(9).setCellValue("RECIBO");
        celda.getCell(9).setCellStyle(style);
        celda.createCell(10).setCellValue("PRIMA TOTAL");
        celda.getCell(10).setCellStyle(style);
        celda.createCell(11).setCellValue("SEXO");
        celda.getCell(11).setCellStyle(style);
        celda.createCell(12).setCellValue("FECHA NAC.");
        celda.getCell(12).setCellStyle(style);
        celda.createCell(13).setCellValue("CALLE");
        celda.getCell(13).setCellStyle(style);
        celda.createCell(14).setCellValue("NUMERO");
        celda.getCell(14).setCellStyle(style);
        celda.createCell(15).setCellValue("COLONIA");
        celda.getCell(15).setCellStyle(style);
        celda.createCell(16).setCellValue("DELEGACION");
        celda.getCell(16).setCellStyle(style);
        celda.createCell(17).setCellValue("CP");
        celda.getCell(17).setCellStyle(style);
        celda.createCell(18).setCellValue("NUM. EMP");
        celda.getCell(18).setCellStyle(style);
        celda.createCell(19).setCellValue("FECHA ANTIGUEDAD");
        celda.getCell(19).setCellStyle(style);
        celda.createCell(20).setCellValue("RFC");
        celda.getCell(20).setCellStyle(style);
        celda.createCell(21).setCellValue("CORREO");
        celda.getCell(21).setCellStyle(style);
        celda.createCell(22).setCellValue("CATG");
        celda.getCell(22).setCellStyle(style);
        celda.createCell(23).setCellValue("TITULARES");
        celda.getCell(23).setCellStyle(style);
        celda.createCell(24).setCellValue("DEPENDIENTES");
        celda.getCell(24).setCellStyle(style);
        celda.createCell(25).setCellValue("FECHA ENDOSO");
        celda.getCell(25).setCellStyle(style);
        celda.createCell(26).setCellValue("PRIMA NETA NAC.");
        celda.getCell(26).setCellStyle(style);
        celda.createCell(27).setCellValue("TIPO SOL.");
        celda.getCell(27).setCellStyle(style);
        celda.createCell(28).setCellValue("ESTATUS SOL.");
        celda.getCell(28).setCellStyle(style);
                
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue("");
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getApellidoP());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getApellidoM());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getNombre());
        	datos.createCell(6).setCellValue("");
        	datos.createCell(7).setCellValue("");
        	datos.createCell(8).setCellValue("");
        	datos.createCell(9).setCellValue("");
        	datos.createCell(10).setCellValue("");
        	datos.createCell(11).setCellValue(reporteCategoriaTO.getSexo());
        	datos.createCell(12).setCellValue(reporteCategoriaTO.getFechaNac());
        	datos.createCell(13).setCellValue(reporteCategoriaTO.getCalle());
        	datos.createCell(14).setCellValue(reporteCategoriaTO.getNumero());
        	datos.createCell(15).setCellValue(reporteCategoriaTO.getColonia());
        	datos.createCell(16).setCellValue(reporteCategoriaTO.getMunicipio());
        	datos.createCell(17).setCellValue(reporteCategoriaTO.getCp());
        	datos.createCell(18).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(19).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(20).setCellValue(reporteCategoriaTO.getRfc());
        	datos.createCell(21).setCellValue(reporteCategoriaTO.getCorreo());
        	datos.createCell(22).setCellValue("");
        	datos.createCell(23).setCellValue("1");
        	datos.createCell(24).setCellValue(reporteCategoriaTO.getNumDep());
        	datos.createCell(25).setCellValue(reporteCategoriaTO.getFechaEndose());
        	datos.createCell(26).setCellValue(reporteCategoriaTO.getTarifaAmp());
        	datos.createCell(27).setCellValue(reporteCategoriaTO.getTipoSol());
        	datos.createCell(28).setCellValue(reporteCategoriaTO.getEstSol());
        	
		}
	}
	
	private void generaReporteAmpliacionDep(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteAmpliacionDep");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Ampliaciones Dep");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,7));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Ampliaciones");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,7));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Rango Fechas: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(reporteParamsTO.getFechaIni()+" - "+reporteParamsTO.getFechaFin());
        encabezado.getCell(1).setCellStyle(style);
        encabezado.createCell(3).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(3).setCellStyle(style);
        encabezado.createCell(4).setCellValue(fechaActual);
        encabezado.getCell(4).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("A. Paterno");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("A. Materno");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Nombre");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Fecha Alta");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Parentesco");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Sexo");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Fecha Nacimiento");
        celda.getCell(7).setCellStyle(style);
        celda.createCell(8).setCellValue("Cobertura");
        celda.getCell(8).setCellStyle(style);
        celda.createCell(9).setCellValue("Tipo Empleado");
        celda.getCell(9).setCellStyle(style);
        
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getApellidoP());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getApellidoM());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getNombre());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getParentesco());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getSexo());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getFechaNac());
        	datos.createCell(8).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(9).setCellValue(reporteCategoriaTO.getTipoEmpleado());
		}
	}
	
	private void generaReporteAmpliacion(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteAmpliaciones");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Ampliaciones");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Ampliaciones");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);
        
        HSSFRow encabezado = sheet.createRow(4);
        encabezado.createCell(0).setCellValue("Rango Fechas: ");
        encabezado.getCell(0).setCellStyle(style);
        encabezado.createCell(1).setCellValue(reporteParamsTO.getFechaIni()+" - "+reporteParamsTO.getFechaFin());
        encabezado.getCell(1).setCellStyle(style);
        encabezado.createCell(3).setCellValue("Fecha del Reporte: ");
        encabezado.getCell(3).setCellStyle(style);
        encabezado.createCell(4).setCellValue(fechaActual);
        encabezado.getCell(4).setCellStyle(style);
        
        HSSFRow celda = sheet.createRow(6);
        celda.createCell(0).setCellValue("Solicitud");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Empleado");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("A. Paterno");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("A. Materno");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Nombre");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Fecha Alta");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Categoria");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Tipo Poliza");
        celda.getCell(7).setCellStyle(style);
        celda.createCell(8).setCellValue("Prima Basica");
        celda.getCell(8).setCellStyle(style);
        celda.createCell(9).setCellValue("Prima Ampl");
        celda.getCell(9).setCellStyle(style);
        celda.createCell(10).setCellValue("Prima Total");
        celda.getCell(10).setCellStyle(style);
        celda.createCell(11).setCellValue("Parentesco");
        celda.getCell(11).setCellStyle(style);
        celda.createCell(12).setCellValue("Fecha Nac.");
        celda.getCell(12).setCellStyle(style);
        celda.createCell(13).setCellValue("Sexo");
        celda.getCell(13).setCellStyle(style);
        
        int rowCount = 7;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getIdSolicitud());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getApellidoP());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getApellidoM());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getNombre());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getFechaAlta());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(8).setCellValue(reporteCategoriaTO.getCostoEmp());
        	datos.createCell(9).setCellValue(reporteCategoriaTO.getCostoAmp());
        	datos.createCell(10).setCellValue(reporteCategoriaTO.getCostoTot());
        	datos.createCell(11).setCellValue(reporteCategoriaTO.getParentesco());
        	datos.createCell(12).setCellValue(reporteCategoriaTO.getFechaNac());
        	datos.createCell(13).setCellValue(reporteCategoriaTO.getSexo());
		}
	}
	
	private void generaReporteEncuesta(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteEncuesta");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Encuesta");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Relacion Encuestas");
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);
                
        HSSFRow celda = sheet.createRow(4);
        celda.createCell(0).setCellValue("Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Fecha Respuesta");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Fecha Actualizacion");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Descripcion");
        celda.getCell(3).setCellStyle(style);
                
        int rowCount = 5;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getFechaResp());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getFechaAct());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getCobertura());
        	
		}
	}
	
	private void generaReporteRenovacionSi(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteRenovacion");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Renovaciones");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Empleados que respondieron SI a la renovacion");
                       
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);
                
        HSSFRow celda = sheet.createRow(4);
        celda.createCell(0).setCellValue("Folio");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Empleado");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Tipo Empleado");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Costo Empleado");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Costo Dependientes");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Costo Ampliacion");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Prima Total");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Tipo Cobertura");
        celda.getCell(7).setCellStyle(style);
        celda.createCell(8).setCellValue("Fecha Respuesta");
        celda.getCell(8).setCellStyle(style);
        celda.createCell(9).setCellValue("Respuesta");
        celda.getCell(9).setCellStyle(style);
        celda.createCell(10).setCellValue("Fecha Renovacion");
        celda.getCell(10).setCellStyle(style);
                
        int rowCount = 5;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getIdSolicitud());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getCostoEmp());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getCostoDep());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getCostoAmp());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getCostoTot());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(8).setCellValue(reporteCategoriaTO.getFechaResp());
        	datos.createCell(9).setCellValue(reporteCategoriaTO.getMensaje());
        	datos.createCell(10).setCellValue(reporteCategoriaTO.getFechaAct());
		}
	}
	
	private void generaReporteRenovacionNo(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteRenovacionNo");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Renovaciones");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Empleados que respondieron NO a la renovacion");
                       
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);
                
        HSSFRow celda = sheet.createRow(4);
        celda.createCell(0).setCellValue("Folio");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Empleado");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Tipo Empleado");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Costo Empleado");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Costo Dependientes");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Prima Total");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Tipo Cobertura");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Respuesta");
        celda.getCell(7).setCellStyle(style);
                
        int rowCount = 5;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getIdSolicitud());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getTipoEmpleado());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getCostoEmp());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getCostoDep());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getCostoTot());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getMensaje());
		}
	}
	
	private void generaReporteRenovacionNoContesto(HSSFWorkbook workbook, List<ReporteTO> lista, 
			ReporteParamsTO reporteParamsTO, String fechaActual){
		logger.info("ReporteExcelView.generaReporteRenovacionNoContesto");
		
	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Renovaciones");
        sheet.setDefaultColumnWidth(15);
         
        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("EMPRESA S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);
        
        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Empleados que NO respondieron");
                       
        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);
                
        HSSFRow celda = sheet.createRow(4);
        celda.createCell(0).setCellValue("Folio");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Empleado");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Tipo Cobertura");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("Costo Empleado");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Costo Dependientes");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Costo Ampliacion");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Prima Total");
        celda.getCell(6).setCellStyle(style);
        celda.createCell(7).setCellValue("Fecha Renovacion");
        celda.getCell(7).setCellStyle(style);
                
        int rowCount = 5;
        
        for (ReporteTO reporteCategoriaTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteCategoriaTO.getIdSolicitud());
        	datos.createCell(1).setCellValue(reporteCategoriaTO.getNumEmp());
        	datos.createCell(2).setCellValue(reporteCategoriaTO.getCobertura());
        	datos.createCell(3).setCellValue(reporteCategoriaTO.getCostoEmp());
        	datos.createCell(4).setCellValue(reporteCategoriaTO.getCostoDep());
        	datos.createCell(5).setCellValue(reporteCategoriaTO.getCostoAmp());
        	datos.createCell(6).setCellValue(reporteCategoriaTO.getCostoTot());
        	datos.createCell(7).setCellValue(reporteCategoriaTO.getFechaAct());
		}
	}

    private void generaReporteMemorialEncuesta(HSSFWorkbook workbook, List<ReporteTO> lista,
                                               ReporteParamsTO reporteParamsTO, String fechaActual){
        logger.info("ReporteExcelView.generaReporteMemorialEncuesta");

        // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Memorial");
        sheet.setDefaultColumnWidth(15);

        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("RADIOMOVIL DIPSA, S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);

        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Empleados que contrataton Memorial");

        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);

        HSSFRow celda = sheet.createRow(4);
        celda.createCell(0).setCellValue("Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Paquete");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("Fecha Renovacion");
        celda.getCell(2).setCellStyle(style);

        int rowCount = 5;

        for (ReporteTO reporteCategoriaTO : lista) {
            HSSFRow datos = sheet.createRow(rowCount++);
            datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
            datos.createCell(1).setCellValue(reporteCategoriaTO.getCobertura());
            datos.createCell(2).setCellValue(reporteCategoriaTO.getFechaAct());
        }
    }

    private void generaReporteMemorialDependiente(HSSFWorkbook workbook, List<ReporteTO> lista,
                                                  ReporteParamsTO reporteParamsTO, String fechaActual){
        logger.info("ReporteExcelView.generaReporteMemorialDependiente");

        // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte Memorial Dependientes");
        sheet.setDefaultColumnWidth(15);

        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // Crear Filas
        HSSFRow tituloUno = sheet.createRow(1);
        tituloUno.createCell(0).setCellValue("RADIOMOVIL DIPSA, S.A DE C.V");
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,10));
        tituloUno.getCell(0).setCellStyle(style);

        HSSFRow tituloDos = sheet.createRow(2);
        tituloDos.createCell(0).setCellValue("Dependientes de Memorial");

        sheet.addMergedRegion(new CellRangeAddress(2,2,0,10));
        tituloDos.getCell(0).setCellStyle(style);

        HSSFRow celda = sheet.createRow(4);
        celda.createCell(0).setCellValue("Empleado");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("Nombre");
        celda.getCell(1).setCellStyle(style);
        celda.createCell(2).setCellValue("A Paterno");
        celda.getCell(2).setCellStyle(style);
        celda.createCell(3).setCellValue("A Materno");
        celda.getCell(3).setCellStyle(style);
        celda.createCell(4).setCellValue("Parentesco");
        celda.getCell(4).setCellStyle(style);
        celda.createCell(5).setCellValue("Fecha Nacimiento");
        celda.getCell(5).setCellStyle(style);
        celda.createCell(6).setCellValue("Id Solicitud");
        celda.getCell(6).setCellStyle(style);

        int rowCount = 5;

        for (ReporteTO reporteCategoriaTO : lista) {
            HSSFRow datos = sheet.createRow(rowCount++);
            datos.createCell(0).setCellValue(reporteCategoriaTO.getNumEmp());
            datos.createCell(1).setCellValue(reporteCategoriaTO.getNombre());
            datos.createCell(2).setCellValue(reporteCategoriaTO.getApellidoP());
            datos.createCell(3).setCellValue(reporteCategoriaTO.getApellidoM());
            datos.createCell(4).setCellValue(reporteCategoriaTO.getParentesco());
            datos.createCell(5).setCellValue(reporteCategoriaTO.getFechaAct());
            datos.createCell(6).setCellValue(reporteCategoriaTO.getIdSolicitud());
        }
    }

} // Fin de la clase