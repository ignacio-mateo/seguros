package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.SolicitudAdminTO;

public interface AdminSolicitudService {
	
	Integer getSolicitudExiste(Integer numEmp);
	SolicitudAdminTO getSolicitud(Integer numEmp);
	List<SolicitudAdminTO> getSolicitudes(Integer numEmp);
	List<SolicitudAdminTO> getDependiente(Integer numEmp);
	List<SolicitudAdminTO> getConsulmed(Integer numEmp);
	List<SolicitudAdminTO> getMemorial(Integer numEmp);
	void setSolicitud(SolicitudAdminTO solicitudAdminTO);
	void setDependiente(SolicitudAdminTO solicitudAdminTO);
	void setConsulmed(SolicitudAdminTO solicitudAdminTO);

}
