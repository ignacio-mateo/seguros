package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.ReporteParamsTO;
import mx.com.orbita.to.ReporteTO;

public interface ReporteService {

	List<ReporteTO> getCambioCategoria(ReporteParamsTO reporteTO);
	List<ReporteTO> getNomina(ReporteParamsTO reporteTO);
	List<ReporteTO> getReporteActual(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmed(ReporteParamsTO reporteTO);
	List<ReporteTO> getConsulmedTotal(ReporteParamsTO reporteTO);
	List<ReporteTO> getDatosFiscales(ReporteParamsTO reporteTO);
	List<ReporteTO> getAmpliacionesDep(ReporteParamsTO reporteTO);
	List<ReporteTO> getAmpliaciones(ReporteParamsTO reporteTO);
	List<ReporteTO> getDescargaSol(ReporteParamsTO reporteTO);
	List<ReporteTO> getRenovacion(ReporteParamsTO reporteTO);
	
}
