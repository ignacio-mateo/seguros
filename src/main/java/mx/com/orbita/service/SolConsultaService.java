package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.AmpliacionTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface SolConsultaService {
	
	EmpleadoTO getEmpleadoBD(Integer numEmp);
	
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	SolicitudTO getSolicitudBaja(Integer numEmp);
	SolicitudTO getSolicitudCob(Integer numEmp, Integer tipoSol, Integer estatusSol);
	Integer getSolicitudId(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<AmpliacionTO> getTipoCob();
	List<AmpliacionTO> getTipoCobMem();
	String getTipoCob(Integer idTipoCob);
	
	List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<DependienteTO> getDependienteRenov(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<Integer> getIdDependiente(Integer idSolicitud, Integer tipoSol);
	DependienteTO getDependienteAltaDetalle(Integer idDependiente);
	DependienteTO getDependienteBajaDetalle(Integer idDependiente);
	List<DependienteTO> getDependienteConsultaBaja(Integer idSolicitud);
	List<DependienteTO> getDependienteBaja(Integer idSolicitud);
	List<Integer> getDependienteBajaxId(Integer idSolicitud);
	
	List<ConsulmedTO> getConsulmed(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB);
	List<Integer> getIdConsulmed(Integer idSolicitud, Integer tipoSol);
	ConsulmedTO getConsulmedAltaDetalle(Integer idDependiente);
	ConsulmedTO getConsulmedBajaDetalle(Integer idDependiente);
	List<ConsulmedTO> getConsulmedConsultaBaja(Integer idSolicitud);
	List<ConsulmedTO> getConsulmedBaja(Integer idSolicitud);
	List<Integer> getConsulmedBajaxId(Integer idSolicitud);
	List<ConsulmedTO> getConsulmedDisponibles(Integer numEmp);
	
	List<ConsulmedTO> getMemorial(Integer idSolicitud);
	
}
