package mx.com.orbita.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.controller.form.CategoriaForm;
import mx.com.orbita.dao.CambioCategoriaDao;
import mx.com.orbita.service.CambioCategoriaService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Redondeos;

@Component
public class CambioCategoriaServiceImpl implements CambioCategoriaService {

	private static final Logger logger = Logger.getLogger(CambioCategoriaServiceImpl.class);

	private CambioCategoriaDao cambioCategoriaDao;
	private SolAltaService solAltaService;

	public EmpleadoTO getEmplado(Integer numEmp){
		return cambioCategoriaDao.getEmpleado(numEmp);
	}

	public SolicitudTO getSolicitud(Integer numEmp){
		return cambioCategoriaDao.getSolicitud(numEmp);
	}

	public Integer getSolicitudExiste(Integer numEmp, Integer idTipoSol, Integer idEstSol){
		return cambioCategoriaDao.getSolicitudExiste(numEmp, idTipoSol, idEstSol);
	}

	public List<DependienteTO> getDependiente(Integer numEmp){
		return cambioCategoriaDao.getDependiente(numEmp);
	}

	public Integer getDependienteProceso(Integer numEmp){
		return cambioCategoriaDao.getDependienteProceso(numEmp, Codigo.EST_SOL_ALTA_EN_PROCESO);
	}

	public CategoriaForm getCategoriaActual(Integer numEmp){
		CategoriaForm categoria = new CategoriaForm();
		EmpleadoTO empleadoTO = cambioCategoriaDao.getEmpleado(numEmp);
		SolicitudTO solicitudTO = cambioCategoriaDao.getSolicitud(numEmp);
		List<DependienteTO> listDep = cambioCategoriaDao.getDependiente(numEmp);
		logger.info("  numEmp: ["+empleadoTO.getNumEmp()+"] idSol: ["+solicitudTO.getIdSol()
				+"] costoTotal: ["+solicitudTO.getCostoTotal()+"] listDep: ["+listDep.size()+"]");
		categoria.setEmpleadoTO(empleadoTO);
		categoria.setSolicitudTO(solicitudTO);
		categoria.setListDep(listDep);
		return categoria;
	}

	public CategoriaForm getCategoriaTransitoria(Integer numEmp, Integer mes, Integer dia){
		CategoriaForm categoria = new CategoriaForm();
		double montoEmpTotal = 0.0;
		double montoDepNuevo = 0.0;
		double montoDepProrrateado = 0.0;
		double montoDepTotal = 0.0;
		double montoAmplNuevo = 0.0;
		double montoAmplTotal = 0.0;
		double montoTotal = 0.0;

		EmpleadoTO empleadoTO = cambioCategoriaDao.getEmpleado(numEmp);
		SolicitudTO solicitudTO = cambioCategoriaDao.getSolicitud(numEmp);
		List<DependienteTO> listDep = cambioCategoriaDao.getDependiente(numEmp);

		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
		Date fecha = new Date();
		try {
			fecha = formatoDelTexto.parse(empleadoTO.getFechaNacF());
			empleadoTO.setFechaNac(fecha);
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage());
		}

		// Calcular el costo del empleado a partir de la fecha seleccionada
		EmpleadoTO empleadoNuevo = solAltaService.getCotizacion(empleadoTO, mes, dia);
		logger.info("  costoEmp: ["+solicitudTO.getCostoEmpleado()
					+"] costoEmpNuevo: ["+empleadoNuevo.getMontoEmpProrateado()+"]");
		montoEmpTotal = solicitudTO.getCostoEmpleado() - empleadoNuevo.getMontoEmpProrateado();
		solicitudTO.setCostoEmpleado(Redondeos.roundNum(montoEmpTotal));

		// Calcular el costo de los dependientes a partir de la fecha seleccionada
		for (DependienteTO dependienteTO : listDep) {
			montoDepNuevo = solAltaService.getMontoDependiente(empleadoTO.getTipoEmp(),
								dependienteTO.getIdParentesco(),dependienteTO.getFechaNac(),
								dependienteTO.getSexo(),mes,dia);
			montoDepProrrateado = dependienteTO.getCostoDep() - montoDepNuevo;
			dependienteTO.setCostoDep(Redondeos.roundNum(montoDepProrrateado));
			montoDepTotal=montoDepTotal+montoDepProrrateado;
			logger.info("  montoDep: ["+dependienteTO.getCostoDep()
						+"] montoDepNuevo: ["+montoDepNuevo
						+"] montoProrr: ["+dependienteTO.getCostoDep()+"]");
		}

		if(solicitudTO.getIdTipoCobertura()!=0){
			montoAmplNuevo = solAltaService.getMontoAmpliacion(solicitudTO.getIdTipoCobertura(),
																	listDep.size(),mes,dia);
		}

		// Resta del monto de ampliacion total = Costo Ampliacion Actual - Nuevo
		montoAmplTotal = solicitudTO.getCostoAmpliacion() - montoAmplNuevo;
		solicitudTO.setCostoAmpliacion(Redondeos.roundNum(montoAmplTotal));

		// Sumatoria total = costo emp + costo dependientes + costo ampliacion
		montoTotal = solicitudTO.getCostoEmpleado() + montoDepTotal + montoAmplTotal;
		solicitudTO.setCostoTotal(Redondeos.roundNum(montoTotal));
		solicitudTO.setCostoTotalAct(Redondeos.roundNum(montoTotal));

		logger.info("  montoEmp: [ "+solicitudTO.getCostoEmpleado()
					+"] montoDepTotal: ["+Redondeos.roundNum(montoDepTotal)
					+"] montoAmplNuevo: ["+montoAmplNuevo
					+"] montoAmpl: ["+solicitudTO.getCostoAmpliacion()
					+"] montoTotal: ["+solicitudTO.getCostoTotal()+"]");

		categoria.setEmpleadoTO(empleadoTO);
		categoria.setSolicitudTO(solicitudTO);
		categoria.setListDep(listDep);
		return categoria;
	}

	public CategoriaForm getCambioCategoria(Integer numEmp, Integer mes, Integer dia) {
		CategoriaForm categoria = new CategoriaForm();
		Calendar cal= Calendar.getInstance();
		int anio= cal.get(Calendar.YEAR);
		String fecha = dia+"/"+mes+"/"+anio;
		double montoAmpliacion = 0.0;
		double montoTotalDep = 0.0;
		double montoTotalDepT = 0.0;
		double montoTotal = 0.0;

		// Consulta el tipo de empleado desde la BD de Nominas
		EmpleadoTO empleadoTO = solAltaService.getEmpleadoWS(numEmp);
		logger.info("  Tipo Empleado Original: ["+empleadoTO.getTipoEmp()+"]");

		// Consulta el tipo de empleado desde la BD de Gastos Medicos
		EmpleadoTO empleadoSGMM = cambioCategoriaDao.getEmpleado(numEmp);

		if(empleadoSGMM.getTipoEmp().equals(1)){
			empleadoTO.setTipoEmp(Codigo.ID_EMPLEADO_SINDICALIZADO);
			empleadoTO.setDescTipoEmp("SINDICALIZADO");
			empleadoTO.setDescEmpleado("EMPLEADO");
		}else if(empleadoSGMM.getTipoEmp().equals(2)){
			empleadoTO.setTipoEmp(Codigo.ID_EMPLEADO_CONFIANZA);
			empleadoTO.setDescTipoEmp("CONFIANZA");
			empleadoTO.setDescEmpleado("EMPLEADO");
		}
		logger.info("  Tipo Empleado CambioCategoria: ["+empleadoTO.getTipoEmp()+"]");

		// Consulta tarifa y monto del empleado
		empleadoTO = solAltaService.getCotizacion(empleadoTO, mes, dia);

		// Calcular los montos de los dependientes
		List<DependienteTO> listDep = cambioCategoriaDao.getDependiente(numEmp);
		for (DependienteTO dependienteTO : listDep) {
			dependienteTO.setCostoDep(solAltaService.getMontoDependiente(empleadoTO.getTipoEmp(),
										dependienteTO.getIdParentesco(),dependienteTO.getFechaNac(),
										dependienteTO.getSexo(),mes,dia));
			montoTotalDep+=dependienteTO.getCostoDep();
			dependienteTO.setFechaAlta(fecha);
			logger.info("  montoDep: ["+dependienteTO.getCostoDep()+"]");
		}

		// Calcular si hay ampliacion
		SolicitudTO solicitudTO = cambioCategoriaDao.getSolicitud(numEmp);
		if(solicitudTO.getIdTipoCobertura()!=0){
			montoAmpliacion = solAltaService.getMontoAmpliacion(solicitudTO.getIdTipoCobertura(),
																	listDep.size(),mes,dia);
			solicitudTO.setCostoAmpliacion(montoAmpliacion);
		}

		solicitudTO.setFechaElaboracion(fecha);

		// Costo Total Dependientes
		montoTotalDepT = Redondeos.roundNum(montoTotalDep);
		montoTotal = Redondeos.roundNum(empleadoTO.getMontoEmpProrateado()
										+montoTotalDepT+solicitudTO.getCostoAmpliacion());
		solicitudTO.setCostoDependiente(montoTotalDepT);

		// Costo Total
		solicitudTO.setCostoTotal(montoTotal);

		// Costo Total Prorrateado
		solicitudTO.setCostoProrrateado(0.0);

		// Costo Total Actualizado
		solicitudTO.setCostoTotalAct(0.0);

		logger.info("  montoEmp: ["+empleadoTO.getMontoEmpProrateado()
					+"] montoTotDep: ["+solicitudTO.getCostoDependiente()
					+"] montoAmpl: ["+solicitudTO.getCostoAmpliacion()
					+"] montoTotal: ["+solicitudTO.getCostoTotal()
					+"] montoProrrateado: ["+solicitudTO.getCostoProrrateado()
					+"] montoTotActualizado: ["+solicitudTO.getCostoTotalAct()+"]");

		categoria.setEmpleadoTO(empleadoTO);
		categoria.setSolicitudTO(solicitudTO);
		categoria.setListDep(listDep);
		return categoria;
	}

	public void setCambioCategoria(EmpleadoTO empleadoTO, EmpTO empTO, SolicitudTO solicitudTO,
																List<DependienteTO> listDep) {
		// Actualizar la solictud de empleado
		cambioCategoriaDao.setSolicitud(empleadoTO.getMontoEmpProrateado(), solicitudTO.getCostoAmpliacion(),
				solicitudTO.getCostoDependiente(),solicitudTO.getCostoTotal(), solicitudTO.getIdTipoCobertura(),
				empleadoTO.getNumEmp(), solicitudTO.getIdSol());

		// Actualizar depedientes
		if(listDep!=null && listDep.size()>0){
			for (DependienteTO dependienteTO : listDep) {
				cambioCategoriaDao.setDependiente(dependienteTO.getCostoDep(), dependienteTO.getIdDependiente());
			}
		}

		// Actualizar el tipo de empleado
		cambioCategoriaDao.setEmpleado(empleadoTO.getTipoEmp(), empleadoTO.getNumEmp());

		// Guardar los datos anteriores
		int categoriaAnt = 0;
		int categoriaNvo = 0;
		if(empleadoTO.getTipoEmp().equals(1)){
			categoriaAnt = Codigo.ID_EMPLEADO_SINDICALIZADO;
			categoriaNvo = Codigo.ID_EMPLEADO_CONFIANZA;
		}else if(empleadoTO.getTipoEmp().equals(2)){
			categoriaAnt = Codigo.ID_EMPLEADO_CONFIANZA;
			categoriaNvo = Codigo.ID_EMPLEADO_SINDICALIZADO;
		}

		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha = new Date();
		try {
			fecha = formatoDelTexto.parse(solicitudTO.getFechaElaboracion());
		} catch (ParseException e) {
			logger.info("  ParseException: "+e.getMessage());
		}

		cambioCategoriaDao.setCategoria(empleadoTO.getNumEmp(), solicitudTO.getIdSol(), solicitudTO.getCostoTotalAnt(),
				solicitudTO.getCostoTotal(), categoriaAnt, categoriaNvo, fecha, empTO.getNumEmp());
	}


	@Autowired
	public void setCambioCategoriaDao(CambioCategoriaDao cambioCategoriaDao) {
		this.cambioCategoriaDao = cambioCategoriaDao;
	}

	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}

}
