package mx.com.orbita.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AdminTarifaDao;
import mx.com.orbita.service.AdminTarifaService;
import mx.com.orbita.to.TarifaTO;

@Component
public class AdminTarifaServiceImpl implements AdminTarifaService {

	private static final Logger logger = Logger.getLogger(AdminTarifaServiceImpl.class);

	private AdminTarifaDao adminTarifaDao;

	public List<TarifaTO> getTarifaConfianza() {
		logger.info("AdminTarifaServiceImpl.getTarifaConfianza");
		return adminTarifaDao.getTarifaConfianza();
	}

	public List<TarifaTO> getTarifaSindicalizado() {
		logger.info("AdminTarifaServiceImpl.getTarifaSindicalizado");
		return adminTarifaDao.getTarifaSindicalizado();
	}

	public List<TarifaTO> getTarifaAmplia() {
		logger.info("AdminTarifaServiceImpl.getTarifaAmplia");
		return adminTarifaDao.getTarifaAmplia();
	}

	public void setTarifaConfianza(List<TarifaTO> lista) {
		logger.info("AdminTarifaServiceImpl.setTarifaConfianza");
		for (TarifaTO tarifaTO : lista) {
			adminTarifaDao.setTarifaConfianza(tarifaTO.getTarifa(), tarifaTO.getTarifaNueva(), tarifaTO.getId());
		}
	}

	public void setTarifaSindicalizado(List<TarifaTO> lista) {
		logger.info("AdminTarifaServiceImpl.setTarifaSindicalizado");
		for (TarifaTO tarifaTO : lista) {
			adminTarifaDao.setTarifaSindicalizado(tarifaTO.getTarifa(), tarifaTO.getTarifaNueva(), tarifaTO.getId());
		}
	}

	public void setTarifaAmplia(List<TarifaTO> lista) {
		logger.info("AdminTarifaServiceImpl.setTarifaAmplia");
		for (TarifaTO tarifaTO : lista) {
			adminTarifaDao.setTarifaAmplia(tarifaTO.getTarifa(), tarifaTO.getTarifaNueva(), tarifaTO.getId());
		}
	}


	@Autowired
	public void setAdminTarifaDao(AdminTarifaDao adminTarifaDao) {
		this.adminTarifaDao = adminTarifaDao;
	}



}
