package mx.com.orbita.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.FechaAntiguedadDao;
import mx.com.orbita.service.FechaAntiguedadService;
import mx.com.orbita.to.FechaAntiguedadTO;
import mx.com.orbita.util.Codigo;

@Component
public class FechaAntiguedadServiceImpl implements FechaAntiguedadService {

	private static final Logger logger = Logger.getLogger(FechaAntiguedadServiceImpl.class);
	private FechaAntiguedadDao fechaAntiguedadDa;

	public Integer existeSolicitud(Integer numEmp){
		return fechaAntiguedadDa.existeSolicitud(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
	}

	public void setFechaAntiguedad(FechaAntiguedadTO fechaAntiguedadTO) {
		Date fechaElab = new Date();
		Date fechaAut = new Date();
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		try {
			if(fechaAntiguedadTO.getOnSeleccion().equals("1")){
				fechaElab = formatoDelTexto.parse(fechaAntiguedadTO.getFechaElabTit());
				fechaAut = formatoDelTexto.parse(fechaAntiguedadTO.getFechaAltaTit());
				logger.info(" Titular fechaElab: ["+fechaElab+"] fechaAlta: ["+fechaAut+"] numEmp: ["+fechaAntiguedadTO.getNumEmpSolicitado()+"]");
				fechaAntiguedadDa.setSolicitud(fechaElab, fechaAut, fechaAntiguedadTO.getNumEmpSolicitado());
			}else if(fechaAntiguedadTO.getOnSeleccion().equals("2")){
				fechaElab = formatoDelTexto.parse(fechaAntiguedadTO.getFechaElabDep());
				fechaAut = formatoDelTexto.parse(fechaAntiguedadTO.getFechaAltaDep());
				logger.info(" Dep. fechaElab: ["+fechaElab+"] fechaAlta: ["+fechaAut+"] idDep: ["+fechaAntiguedadTO.getIdDep()+"]");
				fechaAntiguedadDa.setDependiente(fechaElab, fechaAut, fechaAntiguedadTO.getIdDep());
			}
		} catch (ParseException e) {
			logger.info("  Exception FechaAntiguedad: "+e.getMessage());
		}

	}

	@Autowired
	public void setFechaAntiguedadDa(FechaAntiguedadDao fechaAntiguedadDa) {
		this.fechaAntiguedadDa = fechaAntiguedadDa;
	}

}
