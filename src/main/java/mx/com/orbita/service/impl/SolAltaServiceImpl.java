package mx.com.orbita.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.business.EmpleadoBusiness;
import mx.com.orbita.dao.SolAltaDao;
import mx.com.orbita.dao.SolConsultaDao;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Fechas;
import mx.com.orbita.util.MontoProrrateo;
import mx.com.orbita.util.Redondeos;

@Component
public class SolAltaServiceImpl implements SolAltaService{

	private static final Logger logger = Logger.getLogger(SolAltaServiceImpl.class);
	
	private SolAltaDao solAltaDao;
	private SolConsultaDao solConsultaDao;
	private EmpleadoBusiness empleadoBusiness;
		
	/**
	 * 
	 * Consulta datos del empleado de la BD de Empleados
	 * @param numEmp
	 * 
	 */
	public EmpleadoTO getEmpleadoWS(Integer numEmp){
		return empleadoBusiness.consultaEmpleado(numEmp);
	}
	
	public double getCalculaEdad(Integer parentesco, String fecha){
		Fechas fechas = new Fechas();
		double resultado = 0;
		String claveFecha = fechas.getCodigoAnio(fecha);
		if((parentesco.equals(5) || parentesco.equals(6))  && (!claveFecha.equals("0-19") && !claveFecha.equals("20-24"))){
			resultado = -1;
		}
		return resultado; 
	}
	
	/**
	 * 
	 * Calcula el costo del Dependiente
	 * @param tipoEmp: 1 Confianza, 2 Sindicalizado
	 * @param parentesco:  
	 * @param fecha: Fecha de nacimiento yyyy-MM-dd
	 * @param sexo: M-F
	 * 
	 */
	public double getMontoDependiente(Integer tipoEmp, Integer parentesco, String fecha, String sexo) {
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		Fechas fechas = new Fechas();
		String claveFecha = "";
		double monto = 0.0;
		double montoProrateado = 0.0;
		double totalProrateado = 0.0;
		if(tipoEmp==2){ 		
			claveFecha = fechas.getCodigoAnio(fecha);
			monto = solAltaDao.getMontoSindicalizado(claveFecha,sexo);
		}else if(tipoEmp==1){ 	
			monto = solAltaDao.getMontoConfianza(parentesco);	
		}	
		montoProrateado = montoProrrateo.calculaMontoProporcional(monto);
		totalProrateado = Redondeos.roundNum(montoProrateado);
		return totalProrateado;
	}
	
	/**
	 * 
	 * Calcula el costo del Dependiente de acuerdo a una fecha (dia y mes)
	 * @param tipoEmp: 1 Confianza, 2 Sindicalizado
	 * @param parentesco:  
	 * @param fecha: Fecha de nacimiento yyyy-MM-dd
	 * @param sexo: M-F
	 * 
	 */
	public double getMontoDependiente(Integer tipoEmp, Integer parentesco, String fecha, String sexo, int mes, int dia) {
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		Fechas fechas = new Fechas();
		String claveFecha = "";
		double monto = 0.0;
		double montoProrateado = 0.0;
		double totalProrateado = 0.0;
		if(tipoEmp==2){
			claveFecha = fechas.getCodigoAnio(fecha);
			monto = solAltaDao.getMontoSindicalizado(claveFecha,sexo);
		}else if(tipoEmp==1){ 
			monto = solAltaDao.getMontoConfianza(parentesco);	
		}
		montoProrateado = montoProrrateo.calculaMontoProporcional(monto, mes, dia);
		totalProrateado = Redondeos.roundNum(montoProrateado);				
		return totalProrateado;
	}
	
	/**
	 * 
	 * Calcula el costo de ampliacion
	 * @param tipoAmpl: 1(A1), 2(A2), 3(A3)
	 * @param numDep: cantidad de dependientes
	 * 
	 */
	public double getMontoAmpliacion(int tipoAmpl, int numDep){
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoTotAmp = 0.0;
		double montoProrateado = 0.0;
		
		// Calcula el monto de ampliacion con/sin dependientes
		double monto = 0.0;
		double montoTotal = 0.0;
		
		// El +1 es el del empleado
		monto = solAltaDao.getMontoAmpliacion(tipoAmpl);
		montoTotal = (numDep+1) * monto;
		
		// Calcula el prorateo
		montoProrateado = montoProrrateo.calculaMontoProporcional(montoTotal);
		montoTotAmp = Redondeos.roundNum(montoProrateado);
		return montoTotAmp;
	}
	
	/**
	 * 
	 * Calcula el costo de ampliacion de acuerdo a una fecha (dia y mes)
	 * @param tipoAmpl: 1(A1), 2(A2), 3(A3)
	 * @param numDep: cantidad de dependientes
	 * @param mes 
	 * @param dia
	 * 
	 */
	public double getMontoAmpliacion(int tipoAmpl, int numDep, int mes, int dia){
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoTotAmp = 0.0;
		double montoProrateado = 0.0;
		
		// Calcula el monto de ampliacion con/sin dependientes
		double monto = 0.0;
		double montoTotal = 0.0;
		
		// El +1 es el del empleado
		monto = solAltaDao.getMontoAmpliacion(tipoAmpl);
		montoTotal = (numDep+1) * monto;
				
		// Calcula el prorateo
		montoProrateado = montoProrrateo.calculaMontoProporcional(montoTotal, mes, dia);
		montoTotAmp = Redondeos.roundNum(montoProrateado);
		return montoTotAmp;
	}
		
	/**
	 * 
	 * Calcula el costo de ampliacion para una sola persona
	 * @param tipoAmpl: 1(A1), 2(A2), 3(A3)
	 * @param numDep: 1 persona
	 * 
	 */
	public double getMontoAmpliacionIndividual(int tipoAmpl, int numDep){
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoTotAmp = 0.0;
		double montoProrateado = 0.0;
		
		// Calcula el monto de ampliacion con/sin dependientes
		double monto = 0.0;
		double montoTotal = 0.0;
				
		monto = solAltaDao.getMontoAmpliacion(tipoAmpl);
		montoTotal = numDep * monto;
		
		// Calcula el prorateo
		montoProrateado = montoProrrateo.calculaMontoProporcional(montoTotal);
		montoTotAmp = Redondeos.roundNum(montoProrateado);
		return montoTotAmp;
	}
	
	/**
	 * 
	 * Calcula el costo de ampliacion para una sola persona de acuerdo a una fecha (dia y mes)
	 * @param tipoAmpl: 1(A1), 2(A2), 3(A3)
	 * @param numDep: 1 persona
	 * @param mes 
	 * @param dia
	 * 
	 */
	public double getMontoAmpliacionIndividual(int tipoAmpl, int numDep, int mes, int dia){
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoTotAmp = 0.0;
		double montoProrateado = 0.0;
		
		// Calcula el monto de ampliacion con/sin dependientes
		double monto = 0.0;
		double montoTotal = 0.0;
				
		monto = solAltaDao.getMontoAmpliacion(tipoAmpl);
		montoTotal = numDep * monto;
		
		// Calcula el prorateo
		montoProrateado = montoProrrateo.calculaMontoProporcional(montoTotal, mes, dia);
		montoTotAmp = Redondeos.roundNum(montoProrateado);
		return montoTotAmp;
	}
		
	/**
	*
	* Calcula el costo de ampliacion
	* @param tipoMemorial: M1, M2
	*
	*/
	public double getMontoMemorial(int tipoMemorial){
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoTotAmp = 0.0;
		double montoProrateado = 0.0;
		
		// Calcula el monto de ampliacion con/sin dependientes
		double monto = 0.0;
		
		// Obtiene el costo del paquete
		monto = solAltaDao.getMontoMemorial(tipoMemorial);
		
		// Calcula el prorateo
		montoProrateado = montoProrrateo.calculaMontoProporcional(monto);
		montoTotAmp = Redondeos.roundNum(montoProrateado);
		return montoTotAmp;
	}
	
	/**
	 *
	 * Genera la cotizacion de un empleado a la fecha de hoy
	 * @param empleado: Bean con los datos del empleado
	 */
	public EmpleadoTO getCotizacion(EmpleadoTO empleado) {
		Fechas fechas = new Fechas();
		String formatoFecha = "";
		String claveFecha = "";
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoProrateo = 0.0;
		
		// Obtener el monto de la tarifa del empleado
		if(empleado.getTipoEmp().equals(2)){ // Monto para el empleado sindicalizado
			formatoFecha = fechas.formatoFechas(empleado.getFechaNac(), "yyyy-MM-dd");
			claveFecha = fechas.getCodigoAnio(formatoFecha);
			empleado.setMontoEmp(solAltaDao.getMontoSindicalizado(claveFecha, empleado.getSexo()));
		}else if(empleado.getTipoEmp().equals(1)){ // Monto para el empleado de confianza
			if(empleado.getSexo().equals("M")){
				empleado.setMontoEmp(solAltaDao.getMontoConfianza(1));	// Masculino
			}else{
				empleado.setMontoEmp(solAltaDao.getMontoConfianza(2)); // Femenino
			}
		}else{
			logger.info("  No existe el tipo de empleado");
		}
			
		// Obtener el prorrateo 
		montoProrateo = montoProrrateo.calculaMontoProporcional(empleado.getMontoEmp());
		montoProrateo = Redondeos.roundNum(montoProrateo);
		empleado.setMontoEmpProrateado(montoProrateo);
		return empleado;
	}

	/**
	 *
	 * Genera la cotizacion de un empleado de acuerdo a una fecha
	 * @param empleado: Bean con los datos del empleado
	 * @param mes
	 * @param dia
	 * 
	 */
	public EmpleadoTO getCotizacion(EmpleadoTO empleado, int mes, int dia) {
		Fechas fechas = new Fechas();
		String formatoFecha = "";
		String claveFecha = "";
		MontoProrrateo montoProrrateo = new MontoProrrateo();
		double montoProrateo = 0.0;
		
		// Obtener el monto de la tarifa del empleado
		if(empleado.getTipoEmp().equals(2)){ // Monto para el empleado sindicalizado
			formatoFecha = fechas.formatoFechas(empleado.getFechaNac(), "yyyy-MM-dd");
			claveFecha = fechas.getCodigoAnio(formatoFecha);
			empleado.setMontoEmp(solAltaDao.getMontoSindicalizado(claveFecha, empleado.getSexo()));
		}else if(empleado.getTipoEmp().equals(1)){ // Monto para el empleado de confianza
			if(empleado.getSexo().equals("M")){
				empleado.setMontoEmp(solAltaDao.getMontoConfianza(1));	// Masculino
			}else{
				empleado.setMontoEmp(solAltaDao.getMontoConfianza(2)); // Femenino
			}
		}else{
			logger.info("  No existe el tipo de empleado");
		}
			
		// Obtener el prorrateo 
		montoProrateo = montoProrrateo.calculaMontoProporcional(empleado.getMontoEmp(), mes, dia);
		montoProrateo = Redondeos.roundNum(montoProrateo);
		empleado.setMontoEmpProrateado(montoProrateo);
		return empleado;
	}
	
	/**
	 * 
	 * Inserar solicitud
	 * @param empleadoTO: Bean con datos del empleado
	 * @param solicitudTO: Bean con datos de la solicitud
	 * @param listDep: Lista de Dependientes
	 * @param listCon: Lista de Dependientes Consulmed
	 * 
	 */
	public void setSolicitud(EmpleadoTO empleadoTO, SolicitudTO solicitudTO, 
			List<DependienteTO> listDep, List<ConsulmedTO> listCon, List<ConsulmedTO> listMem){
		EmpleadoTO emp = solConsultaDao.getEmpleadoBD(empleadoTO.getNumEmp());
		if(emp.getNumEmp().equals(0)){
			logger.info("  Insertando Datos Empleado...");
			Integer empleado = solAltaDao.setEmpleado(empleadoTO);
			logger.info("  numEmp: "+empleado);
		}
		
		Integer idSolicitud = 0;
		idSolicitud = solConsultaDao.getSolicitudId(empleadoTO.getNumEmp(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_AUTORIZADA);
		
		if(idSolicitud.equals(0)){
			logger.info("  Insertando Datos Solicitud..."+solicitudTO.getIdTipoMemorial()+" costo: "+solicitudTO.getCostoMemorial());
			solicitudTO.setNumEmp(empleadoTO.getNumEmp());
			solAltaDao.setSolicitud(solicitudTO);
			idSolicitud = solConsultaDao.getSolicitudId(empleadoTO.getNumEmp(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_AUTORIZADA);
			logger.info("  numSol: ["+idSolicitud+"]");
			
			logger.info("  Insertando Datos Dependiente...");
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
			Date fecha = new Date();

			for(DependienteTO dependienteTO : listDep) {
				if((dependienteTO.getIdParentesco()!=null || dependienteTO.getIdParentesco()!=0) && (dependienteTO.getCostoDep()!=null)){
					logger.info("  Fecha: ["+dependienteTO.getFechaNac()+"]");
					try {
						fecha = formatoDelTexto.parse(dependienteTO.getFechaNac());
					}catch (ParseException e) {
						logger.info("  Exception: "+e.getMessage());
					}
					solAltaDao.setDependiente(dependienteTO, idSolicitud, fecha);
				}
			}
			
			logger.info("  Insertando Datos Consulmed...");
			for (ConsulmedTO consulmedTO : listCon) {
				if(!consulmedTO.getNombre().equals("") && consulmedTO.getNombre()!=null){	
					try {
						fecha = formatoDelTexto.parse(consulmedTO.getFechaNac());
					}catch (ParseException e) {
						logger.info("  Exception: "+e.getMessage());
					}
					solAltaDao.setConsulmed(consulmedTO, idSolicitud, fecha);
				}
			}
			
			logger.info("  Insertando Datos Memorial...");
			for (ConsulmedTO consulmedTO : listMem) {
				if(!consulmedTO.getNombre().equals("") && consulmedTO.getNombre()!=null){	
					try {
						fecha = formatoDelTexto.parse(consulmedTO.getFechaNac());
					}catch (ParseException e) {
						logger.info("  Exception: "+e.getMessage());
					}
					logger.info("nombre: "+consulmedTO.getNombre()+" "+consulmedTO.getApellidoP()+" "+consulmedTO.getApellidoM()+" fechaNac: "+consulmedTO.getFechaNac());
					solAltaDao.setMemorial(consulmedTO, idSolicitud, fecha);
				}
				
			}// Fin for			
		}
	}
	
	/**
	 * 
	 * Insertar Dependientes
	 * @param listDep: Lista de Dependientes
	 * @param idSolicitud: Numero de solicitud
	 */
	public void setDependiente(List<DependienteTO> listDep, Integer idSolicitud){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha = new Date();

		for(DependienteTO dependienteTO : listDep) {
			if((dependienteTO.getIdParentesco()!=null || dependienteTO.getIdParentesco()!=0) && (dependienteTO.getCostoDep()!=null)){
				try {
					fecha = formatoDelTexto.parse(dependienteTO.getFechaNac());
				}catch (ParseException e) {
					logger.info("  Exception: "+e.getMessage());
				}
				solAltaDao.setDependiente(dependienteTO, idSolicitud, fecha);
			}
		}
	}
	
	/**
	 * 
	 * Insertar Dependientes Consulmed
	 * @param listDep: Lista de Dependientes
	 * @param idSolicitud: Numero de solicitud
	 */
	public void setConsulmed(List<ConsulmedTO> listCon, Integer idSolicitud){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha = new Date();
		
		for (ConsulmedTO consulmedTO : listCon) {
			if(!consulmedTO.getNombre().equals("") && consulmedTO.getNombre()!=null){	
				try {
					fecha = formatoDelTexto.parse(consulmedTO.getFechaNac());
				}catch (ParseException e) {
					logger.info("  Exception: "+e.getMessage());
				}
				solAltaDao.setConsulmed(consulmedTO, idSolicitud, fecha);
			}
		}
	}
		
	/**
	 * 
	 * Genera una lista inicial de dependientes (10)
	 * 
	 */
	public List<DependienteTO> getListaDependiente(){
		List<DependienteTO> listDep = new ArrayList<DependienteTO>();
		DependienteTO dependienteTO0 = new DependienteTO();
		DependienteTO dependienteTO1 = new DependienteTO();
		DependienteTO dependienteTO2 = new DependienteTO();
		DependienteTO dependienteTO3 = new DependienteTO();
		DependienteTO dependienteTO4 = new DependienteTO();
		DependienteTO dependienteTO5 = new DependienteTO();
		DependienteTO dependienteTO6 = new DependienteTO();
		DependienteTO dependienteTO7 = new DependienteTO();
		DependienteTO dependienteTO8 = new DependienteTO();
		DependienteTO dependienteTO9 = new DependienteTO();
		listDep.add(dependienteTO0);
		listDep.add(dependienteTO1);
		listDep.add(dependienteTO2);
		listDep.add(dependienteTO3);
		listDep.add(dependienteTO4);
		listDep.add(dependienteTO5);
		listDep.add(dependienteTO6);
		listDep.add(dependienteTO7);
		listDep.add(dependienteTO8);
		listDep.add(dependienteTO9);
		return listDep;
	}
	
	/**
	 * 
	 * Genera una lista inicial de dependientes consulmed (4)
	 * 
	 */
	public List<ConsulmedTO> getListaConsulmed(){
		List<ConsulmedTO> listCon = new ArrayList<ConsulmedTO>();
		ConsulmedTO dependMadre = new ConsulmedTO();
		ConsulmedTO dependPadre = new ConsulmedTO();
		ConsulmedTO dependSuegra = new ConsulmedTO();
		ConsulmedTO dependSuegro = new ConsulmedTO();
		
		dependMadre.setIdParentesco(2);
		dependMadre.setParentesco("MADRE");
		dependMadre.setIdSexo(2);
		dependMadre.setSexo("F");
		dependMadre.setCostoConsul(0.00);
		
		dependPadre.setIdParentesco(1);
		dependPadre.setParentesco("PADRE");
		dependPadre.setIdSexo(1);
		dependPadre.setSexo("M");
		dependPadre.setCostoConsul(0.00);
		
		dependSuegra.setIdParentesco(8);
		dependSuegra.setParentesco("SUEGRA");
		dependSuegra.setIdSexo(2);
		dependSuegra.setSexo("F");
		dependSuegra.setCostoConsul(0.00);
		
		dependSuegro.setIdParentesco(7);
		dependSuegro.setParentesco("SUEGRO");
		dependSuegro.setIdSexo(1);
		dependSuegro.setSexo("M");
		dependSuegro.setCostoConsul(0.00);
		
		listCon.add(dependMadre);
		listCon.add(dependPadre);
		listCon.add(dependSuegra);
		listCon.add(dependSuegro);
		return listCon;
	}
	
		
	@Autowired
	public void setSolAltaDao(SolAltaDao solAltaDao) {
		this.solAltaDao = solAltaDao;
	}

	@Autowired
	public void setEmpleadoBusiness(EmpleadoBusiness empleadoBusiness) {
		this.empleadoBusiness = empleadoBusiness;
	}

	@Autowired
	public void setSolConsultaDao(SolConsultaDao solConsultaDao) {
		this.solConsultaDao = solConsultaDao;
	}

}