package mx.com.orbita.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.business.EmpleadoBusiness;
import mx.com.orbita.dao.DatosFiscalesDao;
import mx.com.orbita.service.DatosFiscalesService;
import mx.com.orbita.to.DatFiscalesTO;
import mx.com.orbita.to.EmpleadoTO;

@Component
public class DatosFiscalesServiceImpl implements DatosFiscalesService{
	
	private static final Logger logger = Logger.getLogger(DatosFiscalesServiceImpl.class);
	
	private DatosFiscalesDao datosFiscalesDao;
	private EmpleadoBusiness empleadoBusiness;

	public DatFiscalesTO getDatosFiscales(Integer numEmp) {
		logger.info("DatosFiscalesServiceImpl.getDatosFiscales");
		DatFiscalesTO datFiscalesTO = new DatFiscalesTO(); 
		datFiscalesTO = datosFiscalesDao.getDatosFiscales(numEmp);
		logger.info("  Consulta del Empleado del Servicio Web "+numEmp);
		EmpleadoTO empleado = empleadoBusiness.consultaEmpleado(numEmp);
		logger.info("  Respuesta del Empleado del Servicio Web "+empleado.getNumEmp());
		datFiscalesTO.setNum_empleado(empleado.getNumEmp());
		datFiscalesTO.setNombre(empleado.getNombre()+" "+empleado.getApellidoP()+" "+empleado.getApellidoM());
		datFiscalesTO.setRfc(empleado.getRfc());
		return datFiscalesTO;
	}

	public void setDatosFiscales(DatFiscalesTO datos) {
		logger.info("DatosFiscalesServiceImpl.setDatosFiscales");
		datosFiscalesDao.setDatosFiscales(datos);
	}

	@Autowired
	public void setDatosFiscalesDao(DatosFiscalesDao datosFiscalesDao) {
		this.datosFiscalesDao = datosFiscalesDao;
	}
	@Autowired
	public void setEmpleadoBusiness(EmpleadoBusiness empleadoBusiness) {
		this.empleadoBusiness = empleadoBusiness;
	}
	

}
