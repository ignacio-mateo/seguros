package mx.com.orbita.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.ReembolsoDao;
import mx.com.orbita.service.ReembolsoService;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.ReembolsoConceptoTO;
import mx.com.orbita.to.ReembolsoRelacionTO;

@Component
public class ReembolsoServiceImpl implements ReembolsoService{

	private static final Logger logger = Logger.getLogger(ReembolsoServiceImpl.class);
	
	private ReembolsoDao reembolsoDao;
	
	public ReembolsoRelacionTO getReembolso(Integer numEmp, Integer idReembolso){
		logger.info("ReembolsoServiceImpl.getReembolso");
		return reembolsoDao.getReembolso(numEmp,idReembolso);
	}
	
	public List<Integer> getReembolsoListID(Integer numEmp){
		logger.info("ReembolsoServiceImpl.getReembolsoListID");
		return reembolsoDao.getReembolsoListID(numEmp);
	}
	
	public List<ReembolsoConceptoTO> getReembolsoConceptos(Integer idReembolso){
		logger.info("ReembolsoServiceImpl.getReembolsoConceptos");
		return reembolsoDao.getReembolsoConceptos(idReembolso);
	}
	
	public void setReembolso(List<ReembolsoConceptoTO> lista,ReembolsoRelacionTO reembolsoRelacionTO, Integer numEmp){
		logger.info("ReembolsoServiceImpl.setReembolso");
		Integer idReembolso = reembolsoDao.setReembolso(reembolsoRelacionTO, numEmp);
		logger.info("  Insertar reembolso: "+idReembolso);
				
		for (ReembolsoConceptoTO reembolsoConceptoTO : lista) {
			if(reembolsoConceptoTO.getImporte()!=null && !reembolsoConceptoTO.getImporte().equals("")){
				reembolsoConceptoTO.setIdReembolso(idReembolso);
				logger.info("  Id: "+reembolsoConceptoTO.getIdReembolso());
				logger.info("  NoDoc: "+reembolsoConceptoTO.getNoDocumento());
				logger.info("  Nombre: "+reembolsoConceptoTO.getNombreExpediente());
				logger.info("  Concepto: "+reembolsoConceptoTO.getConcepto());
				logger.info("  Importe: "+reembolsoConceptoTO.getImporte());
				reembolsoDao.setReembolsoConcepto(reembolsoConceptoTO);
			}
		}		
	}
	
	public List<DependienteTO> getDependientes(Integer numEmp, Integer tipoSol, Integer estatusSol){
		logger.info("ReembolsoServiceImpl.getDependientes");
		return reembolsoDao.getDependientes(numEmp, tipoSol, estatusSol);
	}

	public List<ReembolsoConceptoTO> getListaConceptosReembolsos(){
		logger.info("ReembolsoServiceImpl.getListaConceptosReembolsos");
		List<ReembolsoConceptoTO> listConceptos = new ArrayList<ReembolsoConceptoTO>();
		ReembolsoConceptoTO reembolsoConceptoTO1 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO2 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO3 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO4 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO5 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO6 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO7 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO8 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO9 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO10 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO11 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO12 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO13 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO14 = new ReembolsoConceptoTO();
		ReembolsoConceptoTO reembolsoConceptoTO15 = new ReembolsoConceptoTO();
		listConceptos.add(reembolsoConceptoTO1);
		listConceptos.add(reembolsoConceptoTO2);
		listConceptos.add(reembolsoConceptoTO3);
		listConceptos.add(reembolsoConceptoTO4);
		listConceptos.add(reembolsoConceptoTO5);
		listConceptos.add(reembolsoConceptoTO6);
		listConceptos.add(reembolsoConceptoTO7);
		listConceptos.add(reembolsoConceptoTO9);
		listConceptos.add(reembolsoConceptoTO8);
		listConceptos.add(reembolsoConceptoTO10);
		listConceptos.add(reembolsoConceptoTO11);
		listConceptos.add(reembolsoConceptoTO12);
		listConceptos.add(reembolsoConceptoTO13);
		listConceptos.add(reembolsoConceptoTO14);
		listConceptos.add(reembolsoConceptoTO15);
		return listConceptos;
	}

	@Autowired
	public void setReembolsoDao(ReembolsoDao reembolsoDao) {
		this.reembolsoDao = reembolsoDao;
	}
	
}
