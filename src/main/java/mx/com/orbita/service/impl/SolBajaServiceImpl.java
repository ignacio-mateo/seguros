package mx.com.orbita.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SolBajaDao;
import mx.com.orbita.service.SolBajaService;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.util.Codigo;

@Component
public class SolBajaServiceImpl implements SolBajaService{
	
	private static final Logger logger = Logger.getLogger(SolBajaServiceImpl.class);
	
	private SolBajaDao solBajaDao;
		
	public void setSolicitud(Integer numEmp, Integer sol) {
		logger.info("SolBajaServiceImpl.setSolicitud");
		solBajaDao.setSolicitud(numEmp,Codigo.EST_SOL_BAJA_EN_PROCESO,sol);
		
		// Dependientes
		List<Integer> dependientes = solBajaDao.getIdDependiente(Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, numEmp);
		for (Integer integer : dependientes) {
			solBajaDao.setDependiente(integer, Codigo.EST_SOL_BAJA_EN_PROCESO);
		}
		
		// Consulmed
		List<Integer> consulmed = solBajaDao.getIdConsulmed(Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, numEmp);
		for (Integer integer : consulmed) {
			solBajaDao.setConsulmed(integer, Codigo.EST_SOL_BAJA_EN_PROCESO);
		}
	}

	public void setDependiente(List<DependienteTO> lista)  {
		logger.info("SolBajaServiceImpl.setDependiente");
		logger.info("  Parametros del Service [ size lista: "+lista.size()+"]");
		for (DependienteTO dependienteTO : lista) {
			logger.info("  Id.Dependiente: "+dependienteTO.getIdDependiente());
			if(dependienteTO.getCheck()!=null){
				logger.info("  Check: "+dependienteTO.getCheck());
				solBajaDao.setDependiente(dependienteTO.getIdDependiente(),Codigo.EST_SOL_BAJA_EN_PROCESO);
			}
		}		
	}

	public void setConsulmed(List<ConsulmedTO> lista) {
		logger.info("SolBajaServiceImpl.setConsulmed");
		logger.info("  Parametros del Service [ size lista: "+lista.size()+"]");
		for (ConsulmedTO consulmedTO : lista) {
			logger.info("  Id.Dependiente: "+consulmedTO.getIdConsulmed());
			if(consulmedTO.getCheck()!=null){
				logger.info("  Check: "+consulmedTO.getCheck());
				solBajaDao.setConsulmed(consulmedTO.getIdConsulmed(),Codigo.EST_SOL_BAJA_EN_PROCESO);
			}
		}
	}
	
	@Autowired
	public void setSolBajaDao(SolBajaDao solBajaDao) {
		this.solBajaDao = solBajaDao;
	}
	
	
}
