package mx.com.orbita.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AdminSolicitudDao;
import mx.com.orbita.service.AdminSolicitudService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudAdminTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;

@Component
public class AdminSolicitudServiceImpl implements AdminSolicitudService {
	
	private static final Logger logger = Logger.getLogger(AdminSolicitudServiceImpl.class);
	
	private AdminSolicitudDao adminSolicitudDao;
	private SolAltaService solAltaService;
	private SolConsultaService solConsultaService;

	public Integer getSolicitudExiste(Integer numEmp){
		return adminSolicitudDao.getSolicitudExiste(numEmp);
	}	
	
	public SolicitudAdminTO getSolicitud(Integer numEmp) {
		return adminSolicitudDao.getSolicitud(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, 
				Codigo.ID_PARENTESCO_ESPOSO, Codigo.ID_PARENTESCO_ESPOSA, Codigo.EST_SOL_ALTA_RECHAZADA);
	}
	
	public List<SolicitudAdminTO> getSolicitudes(Integer numEmp) {
		return adminSolicitudDao.getSolicitudes(numEmp);
	}

	public List<SolicitudAdminTO> getDependiente(Integer numEmp) {
		return adminSolicitudDao.getDependiente(numEmp);
	}
	
	public List<SolicitudAdminTO> getConsulmed(Integer numEmp) {
		return adminSolicitudDao.getConsulmed(numEmp);
	}
	
	public List<SolicitudAdminTO> getMemorial(Integer numEmp) {
		return adminSolicitudDao.getMemorial(numEmp);
	}

	public void setSolicitud(SolicitudAdminTO solicitudAdminTO) {
		double montoDep = 0.0;
		double montoDepTot = 0.0;
		double montoAmp = 0.0;
		double montoAmpInd = 0.0;
		double montoTotal = 0.0;

		// Actualizar fecha de Alta
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaAlta = new Date();
		try {
			fechaAlta = formatoDelTexto.parse(solicitudAdminTO.getFechaAlta());
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage()); 
		}
		
		// Solicitud de Alta y Estatus en proceso de alta
		if(solicitudAdminTO.getIdTipoSol()==1 && solicitudAdminTO.getIdEstatulSol()==1){
			solicitudAdminTO.setFechaAltaDate(null);
			adminSolicitudDao.setSolicitud(solicitudAdminTO);
		}else if(solicitudAdminTO.getIdTipoSol()==1 && solicitudAdminTO.getIdEstatulSol()==2){ // Solicitud de Alta y Estatus Alta Autorizada
			
			// Datos del Empleado
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(solicitudAdminTO.getNumEmpSolicitado());
			
			// Costo del Empleado
			empleadoTO = solAltaService.getCotizacion(empleadoTO, solicitudAdminTO.getMes(), solicitudAdminTO.getDia());
			
			// Lista de Dependientes
			List<DependienteTO> listaDep = adminSolicitudDao.getDependiente(solicitudAdminTO.getNumEmpSolicitado(),
					Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
			
			// Calcular el monto de ampliacion
			if(solicitudAdminTO.getIdCobertura()!=0){
				montoAmp = solAltaService.getMontoAmpliacion(solicitudAdminTO.getIdCobertura(),listaDep.size(),
						solicitudAdminTO.getMes(), solicitudAdminTO.getDia());
				montoAmpInd = solAltaService.getMontoAmpliacionIndividual(solicitudAdminTO.getIdCobertura(),
						Codigo.DEP_INDIVIDUAL_AMPLIACION, solicitudAdminTO.getMes(), solicitudAdminTO.getDia());
			}
			
			// Lista de Dependientes 
			if(listaDep.size()>0){
				for (DependienteTO dependienteTO : listaDep) {
					montoDep = solAltaService.getMontoDependiente(empleadoTO.getTipoEmp(),
							dependienteTO.getIdParentesco(),dependienteTO.getFechaNac(), 
							dependienteTO.getSexo(),solicitudAdminTO.getMes(),
							solicitudAdminTO.getDia());
					solicitudAdminTO.setIdDependiente(dependienteTO.getIdDependiente());
					solicitudAdminTO.setCosto(montoDep);
					solicitudAdminTO.setCostoAmpliacionInd(montoAmpInd);
					solicitudAdminTO.setFechaAltaDate(fechaAlta);
					// Actualizar montos y fecha de autorizacion para dependientes
					adminSolicitudDao.setDependienteCosto(solicitudAdminTO);
					logger.info("  montoDep: ["+montoDep+"]");
					montoDepTot = montoDepTot + montoDep;
				}
			}
						
			// Monto Total
			montoTotal = empleadoTO.getMontoEmpProrateado() + montoAmp + montoDepTot;
			
			logger.info("  montoEmp: ["+empleadoTO.getMontoEmpProrateado()
							+"] listaDep: ["+listaDep.size()
							+"] montoDepTot: ["+montoDepTot
							+"] montoAmpInd: ["+montoAmpInd
							+"] montoAmp: ["+montoAmp
							+"] montoTotal: ["+montoTotal+"]");
			
			// Actualizar montos en la solicitud de empleado
			solicitudAdminTO.setCostoEmp(empleadoTO.getMontoEmpProrateado());
			solicitudAdminTO.setCostoTotalDep(montoDepTot);
			solicitudAdminTO.setCostoAmpliacion(montoAmp);
			solicitudAdminTO.setCostoAmpliacionInd(montoAmpInd);
			solicitudAdminTO.setCostoTotal(montoTotal);
			adminSolicitudDao.setSolicitudCosto(solicitudAdminTO);
		}
	}

	public void setConsulmed(SolicitudAdminTO solicitudAdminTO) {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaNac = new Date();
		Date fechaAlta = new Date();
		try {
			fechaNac = formatoDelTexto.parse(solicitudAdminTO.getFechaNac());
			fechaAlta = formatoDelTexto.parse(solicitudAdminTO.getFechaAlta());
		} catch (ParseException e) {
			logger.info(" e: "+e.getMessage());
		}
		solicitudAdminTO.setFechaNacDate(fechaNac);
		solicitudAdminTO.setFechaAltaDate(fechaAlta);
		adminSolicitudDao.setCondulmed(solicitudAdminTO);
	}
	
	public void setDependiente(SolicitudAdminTO solicitudAdminTO) {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaAltaDep = new Date();
		
		try {
			solicitudAdminTO.setIdParentesco(solicitudAdminTO.getIdParentescoNvo());
			solicitudAdminTO.setFechaNacDate(formatoDelTexto.parse(solicitudAdminTO.getFechaNacNvo()));
			fechaAltaDep = formatoDelTexto.parse(solicitudAdminTO.getFechaAlta());	
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage()); 
		}
		
		if(solicitudAdminTO.getIdEstatulSol()==1){ 			// ESTATUS: EN PROCESO DE ALTA
			logger.info("  Estatus: [ En proceso de alta ]");
			solicitudAdminTO.setIdTipoSol(Codigo.TIPO_SOL_ALTA);
			solicitudAdminTO.setFechaAltaDate(null);
			adminSolicitudDao.setDependiente(solicitudAdminTO); // Actualizar dependiente 
		}else if(solicitudAdminTO.getIdEstatulSol()==2){ 	// ESTATUS: ALTA AUTORIZADA
			logger.info("  Estatus: [ Alta Autorizada ]");
			solicitudAdminTO.setIdTipoSol(Codigo.TIPO_SOL_ALTA);
			if(solicitudAdminTO.getIdEstatulSolNvo()==2){			// ESTATUS NUEVO: ALTA AUTORIZADA
				logger.info("  Estatus Nuevo: [ Alta Autorizada ]");
				estatusAutorizada(solicitudAdminTO, fechaAltaDep);
				solicitudAdminTO.setFechaAltaDate(fechaAltaDep);
			}else if(solicitudAdminTO.getIdEstatulSolNvo()==3){		// ESTATUS NUEVO: SE RECHAZA ALTA
				logger.info("  Estatus Nuevo: [ Se rechaza alta ]");
				estatusRechazo(solicitudAdminTO);
			}
			adminSolicitudDao.setDependiente(solicitudAdminTO); // Actualizar dependiente
		}else if(solicitudAdminTO.getIdEstatulSol()==5){	// ESTATUS: BAJA - ESTATUS NUEVO: EN PROCESO DE BAJA
			logger.info("  Estatus: [ Baja ]");
			estatusBaja(solicitudAdminTO);
		}
	}

	public void estatusAutorizada(SolicitudAdminTO solicitudAdminTO, Date fechaAltaDep){
			Calendar cal= Calendar.getInstance(); 
			int anio= cal.get(Calendar.YEAR); 
			int diaDep = 0;
			int mesDep = 0;
			double montoDep = 0.0;
			double montoDepAmpActual = 0.0;
			double montoDepAmpTot = 0.0;
			double montoDepEditado = 0.0;
			double montoAmpDepEditado = 0.0;
			
			// Fecha de Alta
			Integer dia = Integer.parseInt(solicitudAdminTO.getFechaAlta().substring(0,2));
			Integer mes = Integer.parseInt(solicitudAdminTO.getFechaAlta().substring(3,5));
			
			// Fecha de Nacimiento yyyy-MM-dd
			logger.info("  fechaNacNuevo: ["+solicitudAdminTO.getFechaNacNvo()
					+"] fechaAltaActual: ["+solicitudAdminTO.getFechaActual()
					+"] fechaAltaNuevo: ["+solicitudAdminTO.getFechaAlta()+"]");
			String diaNac = solicitudAdminTO.getFechaNacNvo().substring(0,2);
			String mesNac = solicitudAdminTO.getFechaNacNvo().substring(3,5);
			String anioNac = solicitudAdminTO.getFechaNacNvo().substring(6,10);
			String fecha = anioNac+"-"+mesNac+"-"+diaNac;
			
			// Obtener el costo del empleado 
			SolicitudTO solicitudTO = adminSolicitudDao.getSolicitud(solicitudAdminTO.getNumEmpSolicitado(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
						
			// Obtener el costo de los dependientes menos el editado y la sumatoria
			List<DependienteTO> listaDep = adminSolicitudDao.getDependienteEditado(solicitudAdminTO.getNumEmpSolicitado(),Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, solicitudAdminTO.getIdDependiente());
			if(listaDep.size()>0){
				for (DependienteTO dependienteTO : listaDep) {
					montoDep = montoDep + dependienteTO.getCostoDep();
					logger.info("  montoDepEditado: ["+dependienteTO.getCostoDep()+"]");
				}
				logger.info("  montoDep: ["+montoDep+"] listaDep: ["+listaDep.size()+"]");
			}
			
			// Calcular el costo de ampliacion individual si existe y la sumatoria
			if(solicitudTO.getIdTipoCobertura()!=0){
				if(listaDep.size()>0){
					for (DependienteTO dependienteTO : listaDep) {
						
						/**
						 * Calcular el dia y mes desde donde se va a calcular el prorrateado,
						 * Si se dio de alta en el periodo actual se calcula en base a la fecha de alta
						 * Si se dio de alta en otro perido de calcula el periodo completo 
						 */
						if(anio==Codigo.PERIODO_INICIAL){ 
							logger.info("  [Periodo Junio A Diciembre] ");
							if(dependienteTO.getAnioAut().equals(anio) && dependienteTO.getMesAut() > 5){
								logger.info("  Alta en el periodo actual");
								diaDep = dependienteTO.getDiaAut();
								mesDep = dependienteTO.getMesAut();
							}else{
								logger.info("  Alta en otro periodo");
								diaDep = Codigo.DIA_RENOVACION;
								mesDep = Codigo.MES_RENOVACION;
							}									
						}else if(anio>Codigo.PERIODO_INICIAL){
							logger.info("  [Periodo Enero A Junio]");
							if(dependienteTO.getAnioAut().equals(anio)){
								logger.info("  Alta en el periodo actual");
								diaDep = dependienteTO.getDiaAut();
								mesDep = dependienteTO.getMesAut();
							}else{
								logger.info("  Alta en otro periodo");
								diaDep = Codigo.DIA_RENOVACION;
								mesDep = Codigo.MES_RENOVACION;
							}		
						}			
						
						montoDepAmpActual = solAltaService.getMontoAmpliacionIndividual(solicitudTO.getIdTipoCobertura(),
								Codigo.DEP_INDIVIDUAL_AMPLIACION, mesDep,diaDep);
						montoDepAmpTot = montoDepAmpTot + montoDepAmpActual;
					}
					logger.info("  montoTotAmpDep: ["+montoDepAmpTot+"]");
				}
			}
							
			// Calcular el costo del Dependiente Editado
			// Validar parentesco y fecha de nacimiento
			// Calcular monto proporcional con la fecha de alta
			if(!solicitudAdminTO.getFechaAlta().equals(solicitudAdminTO.getFechaActual())){
				logger.info("  [Fecha de Alta Modificada]");
				if(solicitudAdminTO.getIdParentesco()!=solicitudAdminTO.getIdParentescoNvo() 
						&& solicitudAdminTO.getFechaNac()!=solicitudAdminTO.getFechaNacNvo()){
					// El parentesco y la fecha son diferentes
					montoDepEditado = solAltaService.getMontoDependiente(solicitudAdminTO.getIdTipoEmpleado(),
							solicitudAdminTO.getIdParentescoNvo(), fecha, solicitudAdminTO.getSexo(), mes, dia);
				}else if(solicitudAdminTO.getIdParentesco()!=solicitudAdminTO.getIdParentescoNvo() 
						&& solicitudAdminTO.getFechaNac()==solicitudAdminTO.getFechaNacNvo()){
					// Solo es diferente el parentesco
					montoDepEditado = solAltaService.getMontoDependiente(solicitudAdminTO.getIdTipoEmpleado(),
							solicitudAdminTO.getIdParentescoNvo(), fecha, solicitudAdminTO.getSexo(), mes, dia);
				}else if(solicitudAdminTO.getIdParentesco()==solicitudAdminTO.getIdParentescoNvo() 
						&& solicitudAdminTO.getFechaNac()!=solicitudAdminTO.getFechaNacNvo()){
					// Solo es diferentes la fecha
					montoDepEditado = solAltaService.getMontoDependiente(solicitudAdminTO.getIdTipoEmpleado(),
							solicitudAdminTO.getIdParentesco(), fecha, solicitudAdminTO.getSexo(), mes, dia);
				}
				// Calcular el costo de ampliacion individual si existe 
				if(solicitudTO.getIdTipoCobertura()!=0){
					montoAmpDepEditado = solAltaService.getMontoAmpliacionIndividual(solicitudTO.getIdTipoCobertura(),
							Codigo.DEP_INDIVIDUAL_AMPLIACION,mes,dia);
					
				}				
			}else{
				logger.info("  [Fecha de Alta Sin Modificar]");
				DependienteTO dependiente = adminSolicitudDao.getDepCostos(solicitudAdminTO.getIdDependiente());
				montoDepEditado = dependiente.getCostoDep();
				montoAmpDepEditado = dependiente.getCostoAmpDep();
				
			}
			logger.info("  montoDepEditado: ["+montoDepEditado+"]");
			logger.info("  montoAmpDepEditado: ["+montoAmpDepEditado+"]");
			
										
			// Obtener el Costo Total de los Dependientes
			double montoDepTotal = montoDep + montoDepEditado;
			
			// Obtener el Costo Total de la Ampliacion
			double montoAmpTotal = solicitudTO.getCostoAmpliacion() + montoDepAmpTot + montoAmpDepEditado;
			
			// Obtener el Costo Total
			double montoTotal = solicitudTO.getCostoEmpleado()+ montoDepTotal + montoAmpTotal;
			
			// Actualizar el Costo del Dependiente
			solicitudAdminTO.setCosto(montoDepEditado);
			solicitudAdminTO.setCostoAmpliacionInd(montoAmpDepEditado);
			solicitudAdminTO.setFechaAltaDate(fechaAltaDep);
			adminSolicitudDao.setDependienteCosto(solicitudAdminTO);
			
			// Actualizar los costos en la Solicitud
			solicitudAdminTO.setCostoTotalDep(montoDepTotal);
			solicitudAdminTO.setCostoAmpliacion(montoAmpTotal);
			solicitudAdminTO.setCostoTotal(montoTotal);
			adminSolicitudDao.setSolicitudCostoxDep(solicitudAdminTO);
				
			logger.info("  CALCULOS FINALES ");
			logger.info("  montoEmp: ["+solicitudTO.getCostoEmpleado()
							+"] montoTot: ["+montoTotal
							+"] montoTotDep: ["+montoDepTotal
							+"] montoTotAmpl: ["+montoAmpTotal+"]");
	}
	
	
	public void estatusRechazo(SolicitudAdminTO solicitudAdminTO){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaAltaSol = new Date();
		double montoAmp = 0.0;
		double montoTot = 0.0;
		// Obtener el costo del empleado 
		SolicitudTO solicitudTO = adminSolicitudDao.getSolicitud(solicitudAdminTO.getNumEmpSolicitado(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
		try {
			fechaAltaSol = formatoDelTexto.parse(solicitudTO.getFechaAutorizacion());		
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage()); 
		}
		solicitudAdminTO.setFechaAltaDate(fechaAltaSol);
		
		Integer dia = Integer.parseInt(solicitudAdminTO.getFechaAlta().substring(0, 2));
		Integer mes = Integer.parseInt(solicitudAdminTO.getFechaAlta().substring(3, 5));
		
		Integer numDep = adminSolicitudDao.getDependienteTotal(solicitudAdminTO.getIdSolicitud(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, solicitudAdminTO.getIdDependiente());
		numDep = numDep - 1;
		
		// Actualizar el monto de ampliacion
		if(solicitudAdminTO.getIdCobertura()!=0){
			montoAmp = solAltaService.getMontoAmpliacion(solicitudAdminTO.getIdCobertura(), numDep, mes, dia);
		}
		
		// Actualizar el monto total restando el monto del dependiente	
		montoTot = (solicitudAdminTO.getCostoTotal() + montoAmp) - solicitudAdminTO.getCosto();
		
		// Monto de los dependientes actuales
		double montoDep = adminSolicitudDao.getDependienteCosto(solicitudAdminTO.getIdSolicitud(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, solicitudAdminTO.getIdDependiente()); 
		solicitudAdminTO.setCosto(montoDep);
		solicitudAdminTO.setCostoTotal(montoTot);
		adminSolicitudDao.setSolicitudCosto(solicitudAdminTO);
	}
	
	
	public void estatusBaja(SolicitudAdminTO solicitudAdminTO){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaAltaSol = new Date();
		double montoAmp = 0.0;
		double montoDepActual = 0.0;
		double montoTot = 0.0;
		// Obtener el costo del empleado 
		SolicitudTO solicitudTO = adminSolicitudDao.getSolicitud(solicitudAdminTO.getNumEmpSolicitado(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
		try {
			fechaAltaSol = formatoDelTexto.parse(solicitudTO.getFechaAutorizacion());		
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage()); 
		}
		solicitudAdminTO.setFechaAltaDate(fechaAltaSol);
		
		Integer dia = Integer.parseInt(solicitudAdminTO.getFechaAlta().substring(0, 2));
		Integer mes = Integer.parseInt(solicitudAdminTO.getFechaAlta().substring(3, 5));
		
		// En proceso de baja 
		solicitudAdminTO.setIdTipoSol(Codigo.TIPO_SOL_BAJA);
			
		Integer numDep = adminSolicitudDao.getDependienteTotal(solicitudAdminTO.getIdSolicitud(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, solicitudAdminTO.getIdDependiente());
		numDep = numDep +1;
		
		// Calcular el monto de ampliacion
		if(solicitudAdminTO.getIdCobertura()!=0){
			montoAmp = solAltaService.getMontoAmpliacion(solicitudAdminTO.getIdCobertura(), numDep, mes, dia);
		}	
		
		// Monto de los dependientes actuales
		montoDepActual = adminSolicitudDao.getDependienteCosto(solicitudAdminTO.getIdSolicitud(), Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA, solicitudAdminTO.getIdDependiente());
		montoDepActual = montoDepActual + solicitudAdminTO.getCosto(); 
		
		// Actualizar el monto total (suma)
		montoTot = solicitudAdminTO.getCostoEmp() + montoDepActual + montoAmp;
					
		solicitudAdminTO.setCosto(montoDepActual);
		solicitudAdminTO.setCostoTotal(montoTot);
		adminSolicitudDao.setSolicitudCosto(solicitudAdminTO);
		adminSolicitudDao.setDependiente(solicitudAdminTO);
	}
	
	
	@Autowired
	public void setAdminSolicitudDao(AdminSolicitudDao adminSolicitudDao) {
		this.adminSolicitudDao = adminSolicitudDao;
	}

	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}

	@Autowired
	public void setSolConsultaService(SolConsultaService solConsultaService) {
		this.solConsultaService = solConsultaService;
	}
	
}