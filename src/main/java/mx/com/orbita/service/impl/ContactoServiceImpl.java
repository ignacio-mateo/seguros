package mx.com.orbita.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.ContactoDao;
import mx.com.orbita.service.ContactoService;
import mx.com.orbita.to.ContactoTO;

@Component
public class ContactoServiceImpl implements ContactoService{

	private static final Logger logger = Logger.getLogger(ContactoServiceImpl.class);
	
	private ContactoDao contactoDao;
	
	public List<ContactoTO> getContacto() {
		logger.info("ContactoServiceImpl.getContacto");
		List<ContactoTO> contactos = new ArrayList<ContactoTO>();
		contactos = contactoDao.getContacto();
		logger.info("  ListaContactos: "+contactos.size());
		return contactos;
	}

	@Autowired
	public void setContactoDao(ContactoDao contactoDao) {
		this.contactoDao = contactoDao;
	}

}
