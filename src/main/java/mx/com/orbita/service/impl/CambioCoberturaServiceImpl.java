package mx.com.orbita.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CambioCoberturaDao;
import mx.com.orbita.service.CambioCoberturaService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.to.CoberturaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;

@Component
public class CambioCoberturaServiceImpl implements CambioCoberturaService {

	private static final Logger logger = Logger.getLogger(CambioCoberturaServiceImpl.class);

	private CambioCoberturaDao cambioCoberturaDao;
	private SolAltaService solAltaService;

	public EmpleadoTO getEmpleado(Integer numEmp){
		return cambioCoberturaDao.getEmpleado(numEmp);
	}

	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		return cambioCoberturaDao.getSolicitud(numEmp, tipoSol, estatusSol);
	}

	public double getCambioCobertura(Integer tipoAmpl, Integer numDep, Integer mes, Integer dia){
		return solAltaService.getMontoAmpliacion(tipoAmpl, numDep, mes, dia);
	}

	public double getCambioCoberturaIndiv(Integer tipoAmpl, Integer numDep, Integer mes, Integer dia){
		return solAltaService.getMontoAmpliacionIndividual(tipoAmpl, numDep, mes, dia);
	}

	public Integer getDependienteProceso(Integer numEmp){
		return cambioCoberturaDao.getDependienteProceso(numEmp, Codigo.EST_SOL_ALTA_EN_PROCESO);
	}

	public List<DependienteTO> getDependiente(Integer numEmp){
		return cambioCoberturaDao.getDependiente(numEmp, Codigo.TIPO_SOL_ALTA,
													Codigo.EST_SOL_ALTA_AUTORIZADA);
	}

	public void setCambioCobertura(CoberturaTO coberturaTO) {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha = new Date();
		try {
			fecha = formatoDelTexto.parse(coberturaTO.getFechaAutorizacion());
			coberturaTO.setFechaAct(fecha);
		} catch (ParseException e) {
			logger.info("  Exception: "+e.getMessage());
		}

		cambioCoberturaDao.setCoberturaHistorico(coberturaTO);
		cambioCoberturaDao.setCambioCobertura(coberturaTO);
		coberturaTO.setIdTipoSolicitud(Codigo.ID_OPERACION_ALTA);
		coberturaTO.setIdEstatusSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
		cambioCoberturaDao.setCambioCoberturaDep(coberturaTO);
	}


	@Autowired
	public void setCambioCoberturaDao(CambioCoberturaDao cambioCoberturaDao) {
		this.cambioCoberturaDao = cambioCoberturaDao;
	}

	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}

}
