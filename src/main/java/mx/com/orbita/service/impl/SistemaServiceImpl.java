package mx.com.orbita.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SistemaDao;
import mx.com.orbita.service.SistemaService;
import mx.com.orbita.util.Codigo;

@Component
public class SistemaServiceImpl implements SistemaService {

	private static final Logger logger = Logger.getLogger(SistemaServiceImpl.class);
	private SistemaDao sistemaDao;
	
	
	public Integer existeSolicitud(Integer numEmp) {
		logger.info("SistemaServiceImpl.existeSolicitud");
		return sistemaDao.existeSolicitud(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_RECHAZADA);
	}
	
	public Integer existeSolicitudBaja(Integer numEmp) {
		logger.info("SistemaServiceImpl.existeSolicitudBaja");
		return sistemaDao.existeSolicitudBaja(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_BAJA_EN_PROCESO, Codigo.TIPO_SOL_BAJA, Codigo.EST_SOL_BAJA_AUTORIZADA, Codigo.TIPO_SOL_BAJA, Codigo.EST_SOL_BAJA);
	}
	
	public Integer existeSolicitudBajaDep(Integer numEmp) {
		logger.info("SistemaServiceImpl.existeSolicitudBajaDep");
		return sistemaDao.existeSolicitudBajaDep(numEmp, 
													Codigo.TIPO_SOL_ALTA, 
													Codigo.EST_SOL_ALTA_AUTORIZADA, 
													Codigo.EST_SOL_BAJA_EN_PROCESO, 
													Codigo.TIPO_SOL_BAJA, 
													Codigo.EST_SOL_BAJA, 
													Codigo.EST_SOL_BAJA_AUTORIZADA);
	}
	
	public Integer existeSolicitudAlta(Integer numEmp) {
		logger.info("SistemaServiceImpl.existeSolicitudAlta");
		return sistemaDao.existeSolicitudAlta(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_AUTORIZADA);
	}
	
	public Integer existeSolicitudAut(Integer numEmp) {
		logger.info("SistemaServiceImpl.existeSolicitudAut");
		return sistemaDao.existeSolicitudAut(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
	}
	
	public Integer getPerfilAdmin(Integer numEmp){
		logger.info("SistemaServiceImpl.getPerfilAdmin");
		return sistemaDao.getPerfilAdmin(numEmp);
	}

	@Autowired
	public void setSistemaDao(SistemaDao sistemaDao) {
		this.sistemaDao = sistemaDao;
	}

}
