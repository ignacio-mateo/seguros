package mx.com.orbita.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AutorizarSolDao;
import mx.com.orbita.service.AutorizarSolService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.to.AutorizacionTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.util.Codigo;

@Component
public class AutorizarSolServiceImpl implements AutorizarSolService {

	public static final Logger logger = Logger.getLogger(AutorizarSolServiceImpl.class);
	
	private AutorizarSolDao autorizarSolDao;
	private SolAltaService solAltaService;
	
	public List<AutorizacionTO> getProcesarSolicitudes(Integer idOperacion, Integer tipoSol, Integer estatusSol) {
		return autorizarSolDao.getProcesarSolicitudes(idOperacion, tipoSol, estatusSol);
	}
	
	public List<AutorizacionTO> getProcesarSolicitudesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol) {
		return autorizarSolDao.getProcesarSolicitudesBaja(idOperacion, tipoSol, estatusSol);
	}
	
	public List<AutorizacionTO> getProcesarDependientes(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
																Integer estatusCicloSol, Integer estatusDep) {
		return autorizarSolDao.getProcesarDependientes(idOperacion, tipoSol, estatusSol, estatusCicloSol, estatusDep);
	}
	
	public List<AutorizacionTO> getProcesarDependientesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
																Integer estatusCicloSol, Integer estatusDep) {
		return autorizarSolDao.getProcesarDependientesBaja(idOperacion, tipoSol, estatusSol, estatusCicloSol, estatusDep);
	}
	
	public List<AutorizacionTO> getProcesarConsulmed(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
																Integer estatusCicloSol, Integer estatusDep) {
		return autorizarSolDao.getProcesarConsulmed(idOperacion, tipoSol, estatusSol, estatusCicloSol, estatusDep);
	}
	
	public List<AutorizacionTO> getProcesarConsulmedBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol, 
																Integer estatusCicloSol, Integer estatusDep) {
		return autorizarSolDao.getProcesarConsulmedBaja(idOperacion, tipoSol, estatusSol, estatusCicloSol, estatusDep);
	}
			
	// Actualizar solicitudes
	
	public void setProcesarSolAlta(List<AutorizacionTO> lista, Integer usuarioAdmin){
		logger.info("AutorizarSolServiceImpl.setProcesarSolAlta");
		Date fechaNac = new Date();
		for (AutorizacionTO autorizacionTO : lista) {
			if(autorizacionTO.getAutorizar()!=null){
				logger.info("  Se autoriza solicitud autorizacion: ["+autorizacionTO.getAutorizar()
								+"] operacion: ["+autorizacionTO.getIdOperacion()+"]"
								+"] folio: ["+autorizacionTO.getIdSolPrimaria()+"] usuarioAdmin: ["+usuarioAdmin+"]");
																
				try {
					SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
					fechaNac = format.parse(autorizacionTO.getFechaNac());
				} catch (ParseException e) {
					logger.info("  No fue posible convertir la fecha");
				}
				
				double montoTotal = 0.0;
				double montoTotalDep = 0.0;
				double montoAmpl = 0.0;
				double montoAmplIndividual = 0.0;
				double montoMem = 0.0;
				EmpleadoTO empleadoTO = new EmpleadoTO();
				empleadoTO.setTipoEmp(autorizacionTO.getIdTipoEmp());
				empleadoTO.setSexo(autorizacionTO.getSexo());
				empleadoTO.setFechaNac(fechaNac);
				
				// Consultar cotizacion del empleado
				empleadoTO = solAltaService.getCotizacion(empleadoTO);
				
				// Consultar adiciones
				List<DependienteTO> listaDep = autorizarSolDao.getDependiente(autorizacionTO.getIdSolPrimaria(),
															Codigo.ID_OPERACION_ALTA,Codigo.EST_SOL_ALTA_EN_PROCESO);
				
				// Calcular el nuevo costo de las adiciones
				if(listaDep.size()!=0){
					for (DependienteTO dependienteTO : listaDep) {
						dependienteTO.setCostoDep(solAltaService.getMontoDependiente(autorizacionTO.getIdTipoEmp(), 
								dependienteTO.getIdParentesco(), dependienteTO.getFechaNac(), dependienteTO.getSexo()));
						montoTotalDep = montoTotalDep + dependienteTO.getCostoDep();
					}
				}
				
				// Calcular el monto de ampliacion total e individual
				if(!autorizacionTO.getIdTipoCob().equals(0)){
					montoAmpl = solAltaService.getMontoAmpliacion(autorizacionTO.getIdTipoCob(), listaDep.size());
					montoAmplIndividual = solAltaService.getMontoAmpliacionIndividual(autorizacionTO.getIdTipoCob(), Codigo.DEP_INDIVIDUAL_AMPLIACION);
				}
				
				// Calcular el monto de memorial
				if(!autorizacionTO.getIdTipoCobMem().equals(0)){
					montoMem = solAltaService.getMontoMemorial(autorizacionTO.getIdTipoCobMem());
				}
								
				// Total
				montoTotal = empleadoTO.getMontoEmpProrateado() + montoAmpl + montoMem + montoTotalDep;
								
				// Actualizar sol con los montos
				autorizarSolDao.setUpdateSol(empleadoTO.getMontoEmpProrateado(),montoAmplIndividual,montoTotalDep,
													montoTotal,montoAmpl,montoMem,autorizacionTO.getIdSolPrimaria());
				
				// Actualizar dep con el costo actualizado
				for (DependienteTO dependienteTO : listaDep) {
					autorizarSolDao.setUpdateSolDep(dependienteTO.getCostoDep(),montoAmplIndividual,dependienteTO.getIdDependiente());
				}
								
				// Autorizar Cambiar de estatus
				autorizarSolDao.setProcesarSolicitudesAlta(autorizacionTO.getIdOperacion(), usuarioAdmin,Codigo.EST_SOL_ALTA_AUTORIZADA,autorizacionTO.getComentario(),autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarDependientesXsolAlta(autorizacionTO.getIdOperacion(),usuarioAdmin,Codigo.EST_SOL_ALTA_AUTORIZADA, autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarConsulmedXsolAlta(autorizacionTO.getIdOperacion(),usuarioAdmin,Codigo.EST_SOL_ALTA_AUTORIZADA, autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarMemorialXsolAlta(autorizacionTO.getIdOperacion(),usuarioAdmin,Codigo.EST_SOL_ALTA_AUTORIZADA, autorizacionTO.getIdSolPrimaria());
				
				logger.info("   costoEmp: ["+empleadoTO.getMontoEmpProrateado()
								+"] montoAmplIndividual: ["+montoAmplIndividual+"] montoAmpl: ["+montoAmpl
								+"] montoTotalDep: ["+montoTotalDep+"] montoMemorial: ["+montoMem+"] montoTotal: ["+montoTotal+"]");
				logger.info("   Autorizacion de Alta Empleado OK");
			}else if(autorizacionTO.getRechazar()!=null){
				autorizarSolDao.setProcesarSolicitudesAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, Codigo.EST_SOL_ALTA_RECHAZADA, autorizacionTO.getComentario(),autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarDependientesXsolAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, Codigo.EST_SOL_ALTA_RECHAZADA, autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarConsulmedXsolAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, Codigo.EST_SOL_ALTA_RECHAZADA, autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarMemorialXsolAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, Codigo.EST_SOL_ALTA_RECHAZADA, autorizacionTO.getIdSolPrimaria());
				logger.info("  Se rechaza solicitud [ folio: "+autorizacionTO.getIdSolPrimaria()+" usuarioAdmin: "+usuarioAdmin+" rechazo: "+autorizacionTO.getRechazar()+"]");
			}else if(autorizacionTO.getAutorizar()==null && autorizacionTO.getRechazar()==null){
				logger.info("  No se realiza ninguna autorizacion [ folio: "+autorizacionTO.getIdSolPrimaria()+" ]");
			}	
		}			
	}
	
	public void setProcesarSolBaja(List<AutorizacionTO> lista, Integer usuarioAdmin){
		logger.info("AutorizarSolServiceImpl.setProcesarSolBaja");
		for (AutorizacionTO autorizacionTO : lista) {
			if(autorizacionTO.getAutorizar()!=null){
				logger.info("  Se autoriza baja de solicitud autorizacion: ["+autorizacionTO.getAutorizar()
									+"] folio: ["+autorizacionTO.getIdSolPrimaria()
									+"] usuarioAdmin: ["+usuarioAdmin
									+"] operacion: "+autorizacionTO.getIdOperacion()+"]");
				
				double montoDepNuevo = 0.0;
				double montoDepProrrateado = 0.0;
				double montoDepProrrateadoTotal = 0.0;
				double montoAmpNuevo = 0.0;
				double montoAmpProrrateado = 0.0;
				double montoAmpProrrateadoTotal = 0.0;
				double montoAmpTotal = 0.0;
				double montoTotal = 0.0;
				double montoMem = 0.0;
							
				// Consulta Dependientes 				
				List<DependienteTO> listaDep = autorizarSolDao.getDependiente(autorizacionTO.getIdSolPrimaria(),
															Codigo.ID_OPERACION_ALTA,Codigo.EST_SOL_BAJA_EN_PROCESO);
											
				for (DependienteTO dependienteTO : listaDep) {
					// Obtiene el costo del dependiente al dia de hoy
					montoDepNuevo = solAltaService.getMontoDependiente(autorizacionTO.getIdTipoEmp(), 
													dependienteTO.getIdParentesco(),dependienteTO.getFechaNac(),
													dependienteTO.getSexo());
					
					// Monto prorreatado = Costo Dep. Actual - Costo Dep. Nuevo
					montoDepProrrateado = dependienteTO.getCostoDep() - montoDepNuevo;
					
					// Monto prorreatado total
					montoDepProrrateadoTotal = montoDepProrrateadoTotal + montoDepProrrateado; 
					logger.info("   montoDepActual: ["+dependienteTO.getCostoDep()
										+"] montoDepNuevo: "+montoDepNuevo
										+"] montoDepProrr: ["+montoDepProrrateado+"]");				
					
					if(!autorizacionTO.getIdTipoCob().equals(0)){
						// Obtiene el costo de ampliacion individual al dia de hoy
						montoAmpNuevo = solAltaService.getMontoAmpliacionIndividual(autorizacionTO.getIdTipoCob(),
																					Codigo.DEP_INDIVIDUAL_AMPLIACION);

						// Monto de ampliacion prorreatado = Costo Amp. Dep Actual - Costo Amp. Dep Nuevo
						montoAmpProrrateado = dependienteTO.getCostoAmpDep() - montoAmpNuevo;
						
						// Monto de ampliacion prorreatado total
						montoAmpProrrateadoTotal = montoAmpProrrateadoTotal + montoAmpProrrateado; 
						logger.info("   montoAmpDepActual: ["+dependienteTO.getCostoAmpDep()
											+"] montoAmpDepNuevo: ["+montoAmpNuevo
											+"] montoAmpProrr: ["+montoAmpProrrateado+"]");
					}
					
					// Actualizar monto de dependientes
					autorizarSolDao.setProcesarCostoCancelacion(montoDepProrrateado,
															montoAmpProrrateado,dependienteTO.getIdDependiente());
				}
						
				montoAmpTotal = autorizacionTO.getCostoAmplInd() + montoAmpProrrateadoTotal;			
				montoTotal = autorizacionTO.getCostoEmp() + montoDepProrrateadoTotal + montoAmpTotal; 
				logger.info("   montoEmp: ["+autorizacionTO.getCostoEmp()
									+"] montoAmpInd: ["+autorizacionTO.getCostoAmplInd()
									+"] montoDeps: ["+montoDepProrrateadoTotal
									+"] montoAmpDeps: ["+montoAmpProrrateadoTotal
									+"] montoAmp: ["+montoAmpTotal
									+"] montoTotal: ["+montoTotal+"]");
								
				// Actualiza montos de la solicitud
				autorizarSolDao.setUpdateSol(autorizacionTO.getCostoEmp(), autorizacionTO.getCostoAmplInd(), 
						montoDepProrrateadoTotal, montoTotal, montoAmpTotal, montoMem, autorizacionTO.getIdSolPrimaria());
								
				// Autorizar estatus sol de baja
				autorizarSolDao.setProcesarSolicitudesBaja(autorizacionTO.getIdOperacion(),usuarioAdmin,
						Codigo.EST_SOL_BAJA_AUTORIZADA,new Date(), autorizacionTO.getComentario(),
						autorizacionTO.getIdSolPrimaria());
								
				// Autorizar estatus dependientes de baja
				autorizarSolDao.setProcesarDependientesXsolBaja(autorizacionTO.getIdOperacion(),usuarioAdmin,
						Codigo.EST_SOL_BAJA, new Date(), autorizacionTO.getIdSolPrimaria(),
						Codigo.EST_SOL_ALTA_RECHAZADA);
								
				// Autorizar estatus consulmed de baja
				autorizarSolDao.setProcesarConsulmedXsolBaja(autorizacionTO.getIdOperacion(),usuarioAdmin,
						Codigo.EST_SOL_BAJA, new Date(), autorizacionTO.getIdSolPrimaria(),
						Codigo.EST_SOL_ALTA_RECHAZADA);
								
				logger.info("   Autorizacion de Baja Empleado OK");
			}else if(autorizacionTO.getRechazar()!=null){
				autorizarSolDao.setProcesarSolicitudesBaja(Codigo.TIPO_SOL_ALTA,usuarioAdmin,
						Codigo.EST_SOL_ALTA_AUTORIZADA, null, autorizacionTO.getComentario(),
						autorizacionTO.getIdSolPrimaria());
				autorizarSolDao.setProcesarDependientesXsolBaja(Codigo.TIPO_SOL_ALTA,usuarioAdmin,
						Codigo.EST_SOL_ALTA_AUTORIZADA, null, autorizacionTO.getIdSolPrimaria(),
						Codigo.EST_SOL_BAJA);
				autorizarSolDao.setProcesarConsulmedXsolBaja(Codigo.TIPO_SOL_ALTA,usuarioAdmin,
						Codigo.EST_SOL_ALTA_AUTORIZADA, null, autorizacionTO.getIdSolPrimaria(),
						Codigo.EST_SOL_BAJA);
				logger.info("  Se rechaza solicitud folio: ["+autorizacionTO.getIdSolPrimaria()
								+"] usuarioAdmin: ["+usuarioAdmin
								+"] rechazo: ["+autorizacionTO.getRechazar()+"]");
			}else if(autorizacionTO.getAutorizar()==null && autorizacionTO.getRechazar()==null){
				logger.info("  No se realiza ninguna autorizacion [ folio: "+autorizacionTO.getIdSolPrimaria()+" ]");
			}	
		} // Fin del for			
	}
	
	public void setProcesarDepAlta(List<AutorizacionTO> lista, Integer usuarioAdmin){
		logger.info("AutorizarSolServiceImpl.setProcesarDepAlta");
		for (AutorizacionTO autorizacionTO : lista) {
			if(autorizacionTO.getAutorizar()!=null){
				logger.info("  Se autoriza dependiente: autorizacion ["+autorizacionTO.getAutorizar()
									+"] operacion ["+autorizacionTO.getIdOperacion()
									+"] folioDep: ["+autorizacionTO.getIdSolPrimaria()
									+"] folioSol: ["+autorizacionTO.getIdSolSecundaria()
									+"] usuarioAdmin ["+usuarioAdmin
									+"] tipo emp ["+autorizacionTO.getIdTipoEmp()
									+"] parentesco ["+autorizacionTO.getIdParentesco()
									+"] fecha nac ["+autorizacionTO.getFechaNac()
									+"] sexo["+autorizacionTO.getSexo()
									+"] tipoCob ["+autorizacionTO.getIdTipoCob()+"]");
				
				double montoAmplIndividual = 0.0;
				double montoAmpl = 0.0;
				double montoTotalDep = 0.0;
				double montoTotal = 0.0;
				double costoTotDepAct = 0.0;
				double costoDep = 0.0;
												
				// Consultar el costo del dependiente por autorizar al dia de hoy
				costoDep = solAltaService.getMontoDependiente(autorizacionTO.getIdTipoEmp(), autorizacionTO.getIdParentesco(), 
																	autorizacionTO.getFechaNac(), autorizacionTO.getSexo());
										
				// Consulta costo total de los dependientes actuales
				costoTotDepAct = autorizarSolDao.getCostoTotDep(autorizacionTO.getIdSolSecundaria());
								
				// Consulta consto total de la ampliacion
				montoAmpl = autorizarSolDao.getCostoTotAmpliacion(autorizacionTO.getIdSolSecundaria());
				logger.info("   costoEmp: ["+autorizacionTO.getCostoEmp()+"] costoTotDepAct: ["
													+costoTotDepAct+"] montoAmpl: ["+montoAmpl+"]");
				
				// Calcular si tiene cobertura 
				if(!autorizacionTO.getIdTipoCob().equals(0)){
					montoAmplIndividual = solAltaService.getMontoAmpliacionIndividual(autorizacionTO.getIdTipoCob(), 
																						Codigo.DEP_INDIVIDUAL_AMPLIACION);
					montoAmpl = montoAmpl + montoAmplIndividual;
				}				
				
				montoTotalDep = montoTotalDep + costoDep + costoTotDepAct;
				montoTotal = autorizacionTO.getCostoEmp() + montoTotalDep + montoAmpl;	
				logger.info("   costoDep: ["+costoDep+"] montoAmplIndividual: ["+montoAmplIndividual+"] montoTotalDep: ["
											+montoTotalDep+"] montoAmplAct: ["+montoAmpl +"] montoTotal: ["+montoTotal+"]");
				
				// Guardar costo del dependiente
				autorizarSolDao.setUpdateSolDep(costoDep,montoAmplIndividual,autorizacionTO.getIdSolPrimaria());
				
				/*	Actualiza los montos en la solicitud	*/
				autorizarSolDao.setProcesarSolDependientes(montoTotal, montoTotalDep, montoAmpl, autorizacionTO.getIdSolSecundaria());
				
				/*	Actualiza la solicitud del dependiente	*/
				autorizarSolDao.setProcesarDependientesAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, 
						Codigo.EST_SOL_ALTA_AUTORIZADA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
									
				logger.info("   Autorizacion de Alta Dep. OK");
			}else if(autorizacionTO.getRechazar()!=null){
				autorizarSolDao.setProcesarDependientesAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, 
						Codigo.EST_SOL_ALTA_RECHAZADA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				logger.info("  Se rechaza dependiente [ folio: "+autorizacionTO.getIdSolPrimaria()+" usuarioAdmin: "
							+usuarioAdmin+" rechazo: "+autorizacionTO.getRechazar()+" comentario: "+autorizacionTO.getComentario()+"]");				
			}else if(autorizacionTO.getAutorizar()==null && autorizacionTO.getRechazar()==null){
				logger.info("  No se realiza ninguna autorizacion [ folio: "+autorizacionTO.getIdSolPrimaria()+" ]");
			}	
		}
	}
	
	public void setProcesarDepBaja(List<AutorizacionTO> lista, Integer usuarioAdmin){
		logger.info("AutorizarSolServiceImpl.setProcesarDepBaja");
		for (AutorizacionTO autorizacionTO : lista) {
			if(autorizacionTO.getAutorizar()!=null){
				logger.info("  Se autoriza dependiente autorizacion: ["+autorizacionTO.getAutorizar()
								+"] folio: ["+autorizacionTO.getIdSolPrimaria()+"] usuarioAdmin: ["+usuarioAdmin
								+"] operacion: ["+autorizacionTO.getIdOperacion()
								+"] autorizacion: "+autorizacionTO.getAutorizar()+"]");
				
				logger.info("   Parametros tipo_cobertura: ["+autorizacionTO.getIdTipoCob()
									+"] tipo emp: ["+autorizacionTO.getIdTipoEmp()
									+"] costo dep: ["+autorizacionTO.getCostoDep()
									+"] costo total: ["+autorizacionTO.getCostoTotal()+"]");
				
				double montoAmplTotal = 0.0;
				double montoAmpl = 0.0;
				double montoAmplResta = 0.0;
				double montoAmplEmp = 0.0;
				double montoAmplDepActTot = 0.0;
				double montoAmplDepAct = 0.0;
				double montoTotalDep = 0.0;
				double montoTotal = 0.0;
				double montoCancelacion = 0.0;
				double montoDepActual = 0.0;
				double costoTotDepAct = 0.0;
				double montoAmplDepActTotTemp = 0.0;
				double montoAmplDepActTemp = 0.0;
												
				//	Calcula Ampliacion si existe
				if(!autorizacionTO.getIdTipoCob().equals(0)){
					// Obtiene el monto de Ampliacion del Empleado 
					montoAmplEmp = autorizarSolDao.getCostoAmpliacionEmp(autorizacionTO.getIdSolSecundaria());
								
					// Obtiene el monto de ampliacion total de todos los dependientes menos el que se va a dar de baja
					montoAmplDepActTotTemp = autorizarSolDao.getCostoAmpliacionDepTot(autorizacionTO.getIdSolSecundaria(),
									Codigo.ID_OPERACION_ALTA,Codigo.EST_SOL_ALTA_AUTORIZADA,autorizacionTO.getIdSolPrimaria());
					
					// Obtiene el monto de ampliacion del dependiente que se va a dar de baja
					montoAmplDepActTemp = autorizarSolDao.getCostoAmpliacionDep(autorizacionTO.getIdSolPrimaria());
																				
					// Obtener el monto de ampliacion del dependiente que se va a dar de baja, al dia de hoy
					montoAmpl = solAltaService.getMontoAmpliacionIndividual(autorizacionTO.getIdTipoCob(), 
																			Codigo.DEP_INDIVIDUAL_AMPLIACION);
					
					montoAmplDepActTot = montoAmplDepActTotTemp;
					montoAmplDepAct = montoAmplDepActTemp;
					
					// Resta del monto de ampliacion original menos el monto de ampliacion al dia de hoy del dep. dado de baja
					montoAmplResta = montoAmplDepAct - montoAmpl;
					
					// Suma del monto de amp. del empleado + deps. actuales + prorrateo de dep. dado de baja
					montoAmplTotal = montoAmplEmp + montoAmplDepActTot + montoAmplResta;
										
					logger.info("   ampliacionEmp: ["+montoAmplEmp+"] ampliacionDeps: ["+montoAmplDepActTot
									+"] ampliacionDepBaja: ["+montoAmplDepAct+"] ampliacionDepAlDia: ["+montoAmpl
									+"] ampliacionDifDepBaja: ["+montoAmplResta+"] ampliacionTotal: ["+montoAmplTotal+"]");
					
				}
				
				// Obtiene el monto del dependiente al dia de hoy
				montoDepActual = solAltaService.getMontoDependiente(autorizacionTO.getIdTipoEmp(), 
						autorizacionTO.getIdParentesco(), autorizacionTO.getFechaNac(), autorizacionTO.getSexo());
								
				// Obtiene costo total de los dependientes
				costoTotDepAct = autorizarSolDao.getCostoTotDep(autorizacionTO.getIdSolSecundaria());
								
				// Monto de Cancelacion = Costo del Dep - Costo del Dep al dia de hoy
				montoCancelacion = autorizacionTO.getCostoDep() - montoDepActual;
				
				// Monto Total Dependientes = 
				montoTotalDep = (costoTotDepAct + montoCancelacion) - autorizacionTO.getCostoDep();
				logger.info("   montoDep: ["+autorizacionTO.getCostoDep()+"] montoDepAct: ["+montoDepActual
								+"] montoTotalDeps:["+costoTotDepAct+"] montoCancelacion: ["+montoCancelacion
								+"] montoTotalDepsNuevo: ["+montoTotalDep+"]");
								
				// Monto total = Monto Emp + Monto Total Deps + Monto Ampl. Total
				montoTotal = autorizacionTO.getCostoEmp() + montoTotalDep + montoAmplTotal;
				logger.info("   montoEmp: ["+autorizacionTO.getCostoEmp()+"] montoTotalDep: ["+montoTotalDep
						+"] montoAmplTotal: ["+montoAmplTotal+"] montoTotal: ["+montoTotal+"]");
																
				// Actualiza el monto de cancelacion
				autorizarSolDao.setProcesarCostoCancelacion(montoCancelacion,montoAmplResta,
																autorizacionTO.getIdSolPrimaria());
											
				//	Actualiza los montos en la solicitud
				autorizarSolDao.setProcesarSolDependientes(montoTotal, montoTotalDep, montoAmplTotal, 
																autorizacionTO.getIdSolSecundaria());
				
				//	Actualiza la solicitud del dependiente
				autorizarSolDao.setProcesarDependientesBaja(autorizacionTO.getIdOperacion(),usuarioAdmin, 
						Codigo.EST_SOL_BAJA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				
				logger.info("   Autorizacion de Baja Dep. OK");
				
			}else if(autorizacionTO.getRechazar()!=null){
				autorizarSolDao.setProcesarDependientesBaja(Codigo.ID_OPERACION_ALTA,usuarioAdmin, Codigo.EST_SOL_ALTA_AUTORIZADA, 
																autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				logger.info("  Se rechaza dependiente folio: ["+autorizacionTO.getIdSolPrimaria()+"] usuarioAdmin: ["+usuarioAdmin
																+"] rechazo: ["+autorizacionTO.getRechazar()+"]");
			}else if(autorizacionTO.getAutorizar()==null && autorizacionTO.getRechazar()==null){
				logger.info("  No se realiza ninguna autorizacion [ folio: "+autorizacionTO.getIdSolPrimaria()+" ]");
			}	
		}
	}
			
	public void setProcesarConAlta(List<AutorizacionTO> lista, Integer usuarioAdmin){
		logger.info("AutorizarSolServiceImpl.setProcesarConAlta");
		for (AutorizacionTO autorizacionTO : lista) {
			if(autorizacionTO.getAutorizar()!=null){
				autorizarSolDao.setProcesarConsulmedAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, 
						Codigo.EST_SOL_ALTA_AUTORIZADA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				logger.info("  Se autoriza consulmed folio: ["+autorizacionTO.getIdSolPrimaria()
									+"] usuarioAdmin: ["+usuarioAdmin+"] operacion: ["+autorizacionTO.getIdOperacion()
									+"] autorizacion: ["+autorizacionTO.getAutorizar()+"]");
				logger.info("   Autorizacion de Alta Dep. Consulmed OK");
			}else if(autorizacionTO.getRechazar()!=null){
				autorizarSolDao.setProcesarConsulmedAlta(autorizacionTO.getIdOperacion(),usuarioAdmin, 
						Codigo.EST_SOL_ALTA_RECHAZADA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				logger.info("  Se rechaza consulmed folio: ["+autorizacionTO.getIdSolPrimaria()
									+"] usuarioAdmin: ["+usuarioAdmin+"] rechazo: ["+autorizacionTO.getRechazar()+"]");
			}else if(autorizacionTO.getAutorizar()==null && autorizacionTO.getRechazar()==null){
				logger.info("  No se realiza ninguna autorizacion [ folio: "+autorizacionTO.getIdSolPrimaria()+" ]");
			}	
		}
	}
	
	public void setProcesarConBaja(List<AutorizacionTO> lista, Integer usuarioAdmin){
		logger.info("AutorizarSolServiceImpl.setProcesarConBaja");
		for (AutorizacionTO autorizacionTO : lista) {
			if(autorizacionTO.getAutorizar()!=null){
				autorizarSolDao.setProcesarConsulmedBaja(autorizacionTO.getIdOperacion(),usuarioAdmin, 
						Codigo.EST_SOL_BAJA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				logger.info("  Se autoriza consulmed folio: ["+autorizacionTO.getIdSolPrimaria()+"] usuarioAdmin: ["+usuarioAdmin
						+"] operacion: ["+autorizacionTO.getIdOperacion()+"] autorizacion: ["+autorizacionTO.getAutorizar()+"]");
				logger.info("   Autorizacion de Baja Dep. Consulmed OK");
			}else if(autorizacionTO.getRechazar()!=null){
				autorizarSolDao.setProcesarConsulmedBaja(Codigo.ID_OPERACION_ALTA,usuarioAdmin, 
						Codigo.EST_SOL_ALTA_AUTORIZADA, autorizacionTO.getComentario(), autorizacionTO.getIdSolPrimaria());
				logger.info("  Se rechaza consulmed folio: ["+autorizacionTO.getIdSolPrimaria()+"] usuarioAdmin: ["+usuarioAdmin
						+"] rechazo: ["+autorizacionTO.getRechazar()+"]");
			}else if(autorizacionTO.getAutorizar()==null && autorizacionTO.getRechazar()==null){
				logger.info("  No se realiza ninguna autorizacion [ folio: "+autorizacionTO.getIdSolPrimaria()+" ]");
			}	
		}
	}
	
	
	@Autowired
	public void setAutorizarSolDao(AutorizarSolDao autorizarSolDao) {
		this.autorizarSolDao = autorizarSolDao;
	}

	@Autowired
	public void setSolAltaService(SolAltaService solAltaService) {
		this.solAltaService = solAltaService;
	}
	
}