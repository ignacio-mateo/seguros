package mx.com.orbita.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.EmpleadoDao;
import mx.com.orbita.dao.ReporteDao;
import mx.com.orbita.service.ReporteService;
import mx.com.orbita.to.ReporteParamsTO;
import mx.com.orbita.to.ReporteTO;
import mx.com.orbita.util.Codigo;

@Component
public class ReporteServiceImpl implements ReporteService {

	private static final Logger logger = Logger.getLogger(ReporteServiceImpl.class);
	
	private ReporteDao reporteDao;
	private EmpleadoDao empleadoDao;
	
	public List<ReporteTO> getCambioCategoria(ReporteParamsTO reporteTO){
		logger.info("ReporteServiceImpl.getCambioCategoria");
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()+"]");
		List<ReporteTO> listaUno = new ArrayList<ReporteTO>();
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(1);
			reporteTO.setTipoEmp2(2);
			listaUno = reporteDao.getCambioCategoria(reporteTO);			
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
			listaUno = reporteDao.getCambioCategoria(reporteTO);
		}
		return listaUno;
	}

	public List<ReporteTO> getNomina(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getNomina");
		List<ReporteTO> lista = new ArrayList<ReporteTO>();
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()+"]");
				
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		
		if(reporteTO.getTipo()==1){ // Altas
			reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getNominaAlta(reporteTO);
		}else if(reporteTO.getTipo()==2){ // Bajas
			reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstSol(Codigo.EST_SOL_BAJA_TOTAL);
			List<ReporteTO> listaBajaTotal = reporteDao.getNominaBaja(reporteTO);
			
			reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstSol(Codigo.EST_SOL_BAJA_AUTORIZADA);
			lista = reporteDao.getNominaBaja(reporteTO);
			
			lista.addAll(listaBajaTotal);
		}else if(reporteTO.getTipo()==3){ // Alta de Dep
			reporteTO.setTipoDep(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstDep(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getNominaDepAdicion(reporteTO);
		}else if(reporteTO.getTipo()==4){ // Baja de Dep
			reporteTO.setTipoDep(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstDep(Codigo.EST_SOL_BAJA);
			lista = reporteDao.getNominaDepBaja(reporteTO);
		}
		return lista;
	}

	public List<ReporteTO> getReporteActual(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getReporteActual");
		reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
		reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()
				+"] tipoSol: ["+reporteTO.getTipoSol()
				+"] estSol: ["+reporteTO.getEstSol()+"]");
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		return reporteDao.getReporteActual(reporteTO);
	}

	public List<ReporteTO> getConsulmed(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getConsulmed");
		List<ReporteTO> lista = new ArrayList<ReporteTO>();
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()+"]");
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		
		if(reporteTO.getTipo()==1){ // Alta 
			reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
			reporteTO.setTipoDep(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstDep(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getConsulmedAlta(reporteTO);
		}else if(reporteTO.getTipo()==2){ // Adiciones
			reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getConsulmedAdicion(reporteTO);
		}else if(reporteTO.getTipo()==3){ // Baja Dep
			reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstSol(Codigo.EST_SOL_BAJA);
			lista = reporteDao.getConsulmedBaja(reporteTO);
		}else if(reporteTO.getTipo()==4){ // Bajas Totales
			reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstSolA(Codigo.EST_SOL_BAJA);
			reporteTO.setEstSolB(Codigo.EST_SOL_BAJA_AUTORIZADA);
			reporteTO.setEstSolC(Codigo.EST_SOL_BAJA_TOTAL);
			lista = reporteDao.getConsulmedBajaTotal(reporteTO);	
		}
		return lista;
	}
	
	public List<ReporteTO> getConsulmedTotal(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getConsulmedTotal");
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		
		reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
		reporteTO.setEstSolA(Codigo.EST_SOL_ALTA_AUTORIZADA);
		reporteTO.setEstSolB(Codigo.EST_SOL_BAJA_EN_PROCESO);
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()
						+"] tipoSol: ["+reporteTO.getTipoSol()
						+"] estSol: ["+reporteTO.getEstSolA()
						+"] estSol: ["+reporteTO.getEstSolB()+"]");
		return reporteDao.getConsulmedTotal(reporteTO);
	}

	public List<ReporteTO> getDatosFiscales(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getDatosFiscales");
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()+"]");
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		
		// Altas
		reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
		reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
		List<ReporteTO> listaAlta = reporteDao.getDatosFiscalesAlta(reporteTO);
		for (ReporteTO reporte : listaAlta) {
			reporte.setCorreo(empleadoDao.getCorreo(reporte.getNumEmp()));
		}
		
		// Bajas Autorizada
		reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
		reporteTO.setEstSol(Codigo.EST_SOL_BAJA_AUTORIZADA);
		
		Calendar cal= Calendar.getInstance(); 
		int anio= cal.get(Calendar.YEAR); 
		String fechaIni = "01/06/"+(anio-1);
		logger.info("  fecha inicial modificada: "+fechaIni);
		reporteTO.setFechaIni(fechaIni);
		
		List<ReporteTO> listaBaja = reporteDao.getDatosFiscalesBaja(reporteTO);
		for (ReporteTO reporte : listaBaja) {
			reporte.setCorreo(empleadoDao.getCorreo(reporte.getNumEmp()));
		}
		
		// Bajas Autorizada
		reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
		reporteTO.setEstSol(Codigo.EST_SOL_BAJA_TOTAL);
		List<ReporteTO> listaBajaTotal = reporteDao.getDatosFiscalesBaja(reporteTO);
		for (ReporteTO reporte : listaBajaTotal) {
			reporte.setCorreo(empleadoDao.getCorreo(reporte.getNumEmp()));
		}
		
		// Bajas por finiquito
		reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
		reporteTO.setEstSol(Codigo.EST_SOL_BAJA_X_FINIQUITO);
		List<ReporteTO> listaFiniquito = reporteDao.getDatosFiscalesFiniquito(reporteTO);
		for (ReporteTO reporte : listaFiniquito) {
			reporte.setCorreo(empleadoDao.getCorreo(reporte.getNumEmp()));
		}

		listaAlta.addAll(listaBaja);
		listaAlta.addAll(listaBajaTotal);
		listaAlta.addAll(listaFiniquito);
		return listaAlta;
	}

	public List<ReporteTO> getAmpliacionesDep(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getAmpliacionesDep");
		reporteTO.setTipoDep(Codigo.TIPO_SOL_ALTA);
		reporteTO.setEstDep(Codigo.EST_SOL_ALTA_AUTORIZADA);
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()
				+"] tipoSol: ["+reporteTO.getTipoSol()
				+"] estSol: ["+reporteTO.getEstDep()+"]");
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		return reporteDao.getAmpliacionesDep(reporteTO);
	}

	public List<ReporteTO> getAmpliaciones(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getAmpliaciones");
		reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
		reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()
						+"] tipoSol: ["+reporteTO.getTipoSol()
						+"] estSol: ["+reporteTO.getEstSol()+"]");
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		return reporteDao.getAmpliaciones(reporteTO);
	}
	
	public List<ReporteTO> getDescargaSol(ReporteParamsTO reporteTO) {
		logger.info("ReporteServiceImpl.getDescargaSol");	
		logger.info(" tipoEmp: ["+reporteTO.getTipoEmp()+"] tipo: ["+reporteTO.getTipo()+"]");
		List<ReporteTO> lista = new ArrayList<ReporteTO>();
		
		if(reporteTO.getTipoEmp()==0){
			reporteTO.setTipoEmp1(Codigo.ID_EMPLEADO_CONFIANZA);
			reporteTO.setTipoEmp2(Codigo.ID_EMPLEADO_SINDICALIZADO);
		}else{
			reporteTO.setTipoEmp1(reporteTO.getTipoEmp());
			reporteTO.setTipoEmp2(reporteTO.getTipoEmp());
		}
		
		if(reporteTO.getTipo()==1){ // Alta Sol
			reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getDescargaSolAlta(reporteTO);
		}else if(reporteTO.getTipo()==2){ // Adiciones
			reporteTO.setTipoSol(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstSol(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getDescargaAdicion(reporteTO);
		}else if(reporteTO.getTipo()==3){ // Baja Dep
			reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstSol(Codigo.EST_SOL_BAJA);
			lista = reporteDao.getDescargaBajaDep(reporteTO);
		}else if(reporteTO.getTipo()==4){ // Bajas Totales
			reporteTO.setTipoSol(Codigo.TIPO_SOL_BAJA);
			reporteTO.setEstSolA(Codigo.EST_SOL_BAJA);
			reporteTO.setEstSolB(Codigo.EST_SOL_BAJA_AUTORIZADA);
			reporteTO.setEstSolC(Codigo.EST_SOL_BAJA_TOTAL);
			lista = reporteDao.getDescargaSolBaja(reporteTO);
		}
		return lista;
	}
		
	public List<ReporteTO> getRenovacion(ReporteParamsTO reporteTO){
		List<ReporteTO> lista = new ArrayList<ReporteTO>();
		lista = reporteDao.getRenovEncuesta();
		lista = reporteDao.getRenovSi(reporteTO);
		lista =reporteDao.getRenovNo(reporteTO);
		
		if(reporteTO.getTipo()==1){ // Encuesta
			lista = reporteDao.getRenovEncuesta();
		}else if(reporteTO.getTipo()==2){ // Si renovaron
			reporteTO.setEstSolA(Codigo.RESPUESTA_RENOV_SI);
			reporteTO.setEstSolB(Codigo.ESTATUS_RENOV_EXITOSA);
			lista = reporteDao.getRenovSi(reporteTO);
		}else if(reporteTO.getTipo()==3){ // No renovaron
			reporteTO.setEstSolA(Codigo.RESPUESTA_RENOV_NO);
			reporteTO.setEstSolB(Codigo.ESTATUS_RENOV_EXITOSA);
			lista = reporteDao.getRenovNo(reporteTO);
		}else if(reporteTO.getTipo()==4){ // No contestaron
			reporteTO.setEstSolA(Codigo.TIPO_SOL_ALTA);
			reporteTO.setEstSolB(Codigo.EST_SOL_ALTA_AUTORIZADA);
			lista = reporteDao.getRenovNoContestaron(reporteTO);
		}else if(reporteTO.getTipo()==5){
			lista = reporteDao.getMemorialEncuesta();
		}else if(reporteTO.getTipo()==6){
			lista = reporteDao.getMemorialDependiente();
		}

		return lista;
	}
	
	
	@Autowired
	public void setReporteDao(ReporteDao reporteDao) {
		this.reporteDao = reporteDao;
	}

	@Autowired
	public void setEmpleadoDao(EmpleadoDao empleadoDao) {
		this.empleadoDao = empleadoDao;
	}
	
}