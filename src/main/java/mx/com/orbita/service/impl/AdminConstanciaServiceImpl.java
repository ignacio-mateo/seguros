package mx.com.orbita.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.AdminConstanciaDao;
import mx.com.orbita.service.AdminConstanciaService;
import mx.com.orbita.to.ConstanciaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.util.Codigo;

@Component
public class AdminConstanciaServiceImpl implements AdminConstanciaService {
	
	private static final Logger loger = Logger.getLogger(AdminConstanciaServiceImpl.class);
	
	private AdminConstanciaDao adminConstanciaDao;

	public ConstanciaTO getEmpleado(Integer numEmp){
		loger.info("AdminConstanciaServiceImpl.getEmpleado");
		return adminConstanciaDao.getEmpleado(numEmp, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
	}
	
	public List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estSol){
		loger.info("AdminConstanciaServiceImpl.getDependiente");
		return adminConstanciaDao.getDependiente(numEmp,tipoSol,estSol);
	}
		
	
	@Autowired
	public void setAdminConstanciaDao(AdminConstanciaDao adminConstanciaDao) {
		this.adminConstanciaDao = adminConstanciaDao;
	}

	
}
