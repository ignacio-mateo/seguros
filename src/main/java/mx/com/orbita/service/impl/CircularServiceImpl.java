package mx.com.orbita.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CircularDao;
import mx.com.orbita.service.CircularService;
import mx.com.orbita.to.CircularTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.util.Codigo;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class CircularServiceImpl implements CircularService {

	private static final Logger loger = Logger.getLogger(CircularServiceImpl.class);

	private CircularDao circularDao;

	public void setRenovacion(CircularTO circularTO){
		loger.info("CircularServiceImpl.setRenovacion");
		circularDao.setRenovacion(circularTO);
	}

	public void setMemorial(CircularTO circularTO){
		circularDao.setMemorial(circularTO);
	}

	public void setDependienteMemorial(List<ConsulmedTO> list){
		loger.info("  SERVICE ");
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");

		Date fecha = new Date();
		Integer idSol = 0;
		Integer resultado = 0;

		for (ConsulmedTO consulmedTO : list) {
			idSol = consulmedTO.getIdSolicitud();
		}
		circularDao.eliminarDependienteMemorial(idSol);

		for (ConsulmedTO consulmedTO : list) {
			if(consulmedTO.getIdConsulmed()!=null && !consulmedTO.getIdConsulmed().equals("")){
				try {
					fecha = formatoDelTexto.parse(consulmedTO.getFechaNac());
					if(consulmedTO.getSexo().equals("M")){
						consulmedTO.setIdSexo(1);
					}else if(consulmedTO.getSexo().equals("F")){
						consulmedTO.setIdSexo(2);
					}
					consulmedTO.setIdTipoSol(Codigo.TIPO_SOL_ALTA);
				}catch (ParseException e) {
					loger.info("  Exception: "+e.getMessage());
				}
				resultado = circularDao.setDependienteMemorial(consulmedTO,fecha);
				loger.info(" resultado: "+resultado);
			}
		}
	}

	public List<DependienteTO> getDependiente(DependienteTO dependienteTO) {
		return circularDao.getDependiente(dependienteTO.getIdSolicitud(),dependienteTO.getIdTipoSol(),dependienteTO.getIdEstatusDep());
	}

	@Autowired
	public void setCircularDao(CircularDao circularDao) {
		this.circularDao = circularDao;
	}

}
