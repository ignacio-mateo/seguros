package mx.com.orbita.service.impl;

import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.FiniquitoDao;
import mx.com.orbita.service.FiniquitoService;
import mx.com.orbita.service.SolAltaService;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.FiniquitoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Redondeos;

@Component
public class FiniquitoServiceImpl implements FiniquitoService {
	
		private static final Logger logger = Logger.getLogger(FiniquitoServiceImpl.class);
				
		private SolAltaService solAltaService;
		private SolConsultaService solConsultaService;	
		private FiniquitoDao finiquitoDao;
				
		public Integer getIdSolicitud(Integer numEmp){
			return finiquitoDao.getIdSolicitud(numEmp, Codigo.ID_OPERACION_ALTA, Codigo.EST_SOL_ALTA_AUTORIZADA);
		}
		
		public FiniquitoTO getFiniquito(Integer numEmp){
			return finiquitoDao.getFiniquito(numEmp);
		}
		
		public FiniquitoTO calcularFiniquito(Integer numEmp, Integer mesFiniquito, Integer diaFiniquito){
			FiniquitoTO finiquito = new FiniquitoTO();
			Calendar cal= Calendar.getInstance(); 
			int anio= cal.get(Calendar.YEAR); 
			double montoEmp = 0.0;
			double montoAmpEmp = 0.0;
			double montoAmpEmpActual = 0.0;
			double montoAmpEmpFin = 0.0;
			double montoDepAlaFecha = 0.0;
			double montoDepFiniquito = 0.0;
			double montoDepFiniquitoTot = 0.0;
			double montoDepAmpActual = 0.0;
			double montoDepAmpAlaFecha = 0.0;
			double montoDepAmpDif = 0.0;
			double montoDepAmpTot = 0.0;
			double montoFiniquito = 0.0;
			int diaEmp = 0;
			int mesEmp = 0;
			int diaDep = 0;
			int mesDep = 0;
						
			// Obtener monto del empleado
			EmpleadoTO empleadoTO = solConsultaService.getEmpleadoBD(numEmp);
			SolicitudTO solicitudTO = finiquitoDao.getSolicitud(numEmp,
						Codigo.ID_OPERACION_ALTA,Codigo.EST_SOL_ALTA_AUTORIZADA);
						
			// Calcular el dia y mes desde donde se va a calcular el prorrateado
			if(anio==Codigo.PERIODO_INICIAL){ 
				logger.info("  Periodo Junio A Diciembre");
				if(solicitudTO.getAnioAut().equals(anio) && solicitudTO.getMesAut() > 5){
					logger.info("  Esta dentro del Periodo");
					diaEmp = solicitudTO.getDiaAut();
					mesEmp = solicitudTO.getMesAut();
				}else{
					logger.info("  Esta fuera del Periodo");
					diaEmp = Codigo.DIA_RENOVACION;
					mesEmp = Codigo.MES_RENOVACION;
				}									
			}else if(anio>Codigo.PERIODO_INICIAL){
				logger.info("  Periodo Enero A Junio");
				if(solicitudTO.getAnioAut().equals(anio)){
					logger.info("  Esta dentro del Periodo");
					diaEmp = solicitudTO.getDiaAut();
					mesEmp = solicitudTO.getMesAut();
				}else{
					logger.info("  Esta fuera del Periodo");
					diaEmp = Codigo.DIA_RENOVACION;
					mesEmp = Codigo.MES_RENOVACION;
				}		
			}	
						
			// Obtnener el monto del empleado proporcional
			empleadoTO = solAltaService.getCotizacion(empleadoTO, mesFiniquito, diaFiniquito);
			montoEmp = (solicitudTO.getCostoEmpleado()) - (empleadoTO.getMontoEmpProrateado());
						
			// Obtener monto de ampliacion del empleado
			if(solicitudTO.getIdTipoCobertura()!=0){
				montoAmpEmpActual = solAltaService.getMontoAmpliacionIndividual(solicitudTO.getIdTipoCobertura(),
						Codigo.DEP_INDIVIDUAL_AMPLIACION, mesEmp, diaEmp);
				montoAmpEmpFin = solAltaService.getMontoAmpliacionIndividual(solicitudTO.getIdTipoCobertura(),
						Codigo.DEP_INDIVIDUAL_AMPLIACION, mesFiniquito, diaFiniquito);
				montoAmpEmp = montoAmpEmpActual - montoAmpEmpFin;
				
			}
						
			// Obtener el monto de los dependientes
			List<DependienteTO> listaDep = finiquitoDao.getDependiente(numEmp,
					Codigo.TIPO_SOL_ALTA,Codigo.EST_SOL_ALTA_AUTORIZADA);
			if(listaDep.size()>0){
				for (DependienteTO dependienteTO : listaDep) {
					montoDepAlaFecha = solAltaService.getMontoDependiente(empleadoTO.getTipoEmp(),
							dependienteTO.getIdParentesco(),dependienteTO.getFechaNac(),
							dependienteTO.getSexo(),mesFiniquito, diaFiniquito);
					montoDepFiniquito = dependienteTO.getCostoDep() - montoDepAlaFecha;
					montoDepFiniquitoTot = montoDepFiniquitoTot + montoDepFiniquito;
					logger.info("  montoDepActual: ["+dependienteTO.getCostoDep()
									+"] montoDepAlaFecha: ["+montoDepAlaFecha
									+"] montoDepFiniquito: "+montoDepFiniquito+"]");
				}
			}
			
			logger.info("  montoEmpBD: ["+solicitudTO.getCostoEmpleado()
							+"] montoEmpAct: ["+empleadoTO.getMontoEmpProrateado()
							+"] montoEmpDif: ["+montoEmp
							+"] montoAmplEmplActual: ["+montoAmpEmpActual
							+"] montoAmplEmplFechaFiniquito: ["+montoAmpEmpFin
							+"] montoAmplEmplProrr: ["+montoAmpEmp
							+"] montoTotalDepFiniquito: ["+montoDepFiniquitoTot+"]");
			
			// Obtener el monto de ampliacion de los dependientes
			if(solicitudTO.getIdTipoCobertura()!=0){
				if(listaDep.size()>0){
					for (DependienteTO dependienteTO : listaDep) {
												
						// Calcular el dia y mes desde donde se va a calcular el prorrateado
						if(anio==Codigo.PERIODO_INICIAL){ 
							logger.info("  Periodo Junio A Diciembre");
							if(dependienteTO.getAnioAut().equals(anio) && dependienteTO.getMesAut() > 5){
								logger.info("  Esta dentro del Periodo");
								diaDep = dependienteTO.getDiaAut();
								mesDep = dependienteTO.getMesAut();
							}else{
								logger.info("  Esta fuera del Periodo");
								diaDep = Codigo.DIA_RENOVACION;
								mesDep = Codigo.MES_RENOVACION;
							}									
						}else if(anio>Codigo.PERIODO_INICIAL){
							logger.info("  Periodo Enero A Junio");
							if(dependienteTO.getAnioAut().equals(anio)){
								logger.info("  Esta dentro del Periodo");
								diaDep = dependienteTO.getDiaAut();
								mesDep = dependienteTO.getMesAut();
							}else{
								logger.info("  Esta fuera del Periodo");
								diaDep = Codigo.DIA_RENOVACION;
								mesDep = Codigo.MES_RENOVACION;
							}		
						}									
						
						montoDepAmpActual = solAltaService.getMontoAmpliacionIndividual(solicitudTO.getIdTipoCobertura(),
								Codigo.DEP_INDIVIDUAL_AMPLIACION, mesDep, diaDep);
						montoDepAmpAlaFecha = solAltaService.getMontoAmpliacionIndividual(solicitudTO.getIdTipoCobertura(),
								Codigo.DEP_INDIVIDUAL_AMPLIACION, mesFiniquito, diaFiniquito);
						montoDepAmpDif = montoDepAmpActual - montoDepAmpAlaFecha;
						montoDepAmpTot = montoDepAmpTot + montoDepAmpDif;
						logger.info("  montoDepAmpActual: ["+montoDepAmpDif
										+"] montoDepAmpAlaFecha: ["+montoDepAmpAlaFecha
										+"] montoDepAmpDif: ["+montoDepAmpDif+"]");
					}
				}
			}
					
			// Total Finiquito
			montoFiniquito = montoEmp + montoAmpEmp + montoDepFiniquitoTot + montoDepAmpTot; 
			logger.info("   Calculos Finales");
			logger.info("   montoEmp: ["+montoEmp
							+"] montoAmpEmp: ["+montoAmpEmp
							+"] montoDepFiniquitoTot: ["+montoDepFiniquitoTot
							+"] montoDepAmpTot: ["+montoDepAmpTot
							+"] montoFiniquito: ["+montoFiniquito+"]");
	
			finiquito.setNombreEmpSolicitado(empleadoTO.getNombre()+" "+empleadoTO.getApellidoP()+" "+empleadoTO.getApellidoM());
			finiquito.setIdSol(solicitudTO.getIdSol());
			finiquito.setMontoEmp(Redondeos.roundNum(montoEmp));
			finiquito.setMontoEmpAmp(Redondeos.roundNum(montoAmpEmp));
			finiquito.setMontoDep(Redondeos.roundNum(montoDepFiniquitoTot));
			finiquito.setMontoDepAmp(Redondeos.roundNum(montoDepAmpTot));
			finiquito.setMontoFiniquito(Redondeos.roundNum(montoFiniquito));
			return finiquito;
		}
		
		public void setFiniquito(FiniquitoTO finiquitoTO){
			finiquitoDao.setFiniquito(finiquitoTO);
			finiquitoDao.setSolicitud(finiquitoTO,
					Codigo.TIPO_SOL_BAJA,
					Codigo.EST_SOL_BAJA_X_FINIQUITO);
			finiquitoDao.setDependiente(finiquitoTO.getIdSol(),
					Codigo.TIPO_SOL_BAJA,
					Codigo.EST_SOL_BAJA_X_FINIQUITO,
					finiquitoTO.getFechaFiniquito());
			finiquitoDao.setConsulmed(finiquitoTO.getIdSol(),
					Codigo.TIPO_SOL_BAJA,
					Codigo.EST_SOL_BAJA_X_FINIQUITO,
					finiquitoTO.getFechaFiniquito());
		}
		
		
		@Autowired
		public void setFiniquitoDao(FiniquitoDao finiquitoDao) {
			this.finiquitoDao = finiquitoDao;
		}

		@Autowired
		public void setSolAltaService(SolAltaService solAltaService) {
			this.solAltaService = solAltaService;
		}

		@Autowired
		public void setSolConsultaService(SolConsultaService solConsultaService) {
			this.solConsultaService = solConsultaService;
		}	
		
}