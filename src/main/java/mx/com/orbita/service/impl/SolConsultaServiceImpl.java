package mx.com.orbita.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.SolConsultaDao;
import mx.com.orbita.service.SolConsultaService;
import mx.com.orbita.to.AmpliacionTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;
import mx.com.orbita.util.Codigo;
import mx.com.orbita.util.Fechas;

@Component
public class SolConsultaServiceImpl implements SolConsultaService{
	
	private static final Logger logger = Logger.getLogger(SolConsultaServiceImpl.class);
	
	private SolConsultaDao solConsultaDao;
	
	// EMPLEADO
	public EmpleadoTO getEmpleadoBD(Integer numEmp) {
		EmpleadoTO empleadoTO = new EmpleadoTO(); 
		Fechas fechas = new Fechas();
		empleadoTO = solConsultaDao.getEmpleadoBD(numEmp);
		empleadoTO.setFechaActualF(fechas.formatoFechas(new Date(),"EEE, MMM dd yyyy").toUpperCase());
		empleadoTO.setFechaIngF(fechas.formatoFechas(empleadoTO.getFechaIng(),"EEE, MMM dd yyyy").toUpperCase());
		empleadoTO.setFechaNacF(fechas.formatoFechas(empleadoTO.getFechaNac(),"EEE, MMM dd yyyy").toUpperCase());
		return empleadoTO;
	}
	
	
	// SOLICITUD
	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB) {
		return solConsultaDao.getSolicitud(numEmp, tipoSol, estatusSolA, estatusSolB);
	}
	
	public SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol){
		return solConsultaDao.getSolicitud(numEmp, tipoSol, estatusSol);
	}
	
	public SolicitudTO getSolicitudCob(Integer numEmp, Integer tipoSol, Integer estatusSol) {
		SolicitudTO solicitudTO = new SolicitudTO();
		solicitudTO = solConsultaDao.getSolicitudCob(numEmp, tipoSol, estatusSol);
		return solicitudTO;
	}
	
	public SolicitudTO getSolicitudBaja(Integer numEmp) {
		SolicitudTO solicitudTO = new SolicitudTO();
		solicitudTO = solConsultaDao.getSolicitudBaja(numEmp, 
				Codigo.TIPO_SOL_ALTA,
				Codigo.EST_SOL_BAJA_EN_PROCESO,
				Codigo.TIPO_SOL_BAJA, 
				Codigo.EST_SOL_BAJA_AUTORIZADA, 
				Codigo.EST_SOL_BAJA_TOTAL,
				Codigo.EST_SOL_BAJA_X_FINIQUITO);
		return solicitudTO;
	}
	
	public Integer getSolicitudId(Integer numEmp, Integer tipoSol, Integer estatusSolA, Integer estatusSolB){
		return solConsultaDao.getSolicitudId(numEmp, tipoSol, estatusSolA, estatusSolB);
	}
	
	public List<AmpliacionTO> getTipoCob(){
		return solConsultaDao.getTipoCob();
	}
	
	public List<AmpliacionTO> getTipoCobMem(){
		return solConsultaDao.getTipoCobMem();
	}
	
	public String getTipoCob(Integer idTipoCob){
		return solConsultaDao.getTipoCob(idTipoCob);
	}
	
	// DEPENDIENTE
	public List<DependienteTO> getDependiente(Integer idSolicitud, Integer tipoSol,Integer estatusSolA, Integer estatusSolB) {
		return solConsultaDao.getDependiente(idSolicitud, tipoSol, estatusSolA, estatusSolB);
	}
	
	public List<DependienteTO> getDependienteRenov(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB){
		return solConsultaDao.getDependienteRenov(idSolicitud, tipoSol, estatusSolA, estatusSolB);
	}
	
	public List<Integer> getIdDependiente(Integer idSolicitud, Integer tipoSol){
		return solConsultaDao.getIdDependiente(idSolicitud, tipoSol,Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_RECHAZADA);
	}
	
	public DependienteTO getDependienteAltaDetalle(Integer idDependiente){
		return solConsultaDao.getDependienteAlta(idDependiente);
	}
	
	public DependienteTO getDependienteBajaDetalle(Integer idDependiente){
		return solConsultaDao.getDependienteBaja(idDependiente);
	}
	
	public List<DependienteTO> getDependienteConsultaBaja(Integer idSolicitud) {
		return solConsultaDao.getDependienteBaja(idSolicitud, Codigo.TIPO_SOL_ALTA, Codigo.TIPO_SOL_BAJA, 
				Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_BAJA_EN_PROCESO, Codigo.EST_SOL_BAJA);
	}
	
	public List<DependienteTO> getDependienteBaja(Integer idSolicitud) {
		return solConsultaDao.getDependienteBaja(idSolicitud, Codigo.TIPO_SOL_ALTA, Codigo.TIPO_SOL_ALTA, 
				Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_BAJA);
	}
	
	public List<Integer> getDependienteBajaxId(Integer idSolicitud){
		return solConsultaDao.getDependienteBajaxId(idSolicitud, 
				Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_BAJA_EN_PROCESO, 
				Codigo.TIPO_SOL_BAJA, Codigo.EST_SOL_BAJA, Codigo.EST_SOL_BAJA_RECHAZADA, Codigo.EST_SOL_BAJA_TOTAL,Codigo.EST_SOL_BAJA_X_FINIQUITO);
	}
	
	
	// CONSULMED
	public List<ConsulmedTO> getConsulmed(Integer idSolicitud, Integer tipoSol, Integer estatusSolA, Integer estatusSolB) {
		return solConsultaDao.getConsulmed(idSolicitud, tipoSol, estatusSolA, estatusSolB);
	}
	
	public List<Integer> getIdConsulmed(Integer idSolicitud, Integer tipoSol){
		return solConsultaDao.getIdConsulmed(idSolicitud, tipoSol, Codigo.EST_SOL_ALTA_EN_PROCESO, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_RECHAZADA);
	}
	
	public ConsulmedTO getConsulmedAltaDetalle(Integer idDependiente){
		return solConsultaDao.getConsulmedAlta(idDependiente);
	}
	
	public ConsulmedTO getConsulmedBajaDetalle(Integer idDependiente){
		return solConsultaDao.getConsulmedBaja(idDependiente);
	}
	
	public List<ConsulmedTO> getConsulmedConsultaBaja(Integer idSolicitud) {
		return solConsultaDao.getConsulmedBaja(idSolicitud, Codigo.TIPO_SOL_ALTA, Codigo.TIPO_SOL_BAJA, 
				Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_BAJA_EN_PROCESO, Codigo.EST_SOL_BAJA);
	}
	
	public List<ConsulmedTO> getConsulmedBaja(Integer idSolicitud) {
		return solConsultaDao.getConsulmedBaja(idSolicitud, Codigo.TIPO_SOL_ALTA, Codigo.TIPO_SOL_ALTA, 
				Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_ALTA_AUTORIZADA, Codigo.EST_SOL_BAJA);
	}
	
	public List<Integer> getConsulmedBajaxId(Integer idSolicitud){
		return solConsultaDao.getConsulmedBajaxId(idSolicitud, Codigo.TIPO_SOL_ALTA, Codigo.EST_SOL_BAJA_EN_PROCESO, 
				Codigo.TIPO_SOL_BAJA, Codigo.EST_SOL_BAJA, Codigo.EST_SOL_BAJA_RECHAZADA, Codigo.EST_SOL_BAJA_TOTAL,Codigo.EST_SOL_BAJA_X_FINIQUITO);
	}
	
	public List<ConsulmedTO> getConsulmedDisponibles(Integer numEmp){
		List<ConsulmedTO> listCon = new ArrayList<ConsulmedTO>();
		//Integer totalSol = solConsultaDao.getConsulmedTotal(Codigo.TIPO_SOL_ALTA, numEmp, Codigo.EST_SOL_ALTA_AUTORIZADA);
		Integer totalSol = solConsultaDao.getConsulmedTotal(Codigo.TIPO_SOL_ALTA, 
															Codigo.EST_SOL_ALTA_AUTORIZADA, 
															numEmp, 
															Codigo.EST_SOL_ALTA_AUTORIZADA,
															Codigo.EST_SOL_BAJA_EN_PROCESO);
		
		if(totalSol>0){
			// Casillas de Dependientes Consulmed libres restando los actuales 
			List<Integer> listaParientes = solConsultaDao.getConsulmedIdParentesco(numEmp,
															Codigo.TIPO_SOL_ALTA,
															Codigo.EST_SOL_ALTA_EN_PROCESO, 
															Codigo.EST_SOL_ALTA_AUTORIZADA,
															Codigo.EST_SOL_ALTA_RECHAZADA,
															Codigo.EST_SOL_BAJA,
															Codigo.EST_SOL_BAJA_RECHAZADA,
															Codigo.EST_SOL_BAJA_AUTORIZADA,
															Codigo.EST_SOL_BAJA_TOTAL,
															Codigo.EST_SOL_BAJA_X_FINIQUITO);
			if(!listaParientes.contains(1)){
				logger.info("  Se agrega pariente Padre");
				ConsulmedTO dependPadre = new ConsulmedTO();
				dependPadre.setIdParentesco(1);
				dependPadre.setParentesco("PADRE");
				dependPadre.setIdSexo(1);
				dependPadre.setSexo("M");
				dependPadre.setCostoConsul(0.00);
				listCon.add(dependPadre);
			}
			if(!listaParientes.contains(2)){
				logger.info("  Se agrega pariente Madre");
				ConsulmedTO dependMadre = new ConsulmedTO();
				dependMadre.setIdParentesco(2);
				dependMadre.setParentesco("MADRE");
				dependMadre.setIdSexo(2);
				dependMadre.setSexo("F");
				dependMadre.setCostoConsul(0.00);
				listCon.add(dependMadre);
			}
			if(!listaParientes.contains(7)){
				logger.info("  Se agrega pariente Suegro");
				ConsulmedTO dependSuegro = new ConsulmedTO();
				dependSuegro.setIdParentesco(7);
				dependSuegro.setParentesco("SUEGRO");
				dependSuegro.setIdSexo(1);
				dependSuegro.setSexo("M");
				dependSuegro.setCostoConsul(0.00);
				listCon.add(dependSuegro);
			}
			if(!listaParientes.contains(8)){
				logger.info("  Se agrega pariente Suegra");
				ConsulmedTO dependSuegra = new ConsulmedTO();
				dependSuegra.setIdParentesco(8);
				dependSuegra.setParentesco("SUEGRA");
				dependSuegra.setIdSexo(2);
				dependSuegra.setSexo("F");
				dependSuegra.setCostoConsul(0.00);
				listCon.add(dependSuegra);
			}
			logger.info("  Se agregaron casillas disponibles para parientes no registrados "+listCon.size());
		}else if(totalSol==0){
			// Ningun Dependiente Consulmed Registrado
			ConsulmedTO dependMadre = new ConsulmedTO();
			ConsulmedTO dependPadre = new ConsulmedTO();
			ConsulmedTO dependSuegra = new ConsulmedTO();
			ConsulmedTO dependSuegro = new ConsulmedTO();
			
			dependPadre.setIdParentesco(1);
			dependPadre.setParentesco("PADRE");
			dependPadre.setIdSexo(1);
			dependPadre.setSexo("M");
			dependPadre.setCostoConsul(0.00);
			
			dependMadre.setIdParentesco(2);
			dependMadre.setParentesco("MADRE");
			dependMadre.setIdSexo(2);
			dependMadre.setSexo("F");
			dependMadre.setCostoConsul(0.00);
			
			dependSuegro.setIdParentesco(7);
			dependSuegro.setParentesco("SUEGRO");
			dependSuegro.setIdSexo(1);
			dependSuegro.setSexo("M");
			dependSuegro.setCostoConsul(0.00);
			
			dependSuegra.setIdParentesco(8);
			dependSuegra.setParentesco("SUEGRA");
			dependSuegra.setIdSexo(2);
			dependSuegra.setSexo("F");
			dependSuegra.setCostoConsul(0.00);
			
			listCon.add(dependMadre);
			listCon.add(dependPadre);
			listCon.add(dependSuegra);
			listCon.add(dependSuegro);
			logger.info("  Ningun pariente registrado, todos los lugares disponibles: "+listCon.size());
		}
		return listCon;
	}
	
	// MEMORIAL
	public List<ConsulmedTO> getMemorial(Integer idSolicitud) {
		return solConsultaDao.getMemorial(idSolicitud);
	}
	
	@Autowired
	public void setSolConsultaDao(SolConsultaDao solConsultaDao) {
		this.solConsultaDao = solConsultaDao;
	}
	
}