package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.CircularTO;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;


public interface CircularService {
	
	void setRenovacion(CircularTO circularTO);
	void setMemorial(CircularTO circularTO);
	void setDependienteMemorial(List<ConsulmedTO> list);
	List<DependienteTO> getDependiente(DependienteTO dependienteTO);

}
