package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.CoberturaTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface CambioCoberturaService {
	
	EmpleadoTO getEmpleado(Integer numEmp);
	SolicitudTO getSolicitud(Integer numEmp, Integer tipoSol, Integer estatusSol);
	double getCambioCobertura(Integer tipoAmpl, Integer numDep, Integer mes, Integer dia);
	double getCambioCoberturaIndiv(Integer tipoAmpl, Integer numDep, Integer mes, Integer dia);
	Integer getDependienteProceso(Integer numEmp);
	List<DependienteTO> getDependiente(Integer numEmp);
	void setCambioCobertura(CoberturaTO coberturaTO);
	
}
