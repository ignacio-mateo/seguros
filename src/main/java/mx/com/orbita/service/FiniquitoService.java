package mx.com.orbita.service;

import mx.com.orbita.to.FiniquitoTO;

public interface FiniquitoService {
	
	Integer getIdSolicitud(Integer numEmp);
	FiniquitoTO getFiniquito(Integer numEmp);
	FiniquitoTO calcularFiniquito(Integer numEmp, Integer mes, Integer dia);
	void setFiniquito(FiniquitoTO finiquitoTO);

}
