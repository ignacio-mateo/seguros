package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface SolAltaService {
	
	double getMontoDependiente(Integer tipoEmp, Integer parentesco, String fecha, String sexo);
	double getMontoDependiente(Integer tipoEmp, Integer parentesco, String fecha, String sexo, int mes, int dia);
	double getMontoAmpliacion(int tipoAmpl, int numDep);
	double getMontoAmpliacion(int tipoAmpl, int numDep, int mes, int dia);
	double getMontoAmpliacionIndividual(int tipoAmpl, int numDep);
	double getMontoAmpliacionIndividual(int tipoAmpl, int numDep, int mes, int dia);
	double getCalculaEdad(Integer parentesco, String fecha);
	double getMontoMemorial(int tipoMemorial);
	EmpleadoTO getEmpleadoWS(Integer numEmp);
	EmpleadoTO getCotizacion(EmpleadoTO empleado);
	EmpleadoTO getCotizacion(EmpleadoTO empleado, int mes, int dia);
	List<DependienteTO> getListaDependiente();
	List<ConsulmedTO> getListaConsulmed();
	void setSolicitud(EmpleadoTO empleadoTO,SolicitudTO solicitudTO,List<DependienteTO> listDep,List<ConsulmedTO> listCon,List<ConsulmedTO> listMem);
	void setConsulmed(List<ConsulmedTO> listCon, Integer idSolicitud);
	void setDependiente(List<DependienteTO> listDep, Integer idSolicitud);
	
}
