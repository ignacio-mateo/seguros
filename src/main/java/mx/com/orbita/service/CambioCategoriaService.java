package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.controller.form.CategoriaForm;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.EmpTO;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.SolicitudTO;

public interface CambioCategoriaService {
	
	Integer getSolicitudExiste(Integer numEmp, Integer idTipoSol, Integer idEstSol);
	Integer getDependienteProceso(Integer numEmp);
	CategoriaForm getCategoriaActual(Integer numEmp);
	CategoriaForm getCategoriaTransitoria(Integer numEmp, Integer mes, Integer dia);
	CategoriaForm getCambioCategoria(Integer numEmp,Integer mes, Integer dia);
	void setCambioCategoria(EmpleadoTO empleadoTO, EmpTO empTO, SolicitudTO solicitudTO, List<DependienteTO> listDep);

}
