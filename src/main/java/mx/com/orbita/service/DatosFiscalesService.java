package mx.com.orbita.service;

import mx.com.orbita.to.DatFiscalesTO;

public interface DatosFiscalesService {
	
		DatFiscalesTO getDatosFiscales(Integer numEmp);
		void setDatosFiscales(DatFiscalesTO datos);

}
