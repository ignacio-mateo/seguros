package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.ContactoTO;

public interface ContactoService {
	
	List<ContactoTO> getContacto();

}
