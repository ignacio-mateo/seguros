package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.ConstanciaTO;
import mx.com.orbita.to.DependienteTO;

public interface AdminConstanciaService {
	
	ConstanciaTO getEmpleado(Integer numEmp);
	List<DependienteTO> getDependiente(Integer numEmp, Integer tipoSol, Integer estSol);

}
