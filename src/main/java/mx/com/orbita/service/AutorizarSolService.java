package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.AutorizacionTO;

public interface AutorizarSolService {
	
	List<AutorizacionTO> getProcesarSolicitudes(Integer idOperacion, Integer tipoSol, Integer estatusSol);
	List<AutorizacionTO> getProcesarSolicitudesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol);
	List<AutorizacionTO> getProcesarDependientes(Integer idOperacion, Integer tipoSol, Integer estatusSol, Integer estatusCicloSol, Integer estatusDep);
	List<AutorizacionTO> getProcesarDependientesBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol, Integer estatusCicloSol, Integer estatusDep); 
	List<AutorizacionTO> getProcesarConsulmed(Integer idOperacion, Integer tipoSol, Integer estatusSol, Integer estatusCicloSol, Integer estatusDep);
	List<AutorizacionTO> getProcesarConsulmedBaja(Integer idOperacion, Integer tipoSol, Integer estatusSol, Integer estatusCicloSol, Integer estatusDep);
	void setProcesarSolAlta(List<AutorizacionTO> lista, Integer usuarioAdmin);
	void setProcesarSolBaja(List<AutorizacionTO> lista, Integer usuarioAdmin);
	void setProcesarDepAlta(List<AutorizacionTO> lista, Integer usuarioAdmin);
	void setProcesarDepBaja(List<AutorizacionTO> lista, Integer usuarioAdmin);
	void setProcesarConAlta(List<AutorizacionTO> lista, Integer usuarioAdmin);
	void setProcesarConBaja(List<AutorizacionTO> lista, Integer usuarioAdmin);

}
