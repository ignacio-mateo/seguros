package mx.com.orbita.service;

public interface SistemaService {
	
	Integer existeSolicitud(Integer numEmp);
	Integer existeSolicitudAlta(Integer numEmp);
	Integer existeSolicitudBaja(Integer numEmp);
	Integer existeSolicitudBajaDep(Integer numEmp);
	Integer existeSolicitudAut(Integer numEmp);
	Integer getPerfilAdmin(Integer numEmp);

}
