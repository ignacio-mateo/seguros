package mx.com.orbita.service;

import mx.com.orbita.to.FechaAntiguedadTO;

public interface FechaAntiguedadService {
	
	Integer existeSolicitud(Integer numEmp);
	void setFechaAntiguedad(FechaAntiguedadTO fechaAntiguedadTO);

}
