package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.ConsulmedTO;
import mx.com.orbita.to.DependienteTO;

public interface SolBajaService {
	
	void setSolicitud(Integer numEmp, Integer sol);
	void setDependiente(List<DependienteTO> lista);
	void setConsulmed(List<ConsulmedTO> lista);
	
}
