package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.DependienteTO;
import mx.com.orbita.to.ReembolsoConceptoTO;
import mx.com.orbita.to.ReembolsoRelacionTO;

public interface ReembolsoService {
	
	ReembolsoRelacionTO getReembolso(Integer numEmp,Integer idReembolso);
	List<Integer> getReembolsoListID(Integer numEmp);
	List<ReembolsoConceptoTO> getReembolsoConceptos(Integer idReembolso);
	void setReembolso(List<ReembolsoConceptoTO> lista,ReembolsoRelacionTO reembolsoRelacionTO, Integer numEmp);
	List<ReembolsoConceptoTO> getListaConceptosReembolsos();
	List<DependienteTO> getDependientes(Integer numEmp, Integer tipoSol, Integer estatusSol);
}
