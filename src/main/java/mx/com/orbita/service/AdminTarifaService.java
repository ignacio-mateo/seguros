package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.TarifaTO;

public interface AdminTarifaService {
	
	List<TarifaTO> getTarifaConfianza();
	List<TarifaTO> getTarifaSindicalizado();
	List<TarifaTO> getTarifaAmplia();
	void setTarifaConfianza(List<TarifaTO> lista);
	void setTarifaSindicalizado(List<TarifaTO> lista);
	void setTarifaAmplia(List<TarifaTO> lista);

}
