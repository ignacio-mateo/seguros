package mx.com.orbita.to;

import java.util.Date;

public class AutorizacionTO {
	
	private Integer idSolPrimaria;
	private Integer idSolSecundaria;
	private Integer idOperacion;
	private Integer idTipoSolicitud;
	private Integer idEmpleado;
	private Integer idTipoEmp;
	private String nombre;
	private String fechaElaboracion;
	private String fechaCancelacion;
	private String comentario;
	private String autorizar;
	private String rechazar;
	private String idUsuario;
	private Integer idTipoCob;
	private Integer numDep;
	private Double costoEmp;
	private Double costoDep;
	private Double costoTotDep;
	private Double costoTotal;
	private Double costoAmpl;
	private Double costoAmplInd;
	private Integer idParentesco;
	private String sexo;
	private String fechaNac;
	private Date fechaNacD;
	private Integer idTipoCobMem;
	
	public Integer getIdSolPrimaria() {
		return idSolPrimaria;
	}
	public void setIdSolPrimaria(Integer idSolPrimaria) {
		this.idSolPrimaria = idSolPrimaria;
	}
	public Integer getIdSolSecundaria() {
		return idSolSecundaria;
	}
	public void setIdSolSecundaria(Integer idSolSecundaria) {
		this.idSolSecundaria = idSolSecundaria;
	}
	public Integer getIdOperacion() {
		return idOperacion;
	}
	public void setIdOperacion(Integer idOperacion) {
		this.idOperacion = idOperacion;
	}
	public Integer getIdTipoSolicitud() {
		return idTipoSolicitud;
	}
	public void setIdTipoSolicitud(Integer idTipoSolicitud) {
		this.idTipoSolicitud = idTipoSolicitud;
	}
	public Integer getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public Integer getIdTipoEmp() {
		return idTipoEmp;
	}
	public void setIdTipoEmp(Integer idTipoEmp) {
		this.idTipoEmp = idTipoEmp;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFechaElaboracion() {
		return fechaElaboracion;
	}
	public void setFechaElaboracion(String fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getAutorizar() {
		return autorizar;
	}
	public void setAutorizar(String autorizar) {
		this.autorizar = autorizar;
	}
	public String getRechazar() {
		return rechazar;
	}
	public void setRechazar(String rechazar) {
		this.rechazar = rechazar;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getIdTipoCob() {
		return idTipoCob;
	}
	public void setIdTipoCob(Integer idTipoCob) {
		this.idTipoCob = idTipoCob;
	}
	public Integer getNumDep() {
		return numDep;
	}
	public void setNumDep(Integer numDep) {
		this.numDep = numDep;
	}
	public Double getCostoEmp() {
		return costoEmp;
	}
	public void setCostoEmp(Double costoEmp) {
		this.costoEmp = costoEmp;
	}
	public Double getCostoDep() {
		return costoDep;
	}
	public void setCostoDep(Double costoDep) {
		this.costoDep = costoDep;
	}
	public Double getCostoTotDep() {
		return costoTotDep;
	}
	public void setCostoTotDep(Double costoTotDep) {
		this.costoTotDep = costoTotDep;
	}
	public Double getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(Double costoTotal) {
		this.costoTotal = costoTotal;
	}
	public Integer getIdParentesco() {
		return idParentesco;
	}
	public void setIdParentesco(Integer idParentesco) {
		this.idParentesco = idParentesco;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public Double getCostoAmpl() {
		return costoAmpl;
	}
	public void setCostoAmpl(Double costoAmpl) {
		this.costoAmpl = costoAmpl;
	}
	public Date getFechaNacD() {
		return fechaNacD;
	}
	public void setFechaNacD(Date fechaNacD) {
		this.fechaNacD = fechaNacD;
	}
	public Double getCostoAmplInd() {
		return costoAmplInd;
	}
	public void setCostoAmplInd(Double costoAmplInd) {
		this.costoAmplInd = costoAmplInd;
	}
	public String getFechaCancelacion() {
		return fechaCancelacion;
	}
	public void setFechaCancelacion(String fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}
	public Integer getIdTipoCobMem() {
		return idTipoCobMem;
	}
	public void setIdTipoCobMem(Integer idTipoCobMem) {
		this.idTipoCobMem = idTipoCobMem;
	}
	
}
