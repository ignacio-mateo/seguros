package mx.com.orbita.to;

import java.util.Date;

public class DependienteTO {

	private Integer idDependiente;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private Integer idSexo;
	private String sexo;
	private Date fecha;
	private String fechaNac;
	private String fechaAlta;
	private String fechaAltaAut;
	private String fechaBaja;
	private String fechaBajaAut;
	private Double costoDep;
	private Double costoAmpDep;
	private Double costoCancelacion;
	private Integer idParentesco;
	private String parentesco;
	private Integer idEstatusDep;
	private String estatusDep;
	private Integer idTipoSol;
	private String tipoSol;
	private Integer idSolicitud;
	private Integer idAdminAutAlta;
	private String adminAutAlta;
	private Integer idAdminAutBaja;
	private String adminAutBaja;
	private String comentario;
	private String check;
	private Integer anioAut;
	private Integer mesAut;
	private Integer diaAut;
	private Integer anioRenov;
	private Integer mesRenov;
	private Integer diaRenov;
	
	public Integer getIdDependiente() {
		return idDependiente;
	}
	public void setIdDependiente(Integer idDependiente) {
		this.idDependiente = idDependiente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public Integer getIdSexo() {
		return idSexo;
	}
	public void setIdSexo(Integer idSexo) {
		this.idSexo = idSexo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getFechaAltaAut() {
		return fechaAltaAut;
	}
	public void setFechaAltaAut(String fechaAltaAut) {
		this.fechaAltaAut = fechaAltaAut;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public String getFechaBajaAut() {
		return fechaBajaAut;
	}
	public void setFechaBajaAut(String fechaBajaAut) {
		this.fechaBajaAut = fechaBajaAut;
	}
	public Double getCostoDep() {
		return costoDep;
	}
	public void setCostoDep(Double costoDep) {
		this.costoDep = costoDep;
	}
	public Double getCostoCancelacion() {
		return costoCancelacion;
	}
	public void setCostoCancelacion(Double costoCancelacion) {
		this.costoCancelacion = costoCancelacion;
	}
	public Integer getIdParentesco() {
		return idParentesco;
	}
	public void setIdParentesco(Integer idParentesco) {
		this.idParentesco = idParentesco;
	}
	public String getParentesco() {
		return parentesco;
	}
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}
	public Integer getIdEstatusDep() {
		return idEstatusDep;
	}
	public void setIdEstatusDep(Integer idEstatusDep) {
		this.idEstatusDep = idEstatusDep;
	}
	public String getEstatusDep() {
		return estatusDep;
	}
	public void setEstatusDep(String estatusDep) {
		this.estatusDep = estatusDep;
	}
	public Integer getIdTipoSol() {
		return idTipoSol;
	}
	public void setIdTipoSol(Integer idTipoSol) {
		this.idTipoSol = idTipoSol;
	}
	public String getTipoSol() {
		return tipoSol;
	}
	public void setTipoSol(String tipoSol) {
		this.tipoSol = tipoSol;
	}
	public Integer getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public Integer getIdAdminAutAlta() {
		return idAdminAutAlta;
	}
	public void setIdAdminAutAlta(Integer idAdminAutAlta) {
		this.idAdminAutAlta = idAdminAutAlta;
	}
	public String getAdminAutAlta() {
		return adminAutAlta;
	}
	public void setAdminAutAlta(String adminAutAlta) {
		this.adminAutAlta = adminAutAlta;
	}
	public Integer getIdAdminAutBaja() {
		return idAdminAutBaja;
	}
	public void setIdAdminAutBaja(Integer idAdminAutBaja) {
		this.idAdminAutBaja = idAdminAutBaja;
	}
	public String getAdminAutBaja() {
		return adminAutBaja;
	}
	public void setAdminAutBaja(String adminAutBaja) {
		this.adminAutBaja = adminAutBaja;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public Integer getAnioAut() {
		return anioAut;
	}
	public void setAnioAut(Integer anioAut) {
		this.anioAut = anioAut;
	}
	public Integer getMesAut() {
		return mesAut;
	}
	public void setMesAut(Integer mesAut) {
		this.mesAut = mesAut;
	}
	public Integer getDiaAut() {
		return diaAut;
	}
	public void setDiaAut(Integer diaAut) {
		this.diaAut = diaAut;
	}
	public Integer getAnioRenov() {
		return anioRenov;
	}
	public void setAnioRenov(Integer anioRenov) {
		this.anioRenov = anioRenov;
	}
	public Integer getMesRenov() {
		return mesRenov;
	}
	public void setMesRenov(Integer mesRenov) {
		this.mesRenov = mesRenov;
	}
	public Integer getDiaRenov() {
		return diaRenov;
	}
	public void setDiaRenov(Integer diaRenov) {
		this.diaRenov = diaRenov;
	}
	public Double getCostoAmpDep() {
		return costoAmpDep;
	}
	public void setCostoAmpDep(Double costoAmpDep) {
		this.costoAmpDep = costoAmpDep;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
}
