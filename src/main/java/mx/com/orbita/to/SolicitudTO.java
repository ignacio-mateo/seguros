package mx.com.orbita.to;

import java.util.Date;

public class SolicitudTO{
	
	private Integer idSol;
	private Integer numEmp;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private Double costoEmpleado;
	private Double costoDependiente;
	private Double costoAmpliacion;
	private Double costoTotal;
	private Double costoTotalAnt;
	private Double costoFiniquito;
	private Double costoProrrateado;
	private Double costoTotalAct;
	private Integer idEstatusSol;
	private String estatusSol;
	private Integer idTipoSolicitud;
	private String tipoSolicitud;
	private Integer idTipoCobertura;
	private String tipoCobertura;
	private Date fechaElab;
	private Date fechaAut;
	private String fechaElaboracion;
	private String fechaAutorizacion;
	private String fechaRenovacion;
	private Date fechaCanc;
	private Date fechaCancAut;
	private String fechaCancelacion;
	private String fechaCancelacionAut;
	private Integer idAdminAutAlta;
	private String adminAutAlta;
	private Integer idAdminAutBaja;
	private String adminAutBaja;
	private String comentario;
	private String region;
	private Integer diaAut;
	private Integer mesAut;
	private Integer anioAut;
	private Integer diaRenov;
	private Integer mesRenov;
	private Integer anioRenov;
	private Integer idTipoMemorial;
	private String tipoMemorial;
	private Double costoMemorial;
	
	public Integer getIdSol() {
		return idSol;
	}
	public void setIdSol(Integer idSol) {
		this.idSol = idSol;
	}
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public Double getCostoEmpleado() {
		return costoEmpleado;
	}
	public void setCostoEmpleado(Double costoEmpleado) {
		this.costoEmpleado = costoEmpleado;
	}
	public Double getCostoDependiente() {
		return costoDependiente;
	}
	public void setCostoDependiente(Double costoDependiente) {
		this.costoDependiente = costoDependiente;
	}
	public Double getCostoAmpliacion() {
		return costoAmpliacion;
	}
	public void setCostoAmpliacion(Double costoAmpliacion) {
		this.costoAmpliacion = costoAmpliacion;
	}
	public Double getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(Double costoTotal) {
		this.costoTotal = costoTotal;
	}
	public Double getCostoFiniquito() {
		return costoFiniquito;
	}
	public void setCostoFiniquito(Double costoFiniquito) {
		this.costoFiniquito = costoFiniquito;
	}
	public Integer getIdEstatusSol() {
		return idEstatusSol;
	}
	public void setIdEstatusSol(Integer idEstatusSol) {
		this.idEstatusSol = idEstatusSol;
	}
	public String getEstatusSol() {
		return estatusSol;
	}
	public void setEstatusSol(String estatusSol) {
		this.estatusSol = estatusSol;
	}
	public Integer getIdTipoSolicitud() {
		return idTipoSolicitud;
	}
	public void setIdTipoSolicitud(Integer idTipoSolicitud) {
		this.idTipoSolicitud = idTipoSolicitud;
	}
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}
	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	public Integer getIdTipoCobertura() {
		return idTipoCobertura;
	}
	public void setIdTipoCobertura(Integer idTipoCobertura) {
		this.idTipoCobertura = idTipoCobertura;
	}
	public String getTipoCobertura() {
		return tipoCobertura;
	}
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	public Date getFechaElab() {
		return fechaElab;
	}
	public void setFechaElab(Date fechaElab) {
		this.fechaElab = fechaElab;
	}
	public Date getFechaAut() {
		return fechaAut;
	}
	public void setFechaAut(Date fechaAut) {
		this.fechaAut = fechaAut;
	}
	public String getFechaElaboracion() {
		return fechaElaboracion;
	}
	public void setFechaElaboracion(String fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}
	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	public Date getFechaCanc() {
		return fechaCanc;
	}
	public void setFechaCanc(Date fechaCanc) {
		this.fechaCanc = fechaCanc;
	}
	public Date getFechaCancAut() {
		return fechaCancAut;
	}
	public void setFechaCancAut(Date fechaCancAut) {
		this.fechaCancAut = fechaCancAut;
	}
	public String getFechaCancelacion() {
		return fechaCancelacion;
	}
	public void setFechaCancelacion(String fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}
	public String getFechaCancelacionAut() {
		return fechaCancelacionAut;
	}
	public void setFechaCancelacionAut(String fechaCancelacionAut) {
		this.fechaCancelacionAut = fechaCancelacionAut;
	}
	public Integer getIdAdminAutAlta() {
		return idAdminAutAlta;
	}
	public void setIdAdminAutAlta(Integer idAdminAutAlta) {
		this.idAdminAutAlta = idAdminAutAlta;
	}
	public String getAdminAutAlta() {
		return adminAutAlta;
	}
	public void setAdminAutAlta(String adminAutAlta) {
		this.adminAutAlta = adminAutAlta;
	}
	public Integer getIdAdminAutBaja() {
		return idAdminAutBaja;
	}
	public void setIdAdminAutBaja(Integer idAdminAutBaja) {
		this.idAdminAutBaja = idAdminAutBaja;
	}
	public String getAdminAutBaja() {
		return adminAutBaja;
	}
	public void setAdminAutBaja(String adminAutBaja) {
		this.adminAutBaja = adminAutBaja;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public Integer getDiaAut() {
		return diaAut;
	}
	public void setDiaAut(Integer diaAut) {
		this.diaAut = diaAut;
	}
	public Integer getMesAut() {
		return mesAut;
	}
	public void setMesAut(Integer mesAut) {
		this.mesAut = mesAut;
	}
	public Integer getAnioAut() {
		return anioAut;
	}
	public void setAnioAut(Integer anioAut) {
		this.anioAut = anioAut;
	}
	public Integer getDiaRenov() {
		return diaRenov;
	}
	public void setDiaRenov(Integer diaRenov) {
		this.diaRenov = diaRenov;
	}
	public Integer getMesRenov() {
		return mesRenov;
	}
	public void setMesRenov(Integer mesRenov) {
		this.mesRenov = mesRenov;
	}
	public Integer getAnioRenov() {
		return anioRenov;
	}
	public void setAnioRenov(Integer anioRenov) {
		this.anioRenov = anioRenov;
	}
	public String getFechaRenovacion() {
		return fechaRenovacion;
	}
	public void setFechaRenovacion(String fechaRenovacion) {
		this.fechaRenovacion = fechaRenovacion;
	}
	public Double getCostoProrrateado() {
		return costoProrrateado;
	}
	public void setCostoProrrateado(Double costoProrrateado) {
		this.costoProrrateado = costoProrrateado;
	}
	public Double getCostoTotalAct() {
		return costoTotalAct;
	}
	public void setCostoTotalAct(Double costoTotalAct) {
		this.costoTotalAct = costoTotalAct;
	}
	public Double getCostoTotalAnt() {
		return costoTotalAnt;
	}
	public void setCostoTotalAnt(Double costoTotalAnt) {
		this.costoTotalAnt = costoTotalAnt;
	}
	public Integer getIdTipoMemorial() {
		return idTipoMemorial;
	}
	public void setIdTipoMemorial(Integer idTipoMemorial) {
		this.idTipoMemorial = idTipoMemorial;
	}
	public Double getCostoMemorial() {
		return costoMemorial;
	}
	public void setCostoMemorial(Double costoMemorial) {
		this.costoMemorial = costoMemorial;
	}
	public String getTipoMemorial() {
		return tipoMemorial;
	}
	public void setTipoMemorial(String tipoMemorial) {
		this.tipoMemorial = tipoMemorial;
	}
		
}