package mx.com.orbita.to;

public class ReembolsoRelacionTO {
	
	private Integer idReembolso;
	private Integer tipoReembolso;
	private Integer idEstatus;
	private String nombreAfectado;
	private String descripcionEnfermedad;
	private String observaciones;
	private Double importeTotal;
	private String parentescoTitular;
	private String importeTotalFormat;
	
	public Integer getIdReembolso() {
		return idReembolso;
	}
	public void setIdReembolso(Integer idReembolso) {
		this.idReembolso = idReembolso;
	}
	public Integer getTipoReembolso() {
		return tipoReembolso;
	}
	public void setTipoReembolso(Integer tipoReembolso) {
		this.tipoReembolso = tipoReembolso;
	}
	public Integer getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(Integer idEstatus) {
		this.idEstatus = idEstatus;
	}
	public String getNombreAfectado() {
		return nombreAfectado;
	}
	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}
	public String getDescripcionEnfermedad() {
		return descripcionEnfermedad;
	}
	public void setDescripcionEnfermedad(String descripcionEnfermedad) {
		this.descripcionEnfermedad = descripcionEnfermedad;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Double getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(Double importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getParentescoTitular() {
		return parentescoTitular;
	}
	public void setParentescoTitular(String parentescoTitular) {
		this.parentescoTitular = parentescoTitular;
	}
	public String getImporteTotalFormat() {
		return importeTotalFormat;
	}
	public void setImporteTotalFormat(String importeTotalFormat) {
		this.importeTotalFormat = importeTotalFormat;
	}
		
}
