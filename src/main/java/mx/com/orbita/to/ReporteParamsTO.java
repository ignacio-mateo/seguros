package mx.com.orbita.to;

public class ReporteParamsTO {
	
	private Integer numEmp;
	private String nombreEmp;
	private Integer perfilEmp;
	private Integer opcionConsulta;
	private Integer reporte;
	private Integer tipo;
	private String fechaIni; 
	private String fechaFin;
	private Integer tipoEmp;
	private Integer tipoEmp1;
	private Integer tipoEmp2;
	private Integer tipoSol;
	private Integer estSol;
	private Integer estSolA;
	private Integer estSolB;
	private Integer estSolC;
	private Integer estSolD;
	private Integer tipoDep;
	private Integer estDep;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombreEmp() {
		return nombreEmp;
	}
	public void setNombreEmp(String nombreEmp) {
		this.nombreEmp = nombreEmp;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	public Integer getOpcionConsulta() {
		return opcionConsulta;
	}
	public void setOpcionConsulta(Integer opcionConsulta) {
		this.opcionConsulta = opcionConsulta;
	}
	public Integer getReporte() {
		return reporte;
	}
	public void setReporte(Integer reporte) {
		this.reporte = reporte;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Integer getTipoEmp() {
		return tipoEmp;
	}
	public void setTipoEmp(Integer tipoEmp) {
		this.tipoEmp = tipoEmp;
	}
	public Integer getTipoEmp1() {
		return tipoEmp1;
	}
	public void setTipoEmp1(Integer tipoEmp1) {
		this.tipoEmp1 = tipoEmp1;
	}
	public Integer getTipoEmp2() {
		return tipoEmp2;
	}
	public void setTipoEmp2(Integer tipoEmp2) {
		this.tipoEmp2 = tipoEmp2;
	}
	public Integer getTipoSol() {
		return tipoSol;
	}
	public void setTipoSol(Integer tipoSol) {
		this.tipoSol = tipoSol;
	}
	public Integer getEstSol() {
		return estSol;
	}
	public void setEstSol(Integer estSol) {
		this.estSol = estSol;
	}
	public Integer getTipoDep() {
		return tipoDep;
	}
	public void setTipoDep(Integer tipoDep) {
		this.tipoDep = tipoDep;
	}
	public Integer getEstDep() {
		return estDep;
	}
	public void setEstDep(Integer estDep) {
		this.estDep = estDep;
	}
	public Integer getEstSolB() {
		return estSolB;
	}
	public void setEstSolB(Integer estSolB) {
		this.estSolB = estSolB;
	}
	public Integer getEstSolC() {
		return estSolC;
	}
	public void setEstSolC(Integer estSolC) {
		this.estSolC = estSolC;
	}
	public Integer getEstSolD() {
		return estSolD;
	}
	public void setEstSolD(Integer estSolD) {
		this.estSolD = estSolD;
	}
	public Integer getEstSolA() {
		return estSolA;
	}
	public void setEstSolA(Integer estSolA) {
		this.estSolA = estSolA;
	}
	
	
}
