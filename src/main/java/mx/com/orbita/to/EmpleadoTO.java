package mx.com.orbita.to;

import java.util.Date;

public class EmpleadoTO{
	
	private Integer numEmp;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private String puesto;
	private Integer idRegion;
	private String region;
	private Integer idSexo;
	private String sexo;
	private Integer tipoEmp;
	private String descTipoEmp;
	private String departamento;
	private String rfc;
	private Date fechaActual;
	private String fechaActualF;
	private Date fechaIng;
	private String fechaIngF;
	private Date fechaNac;
	private String fechaNacF;
	private Double montoEmp;
	private Double montoEmpProrateado;
	private String montoEmpFormat;
	private Double montoAmp;
	private Double montoAmpProrateado;
	private String montoAmpFormat;
	private Integer existeSolicitud;
	private String descEmpleado;
	private String centro;
	private Integer idPerfil;
	private String viaPago;
	private String correo;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public Integer getIdRegion() {
		return idRegion;
	}
	public void setIdRegion(Integer idRegion) {
		this.idRegion = idRegion;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public Integer getIdSexo() {
		return idSexo;
	}
	public void setIdSexo(Integer idSexo) {
		this.idSexo = idSexo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Integer getTipoEmp() {
		return tipoEmp;
	}
	public void setTipoEmp(Integer tipoEmp) {
		this.tipoEmp = tipoEmp;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public Date getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}
	public String getFechaActualF() {
		return fechaActualF;
	}
	public void setFechaActualF(String fechaActualF) {
		this.fechaActualF = fechaActualF;
	}
	public Date getFechaIng() {
		return fechaIng;
	}
	public void setFechaIng(Date fechaIng) {
		this.fechaIng = fechaIng;
	}
	public String getFechaIngF() {
		return fechaIngF;
	}
	public void setFechaIngF(String fechaIngF) {
		this.fechaIngF = fechaIngF;
	}
	public Date getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getFechaNacF() {
		return fechaNacF;
	}
	public void setFechaNacF(String fechaNacF) {
		this.fechaNacF = fechaNacF;
	}
	public Double getMontoEmp() {
		return montoEmp;
	}
	public void setMontoEmp(Double montoEmp) {
		this.montoEmp = montoEmp;
	}
	public Double getMontoEmpProrateado() {
		return montoEmpProrateado;
	}
	public void setMontoEmpProrateado(Double montoEmpProrateado) {
		this.montoEmpProrateado = montoEmpProrateado;
	}
	public String getMontoEmpFormat() {
		return montoEmpFormat;
	}
	public void setMontoEmpFormat(String montoEmpFormat) {
		this.montoEmpFormat = montoEmpFormat;
	}
	public Double getMontoAmp() {
		return montoAmp;
	}
	public void setMontoAmp(Double montoAmp) {
		this.montoAmp = montoAmp;
	}
	public Double getMontoAmpProrateado() {
		return montoAmpProrateado;
	}
	public void setMontoAmpProrateado(Double montoAmpProrateado) {
		this.montoAmpProrateado = montoAmpProrateado;
	}
	public String getMontoAmpFormat() {
		return montoAmpFormat;
	}
	public void setMontoAmpFormat(String montoAmpFormat) {
		this.montoAmpFormat = montoAmpFormat;
	}
	public Integer getExisteSolicitud() {
		return existeSolicitud;
	}
	public void setExisteSolicitud(Integer existeSolicitud) {
		this.existeSolicitud = existeSolicitud;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getDescTipoEmp() {
		return descTipoEmp;
	}
	public void setDescTipoEmp(String descTipoEmp) {
		this.descTipoEmp = descTipoEmp;
	}
	public String getDescEmpleado() {
		return descEmpleado;
	}
	public void setDescEmpleado(String descEmpleado) {
		this.descEmpleado = descEmpleado;
	}
	public String getCentro() {
		return centro;
	}
	public void setCentro(String centro) {
		this.centro = centro;
	}
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public String getViaPago() {
		return viaPago;
	}
	public void setViaPago(String viaPago) {
		this.viaPago = viaPago;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
}
