package mx.com.orbita.to;

import java.util.Date;

public class ConstanciaTO {
		
	private Integer numEmp;
	private Integer idTipoEmp;
	private Integer idTipoSol;
	private Integer idEstatus;
	private Integer idRegion;
	private String tipoCobertura;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private Date fechaAut;
	private Date fechaCan;
	private String tipoEmp;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public Integer getIdTipoEmp() {
		return idTipoEmp;
	}
	public void setIdTipoEmp(Integer idTipoEmp) {
		this.idTipoEmp = idTipoEmp;
	}
	public Integer getIdTipoSol() {
		return idTipoSol;
	}
	public void setIdTipoSol(Integer idTipoSol) {
		this.idTipoSol = idTipoSol;
	}
	public Integer getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(Integer idEstatus) {
		this.idEstatus = idEstatus;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public Date getFechaAut() {
		return fechaAut;
	}
	public void setFechaAut(Date fechaAut) {
		this.fechaAut = fechaAut;
	}
	public Date getFechaCan() {
		return fechaCan;
	}
	public void setFechaCan(Date fechaCan) {
		this.fechaCan = fechaCan;
	}
	public String getTipoEmp() {
		return tipoEmp;
	}
	public void setTipoEmp(String tipoEmp) {
		this.tipoEmp = tipoEmp;
	}
	public Integer getIdRegion() {
		return idRegion;
	}
	public void setIdRegion(Integer idRegion) {
		this.idRegion = idRegion;
	}
	public String getTipoCobertura() {
		return tipoCobertura;
	}
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}	
	
}
