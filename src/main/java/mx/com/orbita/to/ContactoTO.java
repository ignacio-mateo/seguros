package mx.com.orbita.to;

public class ContactoTO{

	private Integer id;
	private String nombre;
	private String puesto;
	private Integer telefono;
	private Integer extension;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public Integer getExtension() {
		return extension;
	}
	public void setExtension(Integer extension) {
		this.extension = extension;
	}	

}
