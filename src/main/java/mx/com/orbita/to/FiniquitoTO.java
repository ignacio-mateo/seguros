package mx.com.orbita.to;

import java.util.Date;

public class FiniquitoTO {
	
	private Integer numEmp;
	private String nombreEmp;
	private Integer perfilEmp;
	private Integer numEmpSolicitado;
	private String nombreEmpSolicitado;
	private Integer idSol;	
	private Double montoEmp;
	private Double montoEmpAmp;
	private Double montoDep;
	private Double montoDepAmp;
	private Double montoFiniquito;
	private Date fechaFiniquito;
	private String fechaFiniq;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombreEmp() {
		return nombreEmp;
	}
	public void setNombreEmp(String nombreEmp) {
		this.nombreEmp = nombreEmp;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	public Integer getNumEmpSolicitado() {
		return numEmpSolicitado;
	}
	public void setNumEmpSolicitado(Integer numEmpSolicitado) {
		this.numEmpSolicitado = numEmpSolicitado;
	}
	public String getNombreEmpSolicitado() {
		return nombreEmpSolicitado;
	}
	public void setNombreEmpSolicitado(String nombreEmpSolicitado) {
		this.nombreEmpSolicitado = nombreEmpSolicitado;
	}
	public Integer getIdSol() {
		return idSol;
	}
	public void setIdSol(Integer idSol) {
		this.idSol = idSol;
	}
	public Double getMontoEmp() {
		return montoEmp;
	}
	public void setMontoEmp(Double montoEmp) {
		this.montoEmp = montoEmp;
	}
	public Double getMontoEmpAmp() {
		return montoEmpAmp;
	}
	public void setMontoEmpAmp(Double montoEmpAmp) {
		this.montoEmpAmp = montoEmpAmp;
	}
	public Double getMontoDep() {
		return montoDep;
	}
	public void setMontoDep(Double montoDep) {
		this.montoDep = montoDep;
	}
	public Double getMontoDepAmp() {
		return montoDepAmp;
	}
	public void setMontoDepAmp(Double montoDepAmp) {
		this.montoDepAmp = montoDepAmp;
	}
	public Double getMontoFiniquito() {
		return montoFiniquito;
	}
	public void setMontoFiniquito(Double montoFiniquito) {
		this.montoFiniquito = montoFiniquito;
	}
	public Date getFechaFiniquito() {
		return fechaFiniquito;
	}
	public void setFechaFiniquito(Date fechaFiniquito) {
		this.fechaFiniquito = fechaFiniquito;
	}
	public String getFechaFiniq() {
		return fechaFiniq;
	}
	public void setFechaFiniq(String fechaFiniq) {
		this.fechaFiniq = fechaFiniq;
	}
		
}
