package mx.com.orbita.to;

public class ReporteTO {
	
	private Integer numEmp;
	private Integer idSolicitud;
	private Integer idTipoSol;
	private Integer idEstSol;
	private String rfc;
	private String apellidoP;
	private String apellidoM;
	private String nombre;
	private String region;
	private String sexo;
	private Double costoEmp;
	private Double costoDep;
	private Double costoAmp;
	private Double costoTot;
	private Double costoTotAnt;
	private String fechaCambio;
	private String fechaNac;
	private String fechaAlta;
	private String fechaResp;
	private String fechaAct;
	private String cobertura;
	private String tipoEmpleado;
	private String parentesco;
	private String calle;
	private String numero;
	private String colonia;
	private String cp;
	private String municipio;
	private String entidad;
	private Double tarifaAmp;
	private String catg;
	private String fechaEndose;
	private String correo;
	private String centro;
	private Integer numDep;
	private Integer tipo;
	private String cambio;
	private String tipoInbursa;
	private String cuenta;
	private String tipoSol;
	private String estSol;
	private String mensaje;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public Integer getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Double getCostoEmp() {
		return costoEmp;
	}
	public void setCostoEmp(Double costoEmp) {
		this.costoEmp = costoEmp;
	}
	public Double getCostoTot() {
		return costoTot;
	}
	public void setCostoTot(Double costoTot) {
		this.costoTot = costoTot;
	}
	public Double getCostoTotAnt() {
		return costoTotAnt;
	}
	public void setCostoTotAnt(Double costoTotAnt) {
		this.costoTotAnt = costoTotAnt;
	}
	public Double getCostoDep() {
		return costoDep;
	}
	public void setCostoDep(Double costoDep) {
		this.costoDep = costoDep;
	}
	public Double getCostoAmp() {
		return costoAmp;
	}
	public void setCostoAmp(Double costoAmp) {
		this.costoAmp = costoAmp;
	}
	public String getFechaCambio() {
		return fechaCambio;
	}
	public void setFechaCambio(String fechaCambio) {
		this.fechaCambio = fechaCambio;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public String getTipoEmpleado() {
		return tipoEmpleado;
	}
	public void setTipoEmpleado(String tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}
	public String getParentesco() {
		return parentesco;
	}
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public Double getTarifaAmp() {
		return tarifaAmp;
	}
	public void setTarifaAmp(Double tarifaAmp) {
		this.tarifaAmp = tarifaAmp;
	}
	public String getCatg() {
		return catg;
	}
	public void setCatg(String catg) {
		this.catg = catg;
	}
	public String getFechaEndose() {
		return fechaEndose;
	}
	public void setFechaEndose(String fechaEndose) {
		this.fechaEndose = fechaEndose;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCentro() {
		return centro;
	}
	public void setCentro(String centro) {
		this.centro = centro;
	}
	public Integer getNumDep() {
		return numDep;
	}
	public void setNumDep(Integer numDep) {
		this.numDep = numDep;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getCambio() {
		return cambio;
	}
	public void setCambio(String cambio) {
		this.cambio = cambio;
	}
	public Integer getIdTipoSol() {
		return idTipoSol;
	}
	public void setIdTipoSol(Integer idTipoSol) {
		this.idTipoSol = idTipoSol;
	}
	public Integer getIdEstSol() {
		return idEstSol;
	}
	public void setIdEstSol(Integer idEstSol) {
		this.idEstSol = idEstSol;
	}
	public String getTipoInbursa() {
		return tipoInbursa;
	}
	public void setTipoInbursa(String tipoInbursa) {
		this.tipoInbursa = tipoInbursa;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getTipoSol() {
		return tipoSol;
	}
	public void setTipoSol(String tipoSol) {
		this.tipoSol = tipoSol;
	}
	public String getEstSol() {
		return estSol;
	}
	public void setEstSol(String estSol) {
		this.estSol = estSol;
	}
	public String getFechaResp() {
		return fechaResp;
	}
	public void setFechaResp(String fechaResp) {
		this.fechaResp = fechaResp;
	}
	public String getFechaAct() {
		return fechaAct;
	}
	public void setFechaAct(String fechaAct) {
		this.fechaAct = fechaAct;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
