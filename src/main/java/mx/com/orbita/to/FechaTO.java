package mx.com.orbita.to;

public class FechaTO{
	
	private Integer dia;
	private Integer mes;
	
	public Integer getDia() {
		return dia;
	}
	public void setDia(Integer dia) {
		this.dia = dia;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	
	
	
}