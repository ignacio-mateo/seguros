package mx.com.orbita.to;

public class CategoriaTO {

	private Integer numEmp;
	private String nombre;
	private Integer idTipoEmp;
	private String tipoEmp;
	private Double montoEmp;
	private Double montoDep;
	private Double montoAmp;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getIdTipoEmp() {
		return idTipoEmp;
	}
	public void setIdTipoEmp(Integer idTipoEmp) {
		this.idTipoEmp = idTipoEmp;
	}
	public String getTipoEmp() {
		return tipoEmp;
	}
	public void setTipoEmp(String tipoEmp) {
		this.tipoEmp = tipoEmp;
	}
	public Double getMontoEmp() {
		return montoEmp;
	}
	public void setMontoEmp(Double montoEmp) {
		this.montoEmp = montoEmp;
	}
	public Double getMontoDep() {
		return montoDep;
	}
	public void setMontoDep(Double montoDep) {
		this.montoDep = montoDep;
	}
	public Double getMontoAmp() {
		return montoAmp;
	}
	public void setMontoAmp(Double montoAmp) {
		this.montoAmp = montoAmp;
	}
		
}
