package mx.com.orbita.to;

public class CatalogoTO {
	
	private Integer idCampo;
	private String descripcion;
	private String campoActual;
	private String campoNuevo;
	
	public Integer getIdCampo() {
		return idCampo;
	}
	public void setIdCampo(Integer idCampo) {
		this.idCampo = idCampo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCampoActual() {
		return campoActual;
	}
	public void setCampoActual(String campoActual) {
		this.campoActual = campoActual;
	}
	public String getCampoNuevo() {
		return campoNuevo;
	}
	public void setCampoNuevo(String campoNuevo) {
		this.campoNuevo = campoNuevo;
	}
	
}
