package mx.com.orbita.to;

import java.util.Date;

public class CircularTO {
	
	private Integer numEmp;
	private Integer estRenov;
	private Integer respRenov;
	private Integer tipoCob;
	private Date fechaResp;
	private Date fechaAct;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public Integer getEstRenov() {
		return estRenov;
	}
	public void setEstRenov(Integer estRenov) {
		this.estRenov = estRenov;
	}
	public Integer getRespRenov() {
		return respRenov;
	}
	public void setRespRenov(Integer respRenov) {
		this.respRenov = respRenov;
	}
	public Integer getTipoCob() {
		return tipoCob;
	}
	public void setTipoCob(Integer tipoCob) {
		this.tipoCob = tipoCob;
	}
	public Date getFechaResp() {
		return fechaResp;
	}
	public void setFechaResp(Date fechaResp) {
		this.fechaResp = fechaResp;
	}
	public Date getFechaAct() {
		return fechaAct;
	}
	public void setFechaAct(Date fechaAct) {
		this.fechaAct = fechaAct;
	}

}
