package mx.com.orbita.to;

import java.util.Date;

public class CoberturaTO {
	
	private Integer numEmp;
	private String nombreEmp;
	private Integer perfilEmp;
	private Integer idTipoSolicitud;
	private Integer idEstatusSol;
	private Integer numEmpSolicitado;
	private Integer idSol;
	private Integer idTipoAmplAnt;
	private Double costoAmpliacionAnt;
	private Double costoAmpliacionInd;
	private Double costoTotalAnt;
	private Integer idTipoAmpl;
	private Double costoAmpliacion;
	private Double costoTotal;
	private Date fechaAct;
	private String fechaAutorizacion;
		
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombreEmp() {
		return nombreEmp;
	}
	public void setNombreEmp(String nombreEmp) {
		this.nombreEmp = nombreEmp;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	public Integer getNumEmpSolicitado() {
		return numEmpSolicitado;
	}
	public void setNumEmpSolicitado(Integer numEmpSolicitado) {
		this.numEmpSolicitado = numEmpSolicitado;
	}
	public Integer getIdSol() {
		return idSol;
	}
	public void setIdSol(Integer idSol) {
		this.idSol = idSol;
	}
	public Integer getIdTipoAmplAnt() {
		return idTipoAmplAnt;
	}
	public void setIdTipoAmplAnt(Integer idTipoAmplAnt) {
		this.idTipoAmplAnt = idTipoAmplAnt;
	}
	public Double getCostoAmpliacionAnt() {
		return costoAmpliacionAnt;
	}
	public void setCostoAmpliacionAnt(Double costoAmpliacionAnt) {
		this.costoAmpliacionAnt = costoAmpliacionAnt;
	}
	public Double getCostoTotalAnt() {
		return costoTotalAnt;
	}
	public void setCostoTotalAnt(Double costoTotalAnt) {
		this.costoTotalAnt = costoTotalAnt;
	}
	public Integer getIdTipoAmpl() {
		return idTipoAmpl;
	}
	public void setIdTipoAmpl(Integer idTipoAmpl) {
		this.idTipoAmpl = idTipoAmpl;
	}
	public Double getCostoAmpliacion() {
		return costoAmpliacion;
	}
	public void setCostoAmpliacion(Double costoAmpliacion) {
		this.costoAmpliacion = costoAmpliacion;
	}
	public Double getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(Double costoTotal) {
		this.costoTotal = costoTotal;
	}
	public Date getFechaAct() {
		return fechaAct;
	}
	public void setFechaAct(Date fechaAct) {
		this.fechaAct = fechaAct;
	}
	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	public Double getCostoAmpliacionInd() {
		return costoAmpliacionInd;
	}
	public void setCostoAmpliacionInd(Double costoAmpliacionInd) {
		this.costoAmpliacionInd = costoAmpliacionInd;
	}
	public Integer getIdTipoSolicitud() {
		return idTipoSolicitud;
	}
	public void setIdTipoSolicitud(Integer idTipoSolicitud) {
		this.idTipoSolicitud = idTipoSolicitud;
	}
	public Integer getIdEstatusSol() {
		return idEstatusSol;
	}
	public void setIdEstatusSol(Integer idEstatusSol) {
		this.idEstatusSol = idEstatusSol;
	}
	
}
