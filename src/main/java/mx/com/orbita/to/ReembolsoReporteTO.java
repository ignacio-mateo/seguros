package mx.com.orbita.to;

public class ReembolsoReporteTO {
	
	private String poliza;
	private String emisor;
	private String carpeta;
	private String numPoliza;
	private String cis;
	private String familia;
	private String contratante;
	private String plan;
	private String asegurado;
	private String rfc;
	private String afectado;
	private String sexo;
	private String fechaNac;
	private String fechaNacN;
	private String fechaAlta;
	private String parentescoAsegurado;
	private String edad;
	private String ocupacion;
	private Integer estatusReclamacion;
	private String fechaReclamacion;
	private String fechaReclamacionN;
	private Integer tipoReclamacion;
	private String padecimientoReclamado;
	private Double desglose1;
	private Double desglose2;
	private Double desglose3;
	private Double desglose4;
	private Double desglose5;
	private Double desglose6;
	private Double desglose7;
	private Double desglose8;
	private Double desglose9;
	private Double desglose10;
	private Double desglose11;
	private Double desglose12;
	private Double desglose13;
	private Double desglose14;
	private Double desglose15;
	private Double desglose16;
	private Double desglose17;
	private Double desgloseTotal;
	private String desglose1F;
	private String desglose2F;
	private String desglose3F;
	private String desglose4F;
	private String desglose5F;
	private String desglose6F;
	private String desglose7F;
	private String desglose8F;
	private String desglose9F;
	private String desglose10F;
	private String desglose11F;
	private String desglose12F;
	private String desglose13F;
	private String desglose14F;
	private String desglose15F;
	private String desglose16F;
	private String desglose17F;
	private String desgloseTotalF;
	private String observaciones;
	
	
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getCarpeta() {
		return carpeta;
	}
	public void setCarpeta(String carpeta) {
		this.carpeta = carpeta;
	}
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	public String getCis() {
		return cis;
	}
	public void setCis(String cis) {
		this.cis = cis;
	}
	public String getFamilia() {
		return familia;
	}
	public void setFamilia(String familia) {
		this.familia = familia;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getAsegurado() {
		return asegurado;
	}
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getAfectado() {
		return afectado;
	}
	public void setAfectado(String afectado) {
		this.afectado = afectado;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getParentescoAsegurado() {
		return parentescoAsegurado;
	}
	public void setParentescoAsegurado(String parentescoAsegurado) {
		this.parentescoAsegurado = parentescoAsegurado;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public Integer getEstatusReclamacion() {
		return estatusReclamacion;
	}
	public void setEstatusReclamacion(Integer estatusReclamacion) {
		this.estatusReclamacion = estatusReclamacion;
	}
	public String getFechaReclamacion() {
		return fechaReclamacion;
	}
	public void setFechaReclamacion(String fechaReclamacion) {
		this.fechaReclamacion = fechaReclamacion;
	}
	public Integer getTipoReclamacion() {
		return tipoReclamacion;
	}
	public void setTipoReclamacion(Integer tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}
	public String getPadecimientoReclamado() {
		return padecimientoReclamado;
	}
	public void setPadecimientoReclamado(String padecimientoReclamado) {
		this.padecimientoReclamado = padecimientoReclamado;
	}
	public Double getDesglose1() {
		return desglose1;
	}
	public void setDesglose1(Double desglose1) {
		this.desglose1 = desglose1;
	}
	public Double getDesglose2() {
		return desglose2;
	}
	public void setDesglose2(Double desglose2) {
		this.desglose2 = desglose2;
	}
	public Double getDesglose3() {
		return desglose3;
	}
	public void setDesglose3(Double desglose3) {
		this.desglose3 = desglose3;
	}
	public Double getDesglose4() {
		return desglose4;
	}
	public void setDesglose4(Double desglose4) {
		this.desglose4 = desglose4;
	}
	public Double getDesglose5() {
		return desglose5;
	}
	public void setDesglose5(Double desglose5) {
		this.desglose5 = desglose5;
	}
	public Double getDesglose6() {
		return desglose6;
	}
	public void setDesglose6(Double desglose6) {
		this.desglose6 = desglose6;
	}
	public Double getDesglose7() {
		return desglose7;
	}
	public void setDesglose7(Double desglose7) {
		this.desglose7 = desglose7;
	}
	public Double getDesglose8() {
		return desglose8;
	}
	public void setDesglose8(Double desglose8) {
		this.desglose8 = desglose8;
	}
	public Double getDesglose9() {
		return desglose9;
	}
	public void setDesglose9(Double desglose9) {
		this.desglose9 = desglose9;
	}
	public Double getDesglose10() {
		return desglose10;
	}
	public void setDesglose10(Double desglose10) {
		this.desglose10 = desglose10;
	}
	public Double getDesglose11() {
		return desglose11;
	}
	public void setDesglose11(Double desglose11) {
		this.desglose11 = desglose11;
	}
	public Double getDesglose12() {
		return desglose12;
	}
	public void setDesglose12(Double desglose12) {
		this.desglose12 = desglose12;
	}
	public Double getDesglose13() {
		return desglose13;
	}
	public void setDesglose13(Double desglose13) {
		this.desglose13 = desglose13;
	}
	public Double getDesglose14() {
		return desglose14;
	}
	public void setDesglose14(Double desglose14) {
		this.desglose14 = desglose14;
	}
	public Double getDesglose15() {
		return desglose15;
	}
	public void setDesglose15(Double desglose15) {
		this.desglose15 = desglose15;
	}
	public Double getDesglose16() {
		return desglose16;
	}
	public void setDesglose16(Double desglose16) {
		this.desglose16 = desglose16;
	}
	public Double getDesglose17() {
		return desglose17;
	}
	public void setDesglose17(Double desglose17) {
		this.desglose17 = desglose17;
	}
	public Double getDesgloseTotal() {
		return desgloseTotal;
	}
	public void setDesgloseTotal(Double desgloseTotal) {
		this.desgloseTotal = desgloseTotal;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getFechaNacN() {
		return fechaNacN;
	}
	public void setFechaNacN(String fechaNacN) {
		this.fechaNacN = fechaNacN;
	}
	public String getFechaReclamacionN() {
		return fechaReclamacionN;
	}
	public void setFechaReclamacionN(String fechaReclamacionN) {
		this.fechaReclamacionN = fechaReclamacionN;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getDesglose1F() {
		return desglose1F;
	}
	public void setDesglose1F(String desglose1f) {
		desglose1F = desglose1f;
	}
	public String getDesglose2F() {
		return desglose2F;
	}
	public void setDesglose2F(String desglose2f) {
		desglose2F = desglose2f;
	}
	public String getDesglose3F() {
		return desglose3F;
	}
	public void setDesglose3F(String desglose3f) {
		desglose3F = desglose3f;
	}
	public String getDesglose4F() {
		return desglose4F;
	}
	public void setDesglose4F(String desglose4f) {
		desglose4F = desglose4f;
	}
	public String getDesglose5F() {
		return desglose5F;
	}
	public void setDesglose5F(String desglose5f) {
		desglose5F = desglose5f;
	}
	public String getDesglose6F() {
		return desglose6F;
	}
	public void setDesglose6F(String desglose6f) {
		desglose6F = desglose6f;
	}
	public String getDesglose8F() {
		return desglose8F;
	}
	public void setDesglose8F(String desglose8f) {
		desglose8F = desglose8f;
	}
	public String getDesglose9F() {
		return desglose9F;
	}
	public void setDesglose9F(String desglose9f) {
		desglose9F = desglose9f;
	}
	public String getDesglose10F() {
		return desglose10F;
	}
	public void setDesglose10F(String desglose10f) {
		desglose10F = desglose10f;
	}
	public String getDesglose11F() {
		return desglose11F;
	}
	public void setDesglose11F(String desglose11f) {
		desglose11F = desglose11f;
	}
	public String getDesglose12F() {
		return desglose12F;
	}
	public void setDesglose12F(String desglose12f) {
		desglose12F = desglose12f;
	}
	public String getDesglose13F() {
		return desglose13F;
	}
	public void setDesglose13F(String desglose13f) {
		desglose13F = desglose13f;
	}
	public String getDesglose14F() {
		return desglose14F;
	}
	public void setDesglose14F(String desglose14f) {
		desglose14F = desglose14f;
	}
	public String getDesglose15F() {
		return desglose15F;
	}
	public void setDesglose15F(String desglose15f) {
		desglose15F = desglose15f;
	}
	public String getDesglose16F() {
		return desglose16F;
	}
	public void setDesglose16F(String desglose16f) {
		desglose16F = desglose16f;
	}
	public String getDesglose17F() {
		return desglose17F;
	}
	public void setDesglose17F(String desglose17f) {
		desglose17F = desglose17f;
	}
	public String getDesgloseTotalF() {
		return desgloseTotalF;
	}
	public void setDesgloseTotalF(String desgloseTotalF) {
		this.desgloseTotalF = desgloseTotalF;
	}
	public String getDesglose7F() {
		return desglose7F;
	}
	public void setDesglose7F(String desglose7f) {
		desglose7F = desglose7f;
	}
			
}
