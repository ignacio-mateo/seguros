package mx.com.orbita.to;

public class ReembolsoEnfermedadTO {
	
	private Integer motivoReclamacion;
	private Integer tipoReclamacion;
	private String razonSocial;
	private String numPoliza;
	private String nombreTitular;
	private String rfcTitular;
	private String nombreAfectado;
	private String rfcAfectado;
	private String numEmp;
	private String fechaAlta;
	private String fechaAltaN;
	private String nacionalidad;
	private String fechaNac;
	private String fechaNacN;
	private String sexo;
	private String parentescoTitular;
	private String email;
	private String telefono;
	private String calle;
	private String noExt;
	private String noInt;
	private String colonia;
	private String cp;
	private String estado;
	private String delegacion;
	private String ocupacion;
	private String empresa;
	private String giroEmpresa;
	private Integer mismoPadecimiento;
	private String siniestro;
	private String aseguradora;
	private String fechaAltaSiniestro;
	private String fechaAltaSiniestroN;
	private Integer estadoReclamacion;
	private String sintomas;
	private String fechaSintomas;
	private String fechaSintomasN;
	private String fechaVisitaMed;
	private String fechaVisitaMedN;
	private String diagnostico;
	private String accidente;
	private String ministerioPublico;
	private Integer seguroAuto;
	private String companiaSeguroAuto;
	private String cobertura;
	private String sumaAsegurada;
	private String numPolizaAuto;
	private String companiaTercero;
	private String hospital;
	private String estudiosDiagnostico;
	private String medicoTratante;
	private String especialidad;
	private String direccionMedico;
	private String telEmailMedico;
	private String medicosConsultados;
	private String causa;
	private String telEmailMedicoCon;
	private String fechaMedicoCon;
	private String fechaMedicoConN;
	
	public Integer getMotivoReclamacion() {
		return motivoReclamacion;
	}
	public void setMotivoReclamacion(Integer motivoReclamacion) {
		this.motivoReclamacion = motivoReclamacion;
	}
	public Integer getTipoReclamacion() {
		return tipoReclamacion;
	}
	public void setTipoReclamacion(Integer tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	public String getNombreTitular() {
		return nombreTitular;
	}
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	public String getRfcTitular() {
		return rfcTitular;
	}
	public void setRfcTitular(String rfcTitular) {
		this.rfcTitular = rfcTitular;
	}
	public String getNombreAfectado() {
		return nombreAfectado;
	}
	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}
	public String getRfcAfectado() {
		return rfcAfectado;
	}
	public void setRfcAfectado(String rfcAfectado) {
		this.rfcAfectado = rfcAfectado;
	}
	public String getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(String numEmp) {
		this.numEmp = numEmp;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getFechaAltaN() {
		return fechaAltaN;
	}
	public void setFechaAltaN(String fechaAltaN) {
		this.fechaAltaN = fechaAltaN;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getFechaNacN() {
		return fechaNacN;
	}
	public void setFechaNacN(String fechaNacN) {
		this.fechaNacN = fechaNacN;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getParentescoTitular() {
		return parentescoTitular;
	}
	public void setParentescoTitular(String parentescoTitular) {
		this.parentescoTitular = parentescoTitular;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNoExt() {
		return noExt;
	}
	public void setNoExt(String noExt) {
		this.noExt = noExt;
	}
	public String getNoInt() {
		return noInt;
	}
	public void setNoInt(String noInt) {
		this.noInt = noInt;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDelegacion() {
		return delegacion;
	}
	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getGiroEmpresa() {
		return giroEmpresa;
	}
	public void setGiroEmpresa(String giroEmpresa) {
		this.giroEmpresa = giroEmpresa;
	}
	public Integer getMismoPadecimiento() {
		return mismoPadecimiento;
	}
	public void setMismoPadecimiento(Integer mismoPadecimiento) {
		this.mismoPadecimiento = mismoPadecimiento;
	}
	public String getSiniestro() {
		return siniestro;
	}
	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}
	public String getAseguradora() {
		return aseguradora;
	}
	public void setAseguradora(String aseguradora) {
		this.aseguradora = aseguradora;
	}
	public String getFechaAltaSiniestro() {
		return fechaAltaSiniestro;
	}
	public void setFechaAltaSiniestro(String fechaAltaSiniestro) {
		this.fechaAltaSiniestro = fechaAltaSiniestro;
	}
	public String getFechaAltaSiniestroN() {
		return fechaAltaSiniestroN;
	}
	public void setFechaAltaSiniestroN(String fechaAltaSiniestroN) {
		this.fechaAltaSiniestroN = fechaAltaSiniestroN;
	}
	public Integer getEstadoReclamacion() {
		return estadoReclamacion;
	}
	public void setEstadoReclamacion(Integer estadoReclamacion) {
		this.estadoReclamacion = estadoReclamacion;
	}
	public String getSintomas() {
		return sintomas;
	}
	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}
	public String getFechaSintomas() {
		return fechaSintomas;
	}
	public void setFechaSintomas(String fechaSintomas) {
		this.fechaSintomas = fechaSintomas;
	}
	public String getFechaSintomasN() {
		return fechaSintomasN;
	}
	public void setFechaSintomasN(String fechaSintomasN) {
		this.fechaSintomasN = fechaSintomasN;
	}
	public String getFechaVisitaMed() {
		return fechaVisitaMed;
	}
	public void setFechaVisitaMed(String fechaVisitaMed) {
		this.fechaVisitaMed = fechaVisitaMed;
	}
	public String getFechaVisitaMedN() {
		return fechaVisitaMedN;
	}
	public void setFechaVisitaMedN(String fechaVisitaMedN) {
		this.fechaVisitaMedN = fechaVisitaMedN;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getAccidente() {
		return accidente;
	}
	public void setAccidente(String accidente) {
		this.accidente = accidente;
	}
	public String getMinisterioPublico() {
		return ministerioPublico;
	}
	public void setMinisterioPublico(String ministerioPublico) {
		this.ministerioPublico = ministerioPublico;
	}
	public Integer getSeguroAuto() {
		return seguroAuto;
	}
	public void setSeguroAuto(Integer seguroAuto) {
		this.seguroAuto = seguroAuto;
	}
	public String getCompaniaSeguroAuto() {
		return companiaSeguroAuto;
	}
	public void setCompaniaSeguroAuto(String companiaSeguroAuto) {
		this.companiaSeguroAuto = companiaSeguroAuto;
	}
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getNumPolizaAuto() {
		return numPolizaAuto;
	}
	public void setNumPolizaAuto(String numPolizaAuto) {
		this.numPolizaAuto = numPolizaAuto;
	}
	public String getCompaniaTercero() {
		return companiaTercero;
	}
	public void setCompaniaTercero(String companiaTercero) {
		this.companiaTercero = companiaTercero;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getEstudiosDiagnostico() {
		return estudiosDiagnostico;
	}
	public void setEstudiosDiagnostico(String estudiosDiagnostico) {
		this.estudiosDiagnostico = estudiosDiagnostico;
	}
	public String getMedicoTratante() {
		return medicoTratante;
	}
	public void setMedicoTratante(String medicoTratante) {
		this.medicoTratante = medicoTratante;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getDireccionMedico() {
		return direccionMedico;
	}
	public void setDireccionMedico(String direccionMedico) {
		this.direccionMedico = direccionMedico;
	}
	public String getTelEmailMedico() {
		return telEmailMedico;
	}
	public void setTelEmailMedico(String telEmailMedico) {
		this.telEmailMedico = telEmailMedico;
	}
	public String getMedicosConsultados() {
		return medicosConsultados;
	}
	public void setMedicosConsultados(String medicosConsultados) {
		this.medicosConsultados = medicosConsultados;
	}
	public String getCausa() {
		return causa;
	}
	public void setCausa(String causa) {
		this.causa = causa;
	}
	public String getTelEmailMedicoCon() {
		return telEmailMedicoCon;
	}
	public void setTelEmailMedicoCon(String telEmailMedicoCon) {
		this.telEmailMedicoCon = telEmailMedicoCon;
	}
	public String getFechaMedicoCon() {
		return fechaMedicoCon;
	}
	public void setFechaMedicoCon(String fechaMedicoCon) {
		this.fechaMedicoCon = fechaMedicoCon;
	}
	public String getFechaMedicoConN() {
		return fechaMedicoConN;
	}
	public void setFechaMedicoConN(String fechaMedicoConN) {
		this.fechaMedicoConN = fechaMedicoConN;
	}
	
}