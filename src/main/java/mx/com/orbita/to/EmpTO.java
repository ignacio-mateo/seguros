package mx.com.orbita.to;

public class EmpTO {
	
	private Integer numEmp;
	private String nombreEmp;
	private Integer perfilEmp;
	private Integer numEmpSolicitado;
	private String mensajeUno;
	private String mensajeDos;
	private String mensajeTres;
	private String mensajeCuatro;
	private String mensajeCinco;
	private Integer parametro;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombreEmp() {
		return nombreEmp;
	}
	public void setNombreEmp(String nombreEmp) {
		this.nombreEmp = nombreEmp;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	public Integer getNumEmpSolicitado() {
		return numEmpSolicitado;
	}
	public void setNumEmpSolicitado(Integer numEmpSolicitado) {
		this.numEmpSolicitado = numEmpSolicitado;
	}
	public String getMensajeUno() {
		return mensajeUno;
	}
	public void setMensajeUno(String mensajeUno) {
		this.mensajeUno = mensajeUno;
	}
	public String getMensajeDos() {
		return mensajeDos;
	}
	public void setMensajeDos(String mensajeDos) {
		this.mensajeDos = mensajeDos;
	}
	public String getMensajeTres() {
		return mensajeTres;
	}
	public void setMensajeTres(String mensajeTres) {
		this.mensajeTres = mensajeTres;
	}
	public String getMensajeCuatro() {
		return mensajeCuatro;
	}
	public void setMensajeCuatro(String mensajeCuatro) {
		this.mensajeCuatro = mensajeCuatro;
	}
	public String getMensajeCinco() {
		return mensajeCinco;
	}
	public void setMensajeCinco(String mensajeCinco) {
		this.mensajeCinco = mensajeCinco;
	}
	public Integer getParametro() {
		return parametro;
	}
	public void setParametro(Integer parametro) {
		this.parametro = parametro;
	}
	
}
