package mx.com.orbita.to;

public class TarifaTO {
	
	private Integer id;
	private Integer tarifa;
	private Integer tarifaNueva;
	private String descripcion;
	private String sexo;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTarifa() {
		return tarifa;
	}
	public void setTarifa(Integer tarifa) {
		this.tarifa = tarifa;
	}
	public Integer getTarifaNueva() {
		return tarifaNueva;
	}
	public void setTarifaNueva(Integer tarifaNueva) {
		this.tarifaNueva = tarifaNueva;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
}
