package mx.com.orbita.to;

public class FechaAntiguedadTO {
	
	private Integer numEmp;
	private String nombreEmp;
	private Integer perfilEmp;
	private Integer numEmpSolicitado;
	private String onSeleccion;
	private String fechaElabTit;
	private String fechaAltaTit;
	private String fechaElabDep;
	private String fechaAltaDep;
	private Integer idDep;
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombreEmp() {
		return nombreEmp;
	}
	public void setNombreEmp(String nombreEmp) {
		this.nombreEmp = nombreEmp;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	public Integer getNumEmpSolicitado() {
		return numEmpSolicitado;
	}
	public void setNumEmpSolicitado(Integer numEmpSolicitado) {
		this.numEmpSolicitado = numEmpSolicitado;
	}
	public String getOnSeleccion() {
		return onSeleccion;
	}
	public void setOnSeleccion(String onSeleccion) {
		this.onSeleccion = onSeleccion;
	}
	public String getFechaElabTit() {
		return fechaElabTit;
	}
	public void setFechaElabTit(String fechaElabTit) {
		this.fechaElabTit = fechaElabTit;
	}
	public String getFechaAltaTit() {
		return fechaAltaTit;
	}
	public void setFechaAltaTit(String fechaAltaTit) {
		this.fechaAltaTit = fechaAltaTit;
	}
	public String getFechaElabDep() {
		return fechaElabDep;
	}
	public void setFechaElabDep(String fechaElabDep) {
		this.fechaElabDep = fechaElabDep;
	}
	public String getFechaAltaDep() {
		return fechaAltaDep;
	}
	public void setFechaAltaDep(String fechaAltaDep) {
		this.fechaAltaDep = fechaAltaDep;
	}
	public Integer getIdDep() {
		return idDep;
	}
	public void setIdDep(Integer idDep) {
		this.idDep = idDep;
	}
	
}