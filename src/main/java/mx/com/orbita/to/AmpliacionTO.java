package mx.com.orbita.to;

public class AmpliacionTO {
	
	private Integer idTipoCob;
	private String descripcion;
	
	public Integer getIdTipoCob() {
		return idTipoCob;
	}
	
	public void setIdTipoCob(Integer idTipoCob) {
		this.idTipoCob = idTipoCob;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
