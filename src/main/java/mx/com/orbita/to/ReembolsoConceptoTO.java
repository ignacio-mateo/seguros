package mx.com.orbita.to;

public class ReembolsoConceptoTO {
	
	private Integer idReembolso;
	private String noDocumento;
	private String nombreExpediente;
	private String concepto;
	private Double importe;
	private String importeFormat;
		
	public Integer getIdReembolso() {
		return idReembolso;
	}
	public void setIdReembolso(Integer idReembolso) {
		this.idReembolso = idReembolso;
	}
	public String getNoDocumento() {
		return noDocumento;
	}
	public void setNoDocumento(String noDocumento) {
		this.noDocumento = noDocumento;
	}
	public String getNombreExpediente() {
		return nombreExpediente;
	}
	public void setNombreExpediente(String nombreExpediente) {
		this.nombreExpediente = nombreExpediente;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	public String getImporteFormat() {
		return importeFormat;
	}
	public void setImporteFormat(String importeFormat) {
		this.importeFormat = importeFormat;
	}
		
}
