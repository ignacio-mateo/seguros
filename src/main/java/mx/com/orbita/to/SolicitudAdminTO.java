package mx.com.orbita.to;

import java.util.Date;

public class SolicitudAdminTO {
	
	private Integer numEmp;
	private String nombreEmp;
	private Integer perfilEmp;
	private Integer numEmpSolicitado;
	private Integer idSolicitud;
	private Integer idDependiente;
	private Integer idConsulmed;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private String fechaNac;
	private String fechaElab;
	private String fechaNacNvo;
	private Date fechaNacDate;
	private String region;
	private Integer idTipoEmpleado;
	private String tipoEmpleado;
	private Double costoEmp;
	private Double costo;
	private String costoCancelacion;
	private Double costoTotalDep;
	private Double costoTotal;
	private Double costoFiniquito;
	private Double costoCobBasica;
	private Double costoAmpliacionDep;
	private String fechaAlta;
	private String fechaActual;
	private Date fechaAltaDate;
	private Integer idTipoSol;
	private String tipoSol;
	private Integer idEstatulSol;
	private Integer idEstatulSolNvo;
	private String estatulSol;
	private String fechaBaja;
	private String fechaRenov;
	private Date fechaBajaDate;
	private Double costoAmpliacion;
	private Double costoAmpliacionInd;
	private Integer idCobertura;
	private String cobertura;
	private Integer idParentesco;
	private Integer idParentescoNvo;
	private String sexo;
	private String parentesco;
	private String comentario;
	private Integer dia;
	private Integer mes;
	private Integer anio;
	private Integer idConyuge;
	private Double costoMemorial;
	private String tipoMemorial;
	
	
	public Integer getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(Integer numEmp) {
		this.numEmp = numEmp;
	}
	public String getNombreEmp() {
		return nombreEmp;
	}
	public void setNombreEmp(String nombreEmp) {
		this.nombreEmp = nombreEmp;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	public Integer getNumEmpSolicitado() {
		return numEmpSolicitado;
	}
	public void setNumEmpSolicitado(Integer numEmpSolicitado) {
		this.numEmpSolicitado = numEmpSolicitado;
	}
	public Integer getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public Integer getIdDependiente() {
		return idDependiente;
	}
	public void setIdDependiente(Integer idDependiente) {
		this.idDependiente = idDependiente;
	}
	public Integer getIdConsulmed() {
		return idConsulmed;
	}
	public void setIdConsulmed(Integer idConsulmed) {
		this.idConsulmed = idConsulmed;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getFechaNacNvo() {
		return fechaNacNvo;
	}
	public void setFechaNacNvo(String fechaNacNvo) {
		this.fechaNacNvo = fechaNacNvo;
	}
	public Date getFechaNacDate() {
		return fechaNacDate;
	}
	public void setFechaNacDate(Date fechaNacDate) {
		this.fechaNacDate = fechaNacDate;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public Integer getIdTipoEmpleado() {
		return idTipoEmpleado;
	}
	public void setIdTipoEmpleado(Integer idTipoEmpleado) {
		this.idTipoEmpleado = idTipoEmpleado;
	}
	public String getTipoEmpleado() {
		return tipoEmpleado;
	}
	public void setTipoEmpleado(String tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}
	public Double getCostoEmp() {
		return costoEmp;
	}
	public void setCostoEmp(Double costoEmp) {
		this.costoEmp = costoEmp;
	}
	public Double getCosto() {
		return costo;
	}
	public void setCosto(Double costo) {
		this.costo = costo;
	}
	public String getCostoCancelacion() {
		return costoCancelacion;
	}
	public void setCostoCancelacion(String costoCancelacion) {
		this.costoCancelacion = costoCancelacion;
	}
	public Double getCostoTotalDep() {
		return costoTotalDep;
	}
	public void setCostoTotalDep(Double costoTotalDep) {
		this.costoTotalDep = costoTotalDep;
	}
	public Double getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(Double costoTotal) {
		this.costoTotal = costoTotal;
	}
	public Double getCostoFiniquito() {
		return costoFiniquito;
	}
	public void setCostoFiniquito(Double costoFiniquito) {
		this.costoFiniquito = costoFiniquito;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Date getFechaAltaDate() {
		return fechaAltaDate;
	}
	public void setFechaAltaDate(Date fechaAltaDate) {
		this.fechaAltaDate = fechaAltaDate;
	}
	public Integer getIdTipoSol() {
		return idTipoSol;
	}
	public void setIdTipoSol(Integer idTipoSol) {
		this.idTipoSol = idTipoSol;
	}
	public String getTipoSol() {
		return tipoSol;
	}
	public void setTipoSol(String tipoSol) {
		this.tipoSol = tipoSol;
	}
	public Integer getIdEstatulSol() {
		return idEstatulSol;
	}
	public void setIdEstatulSol(Integer idEstatulSol) {
		this.idEstatulSol = idEstatulSol;
	}
	public Integer getIdEstatulSolNvo() {
		return idEstatulSolNvo;
	}
	public void setIdEstatulSolNvo(Integer idEstatulSolNvo) {
		this.idEstatulSolNvo = idEstatulSolNvo;
	}
	public String getEstatulSol() {
		return estatulSol;
	}
	public void setEstatulSol(String estatulSol) {
		this.estatulSol = estatulSol;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public Date getFechaBajaDate() {
		return fechaBajaDate;
	}
	public void setFechaBajaDate(Date fechaBajaDate) {
		this.fechaBajaDate = fechaBajaDate;
	}
	public Double getCostoAmpliacion() {
		return costoAmpliacion;
	}
	public void setCostoAmpliacion(Double costoAmpliacion) {
		this.costoAmpliacion = costoAmpliacion;
	}
	public Integer getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Integer idCobertura) {
		this.idCobertura = idCobertura;
	}
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public Integer getIdParentesco() {
		return idParentesco;
	}
	public void setIdParentesco(Integer idParentesco) {
		this.idParentesco = idParentesco;
	}
	public Integer getIdParentescoNvo() {
		return idParentescoNvo;
	}
	public void setIdParentescoNvo(Integer idParentescoNvo) {
		this.idParentescoNvo = idParentescoNvo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getParentesco() {
		return parentesco;
	}
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getDia() {
		return dia;
	}
	public void setDia(Integer dia) {
		this.dia = dia;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public Integer getAnio() {
		return anio;
	}
	public void setAnio(Integer anio) {
		this.anio = anio;
	}
	public Integer getIdConyuge() {
		return idConyuge;
	}
	public void setIdConyuge(Integer idConyuge) {
		this.idConyuge = idConyuge;
	}
	public Double getCostoAmpliacionInd() {
		return costoAmpliacionInd;
	}
	public void setCostoAmpliacionInd(Double costoAmpliacionInd) {
		this.costoAmpliacionInd = costoAmpliacionInd;
	}
	public Double getCostoCobBasica() {
		return costoCobBasica;
	}
	public void setCostoCobBasica(Double costoCobBasica) {
		this.costoCobBasica = costoCobBasica;
	}
	public Double getCostoAmpliacionDep() {
		return costoAmpliacionDep;
	}
	public void setCostoAmpliacionDep(Double costoAmpliacionDep) {
		this.costoAmpliacionDep = costoAmpliacionDep;
	}
	public String getFechaElab() {
		return fechaElab;
	}
	public void setFechaElab(String fechaElab) {
		this.fechaElab = fechaElab;
	}
	public String getFechaRenov() {
		return fechaRenov;
	}
	public void setFechaRenov(String fechaRenov) {
		this.fechaRenov = fechaRenov;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	public Double getCostoMemorial() {
		return costoMemorial;
	}
	public void setCostoMemorial(Double costoMemorial) {
		this.costoMemorial = costoMemorial;
	}
	public String getTipoMemorial() {
		return tipoMemorial;
	}
	public void setTipoMemorial(String tipoMemorial) {
		this.tipoMemorial = tipoMemorial;
	}
	
}
