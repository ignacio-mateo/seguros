package mx.com.orbita.to;

public class DatFiscalesTO {
	
	private Integer num_empleado;
	private String nombre;
	private Integer perfilEmp;
	private String rfc;
	private String calle;
	private String numero;
	private String colonia;
	private String cp;
	private String deleg_mun;
	private String entidad;
	private Boolean existeDatosFis;
	
	
	public Integer getNum_empleado() {
		return num_empleado;
	}
	public void setNum_empleado(Integer num_empleado) {
		this.num_empleado = num_empleado;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getDeleg_mun() {
		return deleg_mun;
	}
	public void setDeleg_mun(String deleg_mun) {
		this.deleg_mun = deleg_mun;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getExisteDatosFis() {
		return existeDatosFis;
	}
	public void setExisteDatosFis(Boolean existeDatosFis) {
		this.existeDatosFis = existeDatosFis;
	}
	public Integer getPerfilEmp() {
		return perfilEmp;
	}
	public void setPerfilEmp(Integer perfilEmp) {
		this.perfilEmp = perfilEmp;
	}
	
}
