package mx.com.orbita.business;

import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoAdrisaTO;

public interface EmpleadoBusiness {
	
	EmpleadoTO consultaEmpleado(Integer numEmpleado);
	ReembolsoAdrisaTO datosEmpleadoReembolso(Integer numEmp);

}
