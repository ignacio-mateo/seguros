package mx.com.orbita.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.EmpleadoDao;
import mx.com.orbita.to.EmpleadoTO;
import mx.com.orbita.to.ReembolsoAdrisaTO;
import mx.com.orbita.util.Fechas;

@Component
public class EmpleadoBusinessImpl implements EmpleadoBusiness{
	
	private static final Logger logger = Logger.getLogger(EmpleadoBusinessImpl.class);
	
	private EmpleadoDao empleadoDao;

	public EmpleadoTO consultaEmpleado(Integer numEmpleado) {
		EmpleadoTO empleado = empleadoDao.datosEmpleado(numEmpleado);
		
		if(empleado.getRegion().equals("R01")){
			empleado.setIdRegion(1);
		}else if(empleado.getRegion().equals("R02")){
			empleado.setIdRegion(2);
		}else if(empleado.getRegion().equals("R03")){
			empleado.setIdRegion(3);
		}else if(empleado.getRegion().equals("R04")){
			empleado.setIdRegion(4);
		}else if(empleado.getRegion().equals("R05")){
			empleado.setIdRegion(5);
		}else if(empleado.getRegion().equals("R06")){
			empleado.setIdRegion(6);
		}else if(empleado.getRegion().equals("R07")){
			empleado.setIdRegion(7);
		}else if(empleado.getRegion().equals("R08")){
			empleado.setIdRegion(8);
		}else if(empleado.getRegion().equals("R09")){
			empleado.setIdRegion(9);
		}else if(empleado.getRegion().equals("R00")){
			empleado.setIdRegion(0);
		}
		
		if(empleado.getSexo().equals("M")){
			empleado.setIdSexo(1);
		}else if(empleado.getSexo().equals("F")){
			empleado.setIdSexo(2);
		}
		
		if(empleado.getDescEmpleado().equals("C")){
			empleado.setTipoEmp(1);
			empleado.setDescTipoEmp("CONFIANZA");
		}else if(empleado.getDescEmpleado().equals("S")){
			empleado.setTipoEmp(2);
			empleado.setDescTipoEmp("SINDICALIZADO");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date fechaNac = new Date();
		Date fechaIng = new Date();
		Fechas fecha = new Fechas();
		
		// Fecha Nacimiento
		try {
			fechaNac = format.parse(empleado.getFechaNacF());
			empleado.setFechaNac(fechaNac);
			empleado.setFechaNacF(fecha.formatoFechas(fechaNac,"EEE, MMM dd yyyy").toUpperCase());
		} catch (ParseException e) {
			logger.info("No fue posible convertir la fecha");
		}
		
		// Fecha Ingreso
		try {
			fechaIng = format.parse(empleado.getFechaIngF());
			empleado.setFechaIng(fechaIng);
			empleado.setFechaIngF(fecha.formatoFechas(fechaIng,"EEE, MMM dd yyyy").toUpperCase());
		} catch (ParseException e) {
			logger.info("No fue posible convertir la fecha");
		}
				
		// Fecha Actual
		empleado.setFechaActualF(fecha.formatoFechas(new Date(),"EEE, MMM dd yyyy").toUpperCase());
		empleado.setFechaActual(new Date());
			
		return empleado;
	}

	
	public ReembolsoAdrisaTO datosEmpleadoReembolso(Integer numEmp){
		logger.info("EmpleadoBusinessImpl.datosEmpleadoReembolso");
		return empleadoDao.datosEmpleadoReembolso(numEmp);
	}
	
	
	@Autowired
	public void setEmpleadoDao(EmpleadoDao empleadoDao) {
		this.empleadoDao = empleadoDao;
	}

}
