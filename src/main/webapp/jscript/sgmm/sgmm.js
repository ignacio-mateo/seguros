jQuery(document).ready(function(){

    jQuery('#botonBuscar').click(function(){
        getBotonBuscar();
    });

    jQuery('#selAmpliacion').change(function(){
        var id = jQuery('#selAmpliacion').val();
        getMontoAmpliacion(id);
    });

    jQuery('#selMemorial').change(function(){
        var id = jQuery('#selMemorial').val();
        getMontoMemorial(id);
    });

    jQuery('#idParent').change(function(){
        validarParentesco();
    });

    jQuery('#idEst').change(function(){
        validarEstatus();
    });

    jQuery('#idEstCon').change(function(){
        validarEstatusCon();
    });

    jQuery('#estReclaIni').click(function(){
        habilitarFechaReclamacion();
    });

    jQuery('#estReclaComp').click(function(){
        deshabilitarFechaReclamacion();
    });

    jQuery('#siRenov').click(function(){
        jQuery('#siA1').attr("disabled",false);
        jQuery('#siA2N').attr("disabled",false);
        jQuery('#siA2E').attr("disabled",false);
        jQuery('#siA3N').attr("disabled",false);
        jQuery('#siA3E').attr("disabled",false);
        jQuery('#siA4N').attr("disabled",false);
        jQuery('#siA4E').attr("disabled",false);
        jQuery('#noA').attr("disabled",false);
    });

    jQuery('#noRenov').click(function(){
        jQuery('#siA1').attr("disabled",true);
        jQuery('#siA2N').attr("disabled",true);
        jQuery('#siA2E').attr("disabled",true);
        jQuery('#siA3N').attr("disabled",true);
        jQuery('#siA3E').attr("disabled",true);
        jQuery('#siA4N').attr("disabled",true);
        jQuery('#siA4E').attr("disabled",true);
        jQuery('#noA').attr("disabled",true);
    });

    jQuery('#siA1').click(function(){
        jQuery('#descTipoCobertura').val('A1');
        getMontoAmpliacionCircular(1);
    });

    jQuery('#siA2N').click(function(){
        jQuery('#descTipoCobertura').val('A2 N');
        getMontoAmpliacionCircular(2);
    });

    jQuery('#siA2E').click(function(){
        jQuery('#descTipoCobertura').val('A2 E');
        getMontoAmpliacionCircular(3);
    });

    jQuery('#siA3N').click(function(){
        jQuery('#descTipoCobertura').val('A3 N');
        getMontoAmpliacionCircular(4);
    });

    jQuery('#siA3E').click(function(){
        jQuery('#descTipoCobertura').val('A3 E');
        getMontoAmpliacionCircular(5);
    });

    jQuery('#siA4N').click(function(){
        jQuery('#descTipoCobertura').val('A4 N');
        getMontoAmpliacionCircular(6);
    });

    jQuery('#siA4E').click(function(){
        jQuery('#descTipoCobertura').val('A4 E');
        getMontoAmpliacionCircular(7);
    });

    jQuery('#noA').click(function(){
        jQuery('#descTipoCobertura').val('N');
        getMontoAmpliacionCircular(100);
    });

    jQuery('#aceptoA3').click(function(){
        validaAceptoA3();
    });

    cargaCalendario();

});

/**
 * MODULO DE COTIZACION Y ALTA DE SOLICITUD
 *
 */
function seleccionarCheck(id){
    var tipoEmpleado = jQuery("#tipoEmp").val();
    var fechaNacimiento = jQuery('#fecha'+id).val();;
    var parentesco = jQuery("#select"+id).val();
    var seleccion = '#check'+id;
    var sexo;

    // Validar checkbox activado
    if(jQuery(seleccion).is(':checked')) {

        // Validar checkbox parentesco seleccionado
        if(parentesco!=0){

            // Validar tipo de empleado
            if(tipoEmpleado==2){ // 2 Sindicalizado

                // Validar fecha de nacimiento
                if(fechaNacimiento!=''){
                    // Validar que no se seleccione una fecha futura
                    if(fechaFuturo(fechaNacimiento)){
                        if(mayoriaEdad(fechaNacimiento)<18 && (parentesco==3 || parentesco==4)){
                            alert('Su conyuge aun no cumple la mayoria de edad, favor de validarlo');
                        }
                        if(mayoriaEdad(fechaNacimiento)>=25 && (parentesco==5 || parentesco==6)){
                            alert('No puede agregar dependientes hijos mayores a 25 anios, favor de corregir');
                            limpiarDependiente(id);
                            jQuery('#check'+id).prop("checked", false);
                        }else{
                            sexo = setSexo(id,parentesco);
                            getMontoDependientes(id,tipoEmpleado,parentesco,fechaNacimiento,sexo);
                            setConyuge(parentesco);
                            limpiarAmpliaciones();
                        }
                    }else{
                        alert('No es posible seleccionar una fecha futura');
                        jQuery('#check'+id).prop("checked", false);
                    }
                }else{
                    alert('selecciona una fecha');
                    jQuery('#check'+id).prop("checked", false);
                }
            }else if(tipoEmpleado==1){ // 1 = Confianza
                // Validar fecha de nacimiento
                if(fechaNacimiento!=''){
                    // Validar que no se seleccione una fecha futura
                    if(fechaFuturo(fechaNacimiento)){
                        if(mayoriaEdad(fechaNacimiento)<18 && (parentesco==3 || parentesco==4)){
                            alert('Su conyuge aun no cumple la mayoria de edad, favor de validarlo');
                        }
                        if(mayoriaEdad(fechaNacimiento)>=25 && (parentesco==5 || parentesco==6)){
                            alert('No puede agregar dependientes hijos mayores a 25 anios, favor de corregir');
                            limpiarDependiente(id);
                            jQuery('#check'+id).prop("checked", false);
                        }else{
                            getMontoDependientes(id,tipoEmpleado,parentesco,fechaNacimiento,sexo);
                            sexo = setSexo(id,parentesco);
                            setConyuge(parentesco);
                            limpiarAmpliaciones();
                        }
                    }else{
                        alert('No es posible seleccionar una fecha futura');
                        jQuery('#check'+id).prop("checked", false);
                    }
                } else{
                    alert('selecciona una fecha');
                    jQuery('#check'+id).prop("checked", false);
                }
            }else{
                alert('No se puede localizar el tipo de empleado que es, favor de recargar la pagina');
            }
        }else{
            alert('Selecciona el parentesco');
            jQuery('#check'+id).prop("checked", false);
        }
    }else{
        limpiarAmpliaciones();
        limpiarConyuge(id);
        eliminaDependientes(id);
        limpiarDependiente(id);
    }
}

// Funcion: copia dependendientes consulmed a dependientes memorial
function seleccionarCheckMem(i){
    if(jQuery('#checkCon'+i).is(':checked')) {
        jQuery('#paternoMem'+i).attr('value',jQuery('#paternoCon'+i).val());
        jQuery('#maternoMem'+i).attr('value',jQuery('#maternoCon'+i).val());
        jQuery('#nombreMem'+i).attr('value',jQuery('#nombreCon'+i).val());
    }

    if(jQuery('#checkMem'+i).is(':not(:checked)')){
        jQuery('#paternoMem'+i).val('');
        jQuery('#maternoMem'+i).val('');
        jQuery('#nombreMem'+i).val('');
    }
}

// Funcion: Calcula el monto de cada dependiente
function getMontoDependientes(id,tipoEmpleado,parentesco,fecha,sexo){
    var fechaFormato = formatoFecha(fecha);
    var url = "getMontoDependiente.htm?tipoEmp="+tipoEmpleado+"&parentesco="+parentesco+"&fecha="+fechaFormato+"&sexo="+sexo;
    jQuery.get(url,function(mensaje){
        var resultado = mensaje;
        //jQuery('#monto'+id).attr('value',resultado);
        jQuery('#monto'+id).val(resultado);
        sumaDependientes(resultado);
    });
}

// Funcion: Suma de dependientes
function sumaDependientes(resultado){
    var total = 0.0;
    var montoInicialDep = jQuery('#montoDep').val();
    total = (parseFloat(montoInicialDep) + parseFloat(resultado)).toFixed(2);
    jQuery('#montoDep').val(total);
    sumaDependientesEmpleado(total);
}

// Funcion: Suma de los dependientes + empleado
function sumaDependientesEmpleado(totalDep){
    var total = 0.0;
    var montoEmpleado = jQuery('#montoEmp').val();
    total = (parseFloat(totalDep) + parseFloat(montoEmpleado)).toFixed(2);
    jQuery('#montoEmpDep').val(total);
    jQuery('#montoTotal').val(total);
    //sumaTotal(total);
}

// Funcion: Calcula el monto de la amplicion
function getMontoAmpliacion(id){
    var numDep = totalChecks();
    var url = "getMontoAmpliacion.htm?numDep="+numDep+"&tipoAmpl="+id;

    if(id!=0){
        jQuery.get(url,function(mensaje){
            var resultado = mensaje;
            jQuery('#montoAmpl').attr('value',resultado);
            sumaTotalAmp(id,resultado);
        });
    }else{
        var suma = 0.0;
        var total = 0.0;
        var montoEmp = jQuery('#montoEmp').val();
        var montoDep = jQuery('#montoDep').val();
        var montoMem = jQuery('#montoMemorial').val();
        suma = parseFloat(montoEmp) + parseFloat(montoDep) + parseFloat(montoMem);
        total = suma.toFixed(2);
        jQuery('#montoTotal').val(total);
        jQuery('#montoAmpl').attr('value','0.00');
    }

}

// Funcion: Calcula el monto de la amplicion en la circular
function getMontoAmpliacionCircular(id){
    var numDep = jQuery('#numDep').val();
    var costoTotalAct = jQuery('#cta').val();
    var costoAmpliacion = jQuery('#ca').val();
    var costoTotal = jQuery('#ct').val();
    var numDep = jQuery('#numDep').val();

    //alert(' NumDep: '+numDep+' costoTotalAct: '+costoTotalAct+' costoAmpliacion: '+costoAmpliacion+' costoTotal: '+costoTotal);
    var url = "getMontoAmpliacionCircular.htm?numDep="+numDep+"&tipoAmpl="+id;

    if(id!=100){
        jQuery.get(url,function(mensaje){
            var resultado = mensaje;
            jQuery('#costoAmpliacion').attr('value',resultado);
            var sumaTotal = parseFloat(costoTotalAct) + parseFloat(resultado);
            jQuery('#costoTotal').val(sumaTotal.toFixed(1));
        });
    }else{
         jQuery('#costoAmpliacion').attr('value','0.0');
         jQuery('#costoTotal').val(costoTotalAct);
    }

}

// Funcion: Calcula el monto de gastos funerarios
function getMontoMemorial(id){
    var total = 0;

    for(i=0;i<=3;i++){
        if(jQuery('#checkMem'+i).is(':checked')) {
            total = total + 1;
                if(validarCamposVaciosMem(i)){
                    jQuery('#selMemorial').val('0');
                    return true;
                }
        }
    }

    if(id==0){
        var suma = 0.0;
        var total = 0.0;
        var montoEmp = jQuery('#montoEmp').val();
        var montoDep = jQuery('#montoDep').val();
        var montoAmpl = jQuery('#montoAmpl').val();
        suma = parseFloat(montoEmp) + parseFloat(montoDep) + parseFloat(montoAmpl);
        total = suma.toFixed(2);
        jQuery('#montoTotal').val(total);
        jQuery('#montoMemorial').attr('value','0.00');
    }else if(id==1){
        if(total == 1  || total == 2 ){
            var url = "getMontoMemorial.htm?idTipoMemorial="+id;
            jQuery.get(url,function(mensaje){
                var resultado = mensaje;
                jQuery('#montoMemorial').attr('value',resultado);
                sumaTotalMemorial(id,resultado);
            });
        }else{
            alert('Debe escoger 1 o 2 Dependientes');
            limpiarSeleccionMemorial();
            return true;
        }
    }else if(id==2){
        if(total > 2){
            var url = "getMontoMemorial.htm?idTipoMemorial="+id;
            jQuery.get(url,function(mensaje){
                var resultado = mensaje;
                jQuery('#montoMemorial').attr('value',resultado);
                sumaTotalMemorial(id,resultado);
            });
        }else{
            alert('Debe escoger 3 o 4 Dependientes');
            limpiarSeleccionMemorial();
            return true;
        }
    }

}

// Funcion: Suma total con la ampliacion
function sumaTotalAmp(id,montoamp){
    var montoEmp = jQuery('#montoEmp').val();
    var montoDep = jQuery('#montoDep').val();
    var montoTot = jQuery('#montoTotal').val();
    var total = 0.0;
    var montoEmpDep = 0.0;
    var montoAmp = 0.0;
    var montoTotal = 0.0;
    var resultado = 0.0;

    montoEmpDep = parseFloat(montoEmp) + parseFloat(montoDep);
    montoAmp = parseFloat(montoamp);
    montoTotal = parseFloat(montoTot);

    if(id!=0){
        total = montoEmpDep + montoAmp;
    }else{
        total = montoTotal - montoAmp;
    }
    resultado = total.toFixed(2);
    jQuery('#montoTotal').val(resultado);
}

// Funcion: Suma total con memorial
function sumaTotalMemorial(id,montoamp){
    var montoEmp = jQuery('#montoEmp').val();
    var montoDep = jQuery('#montoDep').val();
    var montoAmp = jQuery('#montoAmpl').val();
    var montoTot = jQuery('#montoTotal').val();
    var montoEmpDep = 0.0;
    var montoMem = 0.0;
    var montoTotal = 0.0;
    var total = 0.0;
    var resultado = 0.0;

    montoEmpDep = parseFloat(montoEmp) + parseFloat(montoDep) + parseFloat(montoAmp);
    montoMem = parseFloat(montoamp);
    montoTotal = parseFloat(montoTot);

    if(id!=0){
        total = montoEmpDep + montoMem;
    }else{
        total = montoTotal - montoMem;
    }
    resultado = total.toFixed(2);
    jQuery('#montoTotal').val(resultado);
}

// Funcion: Elimina dependientes
function eliminaDependientes(id){
    var montoDependiente = jQuery('#monto'+id).val();
    var montoTotalDependiente = jQuery('#montoDep').val();
    var montoEmpDep = jQuery('#montoEmpDep').val();
    var montoTotal = jQuery('#montoTotal').val();

    var total = 0.0;
    total = (parseFloat(montoTotalDependiente) - parseFloat(montoDependiente)).toFixed(2);
    jQuery('#montoDep').val(total);

    total = 0.0;
    total = (parseFloat(montoEmpDep) - parseFloat(montoDependiente)).toFixed(2);
    jQuery('#montoEmpDep').val(total);

    total = 0.0;
    total = (parseFloat(montoTotal) - parseFloat(montoDependiente)).toFixed(2);
    jQuery('#montoTotal').val(total);
}

// Funcion: Resetear dependientes
function limpiarDependiente(id){
    jQuery('#select'+id).val('0');
    jQuery('#paterno'+id).val('');
    jQuery('#materno'+id).val('');
    jQuery('#nombre'+id).val('');
    jQuery('#sexo'+id).val('');
    jQuery('#monto'+id).val('');
    jQuery('#fecha'+id).val('');
    jQuery('#sexoInput'+id).val('');
}

// Funcion: Resetear las ampliaciones
function limpiarAmpliaciones(){
    var resultado = jQuery('#montoAmpl').val();
    sumaTotalAmp(0,resultado);
    jQuery('#montoAmpl').attr('value','0.00');
    jQuery('#selAmpliacion').val('0');
}

function limpiarConyuge(id){
    var dependiente = jQuery('#select'+id).val();
    if(dependiente==3 || dependiente==4){
        jQuery('#existeConyuge').attr('value',0);
        jQuery('#existeConyuge').val(0);
    }
}

function limpiarSeleccionMemorial(){
    jQuery('#selMemorial').val('0');
    jQuery('#checkMem0').prop("checked", false);
    jQuery('#checkMem1').prop("checked", false);
    jQuery('#checkMem2').prop("checked", false);
    jQuery('#checkMem3').prop("checked", false);
}

// Funcion: formatear fecha
function formatoFecha(fecha){
    var dia = fecha.substring(0,2);
    var mes = fecha.substring(3,5);
    var anio = fecha.substring(6,10);
    var resultado = anio+'-'+mes+'-'+dia;
    return resultado;
}

// Funcion: boton BUSCAR
function getBotonBuscar(){
    var empleado = jQuery('#numEmp').val();
    var url = 'solicitud.htm?tipoEmp='+empleado;
    jQuery('#frmConsulta').attr('action',url);
    jQuery('#frmConsulta').submit();
}

// Funcion: Total de checkbox seleccionados
function totalChecks(){
    var total = 0;
    for(i=0;i<=9;i++){
        if(jQuery('#check'+i).is(':checked')) {
            total = total + 1;
        }
    }
    return total;
}

// Funcion: Setter el campo sexo
function setSexo(id,parentesco){
    var sexo = '';
    if(parentesco==4 || parentesco==6){
        jQuery('#sexo'+id).attr('value','2');
        jQuery('#sexoInput'+id).attr('value','F');
        sexo = 'F';
    }else if(parentesco==3 || parentesco==5){
        jQuery('#sexo'+id).attr('value','1');
        jQuery('#sexoInput'+id).attr('value','M');
        sexo = 'M';
    }
    return sexo;
}

function setConyuge(parentesco){
    if(parentesco==3 || parentesco==4){
        jQuery('#existeConyuge').val(1);
        jQuery('#existeConyuge').attr('value',1);
    }
}

function validarConyuge(id){
    var dependiente = jQuery('#select'+id).val();
    var existeConyuge = jQuery('#existeConyuge').val();
    if(existeConyuge==1 && (dependiente==3 || dependiente==4)){
        alert('Ya tienes agregado un conyuge');
        jQuery('#select'+id).attr('value',0);
        jQuery('#select'+id).val(0);
    }

}

function mayoriaEdad(fechaNac){
    //Fecha Actual
    var date = new Date();
    var diaActual = date.getDate();
    var mesActual = date.getMonth();
    var anioActual = date.getFullYear();

    //Fecha Nacimiento
    var diaNac = fechaNac.substring(0,2);
    var mesNac = fechaNac.substring(3,5);
    var anioNac = fechaNac.substring(6,10);

    var restantes = 0;
    var edad = 0;
    if(anioActual - anioNac == 0){
        restantes = 0;
    }else if(mesNac > (mesActual+1) && anioActual - anioNac != 0){
        restantes = 1;
    }else if(mesNac == (mesActual + 1) && anioActual - anioNac != 0){
        if(diaNac > diaActual){
            restantes = 1;
        }else{
            restantes = 0;
        }
    }
    edad = anioActual - anioNac - restantes;
    //alert('edad: '+edad);
    return edad;
}

function fechaFuturo(fechaNacimiento){
    var fechaActual = new Date();
    var dia = parseInt(fechaNacimiento.substring(0,2));
    var mes = parseInt(fechaNacimiento.substring(3,5)) - 1;
    var anio = parseInt(fechaNacimiento.substring(6,10));
    var fechaNac = new Date(anio, mes, dia);
    var resultado;

    if(fechaActual.valueOf() > fechaNac.valueOf()){ // Fecha Actual es mayor a la fecha de nacimiento
        resultado = true;
    }else{ // Fecha Nacimiento es mayor a la fecha actual
        resultado = false;
    }

    return resultado;
}

function validarFechaConsulmed(id){
    //Fecha Actual
    var date = new Date();
    var anioActual = date.getFullYear();

    //Fecha Nacimiento
    var fechaDep = jQuery('#fechaDep'+id).val();
    var anioNac = fechaDep.substring(6,10);

    var comp = anioActual - anioNac;
    //alert('Anio Actual: '+anioActual+' Anio Nac: '+anioNac+' Diferencia: '+comp);

    if(comp<40){
        alert('Al parecer la fecha de nac. de su dependiente es muy reciente');
    }
    return true;
}

function guardarSolicitud(nombreForm,nombreAction){
    if(validarCamposVaciosDep()){
        return true;
    }else if(validarCamposVaciosCon()){
        return true;
    }
    enviar(nombreForm,nombreAction);
}

function guardarDependiente(nombreForm,nombreAction){
    if(validarCamposVaciosDep()){
        return true;
    }
    enviar(nombreForm,nombreAction);
}

function guardarConsulmed(nombreForm,nombreAction){
    if(validarCamposVaciosCon()){
        return true;
    }
    enviar(nombreForm,nombreAction);
}

function validarCamposVaciosDep(){
    for(i=0;i<=10;i++){
        if(jQuery('#check'+i).is(':checked')) {
            var paterno = jQuery('#paterno'+i).val();
            var materno = jQuery('#materno'+i).val();
            var nombre = jQuery('#nombre'+i).val();

            if(paterno==''){
                alert('Debe de llenar el campo apellido paterno');
                return true;
            }else if(materno==''){
                alert('Debe de llenar el campo apellido materno');
                return true;
            }else if(nombre==''){
                alert('Debe de llenar el campo nombre');
                return true;
            }
        }
    } // Fin del for
}

function validarCamposVaciosCon(){
    for(i=0;i<=3;i++){
        if(jQuery('#checkCon'+i).is(':checked')) {
            var paterno = jQuery('#paternoCon'+i).val();
            var materno = jQuery('#maternoCon'+i).val();
            var nombre = jQuery('#nombreCon'+i).val();
            var fechaDep = jQuery('#fechaDep'+i).val();

            if(paterno==''){
                alert('Debe de llenar el campo apellido paterno consulmed');
                return true;
            }else if(materno==''){
                alert('Debe de llenar el campo apellido materno consulmed');
                return true;
            }else if(nombre==''){
                alert('Debe de llenar el campo nombre consulmed');
                return true;
            }else if(fechaDep==''){
                alert('Debe de llenar el campo fecha de nac consulmed');
                return true;
            }

        }
    } // Fin del for
}

function validarCamposVaciosMem(i){
    if(jQuery('#checkMem'+i).is(':checked')) {
        var paterno = jQuery('#paternoMem'+i).val();
        var materno = jQuery('#maternoMem'+i).val();
        var nombre = jQuery('#nombreMem'+i).val();
        var fechaDep = jQuery('#fechaMem'+i).val();

        if(paterno==''){
            alert('Debe de llenar el campo apellido paterno memorial');
            return true;
        }else if(materno==''){
            alert('Debe de llenar el campo apellido materno memorial');
            return true;
        }else if(nombre==''){
            alert('Debe de llenar el campo nombre memorial');
            return true;
        }else if(fechaDep==''){
            alert('Debe de llenar el campo fecha de nac memorial');
            return true;
        }

    }
}

function seleccionarOpcion(controlador){
    document.forms['frmGastosMed'].action =  controlador;
    document.forms['frmGastosMed'].submit();
}

function verSolAltaDependiente(dependiente){
    frmSolAltaDependiente.idDependiente.value = dependiente;
    document.forms['frmSolAltaDependiente'].action =  'consultaAltaDependiente.htm';
    document.forms['frmSolAltaDependiente'].submit();
}

function verSolAltaConsulmed(dependiente){
    frmSolAltaConsulmed.idDependiente.value = dependiente;
    document.forms['frmSolAltaConsulmed'].action =  'consultaAltaConsulmed.htm';
    document.forms['frmSolAltaConsulmed'].submit();
}

function verSolBajaDependiente(dependiente){
    frmSolBajaDependiente.idDependiente.value = dependiente;
    document.forms['frmSolBajaDependiente'].action =  'consultaBajaDependiente.htm';
    document.forms['frmSolBajaDependiente'].submit();
}

function verSolBajaConsulmed(dependiente){
    frmSolBajaConsulmed.idDependiente.value = dependiente;
    document.forms['frmSolBajaConsulmed'].action =  'consultaBajaConsulmed.htm';
    document.forms['frmSolBajaConsulmed'].submit();
}


/**
 * MODULO: DATOS FISCALES
 * @returns {Boolean}
 */
function validarDatosFiscales(){
    if(validarCamposDatosFiscales()){
        return true;
    }else{
        enviar("frmRegistroDatosFiscales","registroDatosFiscales.htm");
    }
}

function validarCamposDatosFiscales(){
    if(document.getElementById("calle").value==""){
        alert('Campo calle vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("calle").value)){
        alert('Campo calle no permite carateres especiales, ni comas, ni puntos')
        return true;
    }else if(document.getElementById("numero").value==""){
        alert('Campo numero vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("numero").value)){
        alert('Campo numero no permite carateres especiales, ni comas, ni puntos')
        return true;
    }else if(document.getElementById("cp").value==""){
        alert('Campo codigo postal vacio')
        return true;
    }else if(document.getElementById("cp").value.length < 5){
        alert('Campo codigo postal menor a 5 digitos')
        return true;
    }else if(!/^([0-9])*$/.test(document.getElementById("cp").value)){
        alert('Campo codigo postal solo permite numeros')
        return true;
    }else if(document.getElementById("colonia").value==""){
        alert('Campo colonia vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("colonia").value)){
        alert('Campo colonia no permite carateres especiales, ni comas, ni puntos')
        return true;
    }else if(document.getElementById("deleg_mun").value==""){
        alert('Campo delegacion y/o municipio vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("deleg_mun").value)){
        alert('Campo deleg_mun no permite carateres especiales, ni comas, ni puntos')
        return true;
    }else if(document.getElementById("entidad").value==""){
        alert('Campo entidad vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("entidad").value)){
        alert('Campo entidad no permite carateres especiales, ni comas, ni puntos')
        return true;
    }
}

function validarDatosFiscalesSol(){
    if(document.getElementById("calle").value==""){
        alert('Campo calle vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("calle").value)){
        alert('Campo calle no permite carateres especiales #��?[]}{')
        return true;
    }else if(document.getElementById("numero").value==""){
        alert('Campo numero vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("numero").value)){
        alert('Campo numero no permite carateres especiales #��?[]}{')
        return true;
    }else if(document.getElementById("cp").value==""){
        alert('Campo codigo postal vacio')
        return true;
    }else if(document.getElementById("cp").value.length < 5){
        alert('Campo codigo postal menor a 5 digitos')
        return true;
    }else if(!/^([0-9])*$/.test(document.getElementById("cp").value)){
        alert('Campo codigo postal solo permite numeros')
        return true;
    }else if(document.getElementById("colonia").value==""){
        alert('Campo colonia vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("colonia").value)){
        alert('Campo colonia no permite carateres especiales #��?[]}{')
        return true;
    }else if(document.getElementById("deleg_mun").value==""){
        alert('Campo delegacion y/o municipio vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("deleg_mun").value)){
        alert('Campo deleg_mun no permite carateres especiales #��?[]}{')
        return true;
    }else if(document.getElementById("entidad").value==""){
        alert('Campo entidad vacio')
        return true;
    }else if(!/^([A-Za-z0-9\s])*$/.test(document.getElementById("entidad").value)){
        alert('Campo entidad no permite carateres especiales #��?[]}{')
        return true;
    }
    enviar("frmRegistroDatosFiscalesSol","registroDatosFiscalesSol.htm");
}


/**
 *
 * MODULO: AUTORIZACIONES
 *
 */
function verSolicitud(parametro1,parametro2,parametro3,parametro4,parametro5,parametro6,parametro7){
    if(parametro1==1){	// Altas
        if(parametro2==1){ // Ver solicitud
            frmAutorizarSolAlta.numEmpSolicitado.value = parametro3;
            frmAutorizarSolAlta.paramNumEmp.value = parametro5;
            frmAutorizarSolAlta.paramNombreEmp.value = parametro6;
            frmAutorizarSolAlta.paramPerfilEmp.value = parametro7;
            document.forms['frmAutorizarSolAlta'].action =  'consultaDetalleSolicitudAlta.htm';
            document.forms['frmAutorizarSolAlta'].submit();
        }else if(parametro2==2){ // Ver dependiente
            frmAutorizarDepAlta.numEmpSolicitado.value = parametro3;
            frmAutorizarDepAlta.idDependiente.value = parametro4;
            frmAutorizarDepAlta.paramNumEmp.value = parametro5;
            frmAutorizarDepAlta.paramNombreEmp.value = parametro6;
            frmAutorizarDepAlta.paramPerfilEmp.value = parametro7;
            document.forms['frmAutorizarDepAlta'].action =  'consultaDetalleDependienteAlta.htm';
            document.forms['frmAutorizarDepAlta'].submit();
        }else if(parametro2==3){ // Ver consulmed
            frmAutorizarConsulmedAlta.numEmpSolicitado.value = parametro3;
            frmAutorizarConsulmedAlta.idDependiente.value = parametro4;
            frmAutorizarConsulmedAlta.paramNumEmp.value = parametro5;
            frmAutorizarConsulmedAlta.paramNombreEmp.value = parametro6;
            frmAutorizarConsulmedAlta.paramPerfilEmp.value = parametro7;
            document.forms['frmAutorizarConsulmedAlta'].action =  'consultaDetalleConsulmedAlta.htm';
            document.forms['frmAutorizarConsulmedAlta'].submit();
        }
    }else if(parametro1==2){ // Bajas
        if(parametro2==1){ // Ver solicitud
            frmAutorizarSolBaja.numEmpSolicitado.value = parametro3;
            frmAutorizarSolBaja.paramNumEmp.value = parametro5;
            frmAutorizarSolBaja.paramNombreEmp.value = parametro6;
            frmAutorizarSolBaja.paramPerfilEmp.value = parametro7;
            document.forms['frmAutorizarSolBaja'].action =  'consultaDetalleSolicitudBaja.htm';
            document.forms['frmAutorizarSolBaja'].submit();
        }else if(parametro2==2){ // Ver dependiente
            frmAutorizarDepBaja.numEmpSolicitado.value = parametro3;
            frmAutorizarDepBaja.idDependiente.value = parametro4;
            frmAutorizarDepBaja.paramNumEmp.value = parametro5;
            frmAutorizarDepBaja.paramNombreEmp.value = parametro6;
            frmAutorizarDepBaja.paramPerfilEmp.value = parametro7;
            document.forms['frmAutorizarDepBaja'].action =  'consultaDetalleDependienteBaja.htm';
            document.forms['frmAutorizarDepBaja'].submit();
        }else if(parametro2==3){ // Ver consulmed
            frmAutorizarConsulmedBaja.numEmpSolicitado.value = parametro3;
            frmAutorizarConsulmedBaja.idDependiente.value = parametro4;
            frmAutorizarConsulmedBaja.paramNumEmp.value = parametro5;
            frmAutorizarConsulmedBaja.paramNombreEmp.value = parametro6;
            frmAutorizarConsulmedBaja.paramPerfilEmp.value = parametro7;
            document.forms['frmAutorizarConsulmedBaja'].action =  'consultaDetalleConsulmedBaja.htm';
            document.forms['frmAutorizarConsulmedBaja'].submit();
        }
    }
}

function autorizarCheck(id){
    document.getElementById("rechazar"+id).checked = false;
}

function rechazarCheck(id){
    document.getElementById("autorizar"+id).checked = false;
}

function validarComentarios(forma,controlador){
    var tamListSol = jQuery('#tamListSol').val();
    var date = new Date();
    var dia = date.getDate();
    var mes = date.getMonth();
    var yyy = date.getFullYear();
    var fechaFinal = dia+'/'+mes+'/'+yyy;
    var comentario = '';


    for(i=0;i<=tamListSol;i++){
        if(jQuery('#rechazar'+i).is(':checked')) {
            //alert('elemento: '+('rechazar'+i));
            //alert(jQuery('#comentario'+i).val());
            if(jQuery('#comentario'+i).val()==""){
                alert('Debe de llenar el comentario cuando rechaza una solicitud');
                return true;
            }
            comentario = fechaFinal+' '+jQuery('#comentario'+i).val();
            //jQuery('#comentario'+i).attr('value',comentario);
            jQuery('#comentario'+i).val(comentario);
        }
    }
    //    alert('Todos los comentarios estan llenos');
    enviar(forma,controlador);
}


/**
 *
 * MODULO: EDICION DE SOLICITUD
 * @param idSolicitud
 */
function editarSolicitud(){
    var controller = 'editarSolicitudAdmin.htm';
    document.forms['frmSolEdicion'].action =  controller;
    document.forms['frmSolEdicion'].submit();
}

function setSolicitud(){
    var fecha = jQuery('#fechaAlta').val();
    var idSolicitud = jQuery('#idSolicitud').val();
    var idTipoSol = jQuery('#idTipoSol').val();

    if(fecha==''){
        alert('Ingrese la fecha');
        return true;
    }

    var dia = fecha.substring(0,2);
    var mes = fecha.substring(3,5);
    document.getElementById('dia').value = dia;
    document.getElementById('mes').value = mes;
    enviar('frmGuardarSolicitudAdmin','guardarSolicitudAdmin.htm');
}

function editarDependiente(tipoSolicitud,idDependiente,idParentesco,parentesco,apellidoP,apellidoM,nombre,
                            fechaNac,fechaAlta,idEstatulSol,estatulSol,sexo,idTipoEmpleado,costoEmp,
                            idSolicitud,idCobertura,costo,costoTotal){
    var controller = '';
    if(tipoSolicitud==1){ // Editar Dependiente
        controller = 'editarDependienteAdmin.htm';
    }else if(tipoSolicitud==2){ // Editar Consulmed
        controller = 'editarConsumlmedAdmin.htm';
    }
    frmSolEdicion.idDependiente.value = idDependiente;
    frmSolEdicion.idConsulmed.value = idDependiente;
    frmSolEdicion.idParentesco.value = idParentesco;
    frmSolEdicion.parentesco.value = parentesco;
    frmSolEdicion.apellidoP.value = apellidoP;
    frmSolEdicion.apellidoM.value = apellidoM;
    frmSolEdicion.nombre.value = nombre;
    frmSolEdicion.fechaNac.value = fechaNac;
    frmSolEdicion.fechaAlta.value = fechaAlta;
    frmSolEdicion.idEstatulSol.value = idEstatulSol;
    frmSolEdicion.estatulSol.value = estatulSol;
    frmSolEdicion.sexo.value = sexo;
    frmSolEdicion.idTipoEmpleado.value = idTipoEmpleado;
    frmSolEdicion.costoEmp.value = costoEmp;
    frmSolEdicion.idSolicitud.value = idSolicitud;
    frmSolEdicion.idCobertura.value = idCobertura;
    frmSolEdicion.costo.value = costo;
    frmSolEdicion.costoTotal.value = costoTotal;
    document.forms['frmSolEdicion'].action =  controller;
    document.forms['frmSolEdicion'].submit();
}

function validarParentesco(){

    var idParentesco = jQuery('#idParentesco').val();
    var idConyuge = jQuery('#idConyuge').val();
    //alert(idParentesco+'  '+idConyuge);

    var id = jQuery('#idParent').val();
    //alert(id);
    if(id==3 || id==4){ // Si escoge conyuge

        if((idParentesco== 5 || idParentesco== 6) && (idConyuge==3 || idConyuge== 4)){
            alert('Ya tiene un conyuge, no puede seleccionar este estatus');
            return true;
        }else{

            var texto = jQuery("#idParent option:selected").text();
            if(id!=0){
                jQuery('#parentesco').attr('value',texto);
            }

        }

    }else if(id==5 || id==6){ // Si escoge hijos

        var texto = jQuery("#idParent option:selected").text();
        if(id!=0){
            jQuery('#parentesco').attr('value',texto);
        }

    }


}

function validarEstatus(){
    var idEstAnterior = jQuery('#idEstatulSol').val();
    var idEstNuevo = jQuery('#idEst').val();
    var texto = jQuery("#idEst option:selected").text();
    //alert('Est Anterior: '+idEstAnterior+' Est Nuevo: '+idEstNuevo);
    if(idEstAnterior==1 && idEstNuevo ==1){
        // Aplica sin recalculo
        jQuery('#estatulSol').attr('value',texto);
    }else if(idEstAnterior==1 && idEstNuevo ==2){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==1 && idEstNuevo ==3){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==1 && idEstNuevo ==4){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==1 && idEstNuevo ==5){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==1 && idEstNuevo ==6){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==2 && idEstNuevo ==1){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==2 && idEstNuevo ==2){
        // Aplica con recalculo
        jQuery('#estatulSol').attr('value',texto);
    }else if(idEstAnterior==2 && idEstNuevo ==3){
        // Aplica con recalculo
        jQuery('#estatulSol').attr('value',texto);
    }else if(idEstAnterior==2 && idEstNuevo ==4){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==2 && idEstNuevo ==5){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==2 && idEstNuevo ==6){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==3 && idEstNuevo ==1){
        // Aplica sin recalculo
        jQuery('#estatulSol').attr('value',texto);
    }else if(idEstAnterior==3 && idEstNuevo ==2){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==3 && idEstNuevo ==3){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==3 && idEstNuevo ==4){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==3 && idEstNuevo ==5){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==3 && idEstNuevo ==6){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==4 && idEstNuevo ==1){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==4 && idEstNuevo ==2){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==4 && idEstNuevo ==3){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==4 && idEstNuevo ==4){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==4 && idEstNuevo ==5){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==4 && idEstNuevo ==6){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==5 && idEstNuevo ==1){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==5 && idEstNuevo ==2){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==5 && idEstNuevo ==3){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==5 && idEstNuevo ==4){
        // Aplica con recalculo
        jQuery('#estatulSol').attr('value',texto);
    }else if(idEstAnterior==5 && idEstNuevo ==5){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==5 && idEstNuevo ==6){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==6 && idEstNuevo ==1){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==6 && idEstNuevo ==2){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==6 && idEstNuevo ==3){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==6 && idEstNuevo ==4){
        // Aplica sin recalculo
        jQuery('#estatulSol').attr('value',texto);
    }else if(idEstAnterior==6 && idEstNuevo ==5){
        setterValorOriginal();
        return true;
    }else if(idEstAnterior==6 && idEstNuevo ==6){
        setterValorOriginal();
        return true;
    }
}

function validarEstatusCon(){
    var idEstAnterior = jQuery('#idEstatulSol').val();
    var idEstNuevo = jQuery('#idEstCon').val();
    var texto = jQuery("#idEstCon option:selected").text();
    jQuery('#estatulSol').attr('value',texto);
}

function setterValorOriginal(){
    alert('No aplica este cambio de estatus');
    jQuery('#idEst').attr('value',0);
    jQuery('#idEst').val(0);
}

function validarDependiente(){
    var idEstAnterior = jQuery('#idEstatulSol').val();
    var idEstNuevo = jQuery('#idEst').val();
    var idParentescoAnterior = jQuery('#idParentesco').val();
    var idParentesco = jQuery('#idParent').val();
    var fechaNacAnt = jQuery('#fechaNacAnt').val();
    var fechaNacNvo = jQuery('#fechaNacNvo').val();
    var validaParentesco = 0;

    if(idParentesco!=0){
        frmGuardarDependienteAdmin.idParentescoNvo.value = idParentesco;
        validaParentesco = idParentesco;
    }else{
        frmGuardarDependienteAdmin.idParentescoNvo.value = idParentescoAnterior;
        validaParentesco = idParentescoAnterior;
    }

    if(idEstNuevo!=0){
        frmGuardarDependienteAdmin.idEstatulSolNvo.value = idEstNuevo;
    }else{
        frmGuardarDependienteAdmin.idEstatulSolNvo.value = idEstAnterior;
    }

    if(validaParentesco==5 || validaParentesco==6){			// Si es hijo valida que no exceda los 25 anios
        if(mayoriaEdad(fechaNacNvo)>=25 && (validaParentesco==5 || validaParentesco==6)){
            alert('No puede agregar dependientes hijos mayores a 25 anios, favor de corregir');
            return true;
        }
    }else if(validaParentesco==3 || validaParentesco==4){		// Se es conyuge valida que sea mayor de edad
        if(mayoriaEdad(fechaNacNvo)<18 && (validaParentesco==3 || validaParentesco==4)){
            alert('El conyuge aun no cumple la mayoria de edad, favor de validarlo');
            return true;
        }
    }
    enviar('frmGuardarDependienteAdmin','guardarDependienteAdmin.htm');
}

function validarConsulmed(){
    var idParent = jQuery('#idParent').val();
    var idEstatus = jQuery('#idEstCon').val();
    if(idParent!=0){
        frmGuardarConsulmedAdmin.idParentesco.value = idParent;
    }
    if(idEstatus!=0){
        frmGuardarConsulmedAdmin.idEstatulSol.value = idEstatus;
    }
    enviar('frmGuardarConsulmedAdmin','guardarConsulmedAdmin.htm');
}


/**
 *
 * MODULO: REEMBOLSOS
 * Funcion para visualizar la solicitud de Reembolso
 * @param idReembolso
 */
function verDetalleReembolso(idReembolso){
    frmReembolso.idReembolso.value = idReembolso;
    document.forms['frmReembolso'].action =  'consultaDetalleReembolso.htm';
    document.forms['frmReembolso'].submit();
}

/**
 * Funcion para llenar en automatico el formato de enfermedad/accidente
 * @param nombre
 * @param parentesco
 * @param fechaNac
 * @param fechaAutorizacion
 * @param sexo
 * @returns
 */
function llenarCampos(nombre,parentesco,fechaNac,fechaAutorizacion,sexo){
    document.getElementById('nombreAfectado').value = nombre;
    document.getElementById('parentescoTitular').value = parentesco;
    document.getElementById('fechaNac').value = fechaNac;
    document.getElementById('fechaAlta').value = fechaAutorizacion;
    document.getElementById('sexo').value = sexo;
}

function llenarCamposRelacion(nombre,parentesco){
    document.getElementById('nombreAfectado').value = nombre;
    document.getElementById('parentescoTitular').value = parentesco;
}

function sumaReembolso(){
    var desglose1 = parseFloat(document.getElementById('desglose1').value);
    var desglose2 = parseFloat(document.getElementById('desglose2').value);
    var desglose3 = parseFloat(document.getElementById('desglose3').value);
    var desglose4 = parseFloat(document.getElementById('desglose4').value);
    var desglose5 = parseFloat(document.getElementById('desglose5').value);
    var desglose6 = parseFloat(document.getElementById('desglose6').value);
    var desglose7 = parseFloat(document.getElementById('desglose7').value);
    var desglose8 = parseFloat(document.getElementById('desglose8').value);
    var desglose9 = parseFloat(document.getElementById('desglose9').value);
    var desglose10 = parseFloat(document.getElementById('desglose10').value);
    var desglose11 = parseFloat(document.getElementById('desglose11').value);
    var desglose12 = parseFloat(document.getElementById('desglose12').value);
    var desglose13 = parseFloat(document.getElementById('desglose13').value);
    var desglose14 = parseFloat(document.getElementById('desglose14').value);
    var desglose15 = parseFloat(document.getElementById('desglose15').value);
    var desglose16 = parseFloat(document.getElementById('desglose16').value);
    var desglose17 = parseFloat(document.getElementById('desglose17').value);

    var total = desglose1+desglose2+desglose3+desglose4+desglose5+desglose6+desglose7+desglose8+desglose9+desglose10+desglose11+desglose12+desglose13+desglose14+desglose15+desglose16+desglose17;
    document.getElementById('desgloseTotal').value = total;
}

function validarRelacionReembolso(){
    if(document.getElementById("nombreAfectado").value==""){
        alert('Campo Nombre Afectado vacio');
        return true;
    }else if(document.getElementById("descripcionEnfermedad").value==""){
        alert('Campo Tipo de Enfermedad y/o Lesion vacio');
        return true;
    }

    var uno = jQuery("input[id=tipoReembolsoEnf]:checked").val();
    var dos = jQuery("input[id=tipoReembolsoAcc]:checked").val();

    if(uno!=1){
        if(dos!=2){
            alert('No selecciono un tipo de Enfermedad/Accidente');
            return true;
        }
    }

    if(jQuery('#noDocumento0').val()=="" && jQuery('#nombreExpediente0').val()=="" && jQuery('#concepto0').val()=="" && jQuery('#importe0').val()==""){
        alert('Al menos un concepto debe ser llenado');
        return true;
    }else if(jQuery('#noDocumento0').val()==""){
        alert('No Documento debe ser llenado');
        return true;
    }else if(jQuery('#nombreExpediente0').val()==""){
        alert('Nombre de quien expide debe ser llenado');
        return true;
    }else if(jQuery('#concepto0').val()==""){
        alert('Concepto debe ser llenado');
        return true;
    }else if(jQuery('#importe0').val()==""){
        alert('Importe debe ser llenado');
        return true;
    }

    var mascara = /^([0-9])*[.]?[0-9]*$/;
    var importe = jQuery('#importe0').val();

    if (!mascara.test(importe)){
        alert('Solo se aceptan digitos y el punto decimal en el campo importe');
        return true;
    }


    for(i=1;i<15;i++){
        if(jQuery('#noDocumento'+i).val()!=""){
            if(jQuery('#nombreExpediente'+i).val()==""){
                alert('Nombre de quien expide debe ser llenado');
                return true;
            }else if(jQuery('#concepto'+i).val()==""){
                alert('Concepto debe ser llenado');
                return true;
            }else if(jQuery('#importe'+i).val()==""){
                alert('Importe debe ser llenado');
                return true;
            }
            var importe = jQuery('#importe'+i).val();
            if (!mascara.test(importe)){
                alert('Solo se aceptan digitos y el punto decimal en el campo importe');
                return true;
            }
        }
    }

    enviar('frmGuardarRelacionDocumentos','guardarRelacionDocumentos.htm');
}

function validarAccidenteEnfermedad(){

    // Motivo Reclamacion
    var motReclaReem = jQuery("input[id=motReclaReem]:checked").val();
    var motReclaCir = jQuery("input[id=motReclaCir]:checked").val();
    var motReclaPago = jQuery("input[id=motReclaPago]:checked").val();

    if(motReclaReem!=1){
        if(motReclaCir!=2){
            if(motReclaPago!=3){
                alert('No selecciono un motivo de reclamacion');
                return true;
            }
        }
    }

    // Tipo de Reclamacion
    var tipoReclaAcc = jQuery("input[id=tipoReclaAcc]:checked").val();
    var tipoReclaEmb = jQuery("input[id=tipoReclaEmb]:checked").val();
    var tipoReclaEnf = jQuery("input[id=tipoReclaEnf]:checked").val();

    if(tipoReclaAcc!=1){
        if(tipoReclaEmb!=2){
            if(tipoReclaEnf!=3){
                alert('No selecciono un tipo de reclamacion');
                return true;
            }
        }
    }

    // Estado Reclamacion
    var estReclaIni = jQuery("input[id=estReclaIni]:checked").val();
    var estReclaComp = jQuery("input[id=estReclaComp]:checked").val();

    if(estReclaIni!=1){
        if(estReclaComp!=2){
            alert('No selecciono si la reclamacion es inicial o complementaria');
            return true;
        }
    }

    if(jQuery('#fechaSintomas').val()==''){
        alert('No selecciono la fecha en la que ocurrio el accidente o los primeros sintomas ');
        return true;
    }
    if(jQuery('#sintomas').val()==''){
        alert('No indico el tipo de alteraciones que presento ');
        return true;
    }
    if(jQuery('#fechaVisitaMed').val()==''){
        alert('No selecciono la fecha en que visito por primera vez al medico ');
        return true;
    }
    if(jQuery('#diagnostico').val()==''){
        alert('No indico el diagnostico motivo de su reclamacion ');
        return true;
    }
    if(jQuery('#accidente').val()==''){
        alert('No indico si es accidente, el detalle');
        return true;
    }
    if(jQuery('#hospital').val()==''){
        alert('No indico el hostital en el que fue atendido ');
        return true;
    }
    if(jQuery('#estudiosDiagnostico').val()==''){
        alert('No indico los estudios que le realizaron para su tratamiento ');
        return true;
    }
    if(jQuery('#medicoTratante').val()==''){
        alert('No indico el nombre del medico tratante');
        return true;
    }
    if(jQuery('#especialidad').val()==''){
        alert('No indico la especialidad de su medico');
        return true;
    }
    if(jQuery('#direccionMedico').val()==''){
        alert('No indico la direccion de su medico');
        return true;
    }
    if(jQuery('#telEmailMedico').val()==''){
        alert('No indico el telefono y/o correo de su medico ');
        return true;
    }


    // Seguro de Auto
    var seguroAutoSI = jQuery("input[id=seguroAutoSI]:checked").val();
    var seguroAutoNO = jQuery("input[id=seguroAutoNO]:checked").val();

    if(seguroAutoSI!=1){
        if(seguroAutoNO!=2){
            alert('No selecciono si cuenta con seguro de automovil');
            return true;
        }
    }

    enviar('frmFormatoAccidenteEnfermedad','formatoAccidenteEnfermedadLleno.htm');
}

function validarReporteGastosMedicos(){

    // Tipo Reclamacion
    var estReclaIni = jQuery("input[id=estReclaIni]:checked").val();
    var estReclaComp = jQuery("input[id=estReclaComp]:checked").val();

    if(estReclaIni!=1){
        if(estReclaComp!=2){
            alert('No selecciono el tipo de reclamacion');
            return true;
        }
    }

    // Motivo Reclamacion
    var tipoReclaEnf = jQuery("input[id=tipoReclaEnf]:checked").val();
    var tipoReclaAcc = jQuery("input[id=tipoReclaAcc]:checked").val();

    if(tipoReclaEnf!=1){
        if(tipoReclaAcc!=2){
            alert('No selecciono el motivo de la reclamacion');
            return true;
        }
    }

    if(jQuery("#padecimientoReclamado").val()==''){
        alert('No lleno el campo padecimiento reclamado');
        return true;
    }

    var mascara = /^([0-9])*[.]?[0-9]*$/;
    var importe = '';

    for(i=1;i<18;i++){
        importe = jQuery('#desglose'+i).val();
        if (!mascara.test(importe)){
            alert('Solo se aceptan digitos y el punto decimal en el campo importe del desglose '+i);
            importe = '';
            return true;
        }
    }

    enviar('frmReporteGastosMedicosLleno','reporteGastosMedicosLleno.htm');
}

function deshabilitarFechaReclamacion(){
    jQuery('#fechaReclamacion').removeAttr('readonly');
}

function habilitarFechaReclamacion(){
    var date = new Date();
    var diaActual = date.getDate();
    var mesActual = date.getMonth()+1;
    var anioActual = date.getFullYear();

    var fechaActual = diaActual+'/'+mesActual+'/'+anioActual;
    jQuery('#fechaReclamacion').val(fechaActual);
    jQuery('#fechaReclamacion').attr('readonly', true);
}

/**
 * MODULO DE FINIQUITOS
 */
function generacionFiniquito(){
    var numEmp = jQuery('#numEmpSolicitado').val();
    var fecha = jQuery('#fecha').val();

    if(numEmp==''){
        alert('Ingresa el numero de empledo');
        return true;
    }
    if(fecha==''){
        alert('Ingresa la fecha de calculo');
        return true;
    }
    var dia = fecha.substring(0,2);
    var mes = fecha.substring(3,5);
    document.getElementById('dia').value = dia;
    document.getElementById('mes').value = mes;
    document.getElementById('fechaFiniq').value = fecha;
    enviar('frmCotizaFiniquito','generaFiniquito.htm');
}

/**
 * MODULO CAMBIO DE CATEGORIA
 */
function cambioCategoria(){
    var fecha = jQuery('#fecha').val();
    var dia = fecha.substring(0,2);
    var mes = fecha.substring(3,5);
    document.getElementById('dia').value = dia;
    document.getElementById('mes').value = mes;
}

/**
 * MODULO CAMBIO DE COBERTURA
 */
function cambioCobertura(){
    var numEmp = jQuery('#numEmpSolicitado').val();
    var fecha = jQuery('#fecha').val();
    var tipoAmp = jQuery('#tipoAmp').val();
    var dia = fecha.substring(0,2);
    var mes = fecha.substring(3,5);
    if(numEmp==''){
        alert('Ingresa el numero de empledo');
        return true;
    }
    if(fecha==''){
        alert('Ingresa la fecha de calculo');
        return true;
    }
    if(tipoAmp==-1){
        alert('Selecciona un tipo de cobertura');
        return true;
    }
    document.getElementById('dia').value = dia;
    document.getElementById('mes').value = mes;

    enviar('idConsultaCambioCobertura','consultaCambioCobertura.htm');
}

/**
 * MODULO REPORTES
 */
function validaFechasVacias(nombreForm,nombreAction){
    var fechaIni = jQuery('#fechaIni').val();
    var fechaFin = jQuery('#fechaFin').val();

    if(fechaIni==''){
        alert('Fecha inicial vacia');
        return true;
    }else if(fechaFin==''){
        alert('Fecha final vacia');
        return true;
    }else{
        enviar(nombreForm,nombreAction);
    }
}

/**
 * MODULO CONSTANCIAS
 */
function validaConstancia(nombreForm,nombreAction){
    var numEmp = jQuery('#numEmpSolicitado').val();
    var tipo = jQuery('#tipoConstancia').val();
    var fecha = jQuery('#fecha').val();

    if(numEmp==''){
        alert('Ingrese el numero de empleado');
        return true;
    }
    if(tipo==0){
        alert('Seleccione el tipo de reporte');
        return true;
    }
    if(tipo==2 && fecha==''){
        alert('Seleccione una fecha para la constancia de finiquito');
        return true;
    }

    enviar(nombreForm,nombreAction);
}

function validaFechas(nombreForm,nombreAction){

    var fechaIni = jQuery('#fechaIni').val();
    var fechaFin = jQuery('#fechaFin').val();

    if(fechaIni==''){
        alert('Fecha inicial vacia');
        return true;
    }else if(fechaFin==''){
        alert('Fecha final vacia');
        return true;
    }

    var diaIni = fechaIni.substring(0,2);
    var mesIni = fechaIni.substring(3,5);
    var anioIni = fechaIni.substring(6,10);
    var diaFin = fechaFin.substring(0,2);
    var mesFin = fechaFin.substring(3,5);
    var anioFin = fechaFin.substring(6,10);

    if(anioIni!=anioFin){
        alert('El anio no es el mismo');
        return true;
    }else if(mesIni>mesFin){
        alert('El mes inicial es mayor al mes final');
        return true;
    }
    /*
    else if((mesFin-mesIni)>1){
        alert('El rango de fechas no debe ser mayor a 2 meses');
        return true;
    }*/else{
        enviar(nombreForm,nombreAction);
    }

}

function validaReporte(nombreForm,nombreAction){
    var tipo = jQuery('#tipo').val();
    if(tipo==0){
        alert('Seleccione el tipo de reporte');
        return true;
    }
    validaFechas(nombreForm,nombreAction);
}

function validaReporteRenovacion(nombreForm,nombreAction){
    var tipo = jQuery('#tipo').val();
    if(tipo==0){
        alert('Seleccione el tipo de reporte');
        return true;
    }

    if(tipo==1){
        jQuery('#reporte').val(9);
    }else if(tipo==2){
        jQuery('#reporte').val(10);
    }else if(tipo==3){
        jQuery('#reporte').val(11);
    }else if(tipo==4){
        jQuery('#reporte').val(12);
    }else if(tipo==5){
		jQuery('#reporte').val(13);
    }else if(tipo==6){
		jQuery('#reporte').val(14);
    }

    enviar(nombreForm,nombreAction);
}

/**
 * MODULO CIRCULAR
 */
function validarDatosCircular(nombreForm,nombreAction){
    if(validarCamposDatosFiscales()){
        return true;
    }else{
        var siRenov = jQuery("input[id=siRenov]:checked").val();
        var noRenov = jQuery("input[id=noRenov]:checked").val();
        var siA1 = jQuery("input[id=siA1]:checked").val();
        var siA2N = jQuery("input[id=siA2N]:checked").val();
        var siA2E = jQuery("input[id=siA2E]:checked").val();
        var siA3N = jQuery("input[id=siA3N]:checked").val();
        var siA3E = jQuery("input[id=siA3E]:checked").val();
        var siA4N = jQuery("input[id=siA4N]:checked").val();
        var siA4E = jQuery("input[id=siA4E]:checked").val();
        var noA = jQuery("input[id=noA]:checked").val();

        if(siRenov==1 || noRenov==2){
            if(noRenov==2){
                enviar(nombreForm,nombreAction);
            }else if(siA1==1 || siA2N==2 || siA2E==3 || siA3N==4 || siA3E==5 || siA4N==6 || siA4E==7 || noA==0){
                enviar(nombreForm,nombreAction);
            }else{
                alert('No selecciono si desea o no Ampliacion');
                return true;
            }


        }else{
            alert('No selecciono si desea o no Renovar');
            return true;
        }
    }
}

function validaAceptoA3(){
    var aceptoA3 = jQuery("input[id=aceptoA3]:checked").val();
    if(aceptoA3==1){
        jQuery('#ventanaA3').css('display','block');
    }
}

/**
 * Modulo Fecha de Antiguedad
 */
function validarFechaAntiguedad(){
    var numEmp = jQuery('#numEmpSolicitado').val();
    var fechaElabTit = jQuery('#fechaElabTit').val();
    var fechaAltaTit = jQuery('#fechaAltaTit').val();
    var fechaElabDep = jQuery('#fechaElabDep').val();
    var fechaAltaDep = jQuery('#fechaAltaDep').val();
    var idDep = jQuery('#idDep').val();
    var onTit = jQuery("input[id=onTitular]:checked").val();
    var onDep = jQuery("input[id=onDependiente]:checked").val();

    if(numEmp==''){
        alert('Ingrese el numero de empleado');
    }else if(onTit==1){
        if(fechaElabTit==''){
            alert('Ingrese la fecha de elaboracion');
        }else if(fechaAltaTit==''){
            alert('Ingrese la fecha de alta');
        }else{
            enviar("frmFechaAntiguedad","actualizarFechaAntiguedad.htm");
        }
    }else if(onDep==2){
        if(fechaElabDep==''){
            alert('Ingrese la fecha de elaboracion');
        }else if(fechaAltaDep==''){
            alert('Ingrese la fecha de alta');
        }else if(idDep==''){
            alert('Ingrese el numero de sol del dependiente');
        }else{
            enviar("frmFechaAntiguedad","actualizarFechaAntiguedad.htm");
        }
    }else{
        alert('Es necesario escoger el titular o dependiente');
        return true;
    }

}

// FUNCIONES GENERICAS

/**
 * Funcion generica para enviar un action
 * @returns
 */
function enviar(nombreForm,nombreAction){
    document.forms[nombreForm].action =  nombreAction;
    document.forms[nombreForm].submit();
}

/**
 * Funcion para cargar los calendarios
 * @returns
 */
function cargaCalendario(){
    jQuery('[name="fecha"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaAlta"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaIni"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaFin"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaNacNvo"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });


    /* Fechas Reembolso	*/
    jQuery('[name="reembolsoTO.fechaAltaSiniestro"]').datepicker({
        language: 'es-ES',format: 'dd-mm-yyyy', autoHide: true
    });

    jQuery('[name="reembolsoTO.fechaSintomas"]').datepicker({
        language: 'es-ES',format: 'dd-mm-yyyy', autoHide: true
    });

    jQuery('[name="reembolsoTO.fechaVisitaMed"]').datepicker({
        language: 'es-ES',format: 'dd-mm-yyyy', autoHide: true
    });

    jQuery('[name="reembolsoTO.fechaMedicoCon"]').datepicker({
        language: 'es-ES',format: 'dd-mm-yyyy', autoHide: true
    });

    /* Fechas solicitud	*/
    jQuery('[name="listDep[0].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[1].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[2].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[3].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[4].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[5].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[6].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[7].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[8].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listDep[9].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listCon[0].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listCon[1].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listCon[2].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listCon[3].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listMem[0].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listMem[1].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listMem[2].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="listMem[3].fechaNac"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    /* Campos fechas para fecha antiguedad*/
    jQuery('[name="fechaElabTit"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaAltaTit"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaElabDep"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });

    jQuery('[name="fechaAltaDep"]').datepicker({
        language: 'es-ES',format: 'dd/MM/yyyy', autoHide: true
    });
}

function imprimirElemento(elemento){
      var ventana = window.open('', 'PRINT', 'height=1,width=1');
      ventana.document.write('<html><head><title>' + document.title + '</title>');
      ventana.document.write('<link rel="stylesheet" type="text/css" href="estilos/impresion.css"/>');
      ventana.document.write('</head><body >');
      ventana.document.write(elemento.innerHTML);
      ventana.document.write('</body></html>');
      ventana.document.close();
      ventana.focus();
      ventana.print();
      ventana.close();
      return true;
}



function validarCheckedMemorialAlta(id){
    var siRenov = jQuery("input[id=siRenov]:checked").val();
	var noRenov = jQuery("input[id=noRenov]:checked").val();
	var fechaNacimiento = jQuery('#fechaDepAlta'+id).val();

    if(siRenov==2 || noRenov==3){
        if(fechaFuturo(fechaNacimiento)){ // Validar que no se seleccione una fecha futura
                if(mayoriaEdad(fechaNacimiento)>75){
                    alert('Su dependiente no cumple la regla de ser menor de 75 anios, no es posible agregarlo a la Cobertura de Gastos Funerarios');
                    jQuery('#checkConAlta'+id).prop("checked", false);
                }
        }else{
                alert('No es posible seleccionar una fecha futura');
        }
    }else{
        alert('Favor de seleccionar el tipo de paquete');
        jQuery('#checkConAlta'+id).prop("checked", false);
        return true;
    }
} // Fin de la funcion

function validarFechaMemorial(id){
    var fechaNacimiento = jQuery('#fechaMem'+id).val();

    // Validar que no se seleccione una fecha futura
        if(fechaFuturo(fechaNacimiento)){
            if(mayoriaEdad(fechaNacimiento)>75){
                alert('Su dependiente no cumple la regla de ser menor de 75 anios, favor de validarlo');
                jQuery('#fechaDep'+id).val('');
            }
        }else{
            alert('No es posible seleccionar una fecha futura');
            jQuery('#fechaDep'+id).val('');
        }
} // Fin de la funcion

function validarCheckedMemorial(id){
    var siRenov = jQuery("input[id=siRenov]:checked").val();
    var noRenov = jQuery("input[id=noRenov]:checked").val();
    var nombre = jQuery('#nombreCon'+id).val();
    var apellidoP = jQuery('#paternoCon'+id).val();
    var apellidoM = jQuery('#maternoCon'+id).val();
    var fechaNacimiento = jQuery('#fechaDep'+id).val();

    if(siRenov==2 || noRenov==3){
        if(nombre != ''){
            if(apellidoP != ''){
                if(apellidoM != ''){
                    if(fechaNacimiento != ''){

                    }else{
                        alert('El campo fecha de nacimiento no puede estar vacio');
                        jQuery('#checkCon'+id).prop("checked", false);
                        return true;
                    }
                }else{
                    alert('El campo apellido materno no puede estar vacio');
                    jQuery('#checkCon'+id).prop("checked", false);
                    return true;
                }
            }else{
                alert('El campo apellido paterno no puede estar vacio');
                jQuery('#checkCon'+id).prop("checked", false);
                return true;
            }
        }else{
            alert('El campo nombre no puede estar vacio');
            jQuery('#checkCon'+id).prop("checked", false);
            return true;
        }
    }else{
        alert('Favor de seleccionar el tipo de paquete');
        jQuery('#checkCon'+id).prop("checked", false);
        return true;
    }

} // Fin de la funcion

// Enviar peticion
function enviarMemorial(nombreForm,nombreAction){
    var siRenov = jQuery("input[id=siRenov]:checked").val();
    var noRenov = jQuery("input[id=noRenov]:checked").val();
    var checkCon0 = jQuery("input[id=checkCon0]:checked").val();
    var checkCon1 = jQuery("input[id=checkCon1]:checked").val();
    var checkCon2 = jQuery("input[id=checkCon2]:checked").val();
    var checkCon3 = jQuery("input[id=checkCon3]:checked").val();

    if(siRenov==2){
        var total = 0;
        for(i=0;i<=3;i++){
            if(jQuery('#checkCon'+i).is(':checked')) {
                total = total + 1;
             }
        }

        for(i=0;i<=3;i++){
            if(jQuery('#checkConAlta'+i).is(':checked')) {
                total = total + 1;
             }
        }

        if(total == 0 || total == 3 || total == 4){
            alert('Debe escoger 1 o 2 Dependientes');
            jQuery('#checkCon0').prop("checked", false);
            jQuery('#checkCon1').prop("checked", false);
            jQuery('#checkCon2').prop("checked", false);
            jQuery('#checkCon3').prop("checked", false);
            jQuery('#checkConAlta0').prop("checked", false);
            jQuery('#checkConAlta1').prop("checked", false);
            jQuery('#checkConAlta2').prop("checked", false);
            jQuery('#checkConAlta3').prop("checked", false);
            return true;
        }else if(total > 1 || total < 3){
            enviar(nombreForm,nombreAction);
        }
    }else if(noRenov==3){
        var total = 0;

        for(i=0;i<=3;i++){
            if(jQuery('#checkCon'+i).is(':checked')) {
                total = total + 1;
            }
        }

        for(i=0;i<=3;i++){
            if(jQuery('#checkConAlta'+i).is(':checked')) {
                total = total + 1;
             }
        }

        if(total == 0 || total == 1 || total == 2){
            alert('Debe escoger de 3 a 4 Dependientes');
            jQuery('#checkCon0').prop("checked", false);
            jQuery('#checkCon1').prop("checked", false);
            jQuery('#checkCon2').prop("checked", false);
            jQuery('#checkCon3').prop("checked", false);
            jQuery('#checkConAlta0').prop("checked", false);
            jQuery('#checkConAlta1').prop("checked", false);
            jQuery('#checkConAlta2').prop("checked", false);
            jQuery('#checkConAlta3').prop("checked", false);
            return true;
        }else if(total > 2){
            enviar(nombreForm,nombreAction);
        }
    }else{
        alert('Favor de seleccionar el tipo de paquete');
        jQuery('#checkCon'+id).prop("checked", false);
        return true;
    }

} // Fin de la funcion