jQuery(function() {
	var menu_ul = jQuery('.menu > li > ul'),

	menu_a  = jQuery('.menu > li > a');
	menu_ul.hide();
	menu_a.click(function(e) {
        e.preventDefault();
        if(!jQuery(this).hasClass('active')) {
            menu_a.removeClass('active');
            menu_ul.filter(':visible').slideUp('normal');
            jQuery(this).addClass('active').next().stop(true,true).slideDown('normal');
        } else {
        	jQuery(this).removeClass('active');
        	jQuery(this).next().stop(true,true).slideUp('normal');
        }
    });

});