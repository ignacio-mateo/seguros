<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<form method="post" action="registrarBajaSolicitud.htm">
		<div class="cuerpo">
			<input name="numEmp" id="numEmp" value="${empTO.numEmp}" type="hidden">
			<input name="nombreEmp" id="nombreEmp" value="${empTO.nombreEmp}" type="hidden">
			<input name="perfilEmp" id="perfilEmp" value="${empTO.perfilEmp}" type="hidden">
			<input name="parametro" id="parametro" value="${folio}" type="hidden">
	
			<table class="principal">
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucTituloFechaBaja.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucEmpleadoBaja.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucBotonGuardarBajaForm.jsp"/>
			</table>		 
		</div>
 	</form>
 		
</BODY>
</HTML>
