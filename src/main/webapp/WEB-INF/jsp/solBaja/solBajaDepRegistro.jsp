<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<form:form method="post" action="registrarBajaDependiente.htm" modelAttribute="solicitudForm">	
		<div class="cuerpo" >		
			<!-- OBJETOS HIDDEN -->
			<form:hidden path="empTO.numEmp"/>
			<form:hidden path="empTO.nombreEmp"/>
			<form:hidden path="empTO.perfilEmp"/>
			<form:hidden path="empleadoTO.tipoEmp" id="tipoEmp"/>
			<form:hidden path="empleadoTO.idSexo"/>
			<form:hidden path="solicitudTO.tipoSolicitud"/>
			<form:hidden path="solicitudTO.idSol"/>
			
			<table class="principal">
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!-- ENCABEZADO -->
				<%@include file="../estructura/estrucTituloFechaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!--	DEPENDIENTES	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteBajas.jsp"/>
				<%@include file="../estructura/estrucSolDepenBaja.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
							
				<!--	BOTON GUARDAR	-->
				<jsp:include page="../estructura/estrucBotonGuardarBajaForm.jsp"/>
			</table>
		</div>
	</form:form>
	
</BODY>
</HTML>