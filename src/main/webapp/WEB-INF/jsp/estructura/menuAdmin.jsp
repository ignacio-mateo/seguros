<form method="post" id="frmGastosMed">     
	<input type="hidden" name="numEmp" value="${empTO.numEmp}">
	<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
	<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
	<input type="hidden" name="idTipoSol" id="idTipoSol" value="">
	<input type="hidden" name="idEstatus" id="idEstatus" value="">
	<input type="hidden" name="controlador" id="controlador" value="">
	
	<div id="menu">
		<div id="wrapper">
			<ul class="menu">
				
				<!-- MENU AUT. ALTAS -->
				<li class="item1"><a href="#">AUT. ALTA SOL.</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('consultaAltaSolitudesxAut.htm');">Solicitudes</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('consultaAltaDependientesxAut.htm');">Dependientes</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('consultaAltaConsulmedxAut.htm');">Consulmed</a></li>
					</ul>
				</li>
				
				<!-- MENU AUT. BAJAS -->
				<li class="item2"><a href="#">AUT. BAJA SOL.</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('consultaBajaSolitudesxAut.htm');">Solicitudes</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('consultaBajaDependientesxAut.htm');">Dependientes</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('consultaBajaConsulmedxAut.htm');">Consulmed</a></li>
					</ul>
				</li>
				
				<!-- MENU SOLICITUDES -->
				<li class="item3"><a href="#">MODIFICAR SOL.</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('consultaSolicitudAdmin.htm');">Datos de la Solicitud</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('inicioCambioCategoria.htm');">Cambio Categoria</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('inicioCambioCobertura.htm');">Cambio Cobertura</a></li>
						<li class="subitem4"><a href="#" onclick="seleccionarOpcion('generarFiniquitoInicio.htm');">Cotiza Finiquito</a></li>
						<li class="subitem5"><a href="#" onclick="seleccionarOpcion('consultaFiniquitoInicio.htm');">Consulta Finiquito</a></li>
						<li class="subitem6"><a href="#" onclick="seleccionarOpcion('inicioFechaAntiguedad.htm');">Fecha Antiguedad</a></li>
					</ul>
				</li>
				
				<!-- MENU TARIFAS -->
				<li class="item4"><a href="#">ACTUALIZAR TARIFAS</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('getTarifaConf.htm');">Confianza</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('getTarifaSind.htm');">Sindicalizado</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('getTarifaAmplia.htm');">Ampliaciones</a></li>
					</ul>
				</li>
				
				<!-- MENU REPORTES -->
				<li class="item5"><a href="#">REPORTES</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('reporteActual.htm');">Actual</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('reporteNomina.htm');">Nominas</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('reporteDatosFiscales.htm');">Datos Fiscales</a></li>
						<li class="subitem4"><a href="#" onclick="seleccionarOpcion('reporteCategoria.htm');">Cambio Categoria</a></li>
						<li class="subitem5"><a href="#" onclick="seleccionarOpcion('reporteConsulmed.htm');">Consulmed</a></li>
						<li class="subitem6"><a href="#" onclick="seleccionarOpcion('reporteConsulmedTotal.htm');">Consulmed Total</a></li>
						<li class="subitem7"><a href="#" onclick="seleccionarOpcion('reporteAmpliacionesDep.htm');">Ampliaciones Dep</a></li>
						<li class="subitem8"><a href="#" onclick="seleccionarOpcion('reporteAmpliaciones.htm');">Ampliaciones Total</a></li>
						<li class="subitem9"><a href="#" onclick="seleccionarOpcion('reporteDescargas.htm');">Descargas</a></li>
					</ul>
				</li>
				
				<!-- MENU CONSTANCIAS -->
				<li class="item6"><a href="#">CONSTANCIAS</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('initConstancia.htm');">Constancias</a></li>
					</ul>
				</li>
				
				<!-- MENU ENTRAR COMO OTRO USUARIO -->
				<li class="item7"><a href="#">LOGIN</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('entrarUsuario.htm');">Entrar como Empleado</a></li>
					</ul>
				</li>
				
				<!-- MENU SALIR DE ADMIN -->
				<li class="item8"><a href="#">ADMIN</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('salirAdmin.htm');">Salir como Administrador</a></li>
					</ul>
				</li>
				
				<!-- MENU RENOVACION-->
				<li class="item9"><a href="#">RENOVACION</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('inicioCircularAdmin.htm');">Autorizar Renovacion</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('inicioContratacionMemorialAdmin.htm');">Autorizar Memorial</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('reporteRenov.htm');">Reportes</a></li>
					</ul>
				</li>
				
			</ul>
		</div>
	</div>
</form>