
	<c:forEach items="${solicitudForm.listMem}" var="contact" varStatus="status">
		<tr>
			<td><input type='checkbox' class="columna1" name="listMem[${status.index}].idConsulmed" id="checkMem${status.index}" value="" onclick="seleccionarCheckMem('${status.index}');"></td>
			<td><input type="text" class="columna2" name="listMem[${status.index}].apellidoP" id="paternoMem${status.index}" value=""></td>
			<td><input type="text" class="columna3" name="listMem[${status.index}].apellidoM" id="maternoMem${status.index}" value=""></td>
			<td><input type="text" class="columna4" name="listMem[${status.index}].nombre" id="nombreMem${status.index}" value=""></td>
			<td><input type="text" class="columna6" name="listMem[${status.index}].fechaNac" id="fechaMem${status.index}"
				onchange="validarFechaMemorial('${status.index}');" value="">
			</td>
			<td>
				<input type="hidden" name="listMem[${status.index}].idTipoSol" value="1">
				<input type="hidden" name="listMem[${status.index}].idSexo" value="${contact.idSexo}">
				<input type="text" class="columna7" name="listMem[${status.index}].sexo" value="${contact.sexo}" readonly="readonly">
			</td>
			<td>
				<input type="hidden" name="listMem[${status.index}].idParentesco" value="${contact.idParentesco}">
				<input type="text" class="columna5" name="listMem[${status.index}].parentesco" value="${contact.parentesco}" readonly="readonly">
			</td>
			<td>&nbsp;</td>
		</tr>
	</c:forEach>

	<tr>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    </tr>

    <tr>
        <td>&nbsp;</td>
        <td colspan="2">Solicito Gastos Funerarios</td>
        <td>
        		<form:select path="solicitudTO.IdTipoMemorial" id="selMemorial">
        		    <form:option value="0" label="Seleccione"/>
        			<form:options items="${mapCobMem}" />
        		</form:select>
        </td>
        <td>&nbsp;</td>
        <td colspan="2">Total Memorial</td>
        <td><form:input path="solicitudTO.costoMemorial" readonly="true" class="columna8" id="montoMemorial" value="0.00"/></td>
    </tr>

