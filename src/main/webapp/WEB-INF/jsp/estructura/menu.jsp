<form method="post" id="frmGastosMed">     
	<input type="hidden" name="numEmp" value="${empTO.numEmp}">
	<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
	<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
	
	<div id="menu">
		<div id="wrapper">
			<ul class="menu">
			
				<!-- MENU AYUDA --> 
				<li class="item1"><a href="#">INF. GENERAL</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('contactos.htm');">Contactos</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('documentos.htm');">Condiciones</a></li>
					</ul>
				</li>
				
				<!-- MENU SOLICITUDES --> 
				<li class="item2"><a href="#">ALTA DE SOL.</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('cotizacion.htm');">Cotizador</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('solDatosFiscales.htm');">Alta Seguro</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('altaDependiente.htm');">Alta Dependientes</a></li>
						<li class="subitem4"><a href="#" onclick="seleccionarOpcion('altaConsulmed.htm');">Alta Consulmed</a></li>
					</ul>
				</li>
				
				<!-- MENU BAJAS --> 
				<li class="item3"><a href="#">BAJAS SOL.</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('bajaSolicitud.htm');">Seguro</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('bajaDependiente.htm');">Dependientes</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('bajaConsulmed.htm');">Consulmed</a></li>
					</ul>
				</li>
				
				<!-- MENU CONSULTAS -->
				<li class="item4"><a href="#">CONSULTA SOL.</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('consultaAltaSolicitud.htm');">Alta Seguro</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('consultaAltaIdDependiente.htm');">Alta Dependientes</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('consultaAltaIdConsulmed.htm');">Alta Consulmed</a></li>
						<li class="subitem4"><a href="#" onclick="seleccionarOpcion('consultaBajaSolicitud.htm');">Baja Seguro</a></li>
						<li class="subitem5"><a href="#" onclick="seleccionarOpcion('consultaBajaIdDependiente.htm');">Baja Dependientes</a></li>
						<li class="subitem6"><a href="#" onclick="seleccionarOpcion('consultaBajaIdConsulmed.htm');">Baja Consulmed</a></li>
					</ul>
				</li>
				 
				<!-- MENU DATOS FISCALES --> 
				<li class="item5"><a href="#">DATOS FISCALES</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('consultaDatosFiscales.htm');">Actualizar</a></li>
					</ul>
				</li>
				
				<!-- MENU REEMBOLSOS -->
				<li class="item6"><a href="#">REEMBOLSOS</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('guiasReembolso.htm');">Manuales de Reembolso</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('inicioReembolso.htm');">Formatos</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('enviaReembolso.htm');">Envia Documentacion</a></li>
						<!--<li class="subitem3"><a href="#" onclick="seleccionarOpcion('consultaFolioReembolso.htm');">Consulta De Folio</a></li>-->
					</ul>
				</li>

				<!-- MENU MEMORIAL -->
                <li class="item7"><a href="#">MEMORIAL</a>
                    <ul>
                	    <li class="subitem1"><a href="#" onclick="seleccionarOpcion('inicioContracionMemorial.htm');">Contratacion</a></li>
                	</ul>
                </li>
								
				<!-- MENU ADMINISTRADOR -->
				<c:choose>
				    <c:when test="${empTO.perfilEmp==1}">
				        <li class="item8"><a href="#">ADMIN</a>
						<ul>
							<li class="subitem1"><a href="#" onclick="seleccionarOpcion('entrarAdmin.htm');">Entrar como Administrador</a></li>
						</ul>
						</li>
				    </c:when>
				</c:choose>
				
			</ul>
		</div>
	</div>
	
</form>