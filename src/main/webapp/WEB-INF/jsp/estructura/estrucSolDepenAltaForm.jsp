	<c:forEach items="${solicitudForm.listDep}" var="contact" varStatus="status">
		<tr id="fila${status.index}">
			<td>
				<select class="columna1"  id="select${status.index}" name="listDep[${status.index}].idParentesco" onchange="validarConyuge('${status.index}');">
					<option value='0'>Dependientes</option>
					<option value='4'>ESPOSA</option>
					<option value='3'>ESPOSO</option>
					<option value='6'>HIJA</option>
					<option value='5'>HIJO</option>
				</select>
			</td>
			<td><input type="text" class="columna2" name="listDep[${status.index}].apellidoP" id="paterno${status.index}" value=""></td>
			<td><input type="text" class="columna3" name="listDep[${status.index}].apellidoM" id="materno${status.index}" value=""></td>
			<td><input type="text" class="columna4" name="listDep[${status.index}].nombre" id="nombre${status.index}" value=""></td>
			<td><input type="text" class="columna5" name="listDep[${status.index}].fechaNac" id="fecha${status.index}" value=""></td>
			<td>
				<input type="hidden" name="listDep[${status.index}].idTipoSol" value="1">
				<input type="hidden" name="listDep[${status.index}].idSexo" id="sexo${status.index}">
				<input type="text" class="columna6" id="sexoInput${status.index}" readonly="readonly">
			</td>
			<td><input type="checkbox" class="columna7" id="check${status.index}" onclick="seleccionarCheck('${status.index}');"></td>
			<td><input type="text" class="columna8" readonly="readonly" id="monto${status.index}" name="listDep[${status.index}].costoDep" value="" ></td>
		</tr>
	</c:forEach>