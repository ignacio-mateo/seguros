<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<table class="principal">
			<tr>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
				<td class="columna0">&nbsp;</td>
			</tr>
			<tr><td class="encabezado" colspan="8">CONTACTOS</td></tr>
			<tr><td colspan="8">&nbsp;</td></tr>
			<tr>
				<td colspan="3">NOMBRE</td>
				<td colspan="3">PUESTO</td>
				<td colspan="1">TELEFONO</td>
				<td colspan="1">EXTENSION</td>
			</tr>
			<c:forEach items="${contactos}" var="contactoTO">
			<tr>
				<td colspan="3" class="soloLectura">${contactoTO.nombre}</td>
				<td colspan="3" class="soloLectura">${contactoTO.puesto}</td>
				<td class="soloLectura">${contactoTO.telefono}</td>
				<td class="soloLectura">${contactoTO.extension}</td>
			</tr>
			</c:forEach>
		</table>
	</div>
	
</BODY>
</HTML>
