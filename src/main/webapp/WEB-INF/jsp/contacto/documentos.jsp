<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<div>&nbsp;</div>
		<table class="principal">
			<tr><td class="encabezado" colspan="8">Informacion General</td></tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td class="columnaDoc"><a href="doc/Presentacion.pdf"><img src="imagenes/info01.jpg" class="iconoDocumento">Presentacion 1</a></td>
				<td class="columnaDoc"><a href="doc/Presentacion.pdf"><img src="imagenes/info02.jpg" class="iconoDocumento">Presentacion 2</a></td>
				<td class="columnaDoc"><a href="doc/Presentacion.pdf"><img src="imagenes/info03.jpg" class="iconoDocumento">Presentacion 3</a></td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td class="columnaDoc"><a href="doc/Presentacion.pdf"><img src="imagenes/info04.jpg" class="iconoDocumento">Condiciones 4</a></td>
				<td class="columnaDoc"><a href="doc/Presentacion.pdf"><img src="imagenes/info05.jpg" class="iconoDocumento">Condiciones 5</a></td>
				<td class="columnaDoc"><a href="doc/Presentacion.pdf"><img src="imagenes/info06.jpg" class="iconoDocumento">Condiciones 6</a></td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td class="columnaDoc">&nbsp;</td>
				<td class="columnaDoc">&nbsp;</td>
				<td class="columnaDoc">&nbsp;</td>
			</tr>
		</table>		
	</div>
	
</BODY>
</HTML>
