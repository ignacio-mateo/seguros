<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
		
	<form:form method="post" modelAttribute="reembolsoForm">	
		<div>&nbsp;</div>
		<div class="cuerpo">
			<input name="numEmp" value="${empTO.numEmp}" type="hidden">
			<input name="nombreEmp" value="${empTO.nombreEmp}" type="hidden">
			<input name="perfilEmp" value="${empTO.perfilEmp}" type="hidden">
			<div id="areaImpresion">
				<table class="reembolso">
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td colspan="3"><img src="imagenes/aseguradora.jpg"></td>
						<td colspan="5">RELACI&Oacute;N DE DOCUMENTOS ENTREGADOS PARA REEMBOLSO </td>
					</tr>
					<tr>
						<td width="30px">&nbsp;</td>
						<td width="100px">Nombre del Titular</td>
						<td width="330px"><form:input path="empleadoTO.nombre" class="inputTextNombre" readonly="true"/></td>
						<td width="260px">No.Empleado</td>
						<td width="100px"><form:input path="empTO.numEmp" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Puesto </td>
						<td><form:input path="empleadoTO.puesto" class="inputTextNombre" readonly="true"/></td>
						<td>Region</td>
						<td><form:input path="empleadoTO.region" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td>Nombre Afectado </td>
						<td><form:input path="reembolsoRelacionTO.nombreAfectado" id="nombreAfectado" class="inputTextNombre" value=""/></td>
						<td colspan="2">
							ENFERMEDAD <form:radiobutton path="reembolsoRelacionTO.tipoReembolso" id="tipoReembolsoEnf" value="1" disabled="true"/>
							ACCIDENTE <form:radiobutton path="reembolsoRelacionTO.tipoReembolso" id="tipoReembolsoAcc" value="2" disabled="true"/> 
						</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Parentesco </td>
						<td><form:input path="reembolsoRelacionTO.parentescoTitular" class="inputTextNombre" id="parentescoTitular" readonly="true"/></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="4">Mencionar el tipo de enfermedad y/o lesi�n en caso de accidente</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="3"><form:input path="reembolsoRelacionTO.descripcionEnfermedad" id="descripcionEnfermedad" class="inputTextNombre" readonly="true"/></td>
						<td></td>
					</tr>
					<tr>
						<td>No.</td>
						<td>No. de Documento</td>
						<td>Nombre de quien lo expide</td>
						<td>Concepto</td>
						<td>Importe</td>
					</tr>
					
					<c:forEach items="${reembolsoForm.listReembolso}" var="contact" varStatus="status">
					<tr id="fila${status.index}">
						<td>${status.index+1}</td>
						<td><input type="text" class="inputTextNombre" value="${contact.noDocumento}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${contact.nombreExpediente}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${contact.concepto}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${contact.importeFormat}" readonly="readonly"></td>
					</tr>
					</c:forEach>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total:</td>
						<td><form:input path="reembolsoRelacionTO.importeTotalFormat" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">Observaciones</td></tr>
					<tr><td colspan="5"><form:textarea path="reembolsoRelacionTO.observaciones" class="inputTextNombre" style="width:800px" readonly="true"/></td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="3">_____________________________________</td> 
						<td>&nbsp;</td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td colspan="3">Firma del Empleado Titular de la Poliza</td>
						<td>&nbsp;</td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>					
				</table>
			</div>
			<div><img src="imagenes/impresora3.jpg" class="iconoImprimir" id="imprimir"></div>
		</div>
	</form:form>
			
</BODY>
</HTML>
