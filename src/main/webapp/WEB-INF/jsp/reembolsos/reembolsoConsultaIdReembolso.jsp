<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<div>&nbsp;</div>
		<form method="post" id="frmReembolso">
			<input name="numEmp" value="${empTO.numEmp}" type="hidden">
			<input name="nombreEmp" value="${empTO.nombreEmp}" type="hidden">
			<input name="perfilEmp" value="${empTO.perfilEmp}" type="hidden">
			<input type="hidden" name="idReembolso" value="">
			
			<table class="principal">
				<tr><td class="encabezado" colspan="8"> CONSULTA FOLIOS DE REEMBOLSO</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td colspan="8">${mensaje1}</td></tr>
				<c:forEach items="${listaFolios}" var="idReembolso">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">FOLIO:</td>
					<td class="columna0"><input type="text" value="${idReembolso}" readonly="readonly"></td>
					<td class="columna0"><input type="button" onclick="verDetalleReembolso(${idReembolso});" value="Ver Solicitud"></td>
					<td class="columna0">&nbsp;</td>
				</tr>
				</c:forEach> 
			</table>	
		</form>
	</div>
			
</BODY>
</HTML>
