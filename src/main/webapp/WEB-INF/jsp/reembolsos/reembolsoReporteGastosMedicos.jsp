<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
		
	<form:form method="post" id="frmReporteGastosMedicosLleno" modelAttribute="reporteSiniestrosForm">	
		<div>&nbsp;</div>
		<div class="cuerpo" >
			<form:hidden path="empTO.numEmp"/>
			<form:hidden path="empTO.nombreEmp"/>
			<form:hidden path="empTO.perfilEmp"/>
			<table class="principal">
				<tr>
					<td colspan="4"><img src="imagenes/aseguradora.jpg"></td>
					<td colspan="3">REPORTE DE SINIESTROS GASTOS M&Eacute;DICOS</td>
				</tr>
				<tr>
					<td width="115px">P&Oacute;LIZA</td>
					<td width="115px">EMISOR</td>
					<td width="115px">CARPETA</td>
					<td width="115px">NO DE P&Oacute;LIZA</td>
					<td width="115px">C.I.S</td>
					<td width="115px">FAMILIA</td>
					<td width="115px">FECHA DE ENV&Iacute;O</td>
				</tr>
				<tr>
					<td><form:input path="reembolsoReporteTO.poliza" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.emisor" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.carpeta" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.numPoliza" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.cis" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.familia" class="inputTextNombre" readonly="true"/></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">CONTRATANTE</td>
					<td colspan="2">PLAN</td>
					<td colspan="2">FECHA DE EMISI&Oacute;N P&Oacute;LIZA</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoReporteTO.contratante" class="inputTextNombre" readonly="true"/></td>
					<td colspan="2"><form:input path="reembolsoReporteTO.plan" class="inputTextNombre" readonly="true"/></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="3">ASEGURADO O TITULAR</td>
					<td>RFC</td>
					<td>NUM. EMP</td>
					<td>REGION</td>
					<td>FECHA DE OCURRENCIA SINIESTRO</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoReporteTO.asegurado" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.rfc" class="inputTextNombre"/></td>
					<td><form:input path="empleadoTO.numEmp" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="empleadoTO.region" class="inputTextNombre" readonly="true"/></td>
					<td></td>
				</tr>
				<tr><td colspan="7">&nbsp;</td>
				<tr><td colspan="7">Selecciona el nombre del afectado: </td>
				<c:forEach items="${reporteSiniestrosForm.listDep}" var="dependiente" varStatus="status">
					<tr id="fila${status.index}">
					 	<td><input type="radio" name="datosAfectado" onclick="llenarCampos('${dependiente.nombre}','${dependiente.parentesco}',
									'${dependiente.fechaNac}','${dependiente.fechaAltaAut}','${dependiente.sexo}');"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.nombre}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.parentesco}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.fechaNac}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.fechaAltaAut}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.sexo}" readonly="readonly"></td>
					</tr>
				</c:forEach>
				<tr><td colspan="7">&nbsp;</td>	
				<tr>
					<td colspan="3">AFECTADO</td>
					<td>SEXO</td>
					<td>FECHA DE NACIMIENTO</td>
					<td>FECHA ALTA</td>
					<td>PAGADO HASTA</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoReporteTO.afectado" class="inputTextNombre" id="nombreAfectado" value="" readonly="true"/></td>
					<td>
						<form:input path="reembolsoReporteTO.sexo" class="inputTextNombre" id="sexo" readonly="true"/>
					</td>
					<td><form:input path="reembolsoReporteTO.fechaNac" class="inputTextNombre" id="fechaNac" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.fechaAlta" class="inputTextNombre" id="fechaAlta" readonly="true"/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">PARENTESCO CON EL ASEGURADO</td>
					<td>EDAD</td>
					<td>OCUPACI&Oacute;N</td>
					<td colspan="2">FECHA DE AVISO A LA C.I.A</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoReporteTO.parentescoAsegurado" id="parentescoTitular" class="inputTextNombre" readonly="true"/></td>
					<td><form:input path="reembolsoReporteTO.edad" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoReporteTO.ocupacion" class="inputTextNombre"/></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="2">RECLAMACI&Oacute;N</td>
					<td>FECHA DE RECLAMACI&Oacute;N INICIAL</td>
					<td colspan="2">RECLAMACI&Oacute;N POR</td>
					<td>TABLA HONORARIOS QUIRURGICOS</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"> 
						Inicial <form:radiobutton path="reembolsoReporteTO.estatusReclamacion" id="estReclaIni" value="1"/>
						Complementaria <form:radiobutton path="reembolsoReporteTO.estatusReclamacion" id="estReclaComp" value="2"/>
					</td>
					<td><form:input path="reembolsoReporteTO.fechaReclamacion" id="fechaReclamacion" class="inputTextNombre" readonly="true"/></td>
					<td colspan="2">
						Enfermedad <form:radiobutton path="reembolsoReporteTO.tipoReclamacion" id="tipoReclaEnf" value="1"/>
						Accidente <form:radiobutton path="reembolsoReporteTO.tipoReclamacion" id="tipoReclaAcc" value="2"/>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">PADECIMIENTO RECLAMADO</td>
					<td>CLAVE DE PADECIMIENTO</td>
					<td>SUMA ASEGURADA</td>
					<td>DEDUCIBLE</td>
					<td>COASEGURO</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"><form:input path="reembolsoReporteTO.padecimientoReclamado" id="padecimientoReclamado" class="inputTextNombre"/></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="7">Si env&iacute;a facturas o recibos para PAGO DIRECTO, efectuar desglose de estas al reverso.</td>
				</tr>
				<tr>
					<td>GARANT&Iacute;A</td>
					<td colspan="3">Desglose de Gastos</td>
					<td>Gastos reclamados</td>
					<td>Gastos cubiertos</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>01</td>
					<td colspan="3">HONORARIOS M&Eacute;DICOS Y CIRUJANOS</td>
					<td><form:input path="reembolsoReporteTO.desglose1" class="inputTextNombre" id="desglose1" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>02</td>
					<td colspan="3">HONORARIOS PRIMER AYUDANTE</td>
					<td><form:input path="reembolsoReporteTO.desglose2" class="inputTextNombre" id="desglose2" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>03</td>
					<td colspan="3">HONORARIOS SEGUNDO AYUDANTE</td>
					<td><form:input path="reembolsoReporteTO.desglose3" class="inputTextNombre" id="desglose3" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>04</td>
					<td colspan="3">ANESTESIA</td>
					<td><form:input path="reembolsoReporteTO.desglose4" class="inputTextNombre" id="desglose4" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>05</td>
					<td colspan="3">HONORARIOS ANESTESI&Oacute;LOGO </td>
					<td><form:input path="reembolsoReporteTO.desglose5" class="inputTextNombre" id="desglose5" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>06</td>
					<td colspan="3">CUARTO NORMAL Y ALIMENTOS</td>
					<td><form:input path="reembolsoReporteTO.desglose6" class="inputTextNombre" id="desglose6" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>07</td>
					<td colspan="3">CONSULTAS M&Eacute;DICAS POR D&Iacute;A</td>
					<td><form:input path="reembolsoReporteTO.desglose7" class="inputTextNombre" id="desglose7" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>08</td>
					<td colspan="3">ENFERMERA A DOMICILIO </td>
					<td><form:input path="reembolsoReporteTO.desglose8" class="inputTextNombre" id="desglose8" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>09</td>
					<td colspan="3"> AMBULANCIA  </td>
					<td><form:input path="reembolsoReporteTO.desglose9" class="inputTextNombre" id="desglose9" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>			
					<td>10</td>
					<td colspan="3"> MIEMBROS ARTIF. APARATOS ORTOPEDICOS PROT. DENT </td>
					<td><form:input path="reembolsoReporteTO.desglose10" class="inputTextNombre" id="desglose10" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>11</td>
					<td colspan="3">CESARIA Y/O PARTO </td>
					<td><form:input path="reembolsoReporteTO.desglose11" class="inputTextNombre" id="desglose11" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>12</td>
					<td colspan="3">TERAPIA INTENSIVA POR DIA</td>
					<td><form:input path="reembolsoReporteTO.desglose12" class="inputTextNombre" id="desglose12" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>13</td>
					<td colspan="3">MATERIAL Y SALAS DE OPERACIONES </td>
					<td><form:input path="reembolsoReporteTO.desglose13" class="inputTextNombre" id="desglose13" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>14</td>
					<td colspan="3"> MEDICAMENTOS PRESCRITOS </td>
					<td><form:input path="reembolsoReporteTO.desglose14" class="inputTextNombre" id="desglose14" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>15</td>
					<td colspan="3">ESTUDIOS AUXILIARES DE DIAGNOSTICO </td>
					<td><form:input path="reembolsoReporteTO.desglose15" class="inputTextNombre" id="desglose15" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>16</td>
					<td colspan="3">QUIMIOTERAPIA Y RADIOTERAPIA </td>
					<td><form:input path="reembolsoReporteTO.desglose16" class="inputTextNombre" id="desglose16" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>17</td>
					<td colspan="3">OTROS GASTOS</td>
					<td><form:input path="reembolsoReporteTO.desglose17" class="inputTextNombre" id="desglose17" value="0.00"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>FOLIO</td>
					<td>&nbsp;</td>
					<td colspan="2"><input type="button" value="Calcular Total" onclick="sumaReembolso();"></td>
					<td><form:input path="reembolsoReporteTO.desgloseTotal" class="inputTextNombre" id="desgloseTotal" value="0.00" readonly="true"/></td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">Observaciones</td>
					<td colspan="3"></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3"><form:textarea path="reembolsoReporteTO.observaciones" rows="5" cols="40"/></td>
					<td colspan="2">MENOS DEDUCIBLE</td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">COASEGURO</td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">SUBTOTAL</td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">I.V.A.</td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">TOTAL A PAGAR</td>
					<td>__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">Finiquito No. </td>
					<td colspan="3"> Beneficiario del Cheque  </td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">No. de cheque </td>
					<td colspan="2">Importe</td>
					<td colspan="2"> Importe con letra  </td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">Analiz&oacute;</td>
					<td colspan="2">Revis&oacute;</td>
					<td colspan="2">Autoriz&oacute; (Gerente) </td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr>
					<td colspan="2">__________________</td>
					<td colspan="2">__________________</td>
					<td colspan="2">__________________</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">Nombre y Firma</td>
					<td colspan="2">Nombre y Firma</td>
					<td colspan="2">Nombre y Firma</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td colspan="2">__________________</td> 
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td colspan="2">Firma del Empleado Titular de la Poliza</td>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2"><input type="button" value="GENERAR FORMATO" onclick="validarReporteGastosMedicos();"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td> 
				</tr>
			</table>
			
			
		</div>
	</form:form>
			
</BODY>
</HTML>
