<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<form:form method="post" id="frmRelacionDocumentos" modelAttribute="reembolsoForm">	
		<div>&nbsp;</div>
		<div class="cuerpo" >
			<input type="hidden" name="numEmp" value="${numEmp}">
			<div id="areaImpresion">
				<table class="principal">
					<tr>
						<td colspan="3"><img src="imagenes/aseguradora.jpg"></td>
						<td colspan="5">RELACI&Oacute;N DE DOCUMENTOS ENTREGADOS PARA REEMBOLSO</td>
					</tr>
					<tr>
						<td width="30px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="300px">&nbsp;</td>
						<td width="260px">FOLIO:</td>
						<td width="100px"><form:input path="reembolsoRelacionTO.idReembolso" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td width="30px">&nbsp;</td>
						<td width="100px">Nombre del Titular</td>
						<td width="300px"><form:input path="empleadoTO.nombre" class="inputTextNombre" readonly="true"/></td>
						<td width="300px">No.Empleado</td>
						<td width="100px"><input type="text" class="inputTextNombre" value="${empTO.numEmp}" readonly="readonly"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Puesto </td>
						<td><form:input path="empleadoTO.puesto" class="inputTextNombre" readonly="true"/></td>
						<td>Region</td>
						<td><form:input path="empleadoTO.region" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Nombre Afectado </td>
						<td><form:input path="reembolsoRelacionTO.nombreAfectado" class="inputTextNombre" value="" readonly="true"/></td>
						<td>
							ENFERMEDAD <form:radiobutton disabled="true" path="reembolsoRelacionTO.tipoReembolso" value="1"/>
							ACCIDENTE <form:radiobutton disabled="true" path="reembolsoRelacionTO.tipoReembolso" value="2"/> 
						</td>
						<td>&nbsp;</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Parentesco </td>
						<td><form:input path="reembolsoRelacionTO.parentescoTitular" class="inputTextNombre" readonly="true"/></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="4">Mencionar el tipo de enfermedad y/o lesi�n en caso de accidente</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="3"><form:input path="reembolsoRelacionTO.descripcionEnfermedad" class="inputTextNombre" value=""  readonly="true"/></td>
						<td></td>
					</tr>
					<tr>
						<td>No.</td>
						<td>No. de Documento</td>
						<td>Nombre de quien lo expide</td>
						<td>Concepto</td>
						<td>Importe</td>
					</tr>
					
					<c:forEach items="${reembolsoForm.listReembolso}" var="contact" varStatus="status">
						<tr id="fila${status.index}">
							<td>${status.index+1}</td>
							<td>
								<input type="text" class="inputTextNombre" name="listReembolso[${status.index}].noDocumento" value="${contact.noDocumento}" readonly="readonly">
							</td>
							<td><input type="text" class="inputTextNombre" name="listReembolso[${status.index}].nombreExpediente" value="${contact.nombreExpediente}" readonly="readonly"></td>
							<td><input type="text" class="inputTextNombre" name="listReembolso[${status.index}].concepto" value="${contact.concepto}" readonly="readonly"></td>
							<td><input type="text" class="inputTextNombre" name="listReembolso[${status.index}].importe" value="${contact.importe}" readonly="readonly"></td>
						</tr>
					</c:forEach>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total:</td>
						<td><form:input path="reembolsoRelacionTO.importeTotal" class="inputTextNombre"  readonly="true"/></td> 
					</tr>
					<tr>
						<td colspan="5">Observaciones</td>
					</tr>
					<tr>
						<td colspan="5"><form:input path="reembolsoRelacionTO.observaciones" class="inputTextNombre"  readonly="true"/></td> 
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>________________________________________________</td>
						<td>&nbsp;</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>FIRMA DEL EMPLEADO TITULAR DE LA POLIZA</td>
						<td>&nbsp;</td> 
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
				</table>
			</div>
			
			<table class="principal">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0"><img src="imagenes/impresora3.jpg" class="iconoImprimir" id="imprimir"></td>
				</tr>
			</table>
			
		</div>
	</form:form>
			
</BODY>
</HTML>
