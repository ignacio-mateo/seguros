<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
		
	<form:form method="post" modelAttribute="accidenteEnfermedadForm">	
		<div>&nbsp;</div>
		<div class="cuerpo" >
			<input name="numEmp" value="${empTO.numEmp}" type="hidden">
			<input name="nombreEmp" value="${empTO.nombreEmp}" type="hidden">
			<input name="perfilEmp" value="${empTO.perfilEmp}" type="hidden">
			<div id="areaImpresion">
				<table class="reembolso">
					<tr>
						<td colspan="3"><img src="imagenes/aseguradora.jpg"></td>
						<td colspan="3">AVISO DE ACCIDENTE O ENFERMEDAD</td>
					</tr>
					<tr>
						<td width="145px"></td>
						<td width="145px"></td>
						<td width="145px"></td>
						<td width="145px"></td>
						<td width="145px"></td>
						<td width="145px"></td>
					</tr>
					<tr>
						<td colspan="3">Motivo de la Reclamacion</td>
						<td colspan="3">Tipo de Reclamacion</td>
					</tr>
					<tr>
						<td colspan="3">	
								Reembolso <form:radiobutton path="reembolsoTO.motivoReclamacion" value="1" disabled="true"/>
								Programacion de Cirugia-Tratamiento <form:radiobutton path="reembolsoTO.motivoReclamacion" value="2" disabled="true"/>
								Pago Directo <form:radiobutton path="reembolsoTO.motivoReclamacion" value="3" disabled="true"/>
						</td>
						<td colspan="3">
								Accidente <form:radiobutton path="reembolsoTO.tipoReclamacion" value="1" disabled="true"/>
								Embarazo <form:radiobutton path="reembolsoTO.tipoReclamacion" value="2" disabled="true"/>
								Enfermedad <form:radiobutton path="reembolsoTO.tipoReclamacion" value="3" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">NOMBRE O RAZ&Oacute;N SOCIAL DEL CONTRATANTE</td>
						<td colspan="3">No. DE P&Oacute;LIZA</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.razonSocial" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.numPoliza" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">APELLIDO PATERNO, APELLIDO MATERNO Y NOMBRE(s) DEL ASEGURADO TITULAR</td>
						<td colspan="3">R.F.C. o CURP</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.nombreTitular" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.rfcTitular" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">APELLIDO PATERNO, APELLIDO MATERNO Y NOMBRE(s) DEL ASEGURADO AFECTADO</td>
						<td colspan="3">R.F.C. o CURP</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.nombreAfectado" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.rfcAfectado" class="inputTextNombre" readonly="true"/></td>
					</tr>
						<tr>
						<td colspan="2">NO. DE EMPLEADO</td>
						<td colspan="2">FECHA ALTA</td>
						<td colspan="2">NACIONALIDAD</td>
					</tr>
					<tr>
						<td colspan="2"><form:input path="reembolsoTO.numEmp" class="inputTextNombre" readonly="true"/></td>
						<td colspan="2"><form:input path="reembolsoTO.fechaAltaN" class="inputTextNombre" readonly="true"/></td>
						<td colspan="2"><form:input path="reembolsoTO.nacionalidad" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td>FECHA DE NACIMIENTO</td>
						<td>SEXO</td>
						<td>PARENTESCO CON EL TITULAR</td>
						<td>CORREO ELECTR&Oacute;NICO</td>
						<td>TELEFONO DE CONTACTO (INCLUIR CLAVE LADA)</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><form:input path="reembolsoTO.fechaNacN" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.sexo" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.parentescoTitular" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.email" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.telefono" class="inputTextNombre" readonly="true"/></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">DOMICILIO: CALLE</td>
						<td>No. EXTERIOR</td>
						<td>No. INTERIOR</td>
						<td>COLONIA</td>
						<td>C.P.</td>
					</tr>
					<tr>
						<td colspan="2"><form:input path="reembolsoTO.calle" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.noExt" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.noInt" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.colonia" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.cp" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td>ESTADO</td>
						<td colspan="2">DELEGACI&Oacute;N</td>
						<td>OCUPACI&Oacute;N o PROFESI&Oacute;N</td>
						<td>LUGAR DONDE TRABAJA/EMPRESA</td>
						<td>GIRO DE LA EMPRESA</td>
					</tr>
					<tr>
						<td><form:input path="reembolsoTO.estado" class="inputTextNombre" readonly="true"/></td>
						<td colspan="2"><form:input path="reembolsoTO.delegacion" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.ocupacion" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.empresa" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.giroEmpresa" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3" width="435px">�HA PRESENTADO GASTOS ANTERIORES POR ESTE PADECIMIENTO O ACCIDENTE EN OTRA COMPA�IA?</td>
						<td colspan="3">SI SU RESPUESTA FUE AFIRMATIVA INDIQUE No. DE SINIESTRO</td>
					</tr>
					<tr>
						<td colspan="3">
							SI <form:radiobutton path="reembolsoTO.mismoPadecimiento" value="1" disabled="true"/>
							NO <form:radiobutton path="reembolsoTO.mismoPadecimiento" value="2" disabled="true"/>
						</td>
						<td colspan="3"><form:input path="reembolsoTO.siniestro" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">COMPA�IA</td>
						<td colspan="3">FECHA ALTA</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.aseguradora" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.fechaAltaSiniestroN" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">
							TIPO DE RECLAMACION: INICIAL <form:radiobutton path="reembolsoTO.estadoReclamacion" value="1" disabled="true"/>
							COMPLEMENTARIA <form:radiobutton path="reembolsoTO.estadoReclamacion" value="2" disabled="true"/>
						</td>
						<td colspan="3">INDIQUE EL TIPO DE ALTERACIONES Y/O S&Iacute;NTOMAS QUE PRESENT&Oacute;</td>
					</tr>
					<tr>
						<td colspan="3" width="435px">FECHA EN QUE OCURRI&Oacute; EL ACCIDENTE O APARICI&Oacute;N DE LOS PRIMEROS S&Iacute;NTOMAS DE LA ENFERMEDAD</td>
						<td rowspan="4" colspan="3"><form:textarea path="reembolsoTO.sintomas" readonly="true" rows="6" cols="50"></form:textarea></td>
					</tr>
					<tr><td colspan="3"><form:input path="reembolsoTO.fechaSintomasN" class="inputTextNombre" readonly="true"/></td></tr>
					<tr><td colspan="3">FECHA EN QUE VISIT&Oacute; POR PRIMERA VEZ AL M&Eacute;DICO POR ESTA ENFERMEDAD</td></tr>
					<tr><td colspan="3"><form:input path="reembolsoTO.fechaVisitaMedN" class="inputTextNombre" readonly="true"/></td></tr>
					<tr><td colspan="6">INDIQUE EL DIAGN&Oacute;STICO MOTIVO DE SU RECLAMACION</td></tr>
					<tr><td colspan="6"><form:input path="reembolsoTO.diagnostico" class="inputTextNombre" readonly="true"/></td></tr>
					<tr><td colspan="6">SI ES ACCIDENTE,DETALLESE �C&Oacute;MO Y D&Oacute;NDE FU&Eacute;?</td></tr>
					<tr><td colspan="6"><form:input path="reembolsoTO.accidente" class="inputTextNombre" readonly="true"/></td></tr>
					<tr><td colspan="6">AUTORIDAD QUE TOM&Oacute; CONOCIMIENTO DEL ACCIDENTE (ANEXAR COPIAS DEL MINISTERIO PUBLICO)</td></tr>
					<tr><td colspan="6"><form:input path="reembolsoTO.ministerioPublico" class="inputTextNombre" readonly="true"/></td></tr>
					<tr>
						<td colspan="3">EN CASO DE ACCIDENTE AUTOMOVIL&Iacute;STICO �CUENTA CON SEGURO DE AUTOMOVIL?  </td>
						<td colspan="3">NOMBRE DE LA COMPA�IA</td>
					</tr>
					<tr>
						<td colspan="3">
							SI <form:radiobutton path="reembolsoTO.seguroAuto" value="1" disabled="true"/>
							NO <form:radiobutton path="reembolsoTO.seguroAuto" value="2" disabled="true"/> 
						</td>
						<td colspan="3"><form:input path="reembolsoTO.companiaSeguroAuto" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td>COBERTURA</td>
						<td>SUMA ASEGURADA</td>
						<td>No. P&Oacute;LIZA</td>
						<td colspan="2">COMPA�IA DEL TERCERO</td>
					</tr>
					<tr>
						<td><form:input path="reembolsoTO.cobertura" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.sumaAsegurada" class="inputTextNombre" readonly="true"/></td>
						<td><form:input path="reembolsoTO.numPolizaAuto" class="inputTextNombre" readonly="true"/></td>
						<td colspan="2"><input type="text" class="inputTextNombre" name="companiaTercero" value="${reembolsoTO.companiaTercero}" readonly="readonly"></td>
					</tr>
					<tr>
						<td colspan="3">HOSPITAL,CL&Iacute;NICA O SANATORIO EN QUE FUE ATENDIDO</td>
						<td colspan="3"><form:input path="reembolsoTO.hospital" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">�QU&Eacute; ESTUDIOS LE REALIZARON PARA EL DIAGN&Oacute;STICO Y TRATAMIENTO?</td>
						<td colspan="3"><form:input path="reembolsoTO.estudiosDiagnostico" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">NOMBRE DEL M&Eacute;DICO TRATANTE</td>
						<td colspan="3">ESPECIALIDAD</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.medicoTratante" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.especialidad" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">DIRECCI&Oacute;N</td>
						<td colspan="3">TELEFONO Y/O CORREO ELECTR&Oacute;NICO</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.direccionMedico" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.telEmailMedico" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">�M&Eacute;DICOS QUE HA CONSULTADO EN LOS &Uacute;LTIMOS 2 A�OS?</td>
						<td colspan="3">CAUSA</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.medicosConsultados" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.causa" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr>
						<td colspan="3">TEL&Eacute;FONO Y/O CORREO ELECTR&Oacute;NICO</td>
						<td colspan="3">FECHA</td>
					</tr>
					<tr>
						<td colspan="3"><form:input path="reembolsoTO.telEmailMedicoCon" class="inputTextNombre" readonly="true"/></td>
						<td colspan="3"><form:input path="reembolsoTO.fechaMedicoConN" class="inputTextNombre" readonly="true"/></td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td colspan="2">&nbsp;</td>
						<td colspan="2">_______________________________________</td> 
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td colspan="2">&nbsp;</td>
						<td colspan="2">Firma del Empleado Titular de la Poliza</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td colspan="6" width="870px">DOCUMENTOS A PRESENTAR</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">1. Copia de la actuaci&oacute;n del ministerio p&uacute;blico o atenci&oacute;n 
								recibida de la instituci&oacute;n ( en caso de accidente).</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">2. Interpretaci&oacute;n de estudios radiol&oacute;gicos o de gabinete.</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">3. Copia de identificaci&oacute;n oficial del asegurado afectado 
							(ife, pasaporte y en caso de menores de 18 a�os que no cuenten con pasaporte, cualquier otro documento o 
							identificaci&oacute;n oficial con fotograf�a y firma o huella digital).</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">4. Recibos de gastos que cuenten con los requisitos fiscales
							( quedaran si validez copias, recibos provisionales,estados de cuenta,etc.).</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">5. Por cada m&eacute;dico tratante se deber&aacute;n llenar los informes m�dicos correspondientes 
							y su participacion en el evento.</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">NOTAS</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">Se informa que la omisi&oacute;n, inexacta o falsa declaraci&oacute;n proporcionada en el 
							presente aviso, releva de toda responsabilidad a Seguros</td>
					</tr>
					<tr>
						<td colspan="6" width="870px">En este acto autoriz&oacute; a Seguros  para que en 
							caso de que as&iacute; lo decida esa aseguradora, requiera y obtenga de los m&eacute;dicos, hospitales, 
							sanatorios, clinicas, laboratorios, gabinetes, y/o establecimientos que me hayan atendido o que me atiendan 
							en lo sucesivo; toda la informaci&oacute;n completa sobre el diagn&oacute;stico, pron&oacute;stico, evoluci&oacute;n y 
							tratamiento, as&iacute; como el expediente y/o resumen cl&iacute;nico y/o notas y/o reportes y/o cualquier otro 
							documento sobre mi(s) padecimiento(s) anterior(es) y/o actual(es).</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div><img src="imagenes/impresora3.jpg" class="iconoImprimir" id="imprimir"></div>
		</div>
	</form:form>
			
</BODY>
</HTML>
