<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
		
	<form:form method="post" id="frmFormatoAccidenteEnfermedad" modelAttribute="accidenteEnfermedadForm">	
		<div>&nbsp;</div>
		<div class="cuerpo">
			<form:hidden path="empTO.numEmp"/>
			<form:hidden path="empTO.nombreEmp"/>
			<form:hidden path="empTO.perfilEmp"/>
			<table class="principal">
				<tr>
					<td colspan="3"><img src="imagenes/aseguradora.jpg"></td>
					<td colspan="3">AVISO DE ACCIDENTE O ENFERMEDAD</td>
				</tr>
				<tr>
					<td width="145px"></td>
					<td width="145px"></td>
					<td width="145px"></td>
					<td width="145px"></td>
					<td width="145px"></td>
					<td width="145px"></td>
				</tr>
				<tr>
					<td colspan="3">Motivo de la Reclamacion</td>
					<td colspan="3">Tipo de Reclamacion</td>
				</tr>
				<tr>
					<td colspan="3">	
							Reembolso <form:radiobutton path="reembolsoTO.motivoReclamacion" id="motReclaReem" value="1"/>
							Programacion de Cirugia-Tratamiento <form:radiobutton path="reembolsoTO.motivoReclamacion" id="motReclaCir" value="2"/>
							Pago Directo <form:radiobutton path="reembolsoTO.motivoReclamacion" id="motReclaPago" value="3"/>
					</td>
					<td colspan="3">
							Accidente <form:radiobutton path="reembolsoTO.tipoReclamacion" id="tipoReclaAcc" value="1"/>
							Embarazo <form:radiobutton path="reembolsoTO.tipoReclamacion" id="tipoReclaEmb" value="2"/>
							Enfermedad <form:radiobutton path="reembolsoTO.tipoReclamacion" id="tipoReclaEnf" value="3"/>
					</td>
				</tr>
				<tr>
					<td colspan="3">NOMBRE O RAZ&Oacute;N SOCIAL DEL CONTRATANTE</td>
					<td colspan="3">No. DE P&Oacute;LIZA</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.razonSocial" class="inputTextNombre" readonly="true"/></td>
					<td colspan="3"><form:input path="reembolsoTO.numPoliza" class="inputTextNombre" readonly="true"/></td>
				</tr>
				<tr>
					<td colspan="3">APELLIDO PATERNO, APELLIDO MATERNO Y NOMBRE(s) DEL ASEGURADO TITULAR</td>
					<td colspan="3">R.F.C. o CURP</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.nombreTitular" class="inputTextNombre" readonly="true"/></td>
					<td colspan="3"><form:input path="reembolsoTO.rfcTitular" class="inputTextNombre"/></td>
				</tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr><td colspan="6">Selecciona el nombre del afectado: </td>
				</tr>
				<c:forEach items="${accidenteEnfermedadForm.listDep}" var="dependiente" varStatus="status">
					<tr id="fila${status.index}">
					 	<td><input type="radio" name="datosAfectado" onclick="llenarCampos('${dependiente.nombre}','${dependiente.parentesco}',
									'${dependiente.fechaNac}','${dependiente.fechaAltaAut}','${dependiente.sexo}');"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.nombre}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.parentesco}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.fechaNac}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.fechaAltaAut}" readonly="readonly"></td>
						<td><input type="text" class="inputTextNombre" value="${dependiente.sexo}" readonly="readonly"></td>
					</tr>
				</c:forEach>	
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr>
					<td colspan="3">APELLIDO PATERNO, APELLIDO MATERNO Y NOMBRE(s) DEL ASEGURADO AFECTADO</td>
					<td colspan="3">R.F.C. o CURP</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.nombreAfectado" class="inputTextNombre" id="nombreAfectado" readonly="true"/></td>
					<td colspan="3"><form:input path="reembolsoTO.rfcAfectado" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="2">NO. DE EMPLEADO</td>
					<td colspan="2">FECHA ALTA</td>
					<td colspan="2">NACIONALIDAD</td>
				</tr>
				<tr>
					<td colspan="2"><form:input path="reembolsoTO.numEmp" class="inputTextNombre" readonly="true"/></td>
					<td colspan="2"><form:input path="reembolsoTO.fechaAlta" class="inputTextNombre" id="fechaAlta" readonly="true"/></td>
					<td colspan="2"><form:input path="reembolsoTO.nacionalidad" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td>FECHA DE NACIMIENTO</td>
					<td>SEXO</td>
					<td>PARENTESCO CON EL TITULAR</td>
					<td>CORREO ELECTR&Oacute;NICO</td>
					<td>TELEFONO DE CONTACTO (INCLUIR CLAVE LADA)</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><form:input path="reembolsoTO.fechaNac" class="inputTextNombre" id="fechaNac" readonly="true"/></td>
					<td><form:input path="reembolsoTO.sexo" class="inputTextNombre" id="sexo" readonly="true"/></td>
					<td><form:input path="reembolsoTO.parentescoTitular" class="inputTextNombre" id="parentescoTitular" readonly="true"/></td>
					<td><form:input path="reembolsoTO.email" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.telefono" class="inputTextNombre"/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">DOMICILIO: CALLE</td>
					<td>No. EXTERIOR</td>
					<td>No. INTERIOR</td>
					<td>COLONIA</td>
					<td>C.P.</td>
				</tr>
				<tr>
					<td colspan="2"><form:input path="reembolsoTO.calle" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.noExt" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.noInt" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.colonia" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.cp" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td>ESTADO</td>
					<td colspan="2">DELEGACI&Oacute;N</td>
					<td>OCUPACI&Oacute;N o PROFESI&Oacute;N</td>
					<td>LUGAR DONDE TRABAJA/EMPRESA</td>
					<td>GIRO DE LA EMPRESA</td>
				</tr>
				<tr>
					<td><form:input path="reembolsoTO.estado" class="inputTextNombre"/></td>
					<td colspan="2"><form:input path="reembolsoTO.delegacion" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.ocupacion" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.empresa" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.giroEmpresa" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3" width="435px">�HA PRESENTADO GASTOS ANTERIORES POR ESTE PADECIMIENTO O ACCIDENTE EN OTRA COMPA�IA?</td>
					<td colspan="3">SI SU RESPUESTA FUE AFIRMATIVA INDIQUE No. DE SINIESTRO</td>
				</tr>
				<tr>
					<td colspan="3">
						SI <form:radiobutton path="reembolsoTO.mismoPadecimiento" id="mismoPadecimientoSI" value="1"/>
						NO <form:radiobutton path="reembolsoTO.mismoPadecimiento" id="mismoPadecimientoNO" value="2"/>
					</td>
					<td colspan="3"><form:input path="reembolsoTO.siniestro" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">COMPA�IA</td>
					<td colspan="3">FECHA ALTA</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.aseguradora" class="inputTextNombre"/></td>
					<td colspan="3"><form:input path="reembolsoTO.fechaAltaSiniestro" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">
						TIPO DE RECLAMACION: INICIAL <form:radiobutton path="reembolsoTO.estadoReclamacion" id="estReclaIni" value="1"/>
						COMPLEMENTARIA <form:radiobutton path="reembolsoTO.estadoReclamacion" id="estReclaComp" value="2"/>
					</td>
					<td colspan="3">INDIQUE EL TIPO DE ALTERACIONES Y/O S&Iacute;NTOMAS QUE PRESENT&Oacute;</td>
				</tr>
				<tr>
					<td colspan="3" width="435px">FECHA EN QUE OCURRI&Oacute; EL ACCIDENTE O APARICI&Oacute;N DE LOS PRIMEROS S&Iacute;NTOMAS DE LA ENFERMEDAD</td>
					<td rowspan="4" colspan="3"><form:textarea path="reembolsoTO.sintomas" id="sintomas" rows="6" cols="50"></form:textarea></td>
				</tr>
				<tr><td colspan="3"><form:input path="reembolsoTO.fechaSintomas" id="fechaSintomas" class="inputTextNombre"/></td></tr>
				<tr><td colspan="3">FECHA EN QUE VISIT&Oacute; POR PRIMERA VEZ AL M&Eacute;DICO POR ESTA ENFERMEDAD</td></tr>
				<tr><td colspan="3"><form:input path="reembolsoTO.fechaVisitaMed" id="fechaVisitaMed" class="inputTextNombre"/></td></tr>
				<tr><td colspan="6">INDIQUE EL DIAGN&Oacute;STICO MOTIVO DE SU RECLAMACION</td></tr>
				<tr><td colspan="6"><form:input path="reembolsoTO.diagnostico" id="diagnostico" class="inputTextNombre"/></td></tr>
				<tr><td colspan="6">SI ES ACCIDENTE,DETALLESE �C&Oacute;MO Y D&Oacute;NDE FU&Eacute;?</td></tr>
				<tr><td colspan="6"><form:input path="reembolsoTO.accidente" id="accidente" class="inputTextNombre"/></td></tr>
				<tr><td colspan="6">AUTORIDAD QUE TOM&Oacute; CONOCIMIENTO DEL ACCIDENTE (ANEXAR COPIAS DEL MINISTERIO PUBLICO)</td></tr>
				<tr><td colspan="6"><form:input path="reembolsoTO.ministerioPublico" class="inputTextNombre"/></td></tr>
				<tr>
					<td colspan="3">EN CASO DE ACCIDENTE AUTOMOVIL&Iacute;STICO �CUENTA CON SEGURO DE AUTOMOVIL?  </td>
					<td colspan="3">NOMBRE DE LA COMPA�IA</td>
				</tr>
				<tr>
					<td colspan="3">
						SI <form:radiobutton path="reembolsoTO.seguroAuto" id="seguroAutoSI" value="1"/>
						NO <form:radiobutton path="reembolsoTO.seguroAuto" id="seguroAutoNO" value="2"/> 
					</td>
					<td colspan="3"><form:input path="reembolsoTO.companiaSeguroAuto" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td>COBERTURA</td>
					<td>SUMA ASEGURADA</td>
					<td>No. P&Oacute;LIZA</td>
					<td colspan="2">COMPA�IA DEL TERCERO</td>
				</tr>
				<tr>
					<td><form:input path="reembolsoTO.cobertura" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.sumaAsegurada" class="inputTextNombre"/></td>
					<td><form:input path="reembolsoTO.numPolizaAuto" class="inputTextNombre"/></td>
					<td colspan="2"><input type="text" class="inputTextNombre" name="companiaTercero" value="${reembolsoTO.companiaTercero}"></td>
				</tr>
				<tr>
					<td colspan="3">HOSPITAL,CL&Iacute;NICA O SANATORIO EN QUE FUE ATENDIDO</td>
					<td colspan="3"><form:input path="reembolsoTO.hospital" id="hospital" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">�QU&Eacute; ESTUDIOS LE REALIZARON PARA EL DIAGN&Oacute;STICO Y TRATAMIENTO?</td>
					<td colspan="3"><form:input path="reembolsoTO.estudiosDiagnostico" id="estudiosDiagnostico" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">NOMBRE DEL M&Eacute;DICO TRATANTE</td>
					<td colspan="3">ESPECIALIDAD</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.medicoTratante" id="medicoTratante" class="inputTextNombre"/></td>
					<td colspan="3"><form:input path="reembolsoTO.especialidad" id="especialidad" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">DIRECCI&Oacute;N</td>
					<td colspan="3">TELEFONO Y/O CORREO ELECTR&Oacute;NICO</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.direccionMedico" id="direccionMedico" class="inputTextNombre"/></td>
					<td colspan="3"><form:input path="reembolsoTO.telEmailMedico" id="telEmailMedico" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">�M&Eacute;DICOS QUE HA CONSULTADO EN LOS &Uacute;LTIMOS 2 A�OS?</td>
					<td colspan="3">CAUSA</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.medicosConsultados" class="inputTextNombre"/></td>
					<td colspan="3"><form:input path="reembolsoTO.causa" class="inputTextNombre"/></td>
				</tr>
				<tr>
					<td colspan="3">TEL&Eacute;FONO Y/O CORREO ELECTR&Oacute;NICO</td>
					<td colspan="3">FECHA</td>
				</tr>
				<tr>
					<td colspan="3"><form:input path="reembolsoTO.telEmailMedicoCon" id="telEmailMedicoCon" class="inputTextNombre"/></td>
					<td colspan="3"><form:input path="reembolsoTO.fechaMedicoCon" id="fechaMedicoCon" class="inputTextNombre"/></td>
				</tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="button" value="GENERAR FORMATO" onclick="validarAccidenteEnfermedad();"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr><td colspan="6">&nbsp;</td></tr>
			</table>
		</div>
	</form:form>
			
</BODY>
</HTML>
