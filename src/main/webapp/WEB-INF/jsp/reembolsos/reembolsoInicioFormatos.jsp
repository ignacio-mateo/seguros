<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmFormatosReembolso">
			<input name="numEmp" value="${empTO.numEmp}" type="hidden">
			<input name="nombreEmp" value="${empTO.nombreEmp}" type="hidden">
			<input name="perfilEmp" value="${empTO.perfilEmp}" type="hidden">
			<div>&nbsp;</div>
			<table class="principal">
				<tr><td class="encabezado" colspan="8">FORMATOS REEMBOLSOS</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td class="columnaDoc"><a href="doc/Formato.pdf"><img src="imagenes/reembolso01.jpg" class="iconoDocumento">Informe Medico</a></td>
					<td class="columnaDoc"><img src="imagenes/reembolso02.jpg" class="iconoDocumento" onclick="enviar('frmFormatosReembolso','formatoAccidenteEnfermedad.htm');">Accidente o Enfermedad</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td class="columnaDoc"><a href="doc/Formato.pdf"><img src="imagenes/reembolso05.jpg" class="iconoDocumento">Solicitud Reembolso Centauro</a></td>
					<td class="columnaDoc"><a href="doc/Formato.pdf"><img src="imagenes/reembolso06.jpg" class="iconoDocumento">Formato de Transferencia de Pago</a></td>
				</tr>
			</table>	
		</form>
	</div>
	
</BODY>
</HTML>