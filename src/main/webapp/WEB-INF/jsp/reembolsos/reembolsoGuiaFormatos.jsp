<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmFormatosReembolso">
			<input name="numEmp" value="${empTO.numEmp}" type="hidden">
			<input name="nombreEmp" value="${empTO.nombreEmp}" type="hidden">
			<input name="perfilEmp" value="${empTO.perfilEmp}" type="hidden">
			<div>&nbsp;</div>
			<table class="principal">
				<tr><td class="encabezado" colspan="8">MANUALES DE REEMBOLSOS Y CARTAS PASE</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td class="columnaDoc"><a href="doc/Manual.pdf"><img src="imagenes/guiaReembolso_cartaPase.png" class="iconoDocumento"> Manual 1</a></td>
					<td class="columnaDoc"><a href="doc/Manual.pdf"><img src="imagenes/guiaReembolso_reembolso.png" class="iconoDocumento"> Manual 2</a></td>
					<td class="columnaDoc"></td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
			</table>	
		</form>
	</div>
	
</BODY>
</HTML>