<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/include/tagsLibs.jsp" %>

<HTML>
<HEAD><%@include file="/WEB-INF/jsp/include/tagsCss.jsp"%></HEAD>

<BODY>
			
	<%@include file="/WEB-INF/jsp/estructura/encabezado.jsp"%>
	<%@include file="/WEB-INF/jsp/estructura/piepaginaA.jsp"%>

	<form:form method="post" action="actualizarRecarga.htm" modelAttribute="catalogoForm">
		<div class="cuerpo">
			
			<table class="principal">
				<tr><td class="encabezado" colspan="3">RECARGA DEL CATALOGOS</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td class="encabezado">SELECCIONAR</td>
					<td class="encabezado">CAMPO</td>
					<td class="encabezado">VALOR DEFAULT</td>
					<td class="encabezado">VALOR NUEVO</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
					<c:forEach items="${catalogoForm.lista}" var="compo" varStatus="status">
						<tr id="fila${status.index}">
							<td><input type="checkbox" class="columna1" name="lista[${status.index}].idCampo" id="check${status.index}" value="1"></td>
							<td><input type="text" class="columna2" name="lista[${status.index}].descripcion" id="paterno${status.index}" value="${campo.descripcion}" readonly="readonly" ></td>
							<td><input type="text" class="columna2" name="lista[${status.index}].campoActual" id="paterno${status.index}" value="${campo.campoActual}" readonly="readonly" ></td>
							<td><input type="text" class="columna3" name="lista[${status.index}].campoNuevo" id="paterno${status.index}" value="${campo.campoNuevo}" ></td>
						</tr>
					</c:forEach>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3"><input type="submit" class="encabezado" value="Actualizar Datos"></td></tr>
			</table>
			
		</div>
	</form:form>
	
</BODY>
</HTML>