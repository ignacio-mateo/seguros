<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<form:form method="post" modelAttribute="autorizacionForm" id="frmAutorizarConsulmedAlta">
		<form:hidden path="empTO.numEmp"/>
		<form:hidden path="empTO.nombreEmp"/>
		<form:hidden path="empTO.perfilEmp"/>
		<input type="hidden" name="paramNumEmp">
		<input type="hidden" name="paramNombreEmp">
		<input type="hidden" name="paramPerfilEmp">
		<input type="hidden" name="numEmpSolicitado">
		<input type="hidden" name="idDependiente">
		<input type="hidden" id="tamListSol" name="tamListSol" value="${autorizacionForm.tamListSol}">
		
		<div class="cuerpo">
			<table class="principal">
				<jsp:include page="../../estructura/estrucColumnasSolAutorizaciones.jsp"/>
				<c:forEach items="${autorizacionForm.listSol}" var="autorizacionTO" varStatus="status">
					<tr>
						<td><input type="hidden" name="listSol[${status.index}].idOperacion" value="${autorizacionTO.idOperacion}">
							<input type="checkbox" name="listSol[${status.index}].autorizar" id="autorizar${status.index}" 
								onchange="autorizarCheck(${status.index});">					
						</td>
						<td><input type='text' class="columnaGeneral" name="listSol[${status.index}].idSolPrimaria" value="${autorizacionTO.idSolPrimaria}" readonly="readonly"></td>
						<td><input type='text' class="columnaGeneral" name="listSol[${status.index}].idEmpleado" value="${autorizacionTO.idEmpleado}" readonly="readonly"></td>
						<td><input type='text' class="columnaNombre" name="listSol[${status.index}].nombre" value="${autorizacionTO.nombre}" 
								onclick="verSolicitud(${autorizacionTO.idOperacion},${autorizacionTO.idTipoSolicitud},
												${autorizacionTO.idEmpleado},${autorizacionTO.idSolPrimaria},
												${empTO.numEmp},'${empTO.nombreEmp}',${empTO.perfilEmp});" readonly="readonly"></td>
						<td><input type='text' class="columnaFecha" name="listSol[${status.index}].fechaElaboracion" value="${autorizacionTO.fechaElaboracion}" readonly="readonly"></td>
						<td><input type="checkbox" name="listSol[${status.index}].rechazar" id="rechazar${status.index}" onChange="rechazarCheck(${status.index});"></td>
						<td><input type="text" class="columnaComentario" name="listSol[${status.index}].comentario" id="comentario${status.index}" value="${autorizacionTO.comentario}" maxlength="15"></td>
					</tr>
				</c:forEach>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr><td colspan="7">&nbsp;</td></tr>
				<tr>
					<td colspan="5">&nbsp;</td>
					<td><input type="button" value="Actualizar" onclick="validarComentarios('frmAutorizarConsulmedAlta','autorizarConsulmedAlta.htm');"/></td>
					<td>&nbsp;</td>
				</tr>
			</table>		
		</div>
	</form:form>
	
</BODY>
</HTML>
