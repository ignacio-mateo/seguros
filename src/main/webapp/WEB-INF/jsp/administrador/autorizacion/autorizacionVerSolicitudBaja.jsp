<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">	
		<form id="frmConsultaBajaSolitudesxAut" method="post">	
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<div id="areaImpresion">
				<table class="principal">
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/estrucTituloFechaConsulta.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					
					<!-- 	DATOS DEL EMPLEADO -->
					<jsp:include page="../../estructura/estrucEmpleado.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
		
					<!--	DEPENDIENTES	-->
					<jsp:include page="../../estructura/estrucColumnasSolDependiente.jsp"/>
					<c:forEach items="${listaDep}" var="dependienteTO">
					<%@include file="../../estructura/estrucSolDepenConsulta.jsp"%>
					</c:forEach>
													
					<!--	COSTOS	-->
					<jsp:include page="../../estructura/estrucCostos.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
		
					<!--	DEPENDIENTES CONSULMED	-->
					<jsp:include page="../../estructura/estrucColumnasSolDependiente.jsp"/>
					<c:forEach items="${listaConsulmed}" var="consulmedTO">
					<%@include file="../../estructura/estrucSolConsulmedConsulta.jsp"%>
					</c:forEach>
					<jsp:include page="../../estructura/salto.jsp"/>
		
					<!-- PIE DE PAGINA -->
					<jsp:include page="../../estructura/estrucConsentimiento.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/estrucFirma.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/estrucAutorizacion.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/estrucConsentimiento2.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/salto.jsp"/>
					<jsp:include page="../../estructura/estrucSello.jsp"/>
				</table>
			</div>
			<table class="principal">
				<tr>
					<td class="columna0"><img src="imagenes/impresora3.jpg" class="iconoImprimir" id="imprimir"></td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0"><img src="imagenes/flecha.jpg" class="iconoHome" onclick="enviar('frmConsultaBajaSolitudesxAut','consultaBajaSolitudesxAut.htm');">Regresar</td>
				</tr>
			</table>
		</form>
	</div>
	 		
</BODY>
</HTML>
