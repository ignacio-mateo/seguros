<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form id="frmCotizaFiniquito" method="post">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" id="dia" name="dia" value="">
			<input type="hidden" id="mes" name="mes" value="">
			<input type="hidden" id="fechaFiniq" name="fechaFiniq" value="">
			
			<div>&nbsp;</div>
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="2">COTIZACION DE FINIQUITOS</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Numero de Empleado:</td>
						<td><input type="text" id="numEmpSolicitado" name="numEmpSolicitado" value=""></td>
					</tr>
					<tr>
						<td>Fecha de Calculo:</td>
						<td><input type="text" id="fecha" name="fecha" value=""></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center" colspan="12" class="leyenda">${mensaje1}</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="button" value="Buscar" onclick="generacionFiniquito();"></td>
					</tr>
				</table>	
			</form>
	</div>
	
</BODY>
</HTML>