<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmFiniquito">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<div>&nbsp;</div>
				<table class="principal">
					<tr>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
						<td width="100px">&nbsp;</td>
					</tr>
					<tr><td class="fondoGris" colspan="8">CONSULTA DE FINIQUITOS</td></tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr><td align="center" colspan="8" class="leyenda">${mensaje1}</td></tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">Numero de Empleado:</td>
						<td class="fondoGris">${empTO.numEmpSolicitado}</td>
						<td class="fondoAzul">Nombre:</td>
						<td class="fondoGris" colspan="5">${finiquitoTO.nombreEmpSolicitado}</td>
					</tr>
					<tr>
						<td class="fondoAzul">Monto Empleado:</td>
						<td class="fondoGris">${finiquitoTO.montoEmp}</td>
						<td class="fondoAzul">Monto Ampl Emp:</td>
						<td class="fondoGris">${finiquitoTO.montoEmpAmp}</td>
						<td class="fondoAzul">Monto Dependientes:</td>
						<td class="fondoGris">${finiquitoTO.montoDep}</td>
						<td class="fondoAzul">Monto Ampl Dependientes:</td>
						<td class="fondoGris">${finiquitoTO.montoDepAmp}</td>
					</tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">Monto Finiquito:</td>
						<td class="fondoGris">${finiquitoTO.montoFiniquito}</td>
						<td colspan="6">&nbsp;</td>
					</tr>
					<tr>
						<td class="fondoAzul">Fecha de Finiquito:</td>
						<td class="fondoGris">${finiquitoTO.fechaFiniq}</td>
						<td colspan="6">&nbsp;</td>
					</tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr><td colspan="8">&nbsp;</td></tr>
					<tr>
						<td><img src="imagenes/flecha.jpg" title="Regresar" class="iconoHome" onclick="enviar('frmFiniquito','consultaFiniquitoInicio.htm');"></td>
						<td colspan="6">&nbsp;</td>
						<td></td>
					</tr>
				</table>
		</form>
	</div>
	
</BODY>
</HTML>