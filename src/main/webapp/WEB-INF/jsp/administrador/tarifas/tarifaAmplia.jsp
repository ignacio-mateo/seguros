<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<form:form method="post" action="setTarifaAmplia.htm" modelAttribute="tarifaForm">
		<form:hidden path="empTO.numEmp"/>
		<form:hidden path="empTO.nombreEmp"/>
		<form:hidden path="empTO.perfilEmp"/>
		<div class="cuerpo">
			<table class="principal">
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr><td class="encabezado" colspan="4">TARIFAS DE AMPLIACIONES</td></tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td>ID. TARIFA</td>
					<td>DESCRIPCION </td>
					<td>TARIFA</td>
					<td>TARIFA NUEVA</td>
				</tr>
				<c:forEach items="${tarifaForm.listTarifa}" var="contact" varStatus="status">
				<tr>
					<td><input type='text' class="colTarifaId" name="listTarifa[${status.index}].id" value="${contact.id}" readonly="readonly"></td>
					<td><input type='text' class="colTarifaId" name="listTarifa[${status.index}].descripcion" value="${contact.descripcion}" readonly="readonly"></td>
					<td><input type='text' class="colTarifaCosto" name="listTarifa[${status.index}].tarifa" value="${contact.tarifa}" readonly="readonly"></td>
					<td><input type='text' class="colTarifaCosto" name="listTarifa[${status.index}].tarifaNueva" value="${contact.tarifa}"></td>
				</tr>
				</c:forEach>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="submit" value="Actualizar"/></td>
				</tr>
			</table>
		</div>
	</form:form>
		
</BODY>
</HTML>
