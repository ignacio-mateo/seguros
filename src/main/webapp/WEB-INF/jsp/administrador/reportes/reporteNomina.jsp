<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	
	<form method="post" id="frmReporteNomina">		
		<input type="hidden" name="numEmp" value="${empTO.numEmp}">
		<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
		<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
		<input type="hidden" name="opcionConsulta" value="2">
		<input type="hidden" name="reporte" value="2">	
		<input type="hidden" name="tipo" value="${reporteParamsTO.tipo}">	
		<input type="hidden" name="tipoEmp" value="${reporteParamsTO.tipoEmp}">
		<input type="hidden" name="fechaIni" value="${reporteParamsTO.fechaIni}">
		<input type="hidden" name="fechaFin" value="${reporteParamsTO.fechaFin}">
		<div class="cuerpo">
			<table class="principal">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
				</tr>
				<tr><td class="encabezado" colspan="8">REPORTE DE NOMINAS</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">FOLIO</td>
					<td class="columna0">EMPLEADO</td>
					<td class="columna0">NOMBRE</td>
					<td class="columna0">REGION</td>
					<td class="columna0">FECHA ALTA</td>
					<td class="columna0">TIPO POLIZA</td>
					<td class="columna0">IMPORTE</td>
				</tr>
				<c:forEach items="${lista}" var="reporte">
					<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">${reporte.idSolicitud}</td>
					<td class="columna0">${reporte.numEmp}</td>
					<td class="columna0">${reporte.apellidoP} ${reporte.apellidoM} ${reporte.nombre}</td>
					<td class="columna0">${reporte.region}</td>
					<td class="columna0">${reporte.fechaAlta}</td>
					<td class="columna0">${reporte.cobertura}</td>
					<td class="columna0">${reporte.costoEmp}</td>
				</tr>
				</c:forEach>
				<tr>
					<td class="columna0">
						<img src="imagenes/flecha.jpg" 
							class="iconoHome" 
							onclick="enviar('frmReporteNomina','reporteNomina.htm');">Regresar
					</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">
						<input type="text" 
							value="Generar TXT" 
							onclick="enviar('frmReporteNomina','reporteNominaGeneraTxt.htm');">
					</td>
					<td class="columna0">
						<input type="text" 
							value="Generar Excel" 
							onclick="enviar('frmReporteNomina','reporteNominaGenera.htm');">
					</td>
				</tr>
			</table>
		</div>
 	</form>
	 		
</BODY>
</HTML>
