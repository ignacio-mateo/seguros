<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<form method="post" id="frmReporteAmpliaciones">	
		<input type="hidden" name="numEmp" value="${empTO.numEmp}">
		<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
		<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
		<input type="hidden" name="opcionConsulta" value="2">	
		<input type="hidden" name="reporte" value="8">	
		<div class="cuerpo">
			<table class="principal">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
				</tr>
				<tr><td class="encabezado" colspan="8">REPORTE DE AMPLIACIONES TOTALES</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Tipo de Empleado: </td>
					<td><select name="tipoEmp">
							<option value="0">Todos</option>
							<option value="1">Confianza</option>
							<option value="2">Sindicalizado </option>
						</select>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Desde: </td>
					<td><input type="text" name="fechaIni" id="fechaIni"></td>
					<td>Hasta:</td>
					<td><input type="text" name="fechaFin" id="fechaFin"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<!-- 
					<td colspan="2"><input type="button" value="Generar Reporte Txt" class="inputTextNombre" 
						onclick="validaReporte('frmReporteAmpliaciones','reporteAmpliacionesGeneraTxt.htm');"></td>
						 -->
					<td>&nbsp;</td>	
					<td colspan="2"><input type="button" value="Generar Reporte Excel" class="inputTextNombre" 
						onclick="validaReporte('frmReporteAmpliaciones','reporteAmpliacionesGenera.htm');"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
 	</form>

</BODY>
</HTML>
