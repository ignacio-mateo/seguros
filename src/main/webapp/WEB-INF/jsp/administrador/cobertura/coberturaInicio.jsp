<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="idConsultaCambioCobertura">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" id="dia" name="dia" value="">
			<input type="hidden" id="mes" name="mes" value="">
						
			<div>&nbsp;</div>
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="2">CAMBIO DE COBERTURA</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Numero de Empleado:</td>
						<td><input type="text" id="numEmpSolicitado" name="numEmpSolicitado" value=""></td>
					</tr>
					<tr>
						<td>Fecha de Cambio:</td>
						<td><input type="text" id="fecha" name="fecha" value=""></td>
					</tr>
					<tr>
						<td>Tipo de Cobertura: </td>
						<td>
							<select name="idTipoAmpl" id="tipoAmp">
								<option value="-1">Opcion</option>		
								<c:forEach items="${idTipoAmpl}" var="dependiente" varStatus="status">
									<option value="${dependiente.idTipoCob}">${dependiente.descripcion}</option>									
								</c:forEach>								
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center" colspan="12" class="leyenda">${mensaje1}</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="button" onclick="cambioCobertura();" value="Buscar"></td>
					</tr>
				</table>	
			</form>
	</div>
	
</BODY>
</HTML>