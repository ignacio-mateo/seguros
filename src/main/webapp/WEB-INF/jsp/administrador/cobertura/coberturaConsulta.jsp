<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmCobertura">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="numEmpSolicitado" value="${empleadoTO.numEmp}">
			<input type="hidden" name="idSol" value="${solicitudTO.idSol}">
			<input type="hidden" name="idTipoAmplAnt" value="${solicitudTO.idTipoCobertura}">
			<input type="hidden" name="costoAmpliacionAnt" value="${solicitudTO.costoAmpliacion}">
			<input type="hidden" name="costoTotalAnt" value="${solicitudTO.costoTotal}">
			<input type="hidden" name="idTipoAmpl" value="${idTipoAmpl}">
			<input type="hidden" name="costoAmpliacion" value="${montoAmpliacion}">
			<input type="hidden" name="costoAmpliacionInd" value="${montoAmpliacionInd}">
			<input type="hidden" name="costoTotal" value="${montoTotal}">
			<input type="hidden" name="fechaAutorizacion" value="${solicitudTO.fechaElaboracion}">
						
			<div>&nbsp;</div>
				<table class="principal">
					<tr><td class="fondoGris" colspan="14">CAMBIO TIPO DE COBERTURA</td></tr>
					<tr><td align="center" colspan="14">&nbsp;</td></tr>
					<tr><td align="center" colspan="14" class="leyenda">${mensaje1}</td></tr>
					<tr><td align="center" colspan="14">&nbsp;</td></tr>
					<tr><td align="center" colspan="14">Información General del Titular</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA ALTA</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO TOT. DEP.</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px">MONTO TOTAL</td>
						<td class="fondoAzul" width="60px">MONTO PRORR</td>
					</tr>
					<tr>
						<td class="fondoGris">${solicitudTO.idSol}</td>
						<td class="fondoGris">${empleadoTO.numEmp}</td>
						<td class="fondoGris">${empleadoTO.apellidoP} ${empleadoTO.apellidoM} ${empleadoTO.nombre}</td>
						<td class="fondoGris">${empleadoTO.descTipoEmp}</td>
						<td class="fondoGris">${empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${solicitudTO.estatusSol}</td>
						<td class="fondoGris">${solicitudTO.fechaAutorizacion}</td>
						<td class="fondoGris">${solicitudTO.tipoCobertura}</td>
						<td class="fondoGris">${solicitudTO.costoEmpleado}</td>
						<td class="fondoGris">${solicitudTO.costoDependiente}</td>
						<td class="fondoGris">${solicitudTO.costoAmpliacion}</td>
						<td class="fondoGris">${solicitudTO.costoTotal}</td>
						<td class="fondoGris">${montoAmpProrr}</td>
					</tr>
					<c:forEach items="${listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAltaAut}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.costoDep}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris" colspan="2">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td align="center" colspan="14">&nbsp;</td></tr>
					<tr><td align="center" colspan="14">&nbsp;</td></tr>
					<tr><td align="center" colspan="14">Prorrateo con la Ampliacion Nueva</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA SEL.</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO TOT. DEP.</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px" colspan="2">MONTO TOTAL</td>
					</tr>
					<tr>
						<td class="fondoGris">${solicitudTO.idSol}</td>
						<td class="fondoGris">${empleadoTO.numEmp}</td>
						<td class="fondoGris">${empleadoTO.apellidoP} ${empleadoTO.apellidoM} ${empleadoTO.nombre}</td>
						<td class="fondoGris">${empleadoTO.descTipoEmp}</td>
						<td class="fondoGris">${empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${solicitudTO.estatusSol}</td>
						<td class="fondoGris">${solicitudTO.fechaElaboracion}</td>
						<td class="fondoGris">${tipoPoliza}</td>
						<td class="fondoGris">${solicitudTO.costoEmpleado}</td>
						<td class="fondoGris">${solicitudTO.costoDependiente}</td>
						<td class="fondoGris">${montoAmpNvo}</td>
						<td class="fondoGris" colspan="2">${montoTotalParcial}</td>
					</tr>
					<c:forEach items="${listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAlta}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.costoDep}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris" colspan="2">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td colspan="14">&nbsp;</td></tr>
					<tr><td align="center" colspan="14">&nbsp;</td></tr>
					<tr><td align="center" colspan="14">Datos Finales</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA SEL.</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO TOT. DEP.</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px" colspan="2">MONTO TOTAL</td>
					</tr>
					<tr>
						<td class="fondoGris">${solicitudTO.idSol}</td>
						<td class="fondoGris">${empleadoTO.numEmp}</td>
						<td class="fondoGris">${empleadoTO.apellidoP} ${empleadoTO.apellidoM} ${empleadoTO.nombre}</td>
						<td class="fondoGris">${empleadoTO.descTipoEmp}</td>
						<td class="fondoGris">${empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${solicitudTO.estatusSol}</td>
						<td class="fondoGris">${solicitudTO.fechaElaboracion}</td>
						<td class="fondoGris">${tipoPoliza}</td>
						<td class="fondoGris">${solicitudTO.costoEmpleado}</td>
						<td class="fondoGris">${solicitudTO.costoDependiente}</td>
						<td class="fondoGris">${montoAmpliacion}</td>
						<td class="fondoGris" colspan="2">${montoTotal}</td>
					</tr>
					<c:forEach items="${listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAlta}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.costoDep}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris" colspan="2">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td colspan="14">&nbsp;</td></tr>
					<tr><td colspan="14">&nbsp;</td></tr>
					<tr><td colspan="14">&nbsp;</td></tr>
					<tr><td colspan="14">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="4"><img src="imagenes/flecha.jpg" title="Regresar" class="iconoHome" onclick="enviar('frmCobertura','inicioCambioCobertura.htm');"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="2"><input type="button" value="Actualizar" onclick="enviar('frmCobertura','actualizacionCambioCobertura.htm');"></td>
					</tr>
				</table>	
			</form>
	</div>
	
</BODY>
</HTML>