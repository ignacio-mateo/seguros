<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmGuardarSolicitudAdmin">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="numEmpSolicitado" value="${solicitudAdminTO.numEmpSolicitado}">
			<input type="hidden" name="idSolicitud" value="${solicitudAdminTO.idSolicitud}">
			<input type="hidden" name="idTipoSol" value="${solicitudAdminTO.idTipoSol}">
			<input type="hidden" name="idEstatulSol" value="${solicitudAdminTO.idEstatulSol}">
			<input type="hidden" name="idCobertura" value="${solicitudAdminTO.idCobertura}">
			<input type="hidden" id="dia" name="dia" value="">
			<input type="hidden" id="mes" name="mes" value="">
			<div>&nbsp;</div>
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="2">ACTUALIZACION DE INFORMACI&Oacute;N DEL TITULAR</td>
					</tr>
					<tr>
						<td>Numero de Empleado:</td>
						<td>${solicitudAdminTO.numEmpSolicitado}</td>
					</tr>
					<tr>
						<td>Nombre de Empleado:</td>
						<td>${solicitudAdminTO.apellidoP} ${solicitudAdminTO.apellidoM} ${solicitudAdminTO.nombre}</td>
					</tr>
					<tr>
						<td>Fecha de Calculo:</td>
						<td><input type="text" id="fechaAlta" name="fechaAlta"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="button" value="Actualizar" onclick="setSolicitud();"></td>
					</tr>
				</table>	
			</form>
	</div>
	
</BODY>
</HTML>