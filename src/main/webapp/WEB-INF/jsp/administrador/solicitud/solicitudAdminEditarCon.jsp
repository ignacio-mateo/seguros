<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmGuardarConsulmedAdmin">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="numEmp" value="${solicitudAdminTO.numEmpSolicitado}">
			<input type="hidden" name="idConsulmed" value="${solicitudAdminTO.idConsulmed}">
			<input type="hidden" id="idParentesco" name="idParentesco" value="${solicitudAdminTO.idParentesco}">
			<input type="hidden" id="idEstatulSol" name="idEstatulSol" value="${solicitudAdminTO.idEstatulSol}">
			<div>&nbsp;</div>
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="3">ACTUALIZACIÓN DE INFORMACI&Oacute;N DEL DEPENDIENTE CONSULMED</td>
					</tr>
					<tr>
						<td>Numero de Empleado:</td>
						<td>${solicitudAdminTO.numEmpSolicitado}</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Parentesco:</td>
						<td><input type="text" id="parentesco" name="parentesco" value="${solicitudAdminTO.parentesco}"></td>
						<td>
							<select id="idParent">
								<option value='0'>SELECCIONAR</option>
								<option value='1'>PADRE</option>
								<option value='2'>MADRE</option>
								<option value='7'>SUEGRO</option>
								<option value='8'>SUEGRA</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>A.Paterno:</td>
						<td><input type="text" name="apellidoP" value="${solicitudAdminTO.apellidoP}"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>A.Materno:</td>
						<td><input type="text" name="apellidoM" value="${solicitudAdminTO.apellidoM}"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Nombre(s):</td>
						<td><input type="text" name="nombre" value="${solicitudAdminTO.nombre}"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Fecha Nacimiento:</td>
						<td><input type="text" value="${solicitudAdminTO.fechaNac}" name="fechaNac"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Fecha Alta:</td>
						<td><input type="text" value="${solicitudAdminTO.fechaAlta}" name="fechaAlta"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Estatus:</td>
						<td><input type="text" id="estatulSol" name="estatulSol" value="${solicitudAdminTO.estatulSol}"></td>
						<td>
							<select id="idEstCon">
								<option value='0'>SELECCIONAR</option>
								<option value='1'>EN PROCESO DE ALTA</option>
								<option value='2'>ALTA AUTORIZADA</option>
								<option value='3'>SE RECHAZA ALTA</option>
								<option value='4'>EN PROCESO DE BAJA</option>
								<option value='5'>BAJA</option>	
								<option value='6'>SE RECHAZA BAJA</option>									
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="button" value="Actualizar" onclick="validarConsulmed();"></td>
						<td>&nbsp;</td>
					</tr>
				</table>		
		</form>
	</div>
	
</BODY>
</HTML>