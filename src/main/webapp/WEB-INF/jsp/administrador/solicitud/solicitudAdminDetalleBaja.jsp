<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div>&nbsp;</div>
	<div class="cuerpo">
		<form method="post" id="frmSolEdicion">			
			<div>
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="13">ACTUALIZACIÓN DE INFORMACI&Oacute;N DEL TITULAR Y DEPENDIENTES</td>
					</tr>
					<tr>
						<td colspan="13">&nbsp;</td>
					</tr>
					<tr><td colspan="13" align="center">Información General del Titular</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="160px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">REGION</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="60px">FECHA NAC.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="60px">EST. SOL.</td>
						<td class="fondoAzul" width="60px">FECHA ALTA</td>
						<td class="fondoAzul" width="60px">FECHA BAJA</td>
						<td class="fondoAzul" width="60px">MONTO EMP</td>
						<td class="fondoAzul" width="50px">MONTO AMP. EMP.</td>
						<td class="fondoAzul" width="50px">TIPO POLIZA</td>
					</tr>
					<c:forEach items="${listSolicitud}" var="solicitudTO">
						<tr>
							<td class="fondoGris">${solicitudTO.idSolicitud}</td>
							<td class="fondoGris">${solicitudTO.numEmpSolicitado}</td>
							<td class="fondoGris">${solicitudTO.apellidoP} ${solicitudTO.apellidoM} ${solicitudTO.nombre}</td>
							<td class="fondoGris">${solicitudTO.region}</td>
							<td class="fondoGris">${solicitudTO.tipoEmpleado}</td>
							<td class="fondoGris">${solicitudTO.fechaNac}</td>
							<td class="fondoGris">${solicitudTO.tipoSol}</td>
							<td class="fondoGris">${solicitudTO.estatulSol}</td>
							<td class="fondoGris">${solicitudTO.fechaAlta}</td>
							<td class="fondoGris">${solicitudTO.fechaBaja}</td>
							<td class="fondoGris">${solicitudTO.costo}</td>
							<td class="fondoGris">${solicitudTO.costoAmpliacionInd}</td>
							<td class="fondoGris">${solicitudTO.cobertura}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="13">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">FOLIO</td>
						<td class="fondoAzul" colspan="7">COMENTARIO</td>
						<td class="fondoAzul">MONTO TOTAL DEP.</td>
						<td class="fondoAzul">MONTO TOTAL AMP. DEP.</td>
						<td class="fondoAzul">MONTO TOTAL BASICA</td>
						<td class="fondoAzul">MONTO TOTAL AMP.</td>
						<td class="fondoAzul">MONTO TOTAL</td>
					</tr>
					<c:forEach items="${listSolicitud}" var="solicitudTO">
						<tr>
							<td class="fondoGris">${solicitudTO.idSolicitud}</td>
							<td class="fondoGris" colspan="7">${solicitudTO.comentario}</td>
							<td class="fondoGris">${solicitudTO.costoTotalDep}</td>
							<td class="fondoGris">${solicitudTO.costoAmpliacionDep}</td>
							<td class="fondoGris">${solicitudTO.costoCobBasica}</td>
							<td class="fondoGris">${solicitudTO.costoAmpliacion}</td>
							<td class="fondoGris">${solicitudTO.costoTotal}</td>	
						</tr>
					</c:forEach>
					<tr><td colspan="13">&nbsp;</td></tr>
					<tr><td colspan="13">&nbsp;</td></tr>
					<tr><td colspan="13">&nbsp;</td></tr>
				</table>	
				<table class="principal">
					<tr><td colspan="12" align="center">Información General de sus Dependientes</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">FOLIO DEP</td>
						<td class="fondoAzul" width="50px">PARENTESCO</td>
						<td class="fondoAzul" width="180px">NOMBRE</td>
						<td class="fondoAzul" width="70px">FECHA NAC.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="50px">EST. SOL.</td>
						<td class="fondoAzul" width="50px">MONTO</td>
						<td class="fondoAzul" width="50px">MONTO AMP.</td>
						<td class="fondoAzul" width="50px">MONTO CANC.</td>
						<td class="fondoAzul" width="70px">FECHA ALTA</td>
						<td class="fondoAzul" width="70px">FECHA BAJA</td>
					</tr>
					<c:forEach items="${listDependiente}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idSolicitud}</td>
							<td class="fondoGris">${dependienteTO.idDependiente}</td>
							<td class="fondoGris">${dependienteTO.parentesco}</td>
							<td class="fondoGris">${dependienteTO.apellidoP} ${dependienteTO.apellidoM} ${dependienteTO.nombre}</td>
							<td class="fondoGris">${dependienteTO.fechaNac}</td>
							<td class="fondoGris">${dependienteTO.tipoSol}</td>
							<td class="fondoGris">${dependienteTO.estatulSol}</td>
							<td class="fondoGris">${dependienteTO.costo}</td>
							<td class="fondoGris">${dependienteTO.costoAmpliacionInd}</td>
							<td class="fondoGris">${dependienteTO.costoCancelacion}</td>
							<td class="fondoGris">${dependienteTO.fechaAlta}</td>
							<td class="fondoGris">${dependienteTO.fechaBaja}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">FOLIO DEP</td>
						<td class="fondoAzul" colspan="12">COMENTARIOS</td>
					</tr>
					<c:forEach items="${listDependiente}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idDependiente}</td>
							<td class="fondoGris" colspan="12">${dependienteTO.comentario}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td colspan="12">&nbsp;</td></tr>
				</table>
				<table class="principal">
					<tr><td colspan="10" align="center">Información General de sus Dependientes Consulmed</td></tr>
					<tr>
						<td class="fondoAzul" width="30px">FOLIO</td>
						<td class="fondoAzul" width="30px">FOLIO DEP</td>
						<td class="fondoAzul" width="50px">PARENTESCO</td>
						<td class="fondoAzul" width="250px">NOMBRE</td>
						<td class="fondoAzul" width="70px">FECHA NAC.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="50px">EST. SOL.</td>
						<td class="fondoAzul" width="50px">MONTO</td>
						<td class="fondoAzul" width="70px">FECHA ALTA</td>
						<td class="fondoAzul" width="70px">FECHA BAJA</td>
					</tr>
					<c:forEach items="${listConsulmed}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idSolicitud}</td>
							<td class="fondoGris">${dependienteTO.idConsulmed}</td>
							<td class="fondoGris">${dependienteTO.parentesco}</td>
							<td class="fondoGris">${dependienteTO.apellidoP} ${dependienteTO.apellidoM} ${dependienteTO.nombre}</td>
							<td class="fondoGris">${dependienteTO.fechaNac}</td>
							<td class="fondoGris">${dependienteTO.tipoSol}</td>
							<td class="fondoGris">${dependienteTO.estatulSol}</td>
							<td class="fondoGris">${dependienteTO.costo}</td>
							<td class="fondoGris">${dependienteTO.fechaAlta}</td>
							<td class="fondoGris">${dependienteTO.fechaBaja}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="10">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul" width="50px">FOLIO DEP</td>
						<td class="fondoAzul" colspan="9">COMENTARIO</td>						
					</tr>
					<c:forEach items="${listConsulmed}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idConsulmed}</td>
							<td class="fondoGris" colspan="9">${dependienteTO.comentario}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="10">&nbsp;</td></tr>
					<tr><td colspan="10"><img src="imagenes/flecha.jpg" class="iconoHome" onclick="enviar('frmSolEdicion','consultaSolicitudAdmin.htm');">
						Regresar</td></tr>
				</table>
			</div>
		</form>
	</div>
	
</BODY>
</HTML>
