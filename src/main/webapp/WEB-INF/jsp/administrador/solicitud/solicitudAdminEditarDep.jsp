<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmGuardarDependienteAdmin">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="numEmpSolicitado" value="${empTO.numEmpSolicitado}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="idDependiente" value="${solicitudAdminTO.idDependiente}">
			<input type="hidden" id="idParentesco" name="idParentesco" value="${solicitudAdminTO.idParentesco}">
			<input type="hidden" id="idEstatulSol" name="idEstatulSol" value="${solicitudAdminTO.idEstatulSol}">
			<input type="hidden" id="sexo" name="sexo" value="${solicitudAdminTO.sexo}">
			<input type="hidden" id="idTipoEmpleado" name="idTipoEmpleado" value="${solicitudAdminTO.idTipoEmpleado}">
			<input type="hidden" id="costoEmp" name="costoEmp" value="${solicitudAdminTO.costoEmp}">
			<input type="hidden" id="costo" name="costo" value="${solicitudAdminTO.costo}">
			<input type="hidden" id="idSolicitud" name="idSolicitud" value="${solicitudAdminTO.idSolicitud}">
			<input type="hidden" id="idCobertura" name="idCobertura" value="${solicitudAdminTO.idCobertura}">
			<input type="hidden" id="costoTotal" name="costoTotal" value="${solicitudAdminTO.costoTotal}">
			<input type="hidden" id="idConyuge" name="idConyuge" value="${solicitudAdminTO.idConyuge}">
			<input type="hidden" id="fechaNacAnt" name="fechaNacAnt" value="${solicitudAdminTO.fechaNac}">
			<input type="hidden" id="fechaActual" name="fechaActual" value="${solicitudAdminTO.fechaAlta}">
			<input type="hidden" id="idParentescoNvo" name="idParentescoNvo" value="">
			<input type="hidden" id="idEstatulSolNvo" name="idEstatulSolNvo" value="">
						
			<div>&nbsp;</div>
				<table class="principal">
					<tr><td class="fondoGris" colspan="6">ACTUALIZACIÓN DE INFORMACI&Oacute;N DEL DEPENDIENTE</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td width="150px" class="fondoAzul">Numero de Empleado:</td>
						<td width="150px" colspan="2" class="fondoGris">${solicitudAdminTO.numEmpSolicitado}</td>
						<td width="150px">&nbsp;</td>
						<td width="130px">&nbsp;</td>
						<td width="130px">&nbsp;</td>
					</tr>
					<tr>
						<td class="fondoAzul">A.Paterno:</td>
						<td colspan="2" class="fondoGris">${solicitudAdminTO.apellidoP}</td>
						<td colspan="2"><input type="text" name="apellidoP" class="inputTextNombre" value="${solicitudAdminTO.apellidoP}"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="fondoAzul">A.Materno:</td>
						<td colspan="2" class="fondoGris">${solicitudAdminTO.apellidoM}</td>
						<td colspan="2"><input type="text" name="apellidoM" class="inputTextNombre" value="${solicitudAdminTO.apellidoM}"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="fondoAzul">Nombre(s):</td>
						<td colspan="2" class="fondoGris">${solicitudAdminTO.nombre}</td>
						<td colspan="2"><input type="text" name="nombre" class="inputTextNombre" value="${solicitudAdminTO.nombre}"></td>
						<td>&nbsp;</td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">Parentesco:</td>
						<td class="fondoGris">${solicitudAdminTO.parentesco}</td>
						<td>
							<select id="idParent">
								<option value='0'>SELECCIONAR</option>
								<option value='3'>ESPOSO</option>
								<option value='4'>ESPOSA</option>
								<option value='5'>HIJO</option>
								<option value='6'>HIJA</option>
							</select>
						</td>
						<td><input type="text" id="parentesco" name="parentesco" value="${solicitudAdminTO.parentesco}" readonly="readonly"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">Fecha Nacimiento:</td>
						<td class="fondoGris">${solicitudAdminTO.fechaNac}</td>
						<td><input type="text" value="${solicitudAdminTO.fechaNac}" id="fechaNacNvo" name="fechaNacNvo"></td>
						<td class="fondoAzul">Fecha Alta:</td>
						<td class="fondoGris">${solicitudAdminTO.fechaAlta}</td>
						<td><input type="text" value="${solicitudAdminTO.fechaAlta}" name="fechaAlta"></td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">Estatus:</td>
						<td class="fondoGris">${solicitudAdminTO.estatulSol}</td>
						<td>
							<select id="idEst">
								<option value='0'>SELECCIONAR</option>
								<option value='1'>EN PROCESO DE ALTA</option>
								<option value='2'>ALTA AUTORIZADA</option>
								<option value='3'>SE RECHAZA ALTA</option>
								<option value='4'>EN PROCESO DE BAJA</option>
								<option value='5'>BAJA</option>	
								<option value='6'>SE RECHAZA BAJA</option>								
							</select>
						</td>
						<td><input type="text" id="estatulSol" name="estatulSol" value="${solicitudAdminTO.estatulSol}" readonly="readonly"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">NOTAS</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">Cuando actualice un dependiente que este autorizado la regla para la fecha de alta es:</td></tr>
					<tr><td colspan="6"> - Si es un dep. que fue dado de alta en el perido actual, la fecha de alta es a partir del 01 de Junio
						dependiendo la fecha que quiera actualizar el costo.
					</td></tr>
					<tr><td colspan="6"> - Si es un dep. que fue dado de alta en otro periodo, no es necesario mover el campo fecha</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr><td colspan="6">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><input type="button" value="Actualizar" onclick="validarDependiente();"></td>
					</tr>
				</table>
			</form>
	</div>
	
</BODY>
</HTML>