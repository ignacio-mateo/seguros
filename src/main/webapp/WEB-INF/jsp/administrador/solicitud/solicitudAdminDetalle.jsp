<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div>&nbsp;</div>
	<div class="cuerpo">
		<form method="post" id="frmSolEdicion">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="numEmpSolicitado" value="${solicitudAdminTO.numEmpSolicitado}">
			<input type="hidden" name="idSolicitud" value="${solicitudAdminTO.idSolicitud}">
			<input type="hidden" name="apellidoP" value="${solicitudAdminTO.apellidoP}">
			<input type="hidden" name="apellidoM" value="${solicitudAdminTO.apellidoM}">
			<input type="hidden" name="nombre" value="${solicitudAdminTO.nombre}">
			<input type="hidden" name="fechaAlta" value="${solicitudAdminTO.fechaAlta}">
			<input type="hidden" name="idCobertura" value="${solicitudAdminTO.idCobertura}">
			<input type="hidden" name="cobertura" value="${solicitudAdminTO.cobertura}">
			<input type="hidden" name="parentesco" value="${dependienteTO.parentesco}">
			<input type="hidden" name="fechaNac" value="${solicitudAdminTO.fechaNac}">
			<input type="hidden" name="idTipoSol" value="${solicitudAdminTO.idTipoSol}">
			<input type="hidden" name="idEstatulSol" value="${solicitudAdminTO.idEstatulSol}">
			<input type="hidden" name="estatulSol" value="${solicitudAdminTO.estatulSol}">
			<input type="hidden" name="idConyuge" value="${solicitudAdminTO.idConyuge}">
			<input type="hidden" name="idDependiente" value="">
			<input type="hidden" name="idConsulmed" value="">
			<input type="hidden" name="idParentesco" value="">
			<input type="hidden" name="idEstatulSol" value="">
			<input type="hidden" name="sexo" value="">
			<input type="hidden" name="idTipoEmpleado" value="">
			<input type="hidden" name="costoEmp" value="">
			<input type="hidden" name="costo" value="">
			<input type="hidden" name="idSolicitud" value="">
			<input type="hidden" name="idCobertura" value="">
			<input type="hidden" name="costoTotal" value="">
			
			<div>
			
				<!-- TITULAR -->
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="16">ACTUALIZACIÓN DE INFORMACI&Oacute;N DEL TITULAR Y DEPENDIENTES</td>
					</tr>
					<tr>
						<td colspan="16">&nbsp;</td>
					</tr>
					<tr><td colspan="16" align="center">Información General del Titular</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">REGION</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="60px">FECHA NAC.</td>
						<td class="fondoAzul" width="60px">FECHA ELAB.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="60px">EST. SOL.</td>
						<td class="fondoAzul" width="60px">FECHA ALTA</td>
						<td class="fondoAzul" width="60px">FECHA BAJA</td>
						<td class="fondoAzul" width="60px">FECHA RENOV</td>
						<td class="fondoAzul" width="60px">MONTO EMP</td>
						<td class="fondoAzul" width="50px">MONTO AMP. EMP.</td>
						<td class="fondoAzul" width="50px">TIPO POLIZA</td>
						<td class="fondoAzul" width="50px">EDITAR</td>
					</tr>
					<c:forEach items="${listSolicitud}" var="solicitudTO">
						<tr>
							<td class="fondoGris">${solicitudTO.idSolicitud}</td>
							<td class="fondoGris">${solicitudTO.numEmpSolicitado}</td>
							<td class="fondoGris">${solicitudTO.apellidoP} ${solicitudTO.apellidoM} ${solicitudTO.nombre}</td>
							<td class="fondoGris">${solicitudTO.region}</td>
							<td class="fondoGris">${solicitudTO.tipoEmpleado}</td>
							<td class="fondoGris">${solicitudTO.fechaNac}</td>
							<td class="fondoGris">${solicitudTO.fechaElab}</td>
							<td class="fondoGris">${solicitudTO.tipoSol}</td>
							<td class="fondoGris">${solicitudTO.estatulSol}</td>
							<td class="fondoGris">${solicitudTO.fechaAlta}</td>
							<td class="fondoGris">${solicitudTO.fechaBaja}</td>
							<td class="fondoGris">${solicitudTO.fechaRenov}</td>
							<td class="fondoGris">${solicitudTO.costo}</td>
							<td class="fondoGris">${solicitudTO.costoAmpliacionInd}</td>
							<td class="fondoGris">${solicitudTO.cobertura}</td>
							<td class="fondoGris"><input type="button" value="Editar" onclick="editarSolicitud();"></td>
						</tr>
					</c:forEach>
					<tr><td colspan="16">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">FOLIO</td>
						<td class="fondoAzul" colspan="8">COMENTARIO</td>
						<td class="fondoAzul">TIPO MEMORIAL</td>
						<td class="fondoAzul">MONTO MEMORIAL</td>
						<td class="fondoAzul">MONTO TOTAL DEP.</td>
						<td class="fondoAzul">MONTO TOTAL AMP. DEP.</td>
						<td class="fondoAzul">MONTO TOTAL BASICA</td>
						<td class="fondoAzul">MONTO TOTAL AMP.</td>
						<td class="fondoAzul">MONTO TOTAL</td>
					</tr>
					<c:forEach items="${listSolicitud}" var="solicitudTO">
						<tr>
							<td class="fondoGris">${solicitudTO.idSolicitud}</td>
							<td class="fondoGris" colspan="8">${solicitudTO.comentario}</td>
							<td class="fondoGris">${solicitudTO.tipoMemorial}</td>
							<td class="fondoGris">${solicitudTO.costoMemorial}</td>
							<td class="fondoGris">${solicitudTO.costoTotalDep}</td>
							<td class="fondoGris">${solicitudTO.costoAmpliacionDep}</td>
							<td class="fondoGris">${solicitudTO.costoCobBasica}</td>
							<td class="fondoGris">${solicitudTO.costoAmpliacion}</td>
							<td class="fondoGris">${solicitudTO.costoTotal}</td>	
						</tr>
					</c:forEach>
					<tr><td colspan="16">&nbsp;</td></tr>
					<tr><td colspan="16">&nbsp;</td></tr>
					<tr><td colspan="16">&nbsp;</td></tr>
				</table>	
				
				<!-- DEPENDIENTES -->
				
				<table class="principal">
					<tr><td colspan="15" align="center">Información General de sus Dependientes</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">FOLIO DEP</td>
						<td class="fondoAzul" width="50px">PARENTESCO</td>
						<td class="fondoAzul" width="120px">NOMBRE</td>
						<td class="fondoAzul" width="70px">FECHA NAC.</td>
						<td class="fondoAzul" width="70px">FECHA ELAB.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="50px">EST. SOL.</td>
						<td class="fondoAzul" width="50px">MONTO</td>
						<td class="fondoAzul" width="50px">MONTO AMP.</td>
						<td class="fondoAzul" width="50px">MONTO CANC.</td>
						<td class="fondoAzul" width="70px">FECHA ALTA</td>
						<td class="fondoAzul" width="70px">FECHA BAJA</td>
						<td class="fondoAzul" width="70px">FECHA RENOV</td>
						<td class="fondoAzul" width="50px">EDITAR</td>
					</tr>
					<c:forEach items="${listDependiente}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idSolicitud}</td>
							<td class="fondoGris">${dependienteTO.idDependiente}</td>
							<td class="fondoGris">${dependienteTO.parentesco}</td>
							<td class="fondoGris">${dependienteTO.apellidoP} ${dependienteTO.apellidoM} ${dependienteTO.nombre}</td>
							<td class="fondoGris">${dependienteTO.fechaNac}</td>
							<td class="fondoGris">${dependienteTO.fechaElab}</td>
							<td class="fondoGris">${dependienteTO.tipoSol}</td>
							<td class="fondoGris">${dependienteTO.estatulSol}</td>
							<td class="fondoGris">${dependienteTO.costo}</td>
							<td class="fondoGris">${dependienteTO.costoAmpliacionInd}</td>
							<td class="fondoGris">${dependienteTO.costoCancelacion}</td>
							<td class="fondoGris">${dependienteTO.fechaAlta}</td>
							<td class="fondoGris">${dependienteTO.fechaBaja}</td>
							<td class="fondoGris">${dependienteTO.fechaRenov}</td>
							<td><input type="button" value="Editar" 
								onclick="editarDependiente(1,${dependienteTO.idDependiente},'${dependienteTO.idParentesco}','${dependienteTO.parentesco}',
								'${dependienteTO.apellidoP}','${dependienteTO.apellidoM}','${dependienteTO.nombre}',
								'${dependienteTO.fechaNac}','${dependienteTO.fechaAlta}','${dependienteTO.idEstatulSol}',
								'${dependienteTO.estatulSol}','${dependienteTO.sexo}',${solicitudAdminTO.idTipoEmpleado},
								${solicitudAdminTO.costo},${solicitudAdminTO.idSolicitud},${solicitudAdminTO.idCobertura},
								${dependienteTO.costo},${solicitudAdminTO.costoTotal});"></td>
						</tr>
					</c:forEach>
					<tr><td colspan="15">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">FOLIO DEP</td>
						<td class="fondoAzul" colspan="14">COMENTARIOS</td>
					</tr>
					<c:forEach items="${listDependiente}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idDependiente}</td>
							<td class="fondoGris" colspan="14">${dependienteTO.comentario}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="15">&nbsp;</td></tr>
					<tr><td colspan="15">&nbsp;</td></tr>
					<tr><td colspan="15">&nbsp;</td></tr>
				</table>
				
				<!-- CONSULMED -->
				
				<table class="principal">
					<tr><td colspan="13" align="center">Información General de sus Dependientes Consulmed</td></tr>
					<tr>
						<td class="fondoAzul" width="30px">FOLIO</td>
						<td class="fondoAzul" width="30px">FOLIO DEP</td>
						<td class="fondoAzul" width="50px">PARENTESCO</td>
						<td class="fondoAzul" width="180px">NOMBRE</td>
						<td class="fondoAzul" width="70px">FECHA NAC.</td>
						<td class="fondoAzul" width="70px">FECHA ELAB.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="50px">EST. SOL.</td>
						<td class="fondoAzul" width="50px">MONTO</td>
						<td class="fondoAzul" width="70px">FECHA ALTA</td>
						<td class="fondoAzul" width="70px">FECHA BAJA</td>
						<td class="fondoAzul" width="70px">FECHA RENOV</td>
						<td class="fondoAzul" width="50px">EDITAR</td>
					</tr>
					<c:forEach items="${listConsulmed}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idSolicitud}</td>
							<td class="fondoGris">${dependienteTO.idConsulmed}</td>
							<td class="fondoGris">${dependienteTO.parentesco}</td>
							<td class="fondoGris">${dependienteTO.apellidoP} ${dependienteTO.apellidoM} ${dependienteTO.nombre}</td>
							<td class="fondoGris">${dependienteTO.fechaNac}</td>
							<td class="fondoGris">${dependienteTO.fechaElab}</td>
							<td class="fondoGris">${dependienteTO.tipoSol}</td>
							<td class="fondoGris">${dependienteTO.estatulSol}</td>
							<td class="fondoGris">${dependienteTO.costo}</td>
							<td class="fondoGris">${dependienteTO.fechaAlta}</td>
							<td class="fondoGris">${dependienteTO.fechaBaja}</td>
							<td class="fondoGris">${dependienteTO.fechaRenov}</td>
							<td><input type="button" value="Editar" 
								onclick="editarDependiente(2,${dependienteTO.idConsulmed},'${dependienteTO.idParentesco}','${dependienteTO.parentesco}',
								'${dependienteTO.apellidoP}','${dependienteTO.apellidoM}','${dependienteTO.nombre}',
								'${dependienteTO.fechaNac}','${dependienteTO.fechaAlta}','${dependienteTO.idEstatulSol}',
								'${dependienteTO.estatulSol}','${dependienteTO.sexo}',${solicitudAdminTO.idTipoEmpleado},
								${solicitudAdminTO.costo},${solicitudAdminTO.idSolicitud},${solicitudAdminTO.idCobertura},
								${dependienteTO.costo},${solicitudAdminTO.costoTotal});"></td>
						</tr>
					</c:forEach>
					<tr><td colspan="13">&nbsp;</td></tr>
					<tr>
						<td class="fondoAzul">FOLIO DEP</td>
						<td class="fondoAzul" colspan="12">COMENTARIO</td>						
					</tr>
					<c:forEach items="${listConsulmed}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idConsulmed}</td>
							<td class="fondoGris" colspan="12">${dependienteTO.comentario}</td>
						</tr>
					</c:forEach>
					<tr><td colspan="15">&nbsp;</td></tr>
					<tr><td colspan="15">&nbsp;</td></tr>
					<tr><td colspan="15">&nbsp;</td></tr>
				</table>
				
				
				<!-- MEMORIAL -->
				<table class="principal">
					<tr><td colspan="13" align="center">Información General de sus Dependientes Memorial</td></tr>
					<tr>
						<td class="fondoAzul" width="30px">FOLIO</td>
						<td class="fondoAzul" width="30px">FOLIO MEM</td>
						<td class="fondoAzul" width="50px">PARENTESCO</td>
						<td class="fondoAzul" width="180px">NOMBRE</td>
						<td class="fondoAzul" width="70px">FECHA NAC.</td>
						<td class="fondoAzul" width="70px">FECHA ELAB.</td>
						<td class="fondoAzul" width="60px">TIPO SOL.</td>
						<td class="fondoAzul" width="50px">EST. SOL.</td>
						<td class="fondoAzul" width="50px">MONTO</td>
						<td class="fondoAzul" width="70px">FECHA ALTA</td>
						<td class="fondoAzul" width="70px">FECHA BAJA</td>
						<td class="fondoAzul" width="70px">FECHA RENOV</td>
						<td class="fondoAzul" width="50px">EDITAR</td>
					</tr>
					<c:forEach items="${listMemorial}" var="dependienteTO">
						<tr>
							<td class="fondoGris">${dependienteTO.idSolicitud}</td>
							<td class="fondoGris">${dependienteTO.idConsulmed}</td>
							<td class="fondoGris">${dependienteTO.parentesco}</td>
							<td class="fondoGris">${dependienteTO.apellidoP} ${dependienteTO.apellidoM} ${dependienteTO.nombre}</td>
							<td class="fondoGris">${dependienteTO.fechaNac}</td>
							<td class="fondoGris">${dependienteTO.fechaElab}</td>
							<td class="fondoGris">${dependienteTO.tipoSol}</td>
							<td class="fondoGris">${dependienteTO.estatulSol}</td>
							<td class="fondoGris">${dependienteTO.costo}</td>
							<td class="fondoGris">${dependienteTO.fechaAlta}</td>
							<td class="fondoGris">${dependienteTO.fechaBaja}</td>
							<td class="fondoGris">${dependienteTO.fechaRenov}</td>
							<td class="fondoGris"></td>
						</tr>
					</c:forEach>
					<tr><td colspan="13">&nbsp;</td></tr>
					<tr><td colspan="13">&nbsp;</td></tr>
					<tr><td colspan="13"><img src="imagenes/flecha.jpg" class="iconoHome" onclick="enviar('frmSolEdicion','consultaSolicitudAdmin.htm');">
						Regresar</td></tr>
				</table>
				
				
			</div>
		</form>
	</div>
	
</BODY>
</HTML>
