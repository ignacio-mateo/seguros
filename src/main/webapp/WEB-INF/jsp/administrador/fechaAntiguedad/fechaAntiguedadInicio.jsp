<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmFechaAntiguedad">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
									
			<div>&nbsp;</div>
				<table class="principal">
					<tr>
						<td class="fondoGris" colspan="9">FECHA DE ANTIGUEDAD</td>
					</tr>
					<tr><td colspan="9">&nbsp;</td></tr>
					<tr>
						<td>Num. Empleado:</td>
						<td><input type="text" id="numEmpSolicitado" name="numEmpSolicitado" value=""></td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="9">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="radio" id="onTitular" name="onSeleccion" value="1"></td>
						<td>Fecha de Elaboracion:</td>
						<td><input type="text" id="fechaElabTit" name="fechaElabTit" value=""></td>
						<td>&nbsp;</td>
						<td>Fecha de Alta:</td>
						<td><input type="text" id="fechaAltaTit" name="fechaAltaTit" value=""></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><input type="radio" id="onDependiente" name="onSeleccion" value="2"></td>
						<td>Fecha de Elaboracion:</td>
						<td><input type="text" id="fechaElabDep" name="fechaElabDep" value=""></td>
						<td>&nbsp;</td>
						<td>Fecha de Alta:</td>
						<td><input type="text" id="fechaAltaDep" name="fechaAltaDep" value=""></td>
						<td>&nbsp;</td>
						<td>Num. Sol. Dependiente:</td>
						<td><input type="text" id="idDep" name="idDep" value=""></td>
					</tr>
					<tr><td colspan="9">&nbsp;</td></tr>
					<tr><td colspan="9">&nbsp;</td></tr>
					<tr>
						<td colspan="4">&nbsp;</td>
						<td align="center" colspan="12" class="leyenda">${mensaje1}</td>
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr><td colspan="9">&nbsp;</td></tr>
					<tr><td colspan="9">&nbsp;</td></tr>
					<tr>
						<td colspan="8">&nbsp;</td>
						<td><input type="button" value="Actualizar" onclick="validarFechaAntiguedad();"></td>
					</tr>
				</table>	
			</form>
	</div>
	
</BODY>
</HTML>