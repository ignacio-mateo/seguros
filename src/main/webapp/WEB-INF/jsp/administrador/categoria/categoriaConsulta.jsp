<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">
		<form method="post" id="frmCategoria">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<form:hidden path="categoriaTotal.empTO.numEmp"/>
			<form:hidden path="categoriaTotal.empTO.nombreEmp"/>
			<form:hidden path="categoriaTotal.empTO.perfilEmp"/>
			
			<div>&nbsp;</div>
				<table class="principal">
					<tr><td class="fondoGris" colspan="12">CAMBIO DE CATEGORIA</td></tr>
					<tr><td align="center" colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12" class="leyenda">${mensaje1}</td></tr>
					<tr><td align="center" colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12">Informacion General del Titular</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA ALTA</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px">MONTO TOTAL</td>
					</tr>
					<tr>
						<td class="fondoGris">${categoriaActual.solicitudTO.idSol}</td>
						<td class="fondoGris">${categoriaActual.empleadoTO.numEmp}</td>
						<td class="fondoGris">${categoriaActual.empleadoTO.apellidoP} ${categoriaActual.empleadoTO.apellidoM} ${categoriaActual.empleadoTO.nombre}</td>
						<td class="fondoGris">${categoriaActual.empleadoTO.descTipoEmp}</td>
						<td class="fondoGris">${categoriaActual.empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.estatusSol}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.fechaAutorizacion}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.tipoCobertura}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.costoEmpleado}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.costoAmpliacion}</td>
						<td class="fondoGris">${categoriaActual.solicitudTO.costoTotal}</td>
					</tr>
					<c:forEach items="${categoriaActual.listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAltaAut}</td>
							<td class="fondoGris"></td>
							<td class="fondoGris">${dependiente.costoDep}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td align="center" colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12">Prorrateo</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA SEL.</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px">MONTO TOTAL</td>
					</tr>
					<tr>
						<td class="fondoGris">${categoriaTransit.solicitudTO.idSol}</td>
						<td class="fondoGris">${categoriaTransit.empleadoTO.numEmp}</td>
						<td class="fondoGris">${categoriaTransit.empleadoTO.apellidoP} ${categoriaTransit.empleadoTO.apellidoM} ${categoriaTransit.empleadoTO.nombre}</td>
						<td class="fondoGris">${categoriaTransit.empleadoTO.descTipoEmp}</td>
						<td class="fondoGris">${categoriaTransit.empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.estatusSol}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.fechaAutorizacion}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.tipoCobertura}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.costoEmpleado}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.costoAmpliacion}</td>
						<td class="fondoGris">${categoriaTransit.solicitudTO.costoTotal}</td>
					</tr>
					<c:forEach items="${categoriaTransit.listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAltaAut}</td>
							<td class="fondoGris"></td>
							<td class="fondoGris">${dependiente.costoDep}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td align="center" colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12">Cambio de Categoria</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA SEL.</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px">MONTO TOTAL</td>
					</tr>
					<tr>
						<td class="fondoGris">${categoriaNueva.solicitudTO.idSol}</td>
						<td class="fondoGris">${categoriaNueva.empleadoTO.numEmp}</td>
						<td class="fondoGris">${categoriaNueva.empleadoTO.apellidoP} ${categoriaNueva.empleadoTO.apellidoM} ${categoriaNueva.empleadoTO.nombre}</td>
						<td class="fondoGris">${categoriaNueva.empleadoTO.descTipoEmp}</td>
						<td class="fondoGris">${categoriaNueva.empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${categoriaNueva.solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${categoriaNueva.solicitudTO.estatusSol}</td>
						<td class="fondoGris">${categoriaNueva.solicitudTO.fechaElaboracion}</td>
						<td class="fondoGris">${categoriaNueva.solicitudTO.tipoCobertura}</td>
						<td class="fondoGris">${categoriaNueva.empleadoTO.montoEmpProrateado}</td>
						<td class="fondoGris">${categoriaNueva.solicitudTO.costoAmpliacion}</td>
						<td class="fondoGris">${categoriaNueva.solicitudTO.costoTotal}</td>
					</tr>
					<c:forEach items="${categoriaNueva.listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAlta}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.costoDep}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td align="center" colspan="12">Actualizacion de Datos</td></tr>
					<tr>
						<td class="fondoAzul" width="25px">FOLIO</td>
						<td class="fondoAzul" width="25px">EMP.</td>
						<td class="fondoAzul" width="100px">NOMBRE EMPLEADO</td>
						<td class="fondoAzul" width="40px">CATEGORIA</td>
						<td class="fondoAzul" width="80px">TIPO</td>
						<td class="fondoAzul" width="80px">TIPO SOL.</td>
						<td class="fondoAzul" width="80px">EST. SOL.</td>
						<td class="fondoAzul" width="80px">FECHA SEL.</td>
						<td class="fondoAzul" width="60px">TIPO POLIZA</td>
						<td class="fondoAzul" width="60px">MONTO</td>
						<td class="fondoAzul" width="60px">MONTO AMP.</td>
						<td class="fondoAzul" width="60px">MONTO TOTAL</td>
					</tr>
					<tr>
						<td class="fondoGris">${categoriaTotal.solicitudTO.idSol}<form:hidden path="categoriaTotal.solicitudTO.idSol"/></td>
						<td class="fondoGris">${categoriaTotal.empleadoTO.numEmp}<form:hidden path="categoriaTotal.empleadoTO.numEmp"/></td>
						<td class="fondoGris">${categoriaTotal.empleadoTO.apellidoP} ${categoriaTotal.empleadoTO.apellidoM} ${categoriaTotal.empleadoTO.nombre}</td>
						<td class="fondoGris">${categoriaTotal.empleadoTO.descTipoEmp}<form:hidden path="categoriaTotal.empleadoTO.tipoEmp"/></td>
						<td class="fondoGris">${categoriaTotal.empleadoTO.descEmpleado}</td>
						<td class="fondoGris">${categoriaTotal.solicitudTO.tipoSolicitud}</td>
						<td class="fondoGris">${categoriaTotal.solicitudTO.estatusSol}</td>
						<td class="fondoGris">${categoriaTotal.solicitudTO.fechaElaboracion}<form:hidden path="categoriaTotal.solicitudTO.fechaElaboracion"/></td>
						<td class="fondoGris">${categoriaTotal.solicitudTO.tipoCobertura}<form:hidden path="categoriaTotal.solicitudTO.idTipoCobertura"/></td>
						<td class="fondoGris">${categoriaTotal.empleadoTO.montoEmpProrateado}<form:hidden path="categoriaTotal.empleadoTO.montoEmpProrateado"/></td>
						<td class="fondoGris">${categoriaTotal.solicitudTO.costoAmpliacion}<form:hidden path="categoriaTotal.solicitudTO.costoAmpliacion"/>
									<form:hidden path="categoriaTotal.solicitudTO.costoDependiente"/>
									<form:hidden path="categoriaTotal.solicitudTO.costoTotalAnt"/></td>
						<td class="fondoGris">${categoriaTotal.solicitudTO.costoTotal}<form:hidden path="categoriaTotal.solicitudTO.costoTotal"/></td>
					</tr>
					<c:forEach items="${categoriaTotal.listDep}" var="dependiente" varStatus="status">
						<tr id="fila${status.index}">
							<td class="fondoGris"><input type="hidden" name="listDep[${status.index}].idDependiente" value="${dependiente.idDependiente}"></td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.apellidoP} ${dependiente.apellidoM} ${dependiente.nombre}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.parentesco}</td>
							<td class="fondoGris">${dependiente.tipoSol}</td>
							<td class="fondoGris">${dependiente.estatusDep}</td>
							<td class="fondoGris">${dependiente.fechaAlta}</td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">${dependiente.costoDep}<input type="hidden" name="listDep[${status.index}].costoDep" value="${dependiente.costoDep}"></td>
							<td class="fondoGris">&nbsp;</td>
							<td class="fondoGris">&nbsp;</td>
						</tr>
					</c:forEach>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr><td colspan="12">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="4"><img src="imagenes/flecha.jpg" title="Regresar" class="iconoHome" onclick="enviar('frmCategoria','inicioCambioCategoria.htm');"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="2"><input type="button" value="Actualizar" onclick="enviar('frmCategoria','actualizaCambioCategoria.htm');"></td>
					</tr>
				</table>	
			</form>
	</div>
	
</BODY>
</HTML>