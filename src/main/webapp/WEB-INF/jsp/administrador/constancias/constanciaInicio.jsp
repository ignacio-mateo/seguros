<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML>
<HEAD>
	<%@include file="../../include/tagsCss.jsp"%>
</HEAD>

<BODY>

	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>
	 		
 	<form method="post" id="frmConsultaConstancia">	
		<input type="hidden" name="numEmp" value="${empTO.numEmp}">
		<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
		<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			
		<div class="cuerpo">
			<table class="principal">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
				</tr>
				<tr><td class="encabezado" colspan="8">CONSTANCIAS DE GASTOS MEDICOS</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Num. Empleado: </td>
					<td><input type="text" name="numEmpSolicitado" id="numEmpSolicitado"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Tipo de Constancia: </td>
					<td><select name="tipoConstancia" id="tipoConstancia">
							<option value="0">Seleccione una opcion</option>
							<option value="1">Actual</option>
							<option value="2">Finiquito</option>
						</select>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Fecha de Finiquito:</td>
					<td><div id="fechaFiniquito"><input type="text" name="fecha" id="fecha"></div></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Comentarios:</td>
					<td colspan="5"><textarea rows="5" cols="60" maxlength="100" name="comentario" id="comentario"></textarea></td>
				</tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td align="center" colspan="8" class="leyenda">${mensaje1}</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2"><input type="button" value="Generar Constancia" class="inputTextNombre"
						onclick="validaConstancia('frmConsultaConstancia','reporteActualGeneraPdf.htm');"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
 	</form>
 		 		
</BODY>
</HTML>
