<!DOCTYPE HTML>
<%@include file="../../include/tagsLibs.jsp"%>

<HTML>
<HEAD>
	<%@include file="../../include/tagsCss.jsp"%>
</HEAD>

<BODY>
	
	<%@include file="../../estructura/encabezado.jsp"%>
	<%@include file="../../estructura/titulo.jsp"%>
	<%@include file="../../estructura/menuAdmin.jsp"%>
	<%@include file="../../estructura/piepagina.jsp"%>

	
	<div class="cuerpo">
		
		<div class='tituloDos'>CONSTANCIAS DE GASTOS MEDICOS</div>
			
			
			
			<div id="areaImpresion">
			
				<div class='filaTabla'>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'>04 Noviembre, 2013</div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>A quien corresponda:</div>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
				</div>		
				<div class='filaTabla'>
					<div class='celdaTabla'>
					A solicitud de ${constanciaTO.apellidoP} ${constanciaTO.apellidoM} ${constanciaTO.nombreAsegurado}, se expide la presente certificando 
			que se encontra amparado hasta el 10 de Octubre de 2013, en la poliza de Seguro de Gastos Medicos Mayores teniendo las siguientes coberturas:
					</div>
				</div>				
				<div class='filaTabla'>
					<div class='celdaTabla'>Aseguradora:</div>
					<div class='celdaTabla'>${constanciaTO.nombreAseguradora}</div>
				</div>		
				<div class='filaTabla'>
					<div class='celdaTabla'>Domicilio:</div>
					<div class='celdaTabla'>${constanciaTO.calle} ${constanciaTO.numero} ${constanciaTO.colonia} ${constanciaTO.cp} ${constanciaTO.ciudad}</div>
				</div>		
				<div class='filaTabla'>
					<div class='celdaTabla'>RFC:</div>
					<div class='celdaTabla'>${constanciaTO.rfc}</div>
				</div>		
				<div class='filaTabla'>
					<div class='celdaTabla'>Poliza:	</div>
					<div class='celdaTabla'></div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>Certificado No.:</div>
					<div class='celdaTabla'></div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>Vigencia:</div>
					<div class='celdaTabla'></div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>Fecha de Alta:</div>
					<div class='celdaTabla'></div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>Suma Asegurada:</div>
					<div class='celdaTabla'>${constanciaTO.sumaAsegurada} M.N. por padecimiento, excepto Ces�rea y Parto Natural</div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>Deducible:</div>
					<div class='celdaTabla'>${constanciaTO.costoDeducible}</div>
				</div>
				<div class='filaTabla'>
					<div class='celdaTabla'>Coaseguro: </div>
					<div class='celdaTabla'> 10%, Excepto en Enfermedades de Nariz, aplica 30%</div>
				</div>
			    <div class='filaTabla'>
					<div class='celdaTabla'>Cesarea: </div>
					<div class='celdaTabla'>${constanciaTO.costoCesarea}</div>
				</div>                				               
			    <div class='filaTabla'>
					<div class='celdaTabla'>Parto Natural:</div>
					<div class='celdaTabla'>${constanciaTO.costoParto}</div>
				</div>        	
				<div class='filaTabla'>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
				</div>  
				<div class='filaTabla'>
					<div class='celdaTabla'>Titular:</div>
					<div class='celdaTabla'>${constanciaTO.apellidoP} ${constanciaTO.apellidoM} ${constanciaTO.nombreAsegurado} (${constanciaTO.nombreCobertura})</div>
				</div>   
			    <div class='filaTabla'>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
				</div>       
			    <div class='filaTabla'>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
				</div>    
			    <div class='filaTabla'>
					<div class='celdaTabla'>Sin otro particular, me encuentro a sus �rdenes para cualquier duda � aclaraci�n al respecto.</div>
				</div>                      
				<div class='filaTabla'>
					<div class='celdaTabla'>A t e n t a m e n t e,</div>
				</div>    
				<div class='filaTabla'>
					<div class='celdaTabla'></div>
					<div class='celdaTabla'></div>
				</div>    
				<div class='filaTabla'>
					<div class='celdaTabla'>Nombre ApellidoPaterno ApellidoMaterno</div>
					<div class='celdaTabla'></div>
				</div>    
				<div class='filaTabla'>
					<div class='celdaTabla'>Seguros y Fianzas.</div>
					<div class='celdaTabla'></div>
				</div>   
			</div>
			 
			 
		<input type="button" id="imprimir" value="Imprimir Constancia">	 
			 
	</div>

	
 		
</BODY>

</HTML>
