<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	<%@include file="../estructura/estrucEncabezadoCircular.jsp"%>
	
	<form:form method="post" id="frmGuardaCircular" modelAttribute="circularForm">	
		<form:hidden path="empleadoTO.numEmp"/>
		<form:hidden path="datosFiscales.rfc"/>
		<form:hidden path="solicitudTO.costoTotalAct" id="cta"/>
		<form:hidden path="solicitudTO.costoAmpliacion" id="ca"/>
		<form:hidden path="solicitudTO.costoTotal" id="ct"/>
		<form:hidden path="numDep" id="numDep"/>
					
	 	<div class="cuerpo">
			<table class="principal">
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<%@include file="renovacionDatos.jsp"%>	
			</table>
		</div>
	</form:form>
 	
</BODY>
</HTML>