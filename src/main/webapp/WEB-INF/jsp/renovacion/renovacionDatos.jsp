	
	<!-- ENCABEZADO -->
	<tr><td class="encabezado" colspan="6">RENOVACION SEGURO DE GASTOS M�DICOS MAYORES</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr>
		<td class="columna0">Nombre:</td>
		<td class="columna0" colspan="3"><form:input class="inputTextNombre" readonly="true" path="empleadoTO.nombre"/></td>
		<td class="columna0">Num. Emp:</td>
		<td class="columna0"><form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.numEmp" id="numEmp"/></td>
	</tr>
	<tr>
		<td class="columna0">&nbsp;</td>
		<td class="columna0">&nbsp;</td>
		<td class="columna0">&nbsp;</td>
		<td class="columna0">&nbsp;</td>
		<td class="columna0">Region:</td>
		<td class="columna0"><form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.region" id="region"/></td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	


	<!-- DATOS FISCALES -->
	<tr><td class="encabezado" colspan="6">Datos Fiscales</td></tr>
	<tr><td class="leyendaCentrada" colspan="6">Favor de llenar y/o corregir sus datos fiscales del siguiente formulario.</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		<td>Calle:</td>
		<td colspan="3"><form:input path="datosFiscales.calle" class="inputTextNombre" id="calle" maxlength="48"/></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Numero:</td>
		<td><form:input path="datosFiscales.numero" id="numero" maxlength="18"/></td>
		<td>Codigo Postal:</td>
		<td><form:input path="datosFiscales.cp" id="cp" maxlength="5"/></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Colonia:</td>
		<td colspan="3"><form:input path="datosFiscales.colonia" class="inputTextNombre" id="colonia" maxlength="48"/></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Delegacion o Municipio:</td>
		<td colspan="3"><form:input path="datosFiscales.deleg_mun" class="inputTextNombre" id="deleg_mun" maxlength="48"/></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Entidad Federativa:</td>
		<td colspan="3"><form:input path="datosFiscales.entidad" class="inputTextNombre" id="entidad" maxlength="38"/></td>
		<td>&nbsp;</td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				
	
	
	<!-- BENEFICIARIOS -->
	<tr><td class="encabezado" colspan="6">Beneficiarios</td></tr>
	<tr>
		<td class="columna0" colspan="6">De acuerdo a nuestros archivos tiene usted dentro del Seguro de Gastos Medicos a sus siguientes beneficiarios: </td>
	</tr>
	<tr>
		<td class="columna0" colspan="3">Nombre</td>
		<td class="columna0">Parentesco</td>
		<td class="columna0">Fecha Nacimiento</td>
		<td class="columna0">Costo</td>
	</tr>
	<tr>
		<td class="columna0" colspan="3"><form:input class="inputTextNombre" readonly="true" path="empleadoTO.nombre"/></td>
		<td class="columna0"><form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.descEmpleado"/></td>
		<td class="columna0"><form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.fechaNacF"/></td>
		<td class="columna0"><form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.montoEmpProrateado"/></td>
	</tr>
	<c:forEach items="${circularForm.listDep}" var="dependiente" varStatus="status">
	<tr id="fila${status.index}">
		<td class="columna0" colspan="3"><input type="text" class="inputTextNombre" value="${dependiente.nombre} ${dependiente.apellidoP} ${dependiente.apellidoM}" readonly="readonly"></td>
		<td class="columna0"><input type="text" class="inputTextNombreCenter" value="${dependiente.parentesco}" readonly="readonly"></td>
		<td class="columna0"><input type="text" class="inputTextNombreCenter" value="${dependiente.fechaAlta}" readonly="readonly"></td>
		<td class="columna0"><input type="text" class="inputTextNombreCenter" value="${dependiente.costoDep}" readonly="readonly"></td>
	</tr>
	</c:forEach>
	<tr>
    	<td class="columna0" colspan="4">&nbsp;</td>
        <td class="columna0">Tipo de Ampliacion:</td>
        <td class="columna0"><form:input class="leyendaCentrada" readonly="true" id="descTipoCobertura" path="solicitudTO.tipoCobertura"/></td>
	</tr>
	<tr>
		<td class="columna0" colspan="4">&nbsp;</td>
		<td class="columna0">Total:</td>
		<td class="columna0"><form:input readonly="true" path="solicitudTO.costoTotalAct" id="costoTotalAct" class="leyendaCentrada"/></td>
	</tr>
	<tr>
		<td class="columna0" colspan="4">&nbsp;</td>
		<td class="columna0">Total Ampliacion:</td>
		<td class="columna0"><form:input readonly="true" path="solicitudTO.costoAmpliacion" id="costoAmpliacion" class="leyendaCentrada"/></td>
	</tr>
	<tr>
		<td class="columna0" colspan="4">&nbsp;</td>
		<td class="columna0">Suma Total:</td>
		<td class="columna0"><form:input readonly="true" path="solicitudTO.costoTotal" id="costoTotal" class="leyendaCentrada"/></td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	


	<!-- LEYENDA -->
	<tr>
		<td class="columna0" colspan="6">
			<span class="leyendaCircular">*La Suma Total es el importe Anual de la p�liza de GMM. 
				El importe a descontar quincenalmente es determinado por el �rea de N�minas.</span>
		</td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>	
	
	
	
	<!-- CONSULMED -->
	<tr>
		<td class="columna0" colspan="6">Familiares adicionales dentro de la Membresia CONSULMED: </td>
	</tr>
	<tr>
		<td class="columna0" colspan="3">Nombre</td>
		<td class="columna0" colspan="2">Parentesco</td>
		<td class="columna0">Fecha Nacimiento</td>
	</tr>
	<c:forEach items="${circularForm.listCon}" var="dependiente" varStatus="status">
	<tr id="fila${status.index}">
		<td class="columna0" colspan="3"><input type="text" class="inputTextNombre" value="${dependiente.nombre} ${dependiente.apellidoP} ${dependiente.apellidoM}" readonly="readonly"></td>
		<td class="columna0" colspan="2"><input type="text" class="inputTextNombreCenter" value="${dependiente.parentesco}" readonly="readonly"></td>
		<td class="columna0"><input type="text" class="inputTextNombreCenter" value="${dependiente.fechaNac}" readonly="readonly"></td>
	</tr>
	</c:forEach>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	
				
				
	
	<!-- RENOVACION -->
	<tr><td class="encabezado" colspan="6">Renovacion</td></tr>
	<tr>
		<td class="columna0" colspan="6">
			<span class="leyendaCircular">Usted tiene hasta el 13 de junio para enviar su seleccion y en caso de no recibir respuesta de su seguro sera renovado automaticamente</span>
		</td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">[ RENOVACION ]</td></tr>
	<tr>
		<td class="columna0" colspan="6"><form:radiobutton path="estatusRenov" id="siRenov" value="1"/><span class="leyendaAzul">SI</span> deseo renovar</td>
	</tr>
	<tr>
		<td class="columna0" colspan="6"><form:radiobutton path="estatusRenov" id="noRenov" value="2"/><span class="leyenda">NO</span> deseo renovar</td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">[ AMPLIACION ]</td></tr>
	<tr>
		<td class="columna0" colspan="6">
			<form:radiobutton path="estatusAmpl" id="siA1" value="1" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A1]</span> de mi Suma Asegurada, 2039 UMAM*
		</td>
	</tr>
	<tr>
		<td class="columna0" colspan="6">
			<form:radiobutton path="estatusAmpl" id="siA2N" value="2" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A2 N]</span> de mi Suma Asegurada, 4263 UMAM*</td>
	</tr>
	<tr>
		<td class="columna0" colspan="6">
			<form:radiobutton path="estatusAmpl" id="siA2E" value="3" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A2 E]</span> de mi Suma Asegurada, 4263 UMAM*</td>
	</tr>
	<tr>
    	<td class="columna0" colspan="6">
        	<form:radiobutton path="estatusAmpl" id="siA3N" value="4" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A3 N]</span> de mi Suma Asegurada, 6774 UMAM*</td>
	</tr>
    <tr>
		<td class="columna0" colspan="6">
			<form:radiobutton path="estatusAmpl" id="siA3E" value="5" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A3 E]</span> de mi Suma Asegurada, 6774 UMAM*
		</td>
	</tr>
    <tr>
    	<td class="columna0" colspan="6">
        	<form:radiobutton path="estatusAmpl" id="siA4N" value="6" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A4 N]</span> de mi Suma Asegurada, 11409 UMAM*</td>
	</tr>
    <tr>
		<td class="columna0" colspan="6">
			<form:radiobutton path="estatusAmpl" id="siA4E" value="7" disabled="true"/>Solicito Ampliaci�n <span class="leyenda">[A4 E]</span> de mi Suma Asegurada, 11409 UMAM*</td>
	</tr>
	<tr>
		<td class="columna0" colspan="6">
			<form:radiobutton path="estatusAmpl" id="noA" value="0" disabled="true"/><span class="leyenda">NO</span> deseo contratar Ampliaci�n
		</td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr>
		<td class="columna0" colspan="6">
			<input type="button" value="Enviar respuesta" onclick="validarDatosCircular('frmGuardaCircular','guardarCircular.htm');">
		</td>
	</tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>	
	
		
		

	<!-- NOTAS -->
	<tr><td class="encabezado" colspan="6">Notas Importantes</td></tr>
	<tr>
		<td class="columnaNotas" colspan="6">
			<ul>
				<li>Para esta vigencia se podr� optar por siete Ampliaciones de Suma Asegurada:</li>
				<li>&nbsp;</li>
				<li><span class="leyenda">A1</span> que se incrementa en 2,039 UMAM* (5.2 Millones de pesos M.N. aprox.)</li>
				<li><span class="leyenda">A2 N</span> que se incrementa en 4,263 UMAM* (10.9 Millones de pesos M.N. aprox.)</li>
				<li><span class="leyenda">A2 E</span> que se incrementa en 4,263 UMAM* (10.9 Millones de pesos M.N. aprox.)</li>
				<li><span class="leyenda">A3 N</span> que se incrementa en 6,774 UMAM* (17.4 Millones de pesos M.N. aprox.)</li>
				<li><span class="leyenda">A3 E</span> que se incrementa en 6,774 UMAM* (17.4 Millones de pesos M.N. aprox.)</li>
				<li><span class="leyenda">A4 N</span> que se incrementa en 11,409 UMAM* (29.3 Millones de pesos M.N. aprox.)</li>
				<li><span class="leyenda">A4 E</span> que se incrementa en 11,409 UMAM* (29.3 Millones de pesos M.N. aprox.)</li>
				<li>&nbsp;</li>
				<li>La Suma Asegurada de esta nueva p�liza es de 1,035 UMAM*, con un Deducible de 2 UMAM*, y Coaseguro del 10%</li>
				<li>&nbsp;</li>
				<li>Para el Caso de Ces�rea y Parto se cobrar� Deducible y Coaseguro con Carta Pase y/o Pago Directo. (De acuerdo a las condiciones de la p�liza).</li>
				<li>&nbsp;</li>
				<li>Suma Asegurada para Ces�rea 30 UMAM*, menos Deducible y Coaseguro.</li>
				<li>&nbsp;</li>
				<li>Suma Asegurada para Parto Natural 25 UMAM*, menos Deducible y Coaseguro.</li>
				<li>&nbsp;</li>
				<li>Para Cirug�as programadas de Nariz, no aplicar� el uso de Carta Pase o Pago Directo, deber� tramitarse 
					v�a Reembolso y se aplicar� el Deducible de 2 UMAM* y Coaseguro del 30%. En el caso de Accidentes que 
					requieran intervenci�n inmediata, aplicar� el Pago Directo a trav�s de Seguros Inbursa, S.A.</li>	
				<li>&nbsp;</li>	
				<li>*UMAM = Unidad de Medida y Actualizacion Mensual</li>
			</ul>
		</td>
	</tr>		
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	
	
	
	<!-- CONTACTOS -->
	<tr><td class="encabezado" colspan="6">Contactos</td></tr>
	<tr>
		<td class="columnaNotas" colspan="6">
			<ul>
				<li>&nbsp;</li>
				<li>Para cualquier duda o aclaraci�n favor de comunicarse con el �rea de Seguros y Fianzas a las extensiones 3543,5192,6835,4526,2629 y 1924.</li>
				<li>El descuento del importe de este Seguro se iniciar� en la primer quincena de Julio del 2019.</li>
				<li>&nbsp;</li>
				<li>&nbsp;</li>
				<li><span class="leyendaAzul">Atentamente,</span></li>
				<li>&nbsp;</li>
				<li><span class="leyendaAzul">Administraci�n de Seguros y Fianzas.</span></li>
			</ul>
		</td>
	</tr>	
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
	<tr><td class="columna0" colspan="6">&nbsp;</td></tr>		