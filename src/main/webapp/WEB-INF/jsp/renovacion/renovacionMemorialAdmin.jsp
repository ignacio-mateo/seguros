<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="/WEB-INF/jsp/include/tagsCss.jsp"%></HEAD>

<BODY>
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
		
	<form:form method="post" modelAttribute="circularForm" id="frmGuardaMemorial">
		<form:hidden path="empTO.numEmp"/>
		<form:hidden path="empTO.nombreEmp"/>
		<form:hidden path="empTO.perfilEmp"/>
		<form:hidden path="solicitudTO.idSol"/>
						
		<div class="cuerpo">
			<table class="principal">
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				
				<!-- ENCABEZADO -->
				<tr><td class="encabezado" colspan="6">CONTRATACION DE GASTOS FUNERARIOS</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr>
					<td class="columna0">Nombre:</td>
					<td class="columna0" colspan="3">
						<form:input class="inputTextNombre" readonly="true" path="empleadoTO.nombre"/>
					</td>
					<td class="columna0">Num. Emp:</td>
					<td class="columna0">
						<form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.numEmp" id="numEmp"/>
					</td>
				</tr>
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">Region:</td>
					<td class="columna0">
						<form:input class="inputTextNombreCenter" readonly="true" path="empleadoTO.region" id="region"/>
					</td>
				</tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				
				
				
				<!-- SELECCION -->
				<tr><td class="columna0" colspan="6">[ SELECCION PAQUETE ]</td></tr>
				<tr>
					<td class="columna0" colspan="6">
						<form:radiobutton path="estatusRenov" id="siRenov" value="2"/>
						<span class="leyendaAzul">Paquete A</span> 1-2 Dependientes con costo de $416
					</td>
				</tr>
				<tr>
					<td class="columna0" colspan="6">
						<form:radiobutton path="estatusRenov" id="noRenov" value="3"/>
						<span class="leyendaAzul">Paquete B</span> 3-4 Dependientes con costo de $764
					</td>
				</tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr>
					<td class="columna0" colspan="6">
						<span class="leyendaCircular">Usted tiene hasta el 10 de julio para enviar su seleccion </span>
					</td>
				</tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				
									
					
				<!-- CONSULMED -->
				<tr>
					<td class="columna0" colspan="6">Familiares dentro de la Membresia CONSULMED que pueden ser agregados: </td>
				</tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr>
					<td class="columna0">Nombre</td>
					<td class="columna0">A.Paterno</td>
					<td class="columna0">A.Materno</td>
					<td class="columna0">Parentesco</td>
					<td class="columna0">Fecha Nacimiento</td>
					<td class="columna0">Seleccion</td>
				</tr>

				<!-- ACTUALES -->		
				<c:forEach items="${circularForm.listConAlta}" var="dependiente" varStatus="status">
				<tr id="fila${status.index}">
                    <td class="columna0">
                    	<input type="hidden" name="listConAlta[${status.index}].sexo" value="${dependiente.sexo}">
                    	<input type="hidden" name="listConAlta[${status.index}].idParentesco" value="${dependiente.idParentesco}">
                    	<input type="text" class="inputTextNombreCenter" name="listConAlta[${status.index}].nombre" 
                    								value="${dependiente.nombre}" readonly="readonly">
                    </td>
					<td class="columna0">
						<input type="text" class="inputTextNombreCenter" name="listConAlta[${status.index}].apellidoP" 
													value="${dependiente.apellidoP}" readonly="readonly">
					</td>
					<td class="columna0">
						<input type="text" class="inputTextNombreCenter" name="listConAlta[${status.index}].apellidoM" 
													value="${dependiente.apellidoM}" readonly="readonly">
					</td>
					<td class="columna0">
						<input type="text" class="inputTextNombreCenter" value="${dependiente.parentesco}" readonly="readonly">
					</td>
					<td class="columna0">
						<input type="text" class="inputTextNombreCenter" name="listConAlta[${status.index}].fechaNac" 
							id="fechaDepAlta${status.index}" value="${dependiente.fechaNac}" readonly="readonly">
					</td>
					<td>
						<input type='checkbox' class="columna1" name="listConAlta[${status.index}].idConsulmed" 
							id="checkConAlta${status.index}" value="${dependiente.idConsulmed}" onchange="validarCheckedMemorialAlta('${status.index}');">
					</td>
				</tr>
				</c:forEach>

				<!-- VACIOS -->
				<c:forEach items="${circularForm.listCon}" var="dependiente" varStatus="status">
				<tr id="fila${status.index}">
					<td class="columna0">
						<input type="hidden" name="listCon[${status.index}].sexo" value="${dependiente.sexo}">
					    <input type="hidden" name="listCon[${status.index}].idParentesco" value="${dependiente.idParentesco}">
						<input type="text" class="inputTextNombreCenter" name="listCon[${status.index}].nombre" 
											id="nombreCon${status.index}" value="">
					</td>
                    <td class="columna0">
                    	<input type="text" class="inputTextNombreCenter" name="listCon[${status.index}].apellidoP" 
                    						id="paternoCon${status.index}" value="">
                    </td>
                    <td class="columna0">
                    	<input type="text" class="inputTextNombreCenter" name="listCon[${status.index}].apellidoM" 
                    						id="maternoCon${status.index}" value="">
                    </td>
					<td class="columna0">
					   
					    <input type="text" class="inputTextNombreCenter" value="${dependiente.parentesco}" readonly="readonly">
					</td>
					<td class="columna0">
						<input type="text" class="inputTextNombreCenter" name="listCon[${status.index}].fechaNac" 
								id="fechaDep${status.index}" onchange="validarFechaMemorial('${status.index}');" value="">
					</td>
					<td>
						<input type='checkbox' class="columna1" name="listCon[${status.index}].idConsulmed" 
								id="checkCon${status.index}" onchange="validarCheckedMemorial('${status.index}');" value="${status.index}">
					</td>
				</tr>
				</c:forEach>


				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>	
				
				
				<!-- BOTON RESPUESTA -->
				<tr>
					<td class="columna0" colspan="6">
					<input type="button" value="Enviar respuesta" onclick="enviarMemorial('frmGuardaMemorial','guardarMemorial.htm');">
				</td>
				</tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="6">&nbsp;</td></tr>	
			</table>
		</div>
	</form:form>
</BODY>
</HTML>
