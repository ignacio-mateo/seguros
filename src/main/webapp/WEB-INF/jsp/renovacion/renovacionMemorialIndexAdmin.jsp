<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="/WEB-INF/jsp/include/tagsCss.jsp"%></HEAD>

<BODY>

	<jsp:include page="/WEB-INF/jsp/estructura/encabezado.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/jsp/estructura/titulo.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/jsp/estructura/menuAdmin.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/jsp/estructura/piepaginaA.jsp"></jsp:include>
	
	<div class="registroRenov">
		<form action="getContratacionMemorialAdmin.htm" method="post">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="parametro" value="1">
						
			<table class="principal">
					<tr>
						<td colspan="2">CONTRATACION GASTOS FUNERARIOS</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Numero de Empleado:</td>
						<td><input type="text" name="numEmpSolicitado" value=""></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="Buscar"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td></td>
						<td>${mensaje1}</td>
					</tr>
				</table>				
		</form>
	</div>
	
</BODY>
</HTML>