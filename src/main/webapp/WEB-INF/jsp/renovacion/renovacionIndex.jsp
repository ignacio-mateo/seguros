<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="/WEB-INF/jsp/include/tagsCss.jsp"%></HEAD>
<link rel="stylesheet" type="text/css" href="estilos/loginRenovacion.css"/>

<BODY>

	<jsp:include page="/WEB-INF/jsp/estructura/estrucEncabezadoCircular.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/jsp/estructura/piepaginaA.jsp"></jsp:include>
	
	<div class="registroRenov">
		<form action="loginCircular.htm" method="post">
			<fieldset>
				<legend> Acceso a la Renovacion de SGMM </legend>
				<div class="medidas">
					<label for="user">No. Empleado:</label>
					<input type="text" name="numEmp" id="numEmp"> 
				</div>
				<div class="medidas">
					<label for="passwd"> Contrase&ntilde;a: </label>
					<input type="password" name="passwd" id="passwd">
				</div>
				<div>&nbsp;</div>
				<div>&nbsp;</div>
				<div class="medidas">
					<p><input type="submit" name="login" value="Ingresar" id="login"></p>	
				</div>
				<div>&nbsp;</div>
				<div class="leyenda">${mensaje1}</div>
			</fieldset>
		</form>
	</div>
	
</BODY>
</HTML>