<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML>
<HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepaginaA.jsp"%>
	
	<form method="post" id="frmRegistroDatosFiscales">
		<input type="hidden" name="nombre" value="${empTO.nombreEmp}">
		<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
		<input type="hidden" name="num_empleado" value="${datFiscalesTO.num_empleado}">
		<input type="hidden" name="rfc" value="${datFiscalesTO.rfc}">	
		<input type="hidden" value="${datFiscalesTO.nombre}">
		
		<div class="cuerpo">
			<table class="principal">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
				</tr>
				<tr><td class="encabezado" colspan="8">VALIDACION DE DATOS FISCALES</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Num. de Empleado:</td>
					<td class="soloLectura">${datFiscalesTO.num_empleado}</td>
					<td>RFC:</td>
					<td class="soloLectura">${datFiscalesTO.rfc}</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Nombre: </td>
					<td colspan="3" class="soloLectura">${datFiscalesTO.nombre}</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td colspan="8">&nbsp;</td></tr>
				<tr><td colspan="8" class="encabezado">DATOS FISCALES</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="4">Favor de llenar y/o corregir sus datos fiscales del siguiente formulario.</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Calle:</td>
					<td colspan="3"><input type='text' class="inputTextNombre" id="calle" name="calle" value="${datFiscalesTO.calle}" maxlength="48"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Numero:</td>
					<td><input type='text' id="numero" name="numero" value="${datFiscalesTO.numero}" maxlength="18"></td>
					<td>Codigo Postal:</td>
					<td><input type='text' id="cp" name="cp" value="${datFiscalesTO.cp}" maxlength="5"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Colonia:</td>
					<td colspan="3"><input type='text' id="colonia" name="colonia" class="inputTextNombre" value="${datFiscalesTO.colonia}" maxlength="48"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Delegacion o Municipio:</td>
					<td colspan="3"><input type='text' id="deleg_mun" name="deleg_mun" class="inputTextNombre" value="${datFiscalesTO.deleg_mun}" maxlength="48"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Entidad Federativa:</td>
					<td colspan="3"><input type='text' id="entidad" name="entidad" class="inputTextNombre" value="${datFiscalesTO.entidad}" maxlength="38"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td class="columna0" colspan="8">&nbsp;</td></tr>
				<tr><td class="columna0" colspan="8">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2"><input type="button" value="Actualizar Datos" class="inputTextNombre" onclick="validarDatosFiscales();"></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
 	</form>
 		
</BODY>
</HTML>
