<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<script type="text/javascript" src="jscript/jquery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="jscript/sgmm/sgmm.js"></script>
<script type="text/javascript" src="jscript/menu/menu.js"></script>
<script type="text/javascript" src="jscript/imprimir/jquery.PrintArea.js"></script>
<script type="text/javascript" src="jscript/imprimir/imprimir.js"></script>
<script type="text/javascript" src="jscript/datepicker/datepicker.js"></script>
<script type="text/javascript" src="jscript/datepicker/datepicker.es-ES.js"></script>

<link rel="stylesheet" type="text/css" href="estilos/general.css"/>
<link rel="stylesheet" type="text/css" href="estilos/menu.css"/>
<link rel="stylesheet" type="text/css" href="estilos/iconos.css"/>
<link rel="stylesheet" type="text/css" href="estilos/solicitud.css"/>
<link rel="stylesheet" type="text/css" href="estilos/mensajes.css"/>
<link rel="stylesheet" type="text/css" href="estilos/columnas.css"/>
<link rel="stylesheet" type="text/css" href="estilos/datepicker.css"/>