<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<form:form method="post" id="frmGuardarConsulmed" modelAttribute="solicitudForm">	
		<div class="cuerpo" >
			<!-- OBJETOS HIDDEN -->
			<form:hidden path="empleadoTO.numEmp"/>
			<form:hidden path="solicitudTO.idSol"/>
			<form:hidden path="empTO.numEmp"/>
			<form:hidden path="empTO.nombreEmp"/>
			<form:hidden path="empTO.perfilEmp"/>
			
			<table class="principal">
				<jsp:include page="../estructura/salto.jsp"/>
			
				<!-- ENCABEZADO -->
				<%@include file="../estructura/estrucTituloFechaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
			
				<!-- DATOS DEL EMPLEADO -->
				<%@include file="../estructura/estrucEmpleadoForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
			
				<!--	DEPENDIENTES CONSULMED	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteForm.jsp"/>
				<%@include file="../estructura/estrucSolConsulmedConsultaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
			
				<!--	DEPENDIENTES CONSULMED	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteForm.jsp"/>
				<%@include file="../estructura/estrucSolConsulmedAltaForm.jsp"%>	
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
			
				<!--	BOTON GUARDAR	-->
				<tr>
					<td colspan="6">&nbsp;</td>
					<td colspan="2"><input type="button" class="boton" value="Registrar Solicitud" 
						onclick="guardarConsulmed('frmGuardarConsulmed','guardarConsulmed.htm');">
					</td>
				</tr>
				
			</table>
		</div>
	</form:form>
			
</BODY>
</HTML>
