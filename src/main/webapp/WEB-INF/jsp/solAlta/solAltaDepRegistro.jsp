<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>

	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	
	<form:form method="post" id="frmGuardaDependiente" modelAttribute="solicitudForm">	
		<div class="cuerpo" >		
			<!-- OBJETOS HIDDEN -->
			<form:hidden path="empleadoTO.tipoEmp" id="tipoEmp"/>
			<form:hidden path="empleadoTO.idSexo"/>
			<form:hidden path="solicitudTO.tipoSolicitud"/>
			<form:hidden path="solicitudTO.idSol"/>
			<form:hidden path="empTO.numEmp"/>
			<form:hidden path="empTO.nombreEmp"/>
			<form:hidden path="empTO.perfilEmp"/>
			<input type="hidden" id="existeConyuge" name="existeConyuge" value="${existeConyuge}">
			
			<table class="principal">
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!-- ENCABEZADO -->
				<%@include file="../estructura/estrucTituloFechaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!-- DATOS DEL EMPLEADO -->
				<%@include file="../estructura/estrucEmpleadoForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!--	DEPENDIENTES	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteForm.jsp"/>
				<%@include file="../estructura/estrucSolDepenConsultaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!--	DEPENDIENTES	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteForm.jsp"/>
				<%@include file="../estructura/estrucSolDepenAltaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!--	BOTON GUARDAR	-->
				<tr>
					<td colspan="6">&nbsp;</td>
					<td colspan="2"><input type="button" class="boton" value="Registrar Solicitud" 
						onclick="guardarDependiente('frmGuardaDependiente','guardaDependiente.htm');">
					</td>
				</tr>
			</table>
		</div>
	</form:form>
	
</BODY>

</HTML>