<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	
	<form:form method="post" id="frmGuardaSolicitud" modelAttribute="solicitudForm">	
		<div class="cuerpo" >
			<!-- OBJETOS HIDDEN -->
			<form:hidden path="empleadoTO.tipoEmp" id="tipoEmp"/>
			<form:hidden path="empleadoTO.fechaIng" id="fechaIng"/>
			<form:hidden path="empleadoTO.fechaNac" id="fechaNac"/>
			<form:hidden path="empleadoTO.idSexo" id="idSexo"/>
			<form:hidden path="empleadoTO.idRegion" id="idRegion"/>
			<form:hidden path="empleadoTO.departamento" id="departamento"/>
			<form:hidden path="empleadoTO.centro" id="centro"/>
			<form:hidden path="solicitudTO.idTipoSolicitud" id="idTipoSolicitud"/>
			<form:hidden path="empTO.numEmp"/>
			<form:hidden path="empTO.nombreEmp"/>
			<form:hidden path="empTO.perfilEmp"/>
			<input type="hidden" id="existeConyuge" name="existeConyuge" value="${existeConyuge}">
			
			<table class="principal">
				<jsp:include page="../estructura/salto.jsp"/>
				<%@include file="../estructura/estrucTituloFechaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>

				<!-- 	DATOS DEL EMPLEADO -->
				<%@include file="../estructura/estrucEmpleadoForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>

				<!--	COSTO DEL EMPLEADO	-->
				<%@include file="../estructura/estrucCostoEmpForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>

				<!--	DEPENDIENTES	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteForm.jsp"/>
				<%@include file="../estructura/estrucSolDepenAltaForm.jsp"%>

				<!--	COSTOS	-->
				<%@include file="../estructura/estrucCostoDepForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>

				<!-- COSTOS DE AMPLIACION -->
				<%@include file="../estructura/estrucCostoAmpForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>

				<!--	DEPENDIENTES CONSULMED	-->
				<jsp:include page="../estructura/estrucColumnasSolDependienteForm.jsp"/>
				<%@include file="../estructura/estrucSolConsulmedAltaForm.jsp"%>	
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!--    DEPENDIENTES MEMORIAL    -->
				<jsp:include page="../estructura/estrucColumnasSolMemorialForm.jsp"/>
				<%@include file="../estructura/estrucSolMemorialAltaForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!-- COSTO TOTAL -->
				<%@include file="../estructura/estrucCostoTotalForm.jsp"%>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!-- BOTON GUARDAR -->
				<tr>
					<td colspan="6">&nbsp;</td>
					<td colspan="2"><input type="button" class="boton" value="Registrar Solicitud" 
						onclick="guardarSolicitud('frmGuardaSolicitud','guardaSolicitud.htm');">
				</td>
				</tr>
			</table>
		</div>
	</form:form>
		
</BODY>
</HTML>