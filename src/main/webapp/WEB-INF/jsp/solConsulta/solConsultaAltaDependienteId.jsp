<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepaginaA.jsp"%>

	<div class="cuerpo">
		<div>&nbsp;</div>
		<form method="post" id="frmSolAltaDependiente">
			<input type="hidden" name="numEmp" value="${empTO.numEmp}">
			<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
			<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
			<input type="hidden" name="idDependiente" value="">
			<table class="principal">
				<tr><td class="encabezado" colspan="8">CONSULTA LAS ALTAS DE LOS DEPENDIENTES</td></tr>
				<tr><td colspan="8" class="columna0">&nbsp;</td></tr>
				<tr><td colspan="8" class="columna0">${mensaje1}</td></tr>
				<c:forEach items="${listaDep}" var="dependiente">
				<tr>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">FOLIO ALTA:</td>
					<td class="columna0"><input type="text" value="${solicitudTO.idSol}" readonly="readonly"></td>
					<td class="columna0">FOLIO ADICION:</td>
					<td class="columna0"><input type="text" value="${dependiente}" readonly="readonly"></td>
					<td class="columna0"><input type="button" onclick="verSolAltaDependiente(${dependiente});" value="Ver Solicitud"></td>
					<td class="columna0">&nbsp;</td>
				</tr>
				</c:forEach> 
			</table>	
		</form>
	</div>
	 		
</BODY>
</HTML>
