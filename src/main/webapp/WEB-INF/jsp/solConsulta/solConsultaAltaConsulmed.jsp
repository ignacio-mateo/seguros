<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>

	<div class="cuerpo">
		<form method="post" id="frmConsultaAltaIdConsulmed">
		<input type="hidden" name="numEmp" value="${empTO.numEmp}">
		<input type="hidden" name="nombreEmp" value="${empTO.nombreEmp}">
		<input type="hidden" name="perfilEmp" value="${empTO.perfilEmp}">
		<div>
			<div id="areaImpresion">
				<table class="principal">
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/estrucTituloFechaConsultaCon.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/estrucEmpleado.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/estrucColumnasSolDependiente.jsp"/>
					<%@include file="../estructura/estrucSolConsulmedConsulta.jsp"%>
					<jsp:include page="../estructura/estrucCostoTotalCon.jsp"/>	
					<jsp:include page="../estructura/salto.jsp"/>			
					<!-- <jsp:include page="../estructura/estrucConsentimiento.jsp"/> -->
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/estrucFirma.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/estrucAutorizacionCon.jsp"/>				
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<!--<jsp:include page="../estructura/estrucConsentimiento2.jsp"/>-->
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/estrucSello.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
					<jsp:include page="../estructura/salto.jsp"/>
				</table>
			</div>
			<table class="principal">
				<tr>
					<td class="columna0"></td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0">&nbsp;</td>
					<td class="columna0"><img src="imagenes/flecha.jpg" class="iconoHome" onclick="enviar('frmConsultaAltaIdConsulmed','consultaAltaIdConsulmed.htm');">Regresar</td>
				</tr>
			</table>
		</div>
		</form>
	</div>
	 		
</BODY>
</HTML>
