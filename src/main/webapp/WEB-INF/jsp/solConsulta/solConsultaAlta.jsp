<!DOCTYPE HTML>
<%@include file="../include/tagsLibs.jsp"%>

<HTML><HEAD><%@include file="../include/tagsCss.jsp"%></HEAD>

<BODY>
	
	<%@include file="../estructura/encabezado.jsp"%>
	<%@include file="../estructura/titulo.jsp"%>
	<%@include file="../estructura/menu.jsp"%>
	<%@include file="../estructura/piepagina.jsp"%>
	
	<div class="cuerpo">			
		<div id="areaImpresion">
			<table class="principal">
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucTituloFechaConsulta.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!-- 	DATOS DEL EMPLEADO -->
				<jsp:include page="../estructura/estrucEmpleado.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
	
				<!--	DEPENDIENTES	-->
				<jsp:include page="../estructura/estrucColumnasSolDependiente.jsp"/>
				<c:forEach items="${listaDep}" var="dependienteTO">
				<%@include file="../estructura/estrucSolDepenConsulta.jsp"%>
				</c:forEach>
												
				<!--	COSTOS	-->
				<jsp:include page="../estructura/estrucCostos.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
	
				<!--	DEPENDIENTES CONSULMED	-->
				<jsp:include page="../estructura/estrucColumnasSolDependiente.jsp"/>
				<c:forEach items="${listaConsulmed}" var="consulmedTO">
				<%@include file="../estructura/estrucSolConsulmedConsulta.jsp"%>
				</c:forEach>
				<jsp:include page="../estructura/salto.jsp"/>
				
				<!--	DEPENDIENTES MEMORIAL	-->
				<jsp:include page="../estructura/estrucColumnasSolMemorial.jsp"/>
				<c:forEach items="${listaMemorial}" var="consulmedTO">
				<%@include file="../estructura/estrucSolMemorialConsulta.jsp"%>
				</c:forEach>
				<jsp:include page="../estructura/salto.jsp"/>
	
				<!-- PIE DE PAGINA -->
				<jsp:include page="../estructura/estrucConsentimiento.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucFirma.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucAutorizacion.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucConsentimiento2.jsp"/>
				<jsp:include page="../estructura/salto.jsp"/>
				<jsp:include page="../estructura/estrucSello.jsp"/>
			
			</table>
		</div>
		<div><img src="imagenes/impresora3.jpg" class="iconoImprimir" id="imprimirAlt"></div>
	</div>
	 		
</BODY>
</HTML>
